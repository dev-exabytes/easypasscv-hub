import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ConnectedRouter } from "react-router-redux";
import { connect } from "react-redux";
import RedirectRoute from './containers/App/RedirectRoute';
import App from "./containers/App/App";
import asyncComponent from "./helpers/AsyncFunc";
import MainPage from "./containers/Page/index";

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (

  <Route
    {...rest}
    render={props => {
      return isLoggedIn ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/auth",
              state: props.location
            }}
          />
        )
    }
    }
  />
);
const PublicRoutes = ({ history, isLoggedIn }) => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path={"/"} render={props => <RedirectRoute isLoggedIn={isLoggedIn} {...props} />} />
        <Route exact path={"/404"} component={asyncComponent(() => import("./containers/Page/404"))} />
        <Route exact path={"/500"} component={asyncComponent(() => import("./containers/Page/500"))} />
        <Route exact path={"/unsub"} component={asyncComponent(() => import("./containers/Page/unsub"))} />
        <Route
          exact
          path={"/integration/slack"}
          component={asyncComponent(() => import("./containers/Page/slackIntegration"))}
        />
        <Route
          exact
          path={"/reports/daily"}
          component={asyncComponent(() => import("./containers/Home/index"))}
        />
        <Route path={"/auth"} component={MainPage} />
        <RestrictedRoute path="/dashboard" component={App} isLoggedIn={isLoggedIn} />
      </Switch>
    </ConnectedRouter>
  );
};

export default connect(state => ({
  isLoggedIn: state.Auth.get("idToken") !== null
}))(PublicRoutes);

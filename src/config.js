export default {
  apiUrl: process.env.REACT_APP_API_URL
};

const ewHeader = require("./image/easywork-white.png");
const timezone = require("moment-timezone");
const { detect } = require("detect-browser");

const siteConfig = {
  siteName: "EasyWork",
  siteIcon: ewHeader,
  siteHeader: ewHeader,
  footerText: "EasyPass ©2020 Created by EasyWork Asia Sdn. Bhd."
};

const themeConfig = {
  topbar: "themedefault",
  sidebar: "themedefault",
  layout: "themedefault",
  theme: "themedefault"
};
const language = "english";

const baseUrl = process.env.REACT_APP_API_URL;

const slackConfig = {
  clientId: process.env.REACT_APP_SLACK_CLIENT_ID,
  clientSecret: process.env.REACT_APP_SLACK_CLIENT_SECRET
};

const slackUrl = process.env.REACT_APP_SLACK_URL;

const slackRedirectUri = process.env.REACT_APP_SLACK_REDIRECT_URI;

const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;

const googleClientId = process.env.REACT_APP_GOOGLE_CLIENT_ID;

const appEnv = process.env.REACT_APP_ENV;

const browser = detect();
const currentCompany = JSON.parse(localStorage.getItem("current_company"));
const apiHeader = {
  "Time-Zone": timezone.tz.guess(),
  "company-id": currentCompany ? currentCompany.id : null,
  "device-info": JSON.stringify({
    name: browser.name,
    version: browser.version,
  }),
};

const v2 = {
  "Accept": "application/x.app.v2+json"
};

const mandrillKey = process.env.REACT_APP_MANDRILL_KEY;

const publicUrl = process.env.REACT_APP_PUBLIC_URL;

const awsS3 = process.env.REACT_APP_AWS_S3;

export {
  baseUrl,
  siteConfig,
  themeConfig,
  language,
  slackConfig,
  slackUrl,
  slackRedirectUri,
  facebookAppId,
  appEnv,
  apiHeader,
  v2,
  googleClientId,
  mandrillKey,
  publicUrl,
  awsS3
};

import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
border-radius:10px;
  .ant-tabs-content {
    margin-top: 40px;
  }

  .ant-tabs-nav {
    > div {
      color: ${palette('secondary', 2)};

      &.ant-tabs-ink-bar {
        background-color: ${palette('primary', 0)};
      }

      &.ant-tabs-tab-active {
        color: ${palette('primary', 0)};
      }
    }
  }

  .buttonContainer{
    height:40px;
    border-radius:20px;
    background-color: ${palette('ew', 6)};
    color:${palette("ew", 1)};
    border:none;
  }

  .filterContainer{
    display:flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: row;

    @media only screen and (max-width: 767px) {
      flex-direction: column;
    }
  }
  tr.ant-table-expanded-row{
    background-color:#fff;
  }
  .nestedTableWrapper{
    margin-top:10px;
    border: 1px solid #e8e8e8;
    border-radius: 4px;
  }
  .linkContainer{
    color:#000;
  }
  .hightlightRow{
    background-color:${palette('ew', 3)};
    color: #000;
  }
  .buttonPrimary, .buttonPrimary:active, .buttonPrimary:hover{
    background-color:${palette('ew', 0)};
    border-color:${palette('ew', 0)};
    border-radius:5px;
  }
  .columnContainer{
    display:flex;
    flex-direction:row;
    align-items:center;
  }
  .rightButton{
    display:flex;
    justify-content:flex-end;
    margin-right:50px;
    @media only screen and (max-width: 767px) {
      margin-right:0px;
    }
  }
  .orangeText{
    color: ${palette('ew', 4)};
  }
  
 .header-container{
  display: flex;
  justify-content: space-between;
}
`;

export default Style;

import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import Style from "./detail.style";
import actions from "../../redux/easyreward/actions";
import { Carousel, Button, Icon, Card, Avatar, Divider, Modal } from 'antd';
import { Link } from "react-router-dom";
import Linkify from 'react-linkify';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import noLogo from '../../image/placeholder_company_logo.png';

const { Meta } = Card;
const { getReward, checkRewardAccess } = actions;

const displayText = {
  noPhoto: "NO IMAGE",
  gettingReward: "Getting reward...",
  rewardNotExist: "Reward doesn't exist.",
  confirmDelete: "Deleted reward cannot be recovered.",
}

class AnnouncementDetail extends Component {

  componentDidMount() {
    const { getReward, checkRewardAccess, currentCompany, idToken } = this.props;
    const { match: { params } } = this.props;

    this.setState(() => checkRewardAccess(
      currentCompany.id,
      idToken
    ));

    this.setState(getReward(
      currentCompany.id,
      params.rewardId,
      idToken
    ));

  }

  constructor(props) {
    super(props);
    this.state = {
      isDeleting: false,
      isModalVisible: false,
      zoom: {
        isOpen: false,
        photoIndex: 0,
      }
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.carousel = React.createRef();
    this.handleDelete = this.handleDelete.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.showZoom = this.showZoom.bind(this);
  }

  handleDelete() {
    const { deleteAnnouncement, currentCompany, idToken } = this.props;
    const { match: { params } } = this.props;

    this.hideModal();

    this.setState({ isDeleting: true })

    alert("button is for decoration")

  }

  next() {
    this.carousel.next();
  }
  previous() {
    this.carousel.prev();
  }

  showModal() {
    this.setState({
      isModalVisible: true
    })
  }

  hideModal() {
    this.setState({
      isModalVisible: false
    })
  }

  showZoom(indexNo) {
    this.setState({
      zoom: {
        isOpen: true,
        photoIndex: indexNo,
      }
    })
  }

  render() {

    if (this.props.reward && this.props.currentCompany) {
      var reward = this.props.reward.company_id !== this.props.currentCompany.id ? null : this.props.reward;
    }
    const access = this.props.rewardAccess;
    const carouselProps = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    let shouldDisable = this.state.isDeleting && !this.props.deleteFailed

    return (
      <LayoutContentWrapper>
        <Style className="isoLayoutContent">
          {access && reward ?
            <div className="authorizedBtn">
              <div className="delete-container">
                <Button loading={shouldDisable} onClick={this.showModal}>Delete</Button>
              </div>
              <div className="edit-container">
                <Link to={{ pathname: `/dashboard/announcement/detail/${reward.id}/edit`, state: { fromLink: true, reward: reward } }}><Button disabled={shouldDisable} >Edit</Button></Link>
              </div>
            </div> : <div></div>}
          {reward ?
            <div>
              {reward.photo_urls ?
                <div className="carousel-container">
                  <div className="carousel">
                    <Carousel ref={node => (this.carousel = node)} {...carouselProps} arrows>
                      {reward.photo_urls.map((photo, index) => <div className="carousel-img" key={index} onClick={() => this.showZoom(index)}><div className="imageOverlay"><img id="carouselImg" alt="Reward Photos" height="300px" src={photo.url} /></div></div>)}
                    </Carousel>
                  </div>
                  {reward.photo_urls.length > 1 ?
                    <div className="arrow-key">
                      <div className="left-arrow">
                        <Icon type="left-circle" onClick={this.previous} />
                      </div>
                      <div className="right-arrow">
                        <Icon type="right-circle" onClick={this.next} />
                      </div>
                    </div> :
                    <div></div>
                  }
                </div>
                :
                <div></div>
              }
              <Card bordered={false}>
                <Meta
                  avatar={reward.company_logo ? <Avatar size={64} src={reward.company_logo} /> : <Avatar size={64} style={{backgroundColor: '#ccc'}} src={noLogo} />}
                  title={reward.company_name}
                />
              </Card>
              <Divider />
              <div className="sectionContainer">
                <div className="sectionHeader">{"Company"}</div>
                <div className="sectionBody">{reward.company_name}</div>
              </div>
              <div className="sectionContainer">
                <div className="sectionHeader">{"Product Name"}</div>
                <div className="sectionBody">{reward.name}</div>
              </div>
              <div className="sectionContainer">
                <div className="sectionHeader">{"Description"}</div>
                <div className="sectionBody">{reward.description ? reward.description : "-"}</div>
              </div>
              <div className="sectionContainer">
                <div className="sectionHeader">{"Available Quantity"}</div>
                <div className="sectionBody">{reward.quantity === -1 ? "99+" : reward.quantity}</div>
              </div>
              <div className="sectionContainer">
                <div className="sectionHeader">{"Category"}</div>
                <div className="sectionBody">{reward.category_name}</div>
              </div>
              {reward.per_user_claim_limit ?
                <div className="sectionContainer">
                  <div className="sectionHeader">{"Max Redemption (Per Calendar Year)"}</div>
                  <div className="sectionBody">{reward.per_user_claim_limit}</div>
                </div>
                : <div></div>
              }
              <div className="sectionContainer">
                <div className="sectionHeader">{"Terms & Condition"}</div>
                <div className="sectionBody">{reward.terms_condition ? reward.terms_condition : "-"}</div>
              </div>
              <AttachmentZoom
                hide={() => this.setState({ zoom: { isOpen: false } })}
                next={(index) => this.setState({ zoom: { isOpen: true, photoIndex: index } })}
                prev={(index) => this.setState({ zoom: { isOpen: true, photoIndex: index } })}
                images={reward.photo_urls}
                isOpen={this.state.zoom.isOpen}
                photoIndex={this.state.zoom.photoIndex} />
            </div>
            :
            this.props.gettingReward === true ?
              <div>{displayText.gettingReward}</div> :
              <span>{displayText.rewardNotExist}</span>
          }

          <Modal
            title="Are you sure?"
            visible={this.state.isModalVisible}
            onCancel={this.hideModal}
            onOk={this.handleDelete}
            footer={[
              <Button key="submit" loading={shouldDisable} onClick={this.handleDelete}>Yes</Button>,
              <Button key="back" type="primary" loading={shouldDisable} onClick={this.hideModal}>Cancel</Button>,
            ]}
          >
            <p>{displayText.confirmDelete}</p>
          </Modal>
        </Style>
      </LayoutContentWrapper >
    )
  }
}

class AttachmentZoom extends Component {

  render() {
    const { photoIndex, isOpen, images } = this.props;
    return (
      <div>

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex].url}
            nextSrc={images[(photoIndex + 1) % images.length].url}
            prevSrc={images[(photoIndex + images.length - 1) % images.length].url}
            onCloseRequest={() => this.props.hide()}
            onMovePrevRequest={() => this.props.prev((photoIndex + images.length - 1) % images.length)}
            onMoveNextRequest={() => this.props.next((photoIndex + 1) % images.length)}
          />
        )}
      </div>
    );
  }
}


export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      reward: state.EasyReward.get("reward"),
      gettingReward: state.EasyReward.get("gettingReward"),
      deleteSuccess: state.Announcement.get("delete_success"),
      deleteFailed: state.Announcement.get("delete_failed"),
      rewardAccess: state.EasyReward.get("rewardAccess"),
    })
  },
  { getReward, checkRewardAccess }
)(AnnouncementDetail);
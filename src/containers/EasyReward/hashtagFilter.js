import React, { Component } from 'react';
import { Form, Select, InputNumber, Tooltip, Icon } from 'antd';
import moment from 'moment';
import Style from "./index.style";

const { Option } = Select;
export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { form, tags } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
      labelAlign: 'left',
      colon: false
    };
    return (
      <Style >
        <Form {...formItemLayout}>
          <Form.Item
            label={
              <span>
                Hashtag&nbsp;
                <Tooltip title="All hashtags that being used by employee in this company, sort by most used on top">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            }
          >
            {getFieldDecorator('tag', {
              initialValue: tags.length > 0 ? tags[0] : undefined,
              rules: [
                {
                  required: true,
                  message: 'Please select a hashtag',
                }
              ],
            })(
              <Select
                showSearch
                placeholder="Select a hashtag"
                style={{ width: '200px' }}
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {tags.map((v, i) => (
                  <Option key={i} value={v}>{typeof v === 'string' || v instanceof String ? v.toUpperCase() : v}</Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item
            label={
              <span>
                Min Point Required&nbsp;
                <Tooltip title="Nomination will only considered if it achieve this. e.g. > 10 points">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            }
          >
            {getFieldDecorator('min_point', {
              initialValue: 10,
              rules: [
                {
                  required: true,
                  message: 'Please input your mininum point required',
                }
              ],
            },
            )(
              <InputNumber
                min={0}
              />
            )}
          </Form.Item>
          <Form.Item
            label={
              <span>
                Cross Dept Multiplier&nbsp;
                <Tooltip title="If department is different, occurance will be multiplied by this">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            }
          >
            {getFieldDecorator('dept_multiplier', {
              initialValue: 1,
              rules: [
                {
                  required: true,
                  message: 'Please input your cross dept multiplier',
                }
              ],
            },
            )(
              <InputNumber
                min={1}
              />
            )}
          </Form.Item>
        </Form>
      </Style >
    );
  }
}

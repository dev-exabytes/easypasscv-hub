import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import Style from "./index.style";
import { connect } from "react-redux";
import { Button, Form, Icon, Table } from "antd";
import DaysFilter from "../../components/daysFilter/index";
import moment from 'moment';
import timezone from 'moment-timezone';
import Filters from "./hashtagFilter";
import Actions from "../../redux/easyreward/actions";

const { getHashtag, getTrans, getTagReport } = Actions;

const WrappedDaysFilterForm = Form.create()(DaysFilter);
const FilterForm = Form.create()(Filters);

const truncate = (input) => input.length > 15 ? `${input.substring(0, 15)}...` : input;

const columns = [
  {
    title: 'Giver',
    dataIndex: 'giver_name',
    key: 'giver_name'
  },
  {
    title: 'Giver Department',
    dataIndex: 'giver_department',
    key: 'giver_department'
  },
  {
    title: 'Receiver',
    dataIndex: 'receiver_name',
    key: 'receiver_name'
  },
  {
    title: 'Receiver Department',
    dataIndex: 'receiver_department',
    key: 'receiver_department'
  },
  {
    title: 'Points',
    dataIndex: 'point',
    key: 'point'
  },
  {
    title: 'Remarks',
    dataIndex: 'remarks',
    key: 'remarks'
  }
];

const reportColumns = [
  {
    title: 'Receiver',
    dataIndex: 'receiver_name',
    key: 'receiver_name'
  },
  {
    title: 'Department',
    dataIndex: 'receiver_dept',
    key: 'receiver_dept'
  },
  {
    title: 'Total Count',
    dataIndex: 'total',
    key: 'total'
  }
];

const expandableColumn = [
  {
    title: 'Giver',
    dataIndex: 'giver_name',
    key: 'giver_name',
    render(text, record) {
      return {
        props: {
          style: { background: '#fff' },
        },
        children: <span>{text}</span>,
      };
    },
  },
  {
    title: 'Giver Department',
    dataIndex: 'giver_department',
    key: 'giver_department',
    render(text, record) {
      return {
        props: {
          style: { background: '#fff' },
        },
        children: <span>{text}</span>,
      };
    },
  },
  {
    title: 'Points',
    dataIndex: 'point',
    key: 'point',
    render(text, record) {
      return {
        props: {
          style: { background: '#fff' },
        },
        children: <span>{text}</span>,
      };
    },
  },
  {
    title: 'Remarks',
    dataIndex: 'remarks',
    key: 'remarks',
    render(text, record) {
      return {
        props: {
          style: { background: '#fff' },
        },
        children: <span>{text}</span>,
      };
    },
  }
]
class Hashtag extends Component {
  state = {
    loading: false,
    timezone: timezone.tz.guess(),
    dates: [moment().tz(timezone.tz.guess()), moment().tz(timezone.tz.guess())],
    days: null,
  };


  componentDidMount() {
    const { currentCompany, getHashtag } = this.props;
    if (currentCompany) {
      getHashtag(currentCompany.id);
    }
  }

  filterFormRef = formRef => {
    this.formRef = formRef;
  };

  otherFormRef = formRef => {
    this.otherRef = formRef;
  };

  handleSubmit = (page = undefined, reportPage = undefined) => {
    const { getTrans, currentCompany, getTagReport } = this.props;

    // Get date form values
    const dates = this.formRef.props.form.getFieldValue("dates");
    const start = dates[0].format("YYYY-MM-DD");
    const end = dates[1].format("YYYY-MM-DD");

    // Get filter form values
    this.otherRef.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (page === undefined && reportPage === undefined) {
        getTrans(currentCompany.id, values.tag, start, end, values.min_point, 1);
        getTagReport(currentCompany.id, values.tag, start, end, values.min_point, values.dept_multiplier, 1);
      }
      else if (page !== undefined) {
        getTrans(currentCompany.id, values.tag, start, end, values.min_point, page);
      }
      else if (reportPage !== undefined) {
        getTagReport(currentCompany.id, values.tag, start, end, values.min_point, values.dept_multiplier, reportPage);
      }
    });
  };

  onTransPageChange = page => {
    this.handleSubmit(page);
  }

  onReportPageChange = page => {
    this.handleSubmit(undefined, page);
  }
  render() {
    const { tags, loadTags, loadTrans, trans, loadReport, report, transPaginate, tagReportPaginate, hasPrivilege } = this.props;
    return (
      < LayoutContentWrapper >
        <Style className="isoLayoutContent">
          <div style={{ paddingBottom: "20px" }}>
            <h3 style={{ paddingBottom: "10px" }}>EasyReward Hashtag Reporting</h3>
            {!hasPrivilege ? (
              <span className="orangeText">You need Reward feature turn on and "Manage Company Reward" permission to access to this report</span>
            ) : null}
          </div>
          <div>
            <WrappedDaysFilterForm
              wrappedComponentRef={this.filterFormRef}
              dates={[moment().tz(timezone.tz.guess()), moment().tz(timezone.tz.guess())]}
              timezone={timezone.tz.guess()}
              days={"1"}
              orFlag={true}
            />
          </div>
          <div>
            <FilterForm
              wrappedComponentRef={this.otherFormRef}
              loadingTags={loadTags}
              tags={tags}
            />
          </div>
          <div className="rightButton">
            <Button
              className="buttonPrimary"
              type="primary"
              onClick={() => this.handleSubmit()}
              loading={loadTrans || loadReport}
              disabled={!hasPrivilege}
            >
              Filter
              </Button>
          </div>
        </Style>
        {tags.length > 0 ? (
          <Style className="isoLayoutContent" style={{ marginTop: "30px" }}>
            <div>
              <div className="columnContainer">
                <span style={{ fontSize: "16px", fontWeight: "700", color: "#000" }}>Latest Standing</span>
              </div>
              <Table
                loading={loadReport}
                columns={reportColumns}
                dataSource={report}
                className="tableContainer"
                rowKey="user_id"
                size="middle"
                pagination={{
                  pageSize: tagReportPaginate.per_page,
                  current: tagReportPaginate.current_page,
                  total: tagReportPaginate.total,
                  size: "small",
                  onChange: this.onReportPageChange,
                  showTotal: total => `Total ${tagReportPaginate.total} employees`
                }}
                expandedRowRender={record => <div className="nestedTableWrapper">
                  <Table
                    columns={expandableColumn}
                    dataSource={record.transactions}
                    className="tableContainer"
                    rowKey="transaction_id"
                    size="middle"
                    pagination={false}
                  /></div>}
              />
            </div>
          </Style>
        ) : null}
        {tags.length > 0 ? (
          <Style className="isoLayoutContent" style={{ marginTop: "30px" }}>
            <div>
              <div className="columnContainer">
                <span style={{ fontSize: "16px", fontWeight: "700", color: "#000" }}>Latest Transaction</span>
              </div>
              <Table
                loading={loadTrans}
                columns={columns}
                dataSource={trans}
                className="tableContainer"
                rowKey="r_id"
                size="middle"
                pagination={{
                  pageSize: transPaginate.per_page,
                  current: transPaginate.current_page,
                  total: transPaginate.total,
                  size: "small",
                  onChange: this.onTransPageChange,
                  showTotal: total => `Total ${transPaginate.total} transactions`
                }}
              />
            </div>
          </Style>
        ) : null}
      </LayoutContentWrapper >
    );
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      tags: state.EasyReward.get("tags"),
      loadTags: state.EasyReward.get("gettingHastag"),
      loadTrans: state.EasyReward.get("gettingTrans"),
      trans: state.EasyReward.get("trans"),
      loadReport: state.EasyReward.get("gettingTagReport"),
      report: state.EasyReward.get("tagReport"),
      transPaginate: state.EasyReward.get("transPaginate"),
      tagReportPaginate: state.EasyReward.get("tagReportPaginate"),
      hasPrivilege: state.EasyReward.get("hasPrivilege"),
    })
  },
  { getHashtag, getTrans, getTagReport }
)(Hashtag);

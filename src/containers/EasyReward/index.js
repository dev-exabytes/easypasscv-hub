import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import Style from "./index.style";
import actions from "../../redux/easyreward/actions";
import { List, Avatar, Spin, Button, Pagination } from 'antd';
import { Link } from "react-router-dom";

const { getRewardsList, getRewardsListPagination } = actions;

const displayText = {
  pageHeader: "EasyRewards",
  noPhoto: "NO IMAGE",
  pointsNeeded: " points",
  qtyLeft: " left",
  maxRedemption: " max redemption",
  btnAddReward: "Add Reward",
  loadingTip: "Getting rewards...",
  noRewards: "No rewards.",

}

class EasyReward extends Component {

  componentDidMount() {
    const { getRewardsList, getRewardsListPagination, currentCompany, idToken } = this.props;
    var pageNum = 1;
    this.setState(() => getRewardsList(
      currentCompany.id,
      pageNum,
      idToken
    )
    );
    this.setState(() => getRewardsListPagination(
      currentCompany.id,
      pageNum,
      idToken
    )
    );
  }

  onPageChange = (page) => {
    const { getRewardsList, currentCompany, idToken } = this.props;
    this.setState(() => getRewardsList(
      currentCompany.id,
      page,
      idToken
    )
    );
  }

  render() {
    const rewardList = (this.props.rewardsList ? this.props.rewardsList.data : []);
    const rewardListPagingInfo = this.props.paging;
    const hasPrivilege = this.props.hasPrivilege;
    return (
      <LayoutContentWrapper>
        <Style className="isoLayoutContent">
          <div className="header-container">
            <div className="header">
              <span style={{ fontSize: "36px", color: "#000" }}>{displayText.pageHeader}</span>
            </div>
            <div className="create-reward">
              {hasPrivilege ? <Link to={{pathname: "", state: {isEdit: false}}}><Button className="buttonContainer">{displayText.btnAddReward}</Button></Link> : <div></div>}
            </div>
          </div>

          {
            rewardListPagingInfo ?
              <Paginator value={rewardListPagingInfo} onChange={this.onPageChange.bind(this)} /> :
              <div></div>
          }

          {Array.isArray(rewardList) && rewardList.length ?
            <RewardsList value={rewardList} />
            : this.props.loading === true ?
              <div><br /><Spin tip={displayText.loadingTip} /></div> :
              <span><br />{displayText.noRewards}</span>
          }
        </Style>
      </LayoutContentWrapper >
    )
  }
}

class Paginator extends Component {

  onPageChange = (e) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e);
    }
  }

  render() {
    const pagingInfo = this.props.value;

    return (
      <div className="paging-container">
        <Pagination
          total={pagingInfo.total}
          pageSize={pagingInfo.per_page}
          onChange={this.onPageChange}
          hideOnSinglePage={true}
        />
      </div>
    )
  }
}

class RewardsList extends Component {
  render() {
    const rewardsList = this.props.value;
    return (
      <div className="rewardsList">
        <List
          itemLayout="vertical"
          dataSource={rewardsList}
          renderItem={(item, index) => (

            <List.Item
              key={item.index}
            >
              <List.Item.Meta
                avatar={item.photo_urls ? <Avatar size={128} shape="square" src={item.photo_urls[0].url} /> : <Avatar shape="square" size={128}>{displayText.noPhoto}</Avatar>}
                title={(<div className="listItem">
                  <Link to={{ pathname: `/dashboard/rewards/${item.id}` }}>{item.name.length > 60 ? item.name.substring(0, 59) + "..." : item.name}</Link>
                </div>)}
                description={<div>
                  {item.point_needed + displayText.pointsNeeded}<br />
                  <span style={item.quantity > 0 && item.quantity < 10 ? { color: 'red' } : {}} >{item.quantity === -1 ? "99+" : item.quantity}{displayText.qtyLeft}</span><br />
                  {item.per_user_claim_limit ? item.per_user_claim_limit + displayText.maxRedemption : ""}
                </div>}
              />
            </List.Item>

          )}
        />
      </div>
    )
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      rewardsList: state.EasyReward.get("rewardsList"),
      paging: state.EasyReward.get("rewardsListPagination"),
      loading: state.EasyReward.get("gettingRewardsList"),
      hasPrivilege: state.EasyReward.get("hasPrivilege"),
    })
  },
  { getRewardsList, getRewardsListPagination }
)(EasyReward);
import { baseUrl } from "../../config";

export const onCheckAdmin = (idToken, companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/access?access_privilege_id=1`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const getRequest = (idToken, page , companyId, formId, filter,query) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/requests?company=${companyId}&type=to&page=${page}&form=${formId}&query=${query}${filter}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v1+json", "Time-Zone":"Asia/Kuala_Lumpur"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getRequestInputValue = (idToken, request) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/requests/${request}/inputs`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const approveRequest = (idToken, request,comment,companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    var body = {
      company_id : companyId,
      status_id : 1,
    }
    if (comment != ""){
      body['comment'] = comment;
    }
    return fetch(
      `${baseUrl}/v1/requests/${request}/approvereject`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
        body: JSON.stringify(body)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const rejectRequest = (idToken, request,comment,companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    var body = {
      company_id : companyId,
      status_id : 2,
    }
    if (comment != ""){
      body['comment'] = comment;
    }
    return fetch(
      `${baseUrl}/v1/requests/${request}/approvereject`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
        body: JSON.stringify(body)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getRequestById = (idToken, request) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/requests/${request}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

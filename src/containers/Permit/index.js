import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { notification } from "antd";
import { connect } from "react-redux";
import TableWrapper from "./easyworkTable.style";
import { Input, Row, Col, Divider, Modal, message, Button, Spin, Form, Cascader, Icon, DatePicker, Radio, Checkbox, Upload, Select } from "antd";
import { getRequest, getRequestById, getRequestInputValue, approveRequest ,rejectRequest } from "./api.js";
import IntlTelInput from "react-intl-tel-input";
import moment from 'moment';
import "react-intl-tel-input/dist/main.css";

const Search = Input.Search;
const dateFormat = 'YYYY-MM-DD';
const { TextArea } = Input;

class Permit extends Component {
  state = {
    loading: false,
    result: [],
    resultSub : [],
    value: "",
    editVisible: false,
    delVisible: false,
    addVisible: false,
    addEmailVisible: false,
    assignVisible: false,
    user_id: "",
    first_name: "",
    last_name: "",
    phone: "",
    email: "",
    originalEmail: "",
    name: "",
    newPhone: "",
    companyId: "",
    confirmLoading: false,
    companyName: "",
    admin: false,
    invite: "mobile",
    total: "",
    totalSub : "",
    page: 1,
    pageSub : 1,
    country: "",
    filter: [],
    filterType: '&status=0',
    sortText: "",
    sortValue: "",
    selectedRowKeys: [],
    selectedRows: [],
    groupId: 0,
    groupType: "",
    title: "",
    originalTitle: "",
    dob: "",
    dateOfHire: "",
    originalDateOfHire: "",
    supervisor: [],
    subordinate: [],
    selectEmployeeVisible: false,
    addSub: false,
    addSup: false,
    originalSubordinate: [],
    originalSupervisor: [],
    formDetails : [],
    approveVisible : false,
    rejectVisible : false,
    approveComment : "",
    rejectComment : "",
    currentPermit : [],
    currentPermitId : '',
    formId: 1,
  };

  countryCode: string = "";

  componentDidMount() {
    const params = new URLSearchParams(this.props.location.search);
    let form = params.get('type');
    let form_id = 0;
    if (form == 'emergency'){
      form_id = 4;
    }
    if (form == 'household'){
      form_id = 3;
    }
    if (form == 'worker'){
      form_id = 2;
    }
    if (form == 'essentialWorker'){
      form_id = 1;
    }
    this.setState({ loading: true, columns: [] })
    const { idToken } = this.props;
    const currentCompany = JSON.parse(localStorage.getItem("current_company"));
    this.getCompanyDetails(idToken, currentCompany);
    this.setState({ formId : form_id});
    this.getRequestList(form_id);
  }

  componentWillReceiveProps(prevProp, prevState) {
    const { currentCompany, idToken, dateOfHire, title } = this.props;
    if (prevProp.currentCompany !== currentCompany) {
      this.setState({
        loading: true,
      })
      this.getCompanyDetails(idToken, currentCompany);
    }
  }

  getCompanyDetails = (idToken, currentCompany) => {
    this.setState({
      companyId: currentCompany.id,
      companyName: currentCompany.name,
    })
}

  getRequestList = (formId) =>{
    const {page, filterType, value} = this.state;
    const {idToken, currentCompany} = this.props;

    this.setState({ loading: true }, () =>
    getRequest(idToken, page, currentCompany.id, formId,filterType,value ).then(res => {
      this.setState({
        result: res.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    }));
  }

  onSearch = (page) => {
    const { idToken } = this.props;
    const { companyId, value, sortValue,formId,filterType } = this.state;

    this.setState({ loading: true }, () =>
      getRequest(idToken, page, companyId,formId, filterType, value).then(res => {
        console.log(res.data);
          this.setState({
            result: res.data,
            loading: false,
            page: page,
            total: res.total
          })
      }).catch(err => {
        console.log(err);
      }))
  };

  // Edit modal
  showEditModal = (record) => {
    const { idToken } = this.props;

    this.setState({
      editVisible: true,
      loading: true,
      value : "",
      currentPermit : record,
      currentPermitId : record.id
    })
    getRequestInputValue(idToken,record.id).then(res =>{
      this.setState({
        formDetails : res.data,
        loading: false
      });
    }).catch(err => {
      console.log(err);
    });
  }

  handleEditCancel = () => {
    this.setState({
      editVisible: false,
      selectEmployeeVisible: false,
      loading: false
    });
  }
  

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys, selectedRows });
  };

  handleApproveRequest = ()=>{
    const { idToken, currentCompany } = this.props;
    const { currentPermitId, approveComment,formId} = this.state;  
    this.setState({ loading :true})

    approveRequest(idToken, currentPermitId,approveComment, currentCompany.id).then(res => {
      this.setState({ 
        loading :false,
        approveVisible : false
      })
      message.success("Submitted");
    }).catch(err => {
      console.log(err);
    });   

    this.getRequestList(formId);
  }

  handleRejectRequest = () =>{
    const { idToken, currentCompany } = this.props;
    const { currentPermitId, rejectComment,formId} = this.state;
    this.setState({ loading :true})

    rejectRequest(idToken, currentPermitId, rejectComment, currentCompany.id).then(res => {
      this.setState({ 
        loading :false,
        rejectVisible : false
      })
      message.success("Submitted");
    }).catch(err => {
      console.log(err);
    });   
  
    this.getRequestList(formId);
  }

  showApproveModal = (record) => {
    this.setState({
      currentPermitId : record.id,
      currentPermit : record,
      approveVisible: true,
      loading: false,
    })
  }

  showRejectModal = (record) => {
    this.setState({
      currentPermitId : record.id,
      currentPermit : record,
      rejectVisible: true,
      loading: false,
    })
  }

  onChangeFilter  = (value, selectedOptions) => {
    let filterType ;
    filterType = value[0] == 4 ? '' : `&status=${value[0]}`;
    this.setState({filterType: filterType, loading : true});
    const {page, formId} = this.state;
    const {idToken, currentCompany} = this.props;

    getRequest(idToken, page, currentCompany.id, formId,filterType, this.state.value ).then(res => {
      this.setState({
        result: res.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    });
  }

  render() {
    const { selectedRowKeys, loading, supervisor, subordinate, selectEmployeeVisible, formDetails } = this.state;
    const { currentCompany } = this.props;
    const { Option } = Select;

    const filter = [{ value:0, label: 'Pending'},{ value:1, label: 'Approved'},{ value:2, label: 'Rejected'},{ value:4, label: 'All'},];

    const columns = [{
      title: 'Name',
      dataIndex: 'submitted_by.name',
      key: 'submitted_by_name',
      sorter: (a, b) => {
        a = a.submitted_by.name || '';
        b = b.submitted_by.name || '';
        return a.localeCompare(b);
      }
    },{
      title: 'Identification Number',
      dataIndex: 'submitted_by.identification_number',
      key: 'identification_number',
      sorter: (a, b) => {
        a = a.submitted_by.identification_number || '';
        b = b.submitted_by.identification_number || '';
        return a.localeCompare(b);
      }
    },{
      title: 'Permit',
      dataIndex: 'form_detail.name',
      key: 'permit',
      sorter: (a, b) => {
        a = a.form_detail.name|| '';
        b = b.form_detail.name || '';
        return a.localeCompare(b);
      }
    },{
      title: 'Status',
      dataIndex: 'status_name',
      key: 'status_name',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => {
        a = a.status_name || '';
        b = b.status_name || '';
        return a.localeCompare(b);
      }    
    }, {
      title: 'Created At',
      dataIndex: 'local_created_at',
      key: 'local_created_at',
    }, {
      title: 'Total Permit Apply',
      dataIndex: 'submitted_by.total_request_submitted',
      key: 'request_submitted',
    },{
      title: 'Last Apply On',
      dataIndex: 'submitted_by.last_apply_on',
      key: 'last_apply_on',
    },{
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width : 150,
      render: (text, record) => (
        <span>
          <a onClick={() => this.showEditModal(record)}>View</a>
          <Divider type="vertical" />
          <a onClick={() => this.showApproveModal(record)}>Approve</a>
          <Divider type="vertical" />
          <a onClick={() => this.showRejectModal(record)}>Reject</a>
        </span >
      ),
    }];

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
      labelAlign: "left",
    };

    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

    if (currentCompany.is_admin === 1) {
      return (
        < LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent">
          <Row type="flex" justify="start">
              <Col md={12} sm={12} xs={24}>
                <Search
                  placeholder="Search with name/email"
                  onSearch={value => this.setState({ value: value }, () => this.onSearch(1))}
                  style={{
                    marginTop: "10px",
                    marginBottom: "20px",
                    fontSize: "20px"
                  }}
                />
              </Col>
              <Col md={12} sm={12} xs={24}>
                <Cascader options={filter} onChange={this.onChangeFilter}>
                  <Button type="primary" icon="filter"
                      style={{
                        marginTop: "10px",
                        marginBottom: "20px",
                        marginLeft: "25px",
                        marginRight: "25px",
                      }}
                    >Filter Permit</Button>
                  </Cascader>
              </Col>
            </Row>
            <TableWrapper
              dataSource={this.state.result}
              columns={columns}
              loading={this.state.loading}
              className="isoSearchableTable"
              scroll={{ x: 1300 }}
              pagination={{
                showQuickJumper: true,
                pageSize: 20,
                total: this.state.total,
                current: this.state.page,
                showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
                onChange: (page, pageSize) => {
                  this.setState({ loading: true, page:page }, () => this.onSearch(page));
                }
              }}
            />

            <Modal
              visible={this.state.editVisible}
              title="View Permit"
              onOk={this.handleEditCancel}
              onCancel={this.handleEditCancel}
              maskClosable={false}
              footer={[
                <Button key="back" onClick={this.handleEditCancel}>Cancel</Button>,
              ]}>
                <div>
                  <Spin indicator={antIcon} spinning={this.state.loading} />
                  <Form onSubmit={this.handleEditCancel} layout="horizontal">
                  {formDetails.map((e, key) => {
                    if (e.is_url){
                      return <Form.Item label={e.name} {...formItemLayout}>
                        <a href={e.value[0].value.path}><u>{e.value[0].value.name}</u></a>
                      </Form.Item>
                    }
                    else{
                      return <Form.Item label={e.name} {...formItemLayout}>
                      <b>{e.value} </b>
                    </Form.Item>
                    }
                    })}
                  </Form>
                </div>
            </Modal>

            <Modal
              visible={this.state.approveVisible}
              title="Approve Permit"
              onOk={this.handleApproveRequest}
              onCancel={() => this.setState({ approveVisible: false, loading: false })}
              maskClosable={false}
              footer={[
                <Button key="back" onClick={() => this.setState({ approveVisible: false, loading: false })}>Cancel</Button>,
                <Button key="submit" type="primary" loading={this.state.loading} onClick={this.handleApproveRequest}>Approve</Button>,
              ]}>
              <p>Are you sure you want to approve this Permit?</p>
              <b>Comment (optional)</b>
              <TextArea rows={4} onChange={(e) => { this.setState({ approveComment: e.target.value }) }} />
            </Modal>

            <Modal
              visible={this.state.rejectVisible}
              title="Reject Permit"
              onOk={this.handleRejectRequest}
              onCancel={() => this.setState({ rejectVisible: false, loading: false })}
              maskClosable={false}
              footer={[
                <Button key="back" onClick={() => this.setState({ rejectVisible: false, loading: false })}>Cancel</Button>,
                <Button key="submit" type="primary" loading={this.state.confirmLoading} onClick={this.handleRejectRequest}>Reject</Button>,
              ]}>
              <b>Comment (optional)</b>
              <p>Are you sure you want to reject this Permit?</p>
              <TextArea rows={4} onChange={(e) => { this.setState({ rejectComment: e.target.value }) }} />
            </Modal>
          </TableDemoStyle>
        </LayoutContentWrapper >
      );
    }
    else {
      return (
        < LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent">
            <p>You do not have access to view Permit page. Please ask permission from your company's admin </p>
          </TableDemoStyle>
        </ LayoutContentWrapper>
      );
    }
  }
}

export default connect(
  state => ({
    idToken: state.Auth.get("idToken"),
    currentCompany: state.Company.get("current_company"),
    companies: state.Auth.get("companies"),
  }),
)(Permit);

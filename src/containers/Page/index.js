import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import StyleWrapper from "./common.style";
import MainRouter from "./mainRouter";
const easyworkWhiteLogo = require("../../image/easywork-white.png");
export class MainPage extends Component {
  state = {
    redirectToReferrer: false,
  };
  render() {
    const { url } = this.props.match;
    const history = this.props.history.location.state;
    return (
      <StyleWrapper>
        <div className="headerTitleContentWrapper">
          <div>
            <img src={easyworkWhiteLogo} width="45" height="45" alt="EasyWork" />
          </div>
          <div className="headerTitle">
            <h1>Work Made Easy</h1>
            <h3>Productivity Office &amp; HR System</h3>
            <p>Make office work paperless within mobile.</p>
          </div>
        </div>
        <div className="contentWrapper">
          <MainRouter url={url} history={history} />
        </div>
      </StyleWrapper >
    );
  }
}

export default connect(
  state => ({
    //
  }),
  {}
)(MainPage);

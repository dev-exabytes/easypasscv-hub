import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import { Input, Button, notification } from 'antd';
import ResetPasswordStyleWrapper from './resetPassword.style';
import authAction from "../../redux/auth/actions";

const { resetPasswordByPhone, resetPasswordByEmail } = authAction;
const easyworklogo = require("../../image/easywork.png");
const easyworkWhiteLogo = require("../../image/easywork-white.png");
class ResetPassword extends React.Component {
  state = {
    password: "",
    confirmPassword: "",
  };
  passwordChange = (e) => {
    this.setState({
      password: e.target.value
    });
  }

  confirmPasswordChange = (e) => {
    this.setState({
      confirmPassword: e.target.value
    });
  }
  resetPassword = () => {
    const { password, confirmPassword } = this.state;
    const { resetPasswordByPhone, resetPasswordByEmail, phone, countryCode, email, otp } = this.props;
    if (password && confirmPassword) {
      if (password === confirmPassword) {
        if (password.length >= 6) {
          if (phone && countryCode) {
            resetPasswordByPhone(phone, countryCode, otp, password, confirmPassword);
          } else if (email) {
            resetPasswordByEmail(email, otp, password, confirmPassword);
          } else {
            notification.open({
              message: "Error",
              description: "Unable to reset password. Please start over."
            });
          }
        } else {
          notification.open({
            message: "Error",
            description: "Passward need to have minumum 6 characters."
          });
        }
      } else {
        notification.open({
          message: "Error",
          description: "Passward does not match."
        });
      }
    } else {
      notification.open({
        message: "Error",
        description: "All fields required."
      });
    }
  }
  render() {
    const { loading } = this.props;
    return (
      <ResetPasswordStyleWrapper>
        <div className="isoFormContent">
          <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
            <Link to="/dashboard">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </Link>
          </div>
          <div className="titleWrapper">
            <p style={{ textAlign: "center" }}>Enter your new password</p>
          </div>
          <div className="isoResetPassForm">
            <div className="InputContainer">
              <Input.Password
                placeholder="New password"
                style={{ height: "40px" }}
                allowClear={true}
                onChange={this.passwordChange}
              />
            </div>
            <div className="InputContainer">
              <Input.Password
                placeholder="Confirm New password"
                style={{ height: "40px" }}
                allowClear={true}
                onChange={this.confirmPasswordChange}

              />
            </div>
            <div className="InputContainer" style={{ paddingTop: "30px" }}>
              <Button
                className="ResetPasswordButton"
                loading={loading}
                block
                onClick={() => this.resetPassword()}
              >
                Reset Password
                </Button>
            </div>
          </div>
        </div>
      </ResetPasswordStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get("idToken") !== null ? true : false,
    phone: state.Auth.get("phone"),
    email: state.Auth.get("email"),
    countryCode: state.Auth.get("country_code"),
    otp: state.Auth.get("otp"),
    loading: state.Auth.get("reset_password_loading")
  }),
  { resetPasswordByPhone, resetPasswordByEmail }
)(ResetPassword);

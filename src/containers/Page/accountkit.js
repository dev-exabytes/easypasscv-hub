/* @flow */
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Input from "../../components/uielements/input";
// import Button from "../../components/uielements/button";
import authAction from "../../redux/auth/actions";
import IntlMessages from "../../components/utility/intlMessages";
import SignInStyleWrapper from "./accountkit.style";
import IntlTelInput from "react-intl-tel-input";
import "react-intl-tel-input/dist/main.css";
import { Button } from "antd";
import AccountKit from "../../helpers/accountkit/index";
import { facebookAppId } from "../../config.js";
import { Card, notification, Skeleton } from 'antd';

const { accountKitAuth } = authAction;
const easyworklogo = require("../../image/easywork.png");
const easyworkWhiteLogo = require("../../image/easywork-white.png");

type Props = {
  accountKitAuth: any,
  isLoggedIn: boolean,
  loginLoading: boolean,
  history: any,
  location: any
};
type State = {
  redirectToReferrer: boolean,
  loading: boolean,
  os_token: string
};
class AccountKitAuth extends Component<Props, State> {
  state = {
    redirectToReferrer: false,
    os_token: "hub login",
    loading: false
  };
  componentWillReceiveProps(nextProps) {
    if (this.props.isLoggedIn !== nextProps.isLoggedIn && nextProps.isLoggedIn === true) {
      this.setState({ redirectToReferrer: true }, () => console.log(this.state.redirectToReferrer));
    }
    if (this.props.loginLoading !== nextProps.loginLoading) {
      this.setState({ loading: nextProps.loginLoading }, () => console.log(this.state.loading));
    }
  }

  handleLogin = (resp) => {
    const history = this.props.history.location.state;
    const { accountKitAuth } = this.props;
    const { os_token } = this.state;

    if (resp.status === 'PARTIALLY_AUTHENTICATED') {
      let data = {
        os_token,
        auth_code: resp.code
      };
      accountKitAuth(JSON.stringify(data), history);
    } else {
      notification.open({
        message: "Error",
        description: "Unable to login"
      });
    }
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/dashboard" } }
    const { redirectToReferrer,
      loading,
    } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    return (
      <SignInStyleWrapper className="isoSignInPage">
        <div className="headerTitleContentWrapper">
          <div>
            <img src={easyworkWhiteLogo} width="60" height="60" alt="EasyWork" />
          </div>
          <div className="headerTitle">
            <h1>Work Made Easy</h1>
            <h3>Productivity Office &amp; HR System</h3>
            <p>Make office work paperless within mobile.</p>
          </div>
        </div>
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
              <Link to="/dashboard">
                <img src={easyworklogo} width={"50%"} alt="EasyWork" />
              </Link>
            </div>
            <div className="isoSignInForm">
              <div className="isoLinkButtonWrapper">
                <Card title="" bordered={false} className="isoCard" >
                  <Skeleton loading={loading} active>
                    <h4 style={{ textAlign: "center", paddingBottom: "10px" }}><IntlMessages id="page.signUpOrLogin" /></h4>
                    <div className="isoAuthButton">
                      <AccountKit
                        appId={facebookAppId}
                        version="v1.0"
                        onResponse={resp => this.handleLogin(resp)}
                        csrf={'csrf phone token here!'}
                        loginType='PHONE'
                        display='modal'
                        disabled={false}
                      >
                        {p =>
                          <Button {...p} className="isoPhoneButton" disabled={false} block>
                            <IntlMessages id="page.loginWithPhone" />
                          </Button>}
                      </AccountKit>
                    </div>
                    <div className="isoAuthButton">
                      <AccountKit
                        appId={facebookAppId}
                        version="v1.0"
                        onResponse={resp => this.handleLogin(resp)}
                        csrf={'csrf email token here!'}
                        loginType='EMAIL'
                        display='modal'
                        disabled={false}
                      >
                        {p =>
                          <Button {...p} className="isoEmailButton" disabled={false} block>
                            <IntlMessages id="page.loginWithEmail" />
                          </Button>}
                      </AccountKit>
                    </div>
                  </Skeleton>
                </Card>
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper >
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get("idToken") !== null ? true : false,
    loginLoading: state.Auth.get("loginLoading")
  }),
  { accountKitAuth }
)(AccountKitAuth);

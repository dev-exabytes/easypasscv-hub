import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import ResetPasswordStyleWrapper from './resetPasswordOtp.style';
import { Button } from "antd";
import authAction from "../../redux/auth/actions";
import OTPInput from "../../components/otp/OtpInput";
import Countdown from 'react-countdown-now';

const { forgetPasswordByPhone,
  forgetPasswordByEmail,
  forgetPasswordCountDownReset,
  checkOtpByPhone,
  checkOtpByEmail
} = authAction;
const easyworklogo = require("../../image/easywork.png");
const easyworkWhiteLogo = require("../../image/easywork-white.png");


class ResetPasswordOTP extends Component {
  state = {
    redirectToReferrer: false,
    loading: false,
    phone: "",
    countryCode: "",
    email: "",
    otp: "",
  };

  handleChange = otp => {
    const { phone, email, country_code, checkOtpByPhone, checkOtpByEmail } = this.props;
    const history = this.props.history.location.state;
    this.setState({
      otp: otp
    }, () => {
      if (otp.toString().length === 6) {
        if (phone && country_code) {
          checkOtpByPhone(phone, country_code, otp.toString(), 3, history);
        } else if (email) {
          checkOtpByEmail(email, otp.toString(), 3, history);
        }
      }
    });
  }

  onCountDownComaplete = () => {
    const { forgetPasswordCountDownReset } = this.props;
    forgetPasswordCountDownReset();
  }

  onSendOTP = () => {
    const history = this.props.history.location.state;
    const { forgetPasswordByEmail, forgetPasswordByPhone, phone, email, country_code } = this.props;

    if (email) {
      forgetPasswordByEmail(email, history);
    } else if (phone && country_code) {
      forgetPasswordByPhone(phone, country_code, history);
    }
  }
  render() {
    const from = this.props.location.state || { pathname: "/dashboard" }
    const { redirectToReferrer, otp } = this.state;
    const { forgetPasswordCountDown, phone, email, disableOtp } = this.props;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    // Renderer callback with condition
    const renderer = ({ hours, minutes, seconds, completed }) => {
      return <span>Please wait <span className="countDownText">{minutes}:{seconds}</span></span>;
    };

    return (
      <ResetPasswordStyleWrapper>
        <div className="isoFormContent">
          <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
            <Link to="/dashboard">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </Link>
          </div>
          <div className="titleWrapper">
            <p style={{ textAlign: "center" }}>What is your verification code?</p>
          </div>

          <div className="isoForgotPassForm">
            <div className="InputContainer">
              <OTPInput
                value={otp}
                onChange={this.handleChange}
                OTPLength={6}
                otpType="number"
                disabled={false}
                inputClassName="Otp"
                inputStyles={{
                  width: window.innerWidth > 767 ? "50px" : "40px",
                  height: "40px",
                  borderRadius: "4px",
                  border: "none",
                  marginRight: "10px"
                }}
              />
            </div>
            <div className="titleWrapper">
              <p style={{ textAlign: "center" }}>If you havent's gotten a code</p>
            </div>
            <div className="titleWrapper" style={{ paddingTop: "10px" }}>
              {phone || email ? (
                <p>Verify your {phone ? "mobile number : " + phone : "email : " + email}</p>
              ) : <p>Please verify your contact again.</p>}
              <p>
                <Button
                  type="link"
                  onClick={this.props.history.goBack}
                >
                  Change {phone ? "Number" : email ? "Email" : "Contact"}
                </Button>
              </p>
            </div>
            {
              phone || email ? (
                <div className="titleWrapper" style={{ paddingTop: "10px" }}>
                  <p>Check your {phone ? "sms messages" : "email inbox/junk mail"}</p>
                  <p>
                    {!forgetPasswordCountDown ? (
                      <Button
                        type="link"
                        onClick={() => this.onSendOTP()}
                        disabled={disableOtp}
                      >Resend code to {phone ? "Mobile Number" : "Email"}</Button>
                    ) : (
                        <Button
                          type="link"
                          disabled={true}
                        >
                          <Countdown
                            date={forgetPasswordCountDown}
                            renderer={renderer}
                            onComplete={() => this.onCountDownComaplete()}
                          />
                        </Button>
                      )}
                  </p>
                </div>
              ) : null
            }
          </div>
        </div>
      </ResetPasswordStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get("idToken") !== null ? true : false,
    loading: state.Auth.get("forgot_password_loading"),
    forgetPasswordCountDown: state.Auth.get("forget_password_count_down"),
    phone: state.Auth.get("phone"),
    email: state.Auth.get("email"),
    country_code: state.Auth.get("country_code"),
    disableOtp: state.Auth.get("disable_otp")
  }),
  {
    forgetPasswordCountDownReset,
    forgetPasswordByPhone,
    forgetPasswordByEmail,
    checkOtpByPhone,
    checkOtpByEmail
  }
)(ResetPasswordOTP);

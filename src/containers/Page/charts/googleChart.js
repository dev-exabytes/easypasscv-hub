import React, { Component } from "react";
import Async from "../../../helpers/asyncComponent";
import { Row, Col } from "antd";
import Box from "../../../components/utility/box";
import LayoutWrapper from "../../../components/utility/layoutWrapper";
import ContentHolder from "../../../components/utility/contentHolder";
import basicStyle from "../../../config/basicStyle";
import ChartWrapper from "./chart.style";
import { connect } from "react-redux";
import { getAttendanceData } from "../../../redux/charts/api";
import moment from "moment";

const GoogleChart = props => (
  <ChartWrapper>
    <Async
      load={import(/* webpackChunkName: "googleChart" */ "react-google-charts")}
      componentProps={props}
      componentArguement={"googleChart"}
    />
  </ChartWrapper>
);

const width = "100%";
const height = "400px";

class ReCharts extends Component {
  ticks = [
    { v: 0, f: "12 am" },
    // { v: 2, f: "2 am" },
    { v: 4, f: "4 am" },
    // { v: 6, f: "6 am" },
    { v: 8, f: "8 am" },
    // { v: 10, f: "10 am" },
    { v: 12, f: "12 pm" },
    // { v: 14, f: "2 pm" },
    { v: 16, f: "4 pm" },
    // { v: 18, f: "6 pm" },
    { v: 20, f: "8 pm" },
    // { v: 22, f: "10 pm" },
    { v: 24, f: "12 am" }
  ];

  constructor() {
    super();

    this.state = {
      data: null,
      showHiddenBox: false
    };
  }

  componentDidMount() {
    const { token, start, end } = this.props.match.params;
    this._getAttendanceData(start, end, token);
  }

  _getAttendanceData = (start, end, token) => {
    const data = {
      start,
      end
    };

    getAttendanceData(token, data)
      .then(res => {
        if (res && res.data && res.data.length > 0) {
          const response: Array<any> = res.data;

          const timeFormat = "HH.mm";

          const data = response.map((day, index) => {
            const firstIn =
              (day.first_in && Number.parseFloat(moment(`${day.date} ${day.first_in}`).format(timeFormat))) || 0;
            const lastOut =
              (day.last_out && Number.parseFloat(moment(`${day.date} ${day.last_out}`).format(timeFormat))) ||
              firstIn ||
              0;

            return Object.values({
              day: moment(day.date).format("DD/MM"),
              shadowBottom: firstIn,
              bottom: firstIn,
              top: lastOut,
              shadowTop: lastOut
            });
          });

          this.setState(
            {
              data: {
                title: "Clock In/Out",
                key: "CandlestickChart",
                chartType: "CandlestickChart",
                width,
                height,
                data: [["DATE", "val1", "val2", "val3", "val4"], ...data],
                options: {
                  title: "Clock In/Out",
                  titleTextStyle: {
                    color: "#788195"
                  },
                  titlePosition: "none",
                  legend: "none",
                  hAxis: {
                    textStyle: {
                      color: "#788195"
                    }
                  },
                  vAxis: {
                    textStyle: {
                      color: "#788195"
                    },
                    maxValue: 24,
                    minValue: 0,
                    gridlines: {
                      color: "#CCC",
                      count: 6
                    },
                    minorGridlines: {
                      color: "#CCC",
                      count: 6
                    },
                    viewWindow: {
                      min: 0,
                      max: 24
                    },
                    ticks: this.ticks
                  },
                  animation: {
                    duration: 1000,
                    easing: "in",
                    startup: true
                  },
                  colors: ["#48A6F2"],
                  tooltip: {
                    textStyle: {
                      color: "#788195"
                    }
                  }
                }
              }
            },
            () => {
              this.timeout = setTimeout(() => {
                this.setState({ showHiddenBox: true });
              }, 2000);
            }
          );
        }
      })
      .catch(error => {
        this.props.history.push("/404");
      });
  };

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    const chartEvents = [
      {
        eventName: "select",
        callback(Chart) {}
      }
    ];
    const { rowStyle, colStyle, gutter } = basicStyle;
    const { data } = this.state;

    return (
      <LayoutWrapper className="isoMapPage">
        <Row style={rowStyle} gutter={gutter} justify="start">
          {data ? (
            <Col md={24} xs={24} style={colStyle}>
              <Box>
                <ContentHolder>
                  <p style={{ textAlign: "center", fontWeight: 500, fontSize: 18, color: "#777" }}>User Clock In/Out</p>
                  <GoogleChart {...data} chartEvents={chartEvents} />
                </ContentHolder>
              </Box>
            </Col>
          ) : null}

          {this.state.showHiddenBox ? <div id="url2png-cheese" /> : null}
        </Row>
      </LayoutWrapper>
    );
  }
}
export { GoogleChart };

export default connect(state => ({
  idToken: state.Auth.get("idToken")
}))(ReCharts);

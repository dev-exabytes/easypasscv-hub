import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import ForgotPasswordStyleWrapper from './forgotPassword.style';
import IntlTelInput from "react-intl-tel-input";
import "react-intl-tel-input/dist/main.css";
import { Button, Input, notification } from "antd";
import authAction from "../../redux/auth/actions";
import Countdown from 'react-countdown-now';

const { forgetPasswordByPhone, forgetPasswordByEmail, forgetPasswordCountDownReset } = authAction;
const easyworklogo = require("../../image/easywork.png");
const easyworkWhiteLogo = require("../../image/easywork-white.png");
class ForgotPassword extends Component {
  state = {
    redirectToReferrer: false,
    loading: false,
    useEmail: false,
    phone: "",
    countryCode: "",
    email: "",
  };

  componentDidMount() {
    const queryString = require('query-string');
    const parsed = queryString.parse(this.props.history.location.search);
    let getOtp = false;
    if (Object.keys(parsed).length !== 0) {
      getOtp = parsed.otp;
    }
    const { phone, email, country_code } = this.props;
    if (phone && country_code) {
      this.setState({
        phone: phone,
        countryCode: country_code
      }, () => getOtp ? this.onSendOTP() : null);
    } else if (email) {
      this.setState({
        email: email,
        useEmail: true
      }, () => getOtp ? this.onSendOTP() : null);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isLoggedIn !== nextProps.isLoggedIn && nextProps.isLoggedIn === true) {
      this.setState({ redirectToReferrer: true });
    }
    if (this.props.loading !== nextProps.loading) {
      this.setState({ loading: nextProps.loading });
    }
  }

  switchResetPass = () => {
    const { useEmail } = this.state;
    this.setState({
      useEmail: !useEmail
    });
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value
    });
  }
  onPhoneNumberChange = (valid, text, country) => {
    this.setState({
      phone: text,
      countryCode: country.iso2
    });
  }
  onSendOTP = () => {
    const history = this.props.history.location.state;
    const { useEmail, phone, email, countryCode } = this.state;
    const { forgetPasswordByEmail, forgetPasswordByPhone } = this.props;

    if (useEmail && email) {
      forgetPasswordByEmail(email, history);
    } else if (!useEmail && phone && countryCode) {
      forgetPasswordByPhone(phone, countryCode, history);
    } else {
      notification.open({
        message: "Error",
        description: "All fields are required."
      });
    }
  }

  onCountDownComaplete = () => {
    const { forgetPasswordCountDownReset } = this.props;
    forgetPasswordCountDownReset();
  }
  render() {
    const { loading, useEmail, phone, email, countryCode } = this.state;
    const { forgetPasswordCountDown } = this.props;

    // Renderer callback with condition
    const renderer = ({ hours, minutes, seconds, completed }) => {
      return <span>Please wait {minutes}:{seconds}</span>;

    };

    return (
      <ForgotPasswordStyleWrapper>
        <div className="isoFormContent">
          <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
            <Link to="/dashboard">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </Link>
          </div>

          <div className="isoForgotPassForm">
            <div className="InputContainer">
              {useEmail ? (
                <Input
                  placeholder="Email"
                  style={{ height: "40px" }}
                  allowClear={true}
                  onChange={this.onEmailChange}
                  defaultValue={email}
                />
              ) : (
                  <IntlTelInput
                    defaultCountry={countryCode || "my"}
                    preferredCountries={["my", "us", "sg"]}
                    inputClassName="ant-input"
                    style={{ width: "100%", zIndex: 4 }}
                    placeholder="Mobile Number"
                    onPhoneNumberChange={this.onPhoneNumberChange}
                    defaultValue={phone}
                    formatOnInit={false}
                    separateDialCode={true}
                    format={false}
                  />
                )}
            </div>
            <div className="InputContainer">
              <Button
                className="LoginViaEmailText"
                type="link"
                onClick={() => this.switchResetPass()}
              >
                {useEmail ? "Reset password via Mobile Number" : "Reset password via Email"}
              </Button>
            </div>
            <div className="InputContainer">
              <Button
                loading={loading}
                className="ResetPasswordButton"
                onClick={() => this.onSendOTP()}
                block
              >
                {!forgetPasswordCountDown ? ("Get Reset Password OTP") : (
                  <Countdown
                    date={forgetPasswordCountDown}
                    renderer={renderer}
                    onComplete={() => this.onCountDownComaplete()}
                  />
                )}
              </Button>
            </div>
            {forgetPasswordCountDown ? (
              <div className="InputContainer">
                <div className="titleWrapper" >
                  <p style={{ textAlign: "center" }}>
                    <Link to="/auth/resetpassword/otp">Enter code</Link>
                  </p>
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </ForgotPasswordStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get("idToken") !== null ? true : false,
    loading: state.Auth.get("forgot_password_loading"),
    forgetPasswordCountDown: state.Auth.get("forget_password_count_down"),
    phone: state.Auth.get("phone"),
    email: state.Auth.get("email"),
    country_code: state.Auth.get("country_code")
  }),
  { forgetPasswordByPhone, forgetPasswordByEmail, forgetPasswordCountDownReset }
)(ForgotPassword);

import React from 'react';
import Button from '../../components/uielements/button';
import Image from '../../image/slack.png';
import IntlMessages from '../../components/utility/intlMessages';
import SlackIntegrationStyleWrapper from './slackIntegration.style';
import SlackReward from "../../image/slack-tutorial.png";
import { Link } from "react-router-dom";

class SlackIntegration extends React.Component {
  state = {
  };

  render() {
    return (
      <SlackIntegrationStyleWrapper className="isoPage">
        <div className="isoContent">
          <div className="isoImageWrapper">
            <img alt="#" src={Image} />
          </div>
          <h1>EasyWork and Slack</h1>
          <p>
            <IntlMessages id="slack.description" />
          </p>
          <div className="isoInputWrapper">
            <Button>
              <Link to="/dashboard/integrations/slack">
                <span>Click to Setup</span>
              </Link>
            </Button>
          </div>
          <div className="isoFooterWrapper">
            <span>
              You must be a Slack & EasyWork admin to install
          </span>
            <br></br>
            <span>
              New to EasyWork? Complete Sign up and create company process via EasyWork App first.
          </span>
          </div>
        </div>
        <div className="isoImage2Wrapper">
          <img alt="#" src={SlackReward} />
        </div>
      </SlackIntegrationStyleWrapper>
    );
  }
}

export default SlackIntegration;

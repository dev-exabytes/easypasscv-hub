import styled from 'styled-components';
import { palette } from 'styled-theme';
import { transition, borderRadius } from '../../config/style-util';
import WithDirection from '../../config/withDirection';

const SlackIntegrationStyleWrapper = styled.div`
  // width: 100%;
  // display: flex;
  // flex-direction: row;
  // align-items: center;
  // justify-content: center;
  // position: relative;
  background-color: ${palette('ew', 1)};


  @media only screen and (max-width: 767px) {
    width: 100%;
    flex-direction: column;
    flex-wrap: nowrap;
  }

  .isoContent {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding-bottom: 30px;
    padding-top: 50px;

    @media only screen and (max-width: 767px) {
      order: 2;
      margin-top: 20px;
      align-items: center;
      text-align: center;
      flex-direction: column;

      h1 {
        font-size: 36px;
        font-weight: 700;
        line-height: 1;
        margin: 0 0 25px;
      }

      p {
        font-size: 20px;
        font-weight: 400;
        margin: 0 0 10px;
        color: #000;
      }

      span {
        font-size: 16px;
      }
    }

    @media only screen and (min-width: 768px) {
      h1 {
        font-size: 48px;
        font-weight: 700;
        line-height: 1;
        margin: 0 0 25px;
      }

      p {
        font-size: 36px;
        font-weight: 400;
        margin: 0 0 10px;
        color: #000;
      }

      span {
        font-size: 24px;
      }
    }

  }

  .isoFooterWrapper{
    padding-top: 50px;
    align-items: center;

    span {
      font-size: 16px;
    }
  }

  .isoInputWrapper{
    button {
      padding:10px 20px;
      justify-content: center;
      height:60px;
      background-color: ${palette('ew', 0)};
      ${transition()};
      ${borderRadius('15px')};

      a {
        width: 100%;
        height: 100%;
        color: #ffffff;
        text-decoration: none;
      }

      span{
        color:${palette('ew', 1)};
      }

      &:hover {
        background-color: ${palette('ew', 1)};
        border: 1px solid ${palette('ew', 0)};
        a {
          text-decoration: none;
        }
        span{
          color:${palette('ew', 0)};
        }
      }

      &:focus {
        outline: 0;
        box-shadow: none;

        a {
          text-decoration: none;
        }
      }
    }
  }

  .isoImageWrapper{
    margin-top: 20px;
    margin-bottom: 20px;
    img{
      width:150px;
      height:150px;
    }
  }

  .isoImage2Wrapper{
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${palette('ew', 0)};
    margin-top:80px;
    padding-top:20px;
    padding-bottom:20px;

    img {
      width: 50%;
    }
    @media only screen and (max-width: 767px) {
      margin-top:50px;
      img {
        width: 80%;
      }
    }
  }
`;

export default WithDirection(SlackIntegrationStyleWrapper);

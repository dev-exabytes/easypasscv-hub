import styled from 'styled-components';
import { palette } from 'styled-theme';
import bgImage from '../../image/login-screen-image.png';
import WithDirection from '../../config/withDirection';
import { transition, borderRadius } from "../../config/style-util";

const ResetPasswordStyleWrapper = styled.div`
  .isoFormContentWrapper {
    width: 500px;
    height: 100%;
    overflow-y: auto;
    z-index: 10;
    position: relative;
  }

  .isoFormContent {
    min-height: 100%;
    display: flex;
    flex-direction: column;
    padding: 70px 50px;
    position: relative;
    background-color: ${palette("ew", 3)};

    @media only screen and (max-width: 767px) {
      width: 100%;
      padding: 70px 20px;
    }

    .isoLogoWrapper {
      width: 100%;
      display: flex;
      margin-bottom: 30px;
      justify-content: center;
      flex-shrink: 0;

      a {
        font-size: 24px;
        font-weight: 300;
        line-height: 1;
        text-transform: uppercase;
        color: ${palette("secondary", 2)};
      }
    }

    .isoFormHeadText {
      width: 100%;
      display: flex;
      flex-direction: column;
      margin-bottom: 15px;
      justify-content: center;

      h3 {
        font-size: 14px;
        font-weight: 500;
        line-height: 1.2;
        margin: 0 0 7px;
        color: ${palette('text', 0)};
      }

      p {
        font-size: 13px;
        font-weight: 400;
        line-height: 1.2;
        margin: 0;
        color: ${palette('text', 2)};
      }
    }

    .isoForgotPassForm {
      width: 100%;
      display: flex;
      flex-shrink: 0;
      flex-direction: column;

      .ant-input {
        width:50px;
        height:40px;
        text-align: center;
        border:none;
        font-size: 18px;
        cursor: text;
        line-height: 1.5;
        color: ${palette("text", 1)};
        background-color: #fff;
        background-image: none;
        border: 1px solid ${palette("border", 1)};
        ${borderRadius("4px")};
        ${transition()};

        @media only screen and (max-width: 767px) {
          width: 50px;
        }

        &:focus {
          border-color: ${palette("primary", 0)};
        }

        &::-webkit-input-placeholder {
          color: ${palette("grayscale", 0)};
        }

        &:-moz-placeholder {
          color: ${palette("grayscale", 0)};
        }

        &::-moz-placeholder {
          color: ${palette("grayscale", 0)};
        }
        &:-ms-input-placeholder {
          color: ${palette("grayscale", 0)};
        }
        input[type="number"] {
          -moz-appearance: textfield;
        }
        input[type="number"]:hover,
        input[type="number"]:focus {
          -moz-appearance: number-input;
        }
      }
    }
    .ResetPasswordButton{
      color: ${palette("ew", 1)};
      background-color: ${palette("ew", 0)};
      border: none;
      height:50px;
      font-size: 18px;
    }
    .InputContainer{
      padding-bottom: 50px;
      display:flex;
      align-items:center;
      justify-content:center;
    }
    .LoginViaEmailText{
      color: ${palette("ew", 0)};
    }
    .Otp{
      width:100%;
      height:50px;
      justify-content:center;
      align-items:center;
    }
    .OtpInput{
      width:10%;
      height:40px;
      text-align: center;
      border:none;
      font-size: 18px;
    }
    .titleWrapper{
      padding-bottom:10px;
    }
    .countDownText{
      color: ${palette("ew", 0)};
    }
  }
`;

export default WithDirection(ResetPasswordStyleWrapper);

import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import authAction from "../../redux/auth/actions";
import SignInStyleWrapper from "./signin.style";
import IntlTelInput from "react-intl-tel-input";
import "react-intl-tel-input/dist/main.css";
import { Button, Input, notification, Divider, Modal } from "antd";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import { facebookAppId, googleClientId } from "../../config.js";

const { loginByPhone, loginByEmail, socialMediaLogin, preloginWithPhone, preloginWithEmail } = authAction;
const easyworklogo = require("../../image/easywork.png");
const google = require("../../image/google.png");
const facebook = require("../../image/facebook.png");

type Props = {
  loginByPhone: any,
  loginByEmail: any,
  isLoggedIn: boolean,
  loginLoading: boolean,
  history: any,
  location: any,
  socialMediaLogin: any,
  socialLoginLoading: boolean,
  socialProvider: any
};
type State = {
  redirectToReferrer: boolean,
  loading: boolean,
  useEmail: boolean,
  phone: string,
  countryCode: string,
  email: string,
  password: string
};

class SignIn extends Component<Props, State> {
  state = {
    redirectToReferrer: false,
    loading: false,
    useEmail: true,
    showPassword: false,
    phone: "",
    countryCode: "",
    email: "",
    password: ""
  };

  componentWillReceiveProps(nextProps) {
    const { preloginResult, isLoggedIn, loginLoading } = nextProps;
    if (this.props.isLoggedIn !== isLoggedIn && isLoggedIn === true) {
      this.setState({ redirectToReferrer: true });
    }
    if (this.props.loginLoading !== loginLoading) {
      this.setState({ loading: loginLoading });
    }
    if (this.props.preloginResult !== preloginResult && preloginResult) {
      // NO1 : Account sign up with social provider and password_reset_before is false
      if (preloginResult.signup_via_social_provider && !preloginResult.password_reset_before) {
        notification.open({
          message: preloginResult.social_provider_msg
        });
      }
      // NO2 : Handle is account kit and password_reset_before is false
      // Redirect to reset password (also pass url history)
      else if (preloginResult.account_kit && !preloginResult.password_reset_before) {
        this.props.history.push({
          pathname: "/auth/forgotpassword",
          state: this.props.history.location.state,
          search: "?otp=true"
        });
      }
      // NO3 : Handle account not exist but similar account exist (Email only)
      // Prompt similar account message
      // Redirect to login via phone
      else if (!preloginResult.account_exist && preloginResult.similar_account) {
        this.showConfirm(preloginResult.similar_account_msg);
      }
      // NO4 : Show password input when account_exist true
      else if (preloginResult.account_exist) {
        this.setState({
          showPassword: true
        });
      }
      // NO5 : Redirect to sign up when account_exist false (also pass url history)
      else {
        this.props.history.push({
          pathname: "/auth/signup",
          state: this.props.history.location.state
        });
      }
    }
  }

  showConfirm(title) {
    Modal.confirm({
      title,
      onOk: () => {
        this.setState({
          useEmail: false
        });
      }
    });
  }

  switchLogin = () => {
    const { useEmail } = this.state;
    this.setState({
      useEmail: !useEmail
    });
  }

  onPhoneNumberChange = (valid, text, country) => {
    this.setState({
      phone: text,
      countryCode: country.iso2
    });
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value
    });
  }

  onPassChange = (e) => {
    this.setState({
      password: e.target.value
    });
  }
  onLogin = () => {
    const history = this.props.history.location.state;
    const { loginByPhone, loginByEmail, preloginWithPhone, preloginWithEmail } = this.props;
    const { useEmail, showPassword, phone, email, password, countryCode } = this.state;

    // Pre login check
    if (!showPassword && useEmail && email) {
      preloginWithEmail(email);
    } else if (!showPassword && !useEmail && phone && countryCode) {
      preloginWithPhone(phone, countryCode);
    }
    else if (useEmail && email && password) {
      loginByEmail(email, password, history);
    } else if (!useEmail && phone && countryCode && password) {
      loginByPhone(phone, countryCode, password, history);
    } else {
      notification.open({
        message: "Error",
        description: "All fields are required."
      });
    }
  }

  responseFacebook = (response) => {
    const history = this.props.history.location.state;
    const { socialMediaLogin } = this.props;
    try {
      socialMediaLogin(response.email, "facebook", response.id, history);
    } catch (error) {
      //
    }
  }

  responseGoogle = (response) => {
    const history = this.props.history.location.state;
    const { socialMediaLogin } = this.props;
    try {
      socialMediaLogin(response.profileObj.email, "google", response.tokenId, history);
    } catch (error) {
    }
  }

  render() {
    const { loading, useEmail, showPassword } = this.state;
    const { socialLoginLoading, socialProvider } = this.props;
    return (
      <SignInStyleWrapper>
        <div className="isoLoginContent">
          <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
            <Link to="/dashboard">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </Link>
          </div>
          <div className="isoSignInForm">
            <div className="InputContainer">
              {useEmail ? (
                <Input
                  placeholder="Email"
                  style={{ height: "40px" }}
                  allowClear={true}
                  onChange={this.onEmailChange}
                />
              ) : (
                  <IntlTelInput
                    defaultCountry={"my"}
                    preferredCountries={["my", "us", "sg"]}
                    inputClassName="ant-input"
                    style={{ width: "100%", zIndex: 4 }}
                    placeholder="Mobile Number"
                    onPhoneNumberChange={this.onPhoneNumberChange}
                    separateDialCode={true}
                    format={false}
                  />
                )}
            </div>
            {showPassword ? (
              <div className="InputContainer">
                <Input.Password
                  placeholder="Password"
                  onChange={this.onPassChange}
                />
              </div>
            ) : null}
            <div className="InputContainer">
              <Button
                className="LoginViaEmailText"
                type="link"
                onClick={() => this.switchLogin()}
              >
                {useEmail ? "Login via Mobile Number" : "Login via Email"}
              </Button>
            </div>
            <div className="InputContainer">
              <Button
                loading={loading}
                className="LoginButton"
                onClick={() => this.onLogin()}
                block
              >
                Login
                </Button>
            </div>
            <div className="InputContainer" style={{ display: "flex", justifyContent: "space-between" }}>
              <Button
                className="LoginViaEmailText"
                type="link"
              >
                <Link to={{
                  pathname: "/auth/signup",
                  state: this.props.history.location.state
                }}>
                  <span>Register</span>
                </Link>
              </Button>
              <Button
                className="LoginViaEmailText"
                type="link"
              >
                <Link to="/auth/forgotpassword">
                  <span>Forgot Password?</span>
                </Link>
              </Button>
            </div>
            <div className="InputContainer">
              <Divider>or</Divider>
            </div>
            <div className="SocialLoginContainer">
              <FacebookLogin
                appId={facebookAppId}
                fields="name,email,picture"
                callback={this.responseFacebook}
                disableMobileRedirect={true}
                isDisabled={socialProvider === 'facebook' && socialLoginLoading ? true : false}
                render={renderProps => (
                  <Button
                    loading={renderProps.isDisabled}
                    onClick={renderProps.onClick}
                    disabled={renderProps.isDisabled}
                    className="socialMediaButton"
                    style={{
                      backgroundColor: "#3b5998",
                      color: '#fff'
                    }}
                  >
                    <img src={facebook} width="auto" height="15" alt="Facebook" style={{ paddingRight: "5px" }} />
                    Login with Facebook
                        </Button>
                )}
              />
              <GoogleLogin
                clientId={googleClientId}
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
                cookiePolicy={'single_host_origin'}
                disabled={socialProvider === 'google' && socialLoginLoading ? true : false}
                render={renderProps => (
                  <Button
                    loading={renderProps.disabled}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                    className="socialMediaButton"
                  >
                    <img src={google} width="auto" height="15" alt="Google" style={{ paddingRight: "5px" }} />
                    Login with Google
                        </Button>
                )}
              />
            </div>
          </div>
        </div>
      </SignInStyleWrapper >
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get("idToken") !== null ? true : false,
    loginLoading: state.Auth.get("loginLoading"),
    socialLoginLoading: state.Auth.get("social_login_loading"),
    socialProvider: state.Auth.get("social_provider"),
    acc_exist: state.Auth.get("acc_exist"),
    preloginResult: state.Auth.get("prelogin_result")
  }),
  { loginByPhone, loginByEmail, socialMediaLogin, preloginWithPhone, preloginWithEmail }
)(SignIn);

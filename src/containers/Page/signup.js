import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import authAction from "../../redux/auth/actions";
import SignInStyleWrapper from "./signin.style";
import IntlTelInput from "react-intl-tel-input";
import "react-intl-tel-input/dist/main.css";
import { Button, Input, notification, Divider } from "antd";
import IntlMessages from '../../components/utility/intlMessages';
import Countdown from 'react-countdown-now';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import { facebookAppId, googleClientId } from "../../config.js";

const { signupCountDownReset, sendOtpByEmail, sendOtpByPhone, signupWithPhone, signupWithEmail, socialMediaLogin } = authAction;
const easyworklogo = require("../../image/easywork.png");
const google = require("../../image/google.png");
const facebook = require("../../image/facebook.png");
type Props = {
  history: any,
};
type State = {
  redirectToReferrer: boolean,
  phone: string,
  countryCode: string,
  useEmail: boolean,
  email: string,
  password: string
};
class SignUp extends Component<Props, State> {
  state = {
    redirectToReferrer: false,
    phone: "",
    countryCode: "",
    useEmail: true,
    email: "",
    password: ""
  };

  componentDidMount() {
    const queryString = require('query-string');
    const parsed = queryString.parse(this.props.location.search);
    const { email, phone, country_code, password, signupWithPhone, signupWithEmail, otp } = this.props;
    if (Object.keys(parsed).length !== 0) {
      const { signup } = parsed;
      const history = this.props.history.location.state;
      if (signup && phone && country_code && password && otp) {
        signupWithPhone(phone, country_code, password, otp, history);
      } else if (signup && email && password && otp) {
        signupWithEmail(email, password, otp, history);
      }
    }
    if (phone && country_code) {
      this.setState({
        phone: phone,
        countryCode: country_code
      });
    } else if (email) {
      this.setState({
        email: email,
        useEmail: true
      });
    }
  }

  onSendOTP = () => {
    const history = this.props.history.location.state;
    const { useEmail, phone, email, countryCode, password } = this.state;
    const { sendOtpByEmail, sendOtpByPhone } = this.props;
    if (useEmail && email && password) {
      sendOtpByEmail(email, password, history, false);
    } else if (!useEmail && phone && countryCode && password) {
      sendOtpByPhone(phone, countryCode, password, history, false);
    } else {
      notification.open({
        message: "Error",
        description: "All fields are required."
      });
    }
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value
    });
  }

  onPhoneNumberChange = (valid, text, country) => {
    this.setState({
      phone: text,
      countryCode: country.iso2
    });
  }

  onPassChange = (e) => {
    this.setState({
      password: e.target.value
    });
  }

  switchLogin = () => {
    const { useEmail } = this.state;
    this.setState({
      useEmail: !useEmail
    });
  }

  onCountDownComplete = () => {
    const { signupCountDownReset } = this.props;
    signupCountDownReset();
  }

  responseFacebook = (response) => {
    const history = this.props.history.location.state;
    const { socialMediaLogin } = this.props;
    try {
      socialMediaLogin(response.email, "facebook", response.id, history, true);
    } catch (error) {
      //
    }
  }

  responseGoogle = (response) => {
    const history = this.props.history.location.state;
    const { socialMediaLogin } = this.props;
    try {
      socialMediaLogin(response.profileObj.email, "google", response.tokenId, history, true);
    } catch (error) {
    }
  }
  render() {
    const { useEmail, email, phone, countryCode } = this.state;
    const { loading, signupCountDown, otp, signupLoading, socialLoginLoading, socialProvider } = this.props;

    const renderer = ({ hours, minutes, seconds, completed }) => {
      return <span>Please wait {minutes}:{seconds}</span>;

    };
    return (
      <SignInStyleWrapper>
        <div className="isoLoginContent">
          <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
            <Link to="/dashboard">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </Link>
          </div>
          <div className="isoSignInForm">
            <div className="InputContainer">
              {useEmail ? (
                <Input
                  placeholder="Email"
                  style={{ height: "40px" }}
                  allowClear={true}
                  onChange={this.onEmailChange}
                  value={email}
                />
              ) : (
                  <IntlTelInput
                    defaultCountry={countryCode || "my"}
                    preferredCountries={["my", "us", "sg"]}
                    inputClassName="ant-input"
                    style={{ width: "100%", zIndex: 4 }}
                    placeholder="Mobile Number"
                    onPhoneNumberChange={this.onPhoneNumberChange}
                    separateDialCode={true}
                    defaultValue={phone}
                    formatOnInit={false}
                    format={false}
                  />
                )}
            </div>
            <div className="InputContainer">
              <Input.Password
                placeholder="Password"
                onChange={this.onPassChange}
              />
            </div>
            <div className="InputContainer">
              <Button
                className="LoginViaEmailText"
                type="link"
                onClick={() => this.switchLogin()}
              >
                {useEmail ? "Register via Mobile Number" : "Register via Email"}
              </Button>
            </div>
            <div className="InputContainer">
              <Button
                loading={loading || signupLoading}
                className="LoginButton"
                disabled={loading || signupLoading}
                onClick={() => this.onSendOTP()}
                block
              >
                {!signupCountDown || otp ? ("Register") : (
                  <Countdown
                    date={signupCountDown}
                    renderer={renderer}
                    onComplete={() => this.onCountDownComplete()}
                  />
                )}
              </Button>
            </div>
            {signupCountDown && !otp ? (
              <div className="InputContainer">
                <div className="titleWrapper" >
                  <p style={{ textAlign: "center" }}>
                    <Link to="/auth/signup/otp">Enter code</Link>
                  </p>
                </div>
              </div>
            ) : <div className="InputContainer" style={{ paddingLeft: "15px" }}>
                By clicking Register, you agree to our <a href="https://www.easywork.asia/privacy-policy" target="_blank">Terms and Policies</a>.
            </div>}
            <div className="InputContainer">
              <Divider>or</Divider>
            </div>
            <div className="SocialLoginContainer">
              <FacebookLogin
                appId={facebookAppId}
                fields="name,email,picture"
                callback={this.responseFacebook}
                disableMobileRedirect={true}
                isDisabled={socialProvider === 'facebook' && socialLoginLoading ? true : false}
                render={renderProps => (
                  <Button
                    loading={renderProps.isDisabled}
                    onClick={renderProps.onClick}
                    disabled={renderProps.isDisabled}
                    className="socialMediaButton"
                    style={{
                      backgroundColor: "#3b5998",
                      color: '#fff'
                    }}
                  >
                    <img src={facebook} width="auto" height="15" alt="Facebook" style={{ paddingRight: "5px" }} />
                    Register with Facebook
                        </Button>
                )}
              />
              <GoogleLogin
                clientId={googleClientId}
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
                cookiePolicy={'single_host_origin'}
                disabled={socialProvider === 'google' && socialLoginLoading ? true : false}
                render={renderProps => (
                  <Button
                    loading={renderProps.disabled}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                    className="socialMediaButton"
                  >
                    <img src={google} width="auto" height="15" alt="Google" style={{ paddingRight: "5px" }} />
                    Register with Google
                        </Button>
                )}
              />
            </div>
            <div className="InputContainer">
              <Divider>or</Divider>
            </div>
            <div className="InputContainer" style={{ display: "flex", justifyContent: "center" }}>
              <Button
                className="LoginViaEmailText"
                type="link"
              >
                <Link to="/auth/signin">
                  <IntlMessages id="page.signUpAlreadyAccount" />
                </Link>
              </Button>
            </div>
          </div>
        </div>
      </SignInStyleWrapper >
    );
  }
}

export default connect(
  state => ({
    loading: state.Auth.get("signup_otp_loading"),
    signupCountDown: state.Auth.get("signup_count_down"),
    phone: state.Auth.get("phone"),
    email: state.Auth.get("email"),
    country_code: state.Auth.get("country_code"),
    password: state.Auth.get("password"),
    otp: state.Auth.get("otp"),
    signupLoading: state.Auth.get("signup_loading"),
    socialLoginLoading: state.Auth.get("social_login_loading"),
    socialProvider: state.Auth.get("social_provider"),
  }),
  { signupCountDownReset, sendOtpByEmail, sendOtpByPhone, signupWithPhone, signupWithEmail, socialMediaLogin }
)(SignUp);

import styled from "styled-components";
import { palette } from "styled-theme";
import bgImage from "../../image/login-screen-image.png";
import WithDirection from "../../config/withDirection";

const StyleWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  height: 100vh;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  position: relative;
  background: url(${bgImage}) no-repeat left center;
  background-size: cover;

  &:before {
    content: "";
    width: 100%;
    height: 100%;
    display: flex;
    background-color: rgba(0, 0, 0, 0.3);
    position: absolute;
    z-index: 1;
    top: 0;
    left: ${props => (props["data-rtl"] === "rtl" ? "inherit" : "0")};
    right: ${props => (props["data-rtl"] === "rtl" ? "0" : "inherit")};
  }

  .contentWrapper {
    width: 500px;
    height: 100%;
    z-index: 10;
    position: relative;
    background-color: ${palette("ew", 3)};
  }

  .headerTitleContentWrapper{
    position: absolute;
    width:auto;
    height:200px;
    z-index:2;
    bottom: 0;
    left: ${props => (props["data-rtl"] === "rtl" ? "inherit" : "0")};
    right: ${props => (props["data-rtl"] === "rtl" ? "0" : "inherit")};
    padding: 30px 50px;
    display:flex;
    flex-direction:row;
    align-items:top;
    overflow-y: auto;

    @media only screen and (max-width: 767px) {
      width: 0%;
    }

    .headerTitle{
      padding-left:10px;
      @media only screen and (max-width: 800px) {
        display: none;
      }
    }

    h1, h3, p{
      color: ${palette("ew", 3)};
    }

    h1{
      font-size: 30px;
      line-height:40px;
      font-weight:900;
    }
    h3{
      font-size: 20px;
    }
    p{
      font-size: 16px;
    }

  }
`;

export default WithDirection(StyleWrapper);

import React from 'react';
import { connect } from "react-redux";
import Image from '../../image/sad-dog.png';
import DogHappy from "../../image/dog-happy.png";
import IntlMessages from '../../components/utility/intlMessages';
import Style from './unsub.style';
import { Button, notification, Input } from 'antd';
import authAction from "../../redux/auth/actions";
import easyworklogo from "../../image/easywork-logo-vertical.png";

const { resubscribe } = authAction;
class Unsub extends React.Component {
  state = {
    email: ""
  };
  onResub = () => {
    const { resubscribe } = this.props;
    const { email } = this.state;

    if (email) {
      if (email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
        resubscribe(email);
      } else {
        notification.open({
          message: "Error",
          description: "Email is invalid"
        });
      }
    } else {
      notification.open({
        message: "Error",
        description: "Email not specified"
      });
    }
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value
    });
  }
  render() {
    const { loading, resubscribed } = this.props;
    return (
      <Style>
        <div className="leftContent" style={{ backgroundColor: resubscribed ? "#94defd" : "#ff6600" }}>
          {resubscribed ? (
            <img alt="#" src={DogHappy} className="dogImg" />)
            : (
              <img alt="#" src={Image} className="dogImg" />
            )}
        </div>
        <div className="rightContent">
          <div className="isoLogoWrapper">
            <a href="https://www.easywork.asia">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </a>
          </div>
          <span className="title">
            {resubscribed ? (
              <IntlMessages id="unsub.subSuccess" />)
              : (
                <IntlMessages id="unsub.title" />
              )}
          </span>
          <span className="subtitle">
            {resubscribed ? (
              <IntlMessages id="unsub.subSuccessDesc" />)
              : (
                <IntlMessages id="unsub.subTitle" />
              )}

          </span>
          {!resubscribed ? (
            <div className="buttonContainer">
              <Input
                placeholder="Email"
                className="subInput"
                allowClear={true}
                onChange={this.onEmailChange}
              />
              <Button
                className="subButton"
                loading={loading}
                onClick={() => this.onResub()}
              >
                <IntlMessages id="unsub.resub" />
              </Button>
            </div>)
            : null}

        </div>
      </Style >
    );
  }
}
export default connect(
  state => ({
    loading: state.Auth.get("resubscribe_loading"),
    resubscribed: state.Auth.get("resubscribe"),
  }),
  { resubscribe }
)(Unsub);

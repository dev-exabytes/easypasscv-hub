import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '../../config/withDirection';

const Style = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;

  @media only screen and (max-width: 760px) {
    flex-direction: column;
    padding:10px;

  }
  .leftContent{
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    flex:1;
    height: 100vh;
    @media only screen and (max-width: 760px) {
      display:none;
    }
  }
  .rightContent{
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    flex-wrap: wrap;
    flex:1;
    height: 100vh;
    padding:20px 120px;
    @media only screen and (max-width: 760px) {
      padding:10px;
      justify-content: center;
    }
  }
  .title{
    font-size:36px;
    font-weight: 700;
    color: ${palette('ew', 0)};
  }
  .subtitle{
    font-size:18px;
  }
  .dogImg{
    width:90%;
  }
  .subButton{
    height:40px;
    background-color: ${palette('ew', 4)};
    border:none;
    color:#fff;
    font-size:18px;
    padding:5px 10px;
  }
  .subInput{
    height:40px;
    font-size:18px;
  }
  .buttonContainer{
    display:flex;
    align-items:center;
    margin-top:20px;
    @media only screen and (max-width: 760px) {
      justify-content:flex-start;
    }
  }
  .isoLogoWrapper {
    width: 100%;
    display: flex;
    justify-content: flex-start;
    flex-shrink: 0;
    margin-top:20px;
    margin-bottom:30px;
    }
  }
`;

export default WithDirection(Style);

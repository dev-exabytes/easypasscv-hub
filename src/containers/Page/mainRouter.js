import React from "react";
import { Switch, Route } from "react-router-dom";
import asyncComponent from "../../helpers/AsyncFunc";
class MainRouter extends React.Component {
  render() {
    const { url } = this.props;
    return (
      <Switch>
        <Route exact path={`${url}/`} component={asyncComponent(() => import("./signin"))} />
        <Route exact path={`${url}/signin`} component={asyncComponent(() => import("./signin"))} />
        <Route exact path={`${url}/signup`} component={asyncComponent(() => import("./signup"))} />
        <Route exact path={`${url}/signup/otp`} component={asyncComponent(() => import("./signupOtp"))} />
        <Route exact path={`${url}/forgotpassword`} component={asyncComponent(() => import("./forgotPassword"))} />
        <Route exact path={`${url}/resetpassword/otp`} component={asyncComponent(() => import("./resetPasswordOtp"))} />
        <Route exact path={`${url}/resetpassword`} component={asyncComponent(() => import("./resetPassword"))} />
      </Switch>
    );
  }
}

export default MainRouter;

import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import ResetPasswordStyleWrapper from './resetPasswordOtp.style';
import { Button, notification } from "antd";
import authAction from "../../redux/auth/actions";
import OTPInput from "../../components/otp/OtpInput";
import Countdown from 'react-countdown-now';

const { signupCountDownReset, sendOtpByEmail, sendOtpByPhone, checkOtpByPhone, checkOtpByEmail } = authAction;
const easyworklogo = require("../../image/easywork.png");

class SignupOTP extends Component {
  state = {
    redirectToReferrer: false,
    loading: false,
    phone: "",
    countryCode: "",
    email: "",
    otp: "",
  };

  handleChange = otp => {
    const history = this.props.history.location.state;
    const { phone, email, country_code, checkOtpByPhone, checkOtpByEmail } = this.props;
    this.setState({
      otp: otp
    }, () => {
      if (otp.toString().length === 6) {
        if (phone && country_code) {
          checkOtpByPhone(phone, country_code, otp.toString(), 2, history);
        } else if (email) {
          checkOtpByEmail(email, otp.toString(), 2, history);
        }
      }
    });
  }

  onCountDownComplete = () => {
    const { signupCountDownReset } = this.props;
    signupCountDownReset();
  }

  onSendOTP = () => {
    const history = this.props.history.location.state;
    const { sendOtpByEmail, sendOtpByPhone, phone, email, country_code, password } = this.props;

    if (email && password) {
      sendOtpByEmail(email, password, history, true);
    } else if (phone && country_code && password) {
      sendOtpByPhone(phone, country_code, password, history, true);
    } else {
      notification.open({
        message: "Error"
      });
    }
  }
  render() {
    const from = this.props.location.state || { pathname: "/dashboard" };
    const { redirectToReferrer, otp } = this.state;
    const { signupCountDown, phone, email, disableOtp, loading } = this.props;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    // Renderer callback with condition
    const renderer = ({ hours, minutes, seconds, completed }) => {
      return <span>Please wait <span className="countDownText">{minutes}:{seconds}</span></span>;
    };

    return (
      <ResetPasswordStyleWrapper>
        <div className="isoFormContent">
          <div className="isoLogoWrapper" style={{ textAlign: "center" }}>
            <Link to="/dashboard">
              <img src={easyworklogo} width={"40%"} alt="EasyWork" />
            </Link>
          </div>
          <div className="titleWrapper">
            <p style={{ textAlign: "center" }}>What is your verification code?</p>
          </div>

          <div className="isoForgotPassForm">
            <div className="InputContainer">
              <OTPInput
                value={otp}
                onChange={this.handleChange}
                OTPLength={6}
                otpType="number"
                disabled={false}
                inputClassName="Otp"
                inputStyles={{
                  width: window.innerWidth > 767 ? "50px" : "40px",
                  height: "40px",
                  borderRadius: "4px",
                  border: "none",
                  marginRight: "10px"
                }}
              />
            </div>
            <div className="titleWrapper">
              <p style={{ textAlign: "center" }}>If you havent's gotten a code</p>
            </div>
            <div className="titleWrapper" style={{ paddingTop: "10px" }}>
              {phone || email ? (
                <p>Verify your {phone ? "mobile number : " + phone : "email : " + email}</p>
              ) : <p>Please verify your contact again.</p>}
              <p>
                <Button
                  type="link"
                  onClick={this.props.history.goBack}
                >
                  Change {phone ? "Number" : email ? "Email" : "Contact"}
                </Button>
              </p>
            </div>
            {
              phone || email ? (
                <div className="titleWrapper" style={{ paddingTop: "10px" }}>
                  <p>Check your {phone ? "sms messages" : "email inbox/junk mail"}</p>
                  <p>
                    {!signupCountDown ? (
                      <Button
                        type="link"
                        loading={loading}
                        onClick={() => this.onSendOTP()}
                        disabled={disableOtp}
                      >Resend code to {phone ? "Mobile Number" : "Email"}</Button>
                    ) : (
                        <Button
                          type="link"
                          disabled={true}
                        >
                          <Countdown
                            date={signupCountDown}
                            renderer={renderer}
                            onComplete={() => this.onCountDownComplete()}
                          />
                        </Button>
                      )}
                  </p>
                </div>
              ) : null
            }
          </div>
        </div>
      </ResetPasswordStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    loading: state.Auth.get("signup_otp_loading"),
    signupCountDown: state.Auth.get("signup_count_down"),
    phone: state.Auth.get("phone"),
    email: state.Auth.get("email"),
    country_code: state.Auth.get("country_code"),
    password: state.Auth.get("password"),
    disableOtp: state.Auth.get("disable_otp")
  }),
  {
    signupCountDownReset, sendOtpByEmail, sendOtpByPhone, checkOtpByPhone, checkOtpByEmail
  }
)(SignupOTP);

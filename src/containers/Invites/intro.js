import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import { Steps, Button, Form, Divider, Modal } from 'antd';
import IntlMessages from "../../components/utility/intlMessages";
import Style from "./join.style";
import UpdateProfileForm from "./updateProfile";
import authAction from "../../redux/auth/actions";
import UpdateWorkspaceForm from "./updateWorkspace";
import joinAction from "../../redux/join/actions";
import UpdateRequestForInvitationForm from "./updateRequestForInvitation";

const { updateProfile } = authAction;
const { joinCompany, clearError, sendRequestToAdmin } = joinAction;
const { Step } = Steps;
const steps = [
  {
    title: "auth.updateProfile",
    content: "auth.updateProfileMsg",
    placeholder: {
      first_name: "antTable.title.firstName",
      last_name: "antTable.title.lastName"
    }
  },
  {
    title: "join.introAction",
    content: "join.intro",
    firstForm: "join.requestAdmin",
    secondForm: "join.workspace"
  }
];

const WrappedUpdateProfileForm = Form.create()(UpdateProfileForm);
const WrappedUpdateWorkspaceForm = Form.create()(UpdateWorkspaceForm);
const WrappedUpdateRequestForInvitationForm = Form.create()(UpdateRequestForInvitationForm);
class Intro extends Component {
  constructor(props) {
    super(props);
    this.updateProfileChild = React.createRef();
    this.state = {
      current: 0,
      isDirtyWorkspaceField: false,
      workspace: null,
      isSignup: false,
      updateProfileSucceed: false,
      user: this.props.user,
      prevProfileSuceed: false,
      prevIsSignup: false,
      join422: false,
      prevJoin422: false,
      joinCompanySucceed: false,
      prevJoinCompanySucceed: false,
      companyName: null
    };
  }
  componentDidMount() {
    const {
      updateProfileSucceed,
      prevProfileSuceed,
    } = this.state;

    // Get workspace
    const { workspace } = this.props.match.params;
    if (workspace) {
      this.setState({
        workspace
      });
    }
    const queryString = require('query-string');
    const parsed = queryString.parse(this.props.location.search);
    if (Object.keys(parsed).length !== 0) {
      if (parsed.name) {
        this.setState({
          companyName: parsed.name
        });
      }
    }
    if (updateProfileSucceed !== prevProfileSuceed) {
      if (updateProfileSucceed) {
        this.next();
      }
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.updateProfileSucceed !== nextProps.updateProfileSucceed) {
      return {
        updateProfileSucceed: nextProps.updateProfileSucceed,
        prevProfileSuceed: prevState.updateProfileSucceed
      }
    }
    if (prevState.join422 !== nextProps.join422) {
      return {
        join422: nextProps.join422,
        prevJoin422: prevState.join422
      }
    }
    if (prevState.joinCompanySucceed !== nextProps.joinCompanySucceed) {
      return {
        joinCompanySucceed: nextProps.joinCompanySucceed,
        prevJoinCompanySucceed: prevState.joinCompanySucceed,
        current: nextProps.joinCompanySucceed ? 0 : 1
      }
    }
    return null;
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  updateProfileFormRef = formRef => {
    this.formRef = formRef;
  };
  updateWorkspaceFormRef = formRef => {
    this.workspaceFormRef = formRef;
  };

  updateRequestAdminRef = formRef => {
    this.requestAdminFormRef = formRef;
  };

  handleSubmit = () => {
    const { form } = this.formRef.props;
    const { updateProfile } = this.props;
    const { user } = this.state;

    form.validateFields(['first_name', 'last_name'], (err, values) => {
      if (err) {
        return;
      }
      if (user) {
        if (user.first_name !== values.first_name || user.last_name !== values.last_name) {
          updateProfile(values);
        } else {
          this.next();
        }
      } else {
        updateProfile(values);
      }
    });
  };

  handleJoinCompany = () => {
    const { joinCompany, sendRequestToAdmin, user } = this.props;
    const workspaceFlag = this.workspaceFormRef.props.form.isFieldsTouched();
    const rtjFlag = this.requestAdminFormRef.props.form.isFieldsTouched();
    if (!workspaceFlag && !rtjFlag) {
      Modal.warning({
        title: 'Please fill in either 1 option to join company',
      });
    }
    if (workspaceFlag) {
      this.workspaceFormRef.props.form.validateFields(['workspace'], (err, values) => {
        if (err) {
          return;
        }
        joinCompany(values.workspace);
      });
    }
    if (rtjFlag) {
      this.requestAdminFormRef.props.form.validateFields(['email', 'company_name'], (err, values) => {
        if (err) {
          return;
        }
        sendRequestToAdmin(user.id, values.email, values.company_name);
      });
    }
  };

  render() {
    const {
      current,
      workspace,
      user,
      join422,
      companyName
    } = this.state;
    const { clearError, joinLoading, updateProfileLoading, rtjLoading } = this.props;
    return (
      <LayoutContentWrapper>
        <Style>
          <Steps current={current}>
            {steps.map(item => (
              <Step key={item.title} title={<IntlMessages id={`${item.title}`} />} />
            ))
            }
          </Steps>
          {steps[current].content === "auth.updateProfileMsg" ? (
            <div className="steps-content">
              <p style={{ paddingBottom: "20px" }}>
                <IntlMessages id={`${steps[current].content}`} />
              </p>
              <WrappedUpdateProfileForm
                wrappedComponentRef={this.updateProfileFormRef}
                onSubmit={this.handleSubmit}
                firstName={user ? user.first_name : ""}
                lastName={user ? user.last_name : ""}
              />

            </div>
          ) : steps[current].content === "join.intro" ? (
            <div >
              <div className="steps-content">
                <p className="titleWrapper">
                  <IntlMessages id={`${steps[current].firstForm}`} />
                </p>
                <WrappedUpdateRequestForInvitationForm
                  wrappedComponentRef={this.updateRequestAdminRef}
                  companyName={companyName}
                />
              </div>
              <div className="dividerContainer">
                <Divider>or</Divider>
              </div>
              <div className="steps-content">
                <p className="titleWrapper">
                  <IntlMessages id={`${steps[current].secondForm}`} />
                </p>
                <WrappedUpdateWorkspaceForm
                  wrappedComponentRef={this.updateWorkspaceFormRef}
                  onSubmit={this.handleJoinCompany}
                  workspace={workspace}
                  joinError={join422}
                  clearError={clearError}
                />
              </div>
            </div>
          ) : null}
          <div className="steps-action">
            {
              steps[current].content === "auth.updateProfileMsg" && (
                <Button
                  className="buttonContainer"
                  onClick={() => this.handleSubmit()}
                  loading={updateProfileLoading}
                >
                  Next
                </Button>
              )}
            {steps[current].content === "join.intro" && (
              <Button
                className="buttonContainer"
                onClick={() => this.handleJoinCompany()}
                loading={joinLoading || rtjLoading}
              >
                Request Join Company
              </Button>
            )}
            {current > 0 && (
              <Button
                className="plainButtonContainer"
                style={{ marginLeft: 8 }}
                onClick={() => this.prev()}
              >
                Previous
              </Button>
            )}
          </div>
        </Style>
      </LayoutContentWrapper >
    )
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      isSignup: state.Auth.get("is_signup"),
      user: state.Auth.get("user"),
      updateProfileLoading: state.Auth.get("update_profile_loading"),
      updateProfileSucceed: state.Auth.get("update_profile_succeed"),
      join422: state.Join.get("join422"),
      joinCompanySucceed: state.Join.get("join_company_succeed"),
      joinLoading: state.Join.get("loading"),
      rtjLoading: state.Join.get("rtjLoading")
    })
  },
  { updateProfile, joinCompany, clearError, sendRequestToAdmin }
)(Intro);

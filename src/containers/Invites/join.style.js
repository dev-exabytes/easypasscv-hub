import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  border-radius:10px;
  background-color:${palette("ew", 1)};
  overflow:hidden;
  padding:30px;
  width:70%;
  @media only screen and (max-width: 800px) {
    width: 90%;
  }

  .ant-input-lg {
    height: 42px;
    padding: 6px 20px;
  }

  .isoInputWrapper {
    display: flex;
    margin-bottom: 15px;
    flex-direction: row;

    &:last-of-type {
      margin-bottom: 0;
    }

    input {
      &::-webkit-input-placeholder {
        color: ${palette("grayscale", 0)};
      }

      &:-moz-placeholder {
        color: ${palette("grayscale", 0)};
      }

      &::-moz-placeholder {
        color: ${palette("grayscale", 0)};
      }
      &:-ms-input-placeholder {
        color: ${palette("grayscale", 0)};
      }
    }
  }


  .steps-content {
    margin-top: 30px;
    background-color: #fff;
    padding-horizontal:50px;
    color: #000;
    display: flex;
    align-items: center;
    flex-direction: column;
  }

  .steps-action {
    margin-top: 24px;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding-horizontal:50px;
    flex-wrap: wrap;
  }

  .formContainer{
    width: 60%;
    text-align: left;

    @media only screen and (max-width: 767px) {
      width: 90%;
    }
  }

  .updateWorkspaceFormContainer{
    width: 50%;
    text-align: left;

    @media only screen and (max-width: 767px) {
      width: 90%;
    }
  }
  .titleWrapper{
    width: 50%;
    padding-bottom:10px;
    text-align: left;
    @media only screen and (max-width: 767px) {
      width: 90%;
    }
  }
  .workspaceInput{
    height: 50px;
  }
  .skipButton{
    marginLeft: 8;
    border: none;
    color: ${palette("ew", 4)};
    box-shadow: none;
  }
  .buttonContainer{
    height:40px;
    border-radius:20px;
    background-color: ${palette('ew', 6)};
    color:${palette("ew", 1)};
    border:none;
  }
  .plainButtonContainer{
    height:40px;
    border-radius:20px;
    background-color: ${palette('ew', 1)};
  }
  .ant-divider-horizontal.ant-divider-with-text-center::before,
  .ant-divider-horizontal.ant-divider-with-text-center::after{
    border-top: 2px solid ${palette("border", 1)};
  }
  .ant-divider-horizontal.ant-divider-with-text-center{
      font-weight: 400;
      color: ${palette("text", 1)};
  }
  .ant-form-item {
    margin-bottom: 5px !important;
  }
  .dividerContainer{
    padding: 10px 50px 0 50px;
    @media only screen and (max-width: 767px) {
      padding: 10px 0 0 0;
    }
  }

`;

export default Style;

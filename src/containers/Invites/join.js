import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import Style from "./join.style";
import { connect } from "react-redux";
import actions from "../../redux/join/actions";
import { List } from 'antd';
import TableWrapper from "../Employee/easyworkTable.style";


const { getSubmitted } = actions;
class Join extends Component {
  state = {
    loading: this.props.loading,
    data: this.props.data,
    page: 1,
  };

  componentDidMount() {
    // Get workspace
    const { workspace } = this.props.match.params;
    if (!workspace) {
      const workspace = this.props.currentCompany.workspace_name;
    }

    // Get join company request submitted
    const { getSubmitted } = this.props;
    getSubmitted();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.data !== nextProps.data) {
      this.setState({ data: nextProps.data }, () => console.log(this.state.data));
    }
    if (this.props.loading !== nextProps.loading) {
      this.setState({ loading: nextProps.loading }, () => console.log(this.state.data));
    }
  }

  render() {
    const { data, loading } = this.state;
    return (
      <LayoutContentWrapper>
        <TableWrapper
          className="isoLayoutContent"
          itemLayout="horizontal"
          dataSource={data}
          loading={loading}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                title={item.company_name}
                description={item.created_at}
              />
              <div>{item.status_name}</div>
            </List.Item>
          )}
        >
        </TableWrapper>
      </LayoutContentWrapper>
    )
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      loading: state.Join.get("loading"),
      data: state.Join.get("data")
    })
  },
  { getSubmitted }
)(Join);

import React, { Component } from "react";
import {
  Form,
  Input
} from 'antd';

export default class UpdateRequestForInvitationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      company_name: null
    };
  }
  render() {
    const { companyName } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className="updateWorkspaceFormContainer">
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail',
              },
              {
                required: true,
                message: "Please input your admin's E-mail",
              },
            ]
          })(
            <Input
              placeholder="admin@yourcompany.com"
              allowClear={true}
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('company_name', {
            initialValue: companyName,
            rules: [
              {
                required: true,
                message: "Please input your company name",
                whitespace: true
              },
            ]
          })(
            <Input
              placeholder="Your company name"
              allowClear={true}
            />,
          )}
        </Form.Item>
      </Form>
    );
  }
}

import React, { Component } from "react";
import {
  Form,
  Input
} from 'antd';

export default class UpdateProfileForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: null,
      last_name: null,
    };
  }

  componentDidMount() {
    const { form, firstName, lastName } = this.props;

    form.setFieldsValue({
      first_name: firstName,
      last_name: lastName
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="vertical" className="formContainer">
        <Form.Item label="First Name" required={true} >
          {getFieldDecorator('first_name', {
            rules: [
              {
                required: true,
                message: 'Please enter your first name',
                whitespace: true
              },
            ],
          })(
            <Input
              placeholder="First Name"
            />,
          )}
        </Form.Item>
        <Form.Item label="Last Name" required={true} >
          {getFieldDecorator('last_name', {
            rules: [
              {
                required: true,
                message: 'Please enter your last name',
                whitespace: true
              },
            ],
          })(
            <Input
              placeholder="Last Name"
            />,
          )}
        </Form.Item>
      </Form>
    );
  }
}

import React, { Component } from "react";
import {
  Form,
  Input
} from 'antd';

export default class UpdateWorkspaceForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workspace: null
    };
  }

  componentDidMount() {
    const { form, workspace } = this.props;

    form.setFieldsValue({
      workspace
    });
  }


  handle422 = (rule, value, callback) => {
    const { joinError, workspace } = this.props;
    if (workspace === value && joinError) {
      callback("Invalid company workspace");
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="vertical" className="updateWorkspaceFormContainer">
        <Form.Item required={true}>
          {getFieldDecorator('workspace', {
            rules: [
              {
                required: true,
                message: 'Please enter your company workspace name.',
                whitespace: true
              },
              {
                validator: this.handle422
              }
            ],
          })(
            <Input
              addonAfter=".easywork.asia"
              placeholder="workspace"
              style={{ height: "50px", fontSize: "18px" }}
              allowClear={true}
            />,
          )}
        </Form.Item>
      </Form>
    );
  }
}

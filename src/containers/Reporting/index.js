import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Button, Form, Icon, Table } from "antd";
import DaysFilter from "../../components/daysFilter/index";
import moment from 'moment';
import timezone from 'moment-timezone';
import { palette } from 'styled-theme';
import actions from "../../redux/reporting/actions";

const { generateReport, getHistory } = actions;

const WrappedDaysFilterForm = Form.create()(DaysFilter);

const truncate = (input) => input.length > 15 ? `${input.substring(0, 15)}...` : input;

const columns = [
  {
    title: 'Generate By',
    dataIndex: 'created_by',
    key: 'created_by',
    render: (text, record) => {
      if (record.links.length > 0) {
        return <p>{truncate(record.links[0].created_by || "-")}</p>
      }
      return null;
    }
  },
  {
    title: 'Generate At',
    dataIndex: 'created_at',
    key: 'created_at',
    render: (text, record) => {
      if (record.links.length > 0) {
        return <p>{record.links[0].created_at}</p>
      }
      return null;
    }
  },
  {
    title: (<span>Download <Icon type="download" style={{ color: palette('primary', 0) }} /></span>),
    dataIndex: 'links',
    key: 'links',
    render: (text, record) => (
      <ul style={{ listStyleType: "circle" }}>
        {record.links.map(item => (
          <li key={item.id}>
            <a
              rel="noopener noreferrer"
              href={item.path}
              target="_blank"
            >
              {item.filename}
            </a>
          </li>
        ))}
      </ul>
    )
  },
];
class Reporting extends Component {
  state = {
    loading: false,
    timezone: timezone.tz.guess(),
    dates: [moment().tz(timezone.tz.guess()), moment().tz(timezone.tz.guess())],
    days: null,
    batch: null
  };


  componentDidMount() {
    const { getHistory, currentCompany } = this.props;
    if (currentCompany) {
      getHistory(currentCompany.id, null);
    }
    const { batch } = this.props.match.params;
    if (batch) {
      this.setState({
        batch
      });
    }
  }

  filterFormRef = formRef => {
    this.formRef = formRef;
  };

  handleSubmit = () => {
    const { form } = this.formRef.props;
    const { getFieldValue } = form;
    const { generateReport, currentCompany } = this.props;

    const dates = getFieldValue("dates");
    const start = dates[0].format("YYYY-MM-DD");
    const end = dates[1].format("YYYY-MM-DD");
    generateReport(currentCompany.id, start, end);
  };

  onGetHistory = () => {
    const { getHistory, currentCompany } = this.props;
    getHistory(currentCompany.id, null);
  }

  render() {
    const { history, generating, getting } = this.props;
    const { batch } = this.state;
    return (
      < LayoutContentWrapper >
        <TableDemoStyle className="isoLayoutContent">
          <div style={{ paddingBottom: "20px" }}>
            <h3 style={{ paddingBottom: "10px" }}>Detailed transaction reports of your company in CSV format can be downloaded here</h3>
          </div>
          <div>
            <p style={{ paddingBottom: "10px" }}>Step 1: Filter the date range</p>
            <WrappedDaysFilterForm
              wrappedComponentRef={this.filterFormRef}
              onSubmit={this.handleSubmit}
              dates={[moment().tz(timezone.tz.guess()), moment().tz(timezone.tz.guess())]}
              timezone={timezone.tz.guess()}
              days={"1"}
              orFlag={true}
            />
          </div>
          <div style={{ paddingBottom: "20px" }}>
            <p style={{ paddingBottom: "10px" }}>Step 2: Tap on "Generate Report"</p>
            <Button
              className="buttonPrimary"
              type="primary"
              onClick={() => this.handleSubmit()}
              loading={generating}
            >
              Generate Report
              </Button>
          </div>
        </TableDemoStyle>
        <TableDemoStyle className="isoLayoutContent" style={{ marginTop: "30px" }}>
          <div>
            <div className="columnContainer">
              <span style={{ fontSize: "16px", fontWeight: "700", color: "#000" }}>History</span>
              <Button
                type="primary" icon="sync" shape="circle"
                onClick={() => this.onGetHistory()}
                style={{ overflow: "hidden", borderRadius: "20px", marginLeft: "10px", backgroundColor: "#ff6600", border: "none" }}
              />
            </div>
            <Table
              title={() => "Show last 20 reports generated."}
              loading={getting}
              columns={columns}
              dataSource={history}
              pagination={false}
              className="tableContainer"
              rowKey="batch"
              rowClassName={(record, index) => {
                return batch && record.batch === batch ? 'hightlightRow' : null;
              }}
            />
          </div>
        </TableDemoStyle>
      </LayoutContentWrapper >
    );
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      history: state.CompanyReport.get("data"),
      generating: state.CompanyReport.get("generating"),
      getting: state.CompanyReport.get("getting"),
    })
  },
  { generateReport, getHistory }
)(Reporting);

import { baseUrl } from "../../config";

export const getAttendanceReport = (idToken, companyId, start_dt, end_dt) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/attendances/report?start_dt=${start_dt}&end_dt=${end_dt}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", "Time-Zone": "Asia/Kuala_Lumpur", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json",
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const addCompanies = (idToken, keywords, loop) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(`${baseUrl}/v1/marketing/companies`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
        body: JSON.stringify({
          keywords: keywords,
          loop: loop,
        })
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

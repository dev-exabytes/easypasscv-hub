import styled from 'styled-components';
import { palette } from 'styled-theme';

const TableDemoStyle = styled.div`
  border-radius:10px;
  .ant-tabs-content {
    margin-top: 40px;
  }

  .ant-tabs-nav {
    > div {
      color: ${palette('secondary', 2)};

      &.ant-tabs-ink-bar {
        background-color: ${palette('primary', 0)};
      }

      &.ant-tabs-tab-active {
        color: ${palette('primary', 0)};
      }
    }
  }

  .filterContainer{
    display:flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: row;

    @media only screen and (max-width: 767px) {
      flex-direction: column;
    }
  }

  .tableContainer{
    '& thead > tr > th': {
      background-color: darkblue,
    }
  }
  .linkContainer{
    color:#000;
  }
  .hightlightRow{
    background-color:${palette('ew', 3)};
    color: #000;
  }
  .buttonPrimary, .buttonPrimary:active, .buttonPrimary:hover{
    background-color:${palette('ew', 0)};
    border-color:${palette('ew', 0)};
    border-radius:5px;
  }
  .columnContainer{
    display:flex;
    flex-direction:row;
    align-items:center;
  }
`;

export default TableDemoStyle;

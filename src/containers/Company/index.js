import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import Style from "./index.style";
import { Form, List, Icon, Input, Button, Divider, Modal } from 'antd';
import lottie from 'lottie-web';
import Actions from "../../redux/company/actions";
import animationData from "../../image/createCompany.json";

const { createCompany, checkCompany } = Actions;
let animObj = null;
const intro = [
  'Create company to group your employee or invite your colleagues to join your company.',
  'May assign few of your colleagues as administrator to help you manage the company.',
  'Make your work easy with many amazing tools. Create company or join now! May share EasyWork with your office administrator.'
];
class CreateCompany extends Component {
  constructor(props) {
    super(props);
    this.createCompanyChild = React.createRef();
    this.state = {
      companyName: "",
      companyExist: false,
      modalVisible: false
    };
  }

  componentDidMount() {
    //call the loadAnimation to start the animation
    animObj = lottie.loadAnimation({
      container: this.animBox, // the dom element that will contain the animation
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: animationData // the path to the animation json
    });
  }

  componentWillReceiveProps(nextProps) {

    if (this.props.companyExist !== nextProps.companyExist) {
      if (nextProps.companyExist === false) {
        // Proceed create company
        this.onCreateCompany();
      } else if (nextProps.companyExist) {
        this.setState({
          modalVisible: true
        });
      }
    }
  }

  handleStop = () => {
    animObj.stop();
  }
  handlePlay() {
    animObj.play();
  }
  handleSubmit = () => {
    const { companyName } = this.state;
    if (companyName.trim()) {
      this.props.checkCompany(companyName);
    }
  };

  onCreateCompany = () => {
    const { companyName } = this.state;
    const history = this.props.history.location.state;
    this.props.createCompany(companyName, history);
  }

  goToJoinCompany = () => {
    this.props.history.push("/dashboard/join");
  }

  onChange = e => {
    this.setState({
      companyName: e.target.value
    });
  }

  handleOk = () => {
    this.setState({
      modalVisible: false
    }, () => this.onCreateCompany());
  };

  handleJoin = () => {
    this.setState({
      modalVisible: false,
    }, () => this.props.history.push({
      pathname: "/dashboard/join",
      search: `?name=${this.state.companyName}`
    }));
  };
  handleCancel = () => {
    this.setState({
      modalVisible: false,
    });
  }
  render() {
    const { checking, loading } = this.props;
    const { modalVisible, companyName } = this.state;
    return (
      <LayoutContentWrapper>
        <Style>
          <div className="rowContainer">
            <div className="leftContainer">
              <div style={{ width: 150, margin: '0 auto' }} ref={ref => this.animBox = ref}></div>
              <List
                header={<h1 className="header">Why Create Company?</h1>}
                dataSource={intro}
                renderItem={item => (
                  <List.Item className="listWrapper">
                    <Icon type="check-circle" className="icon" theme="filled" />
                    <List.Item.Meta
                      description={item}
                    />
                  </List.Item>
                )}
              />
            </div>
            <div className="rightContainer">
              <div className="smallWrapper">
                <div className="titleWrapper">
                  <span className="title">Business Owner / HR Admin</span>
                </div>
                <div style={{ width: "80%" }}>
                  <Input className="inputContainer" placeholder="Company Name" allowClear onChange={this.onChange} />
                  <Button
                    className="buttonContainer"
                    onClick={this.handleSubmit}
                    loading={checking || loading}
                    block>
                    Create Company</Button>
                </div>
              </div>
              <div>
                <Divider>or</Divider>
              </div>
              <div className="smallWrapper">
                <div className="titleWrapper">
                  <span className="title">Employee</span>
                </div>
                <div style={{ width: "80%" }}>
                  <Button className="buttonContainer" onClick={this.goToJoinCompany} block>
                    Request for Invitation</Button>
                  <span className="description">Submit request to your HR admin so that they can add you to your company</span>
                </div>
              </div>
            </div>
          </div>
          {modalVisible ? (
            <Modal
              title="Oops"
              visible={modalVisible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              footer={[
                <Button key="join" type="primary" onClick={this.handleJoin}>
                  Join
              </Button>,
                <Button key="create" loading={loading} onClick={this.handleOk}>
                  Create
              </Button>,
              ]}
            >
              <p>There is existing company with the name <b>{companyName}</b>. Do you want to join them instead or create a new one?</p>
            </Modal>
          ) : null}
        </Style>
      </LayoutContentWrapper>
    )
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      loading: state.Company.get("creating"),
      checking: state.Company.get("checking"),
      companyExist: state.Company.get("company_exist")
    })
  },
  { createCompany, checkCompany }
)(CreateCompany);

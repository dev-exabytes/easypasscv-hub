import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  border-radius:10px;
  background-color:${palette("ew", 1)};
  overflow:hidden;

  .titleWrapper{
    margin-bottom:20px;
  }
  .title{
    font-size:18px;
    font-weight:600;
    color:#000;
  }
  .formContainer{
    text-align: left;
  }
  .buttonContainer{
    height:40px;
    border-radius:20px;
    background-color: ${palette('ew', 6)};
    color:${palette("ew", 1)};
    border:none;
    margin-bottom: 20px;
  }
  .rowContainer{
    display:flex;
    @media only screen and (max-width: 800px) {
      flex-direction:column;
    }
  }
  .leftContainer{
    flex:1;
    flex-grow:1;
    display:flex:
    flex-direction:column;
    padding:30px;
  }
  .rightContainer{
    flex:1;
    flex-grow:1;
    background-color:${palette("ew", 3)};
    padding:30px;
    display:flex;
    flex-direction:column;
  }
  .listWrapper{
    display:flex;
    align-items: flex-start;
    justify-content:flex-start;
  }
  .icon{
    color:${palette("ew", 6)};
    font-size: 18px;
    padding-top:5px;
    padding-right:10px;
  }
  .ant-list-item-meta-description {
    color: #000;
    font-weight:400;
  }
  .ant-list-split .ant-list-item {
    border: none;
  }
  .ant-list-item {
    padding: 0 0 10px 0;
  }
  .ant-list-header {
    padding:5px 0;
  }
  .ant-list-split .ant-list-header {
    border:none;
  }
  .header{
    text-align:center;
    font-size:18px;
    font-weight:600;
    color:#000;
  }
  .smallWrapper{
    display:flex;
    flex-direction:column;
    align-items:center;
    justify-content:center;
    align-self:center;
    width:70%;
    @media only screen and (max-width: 800px) {
      width:90%;
    }
  }
  .inputContainer{
    height:40px;
    margin-bottom:10px;
    text-align:center;
  }
  .ant-input {
    border:none;
  }
  .ant-divider-horizontal.ant-divider-with-text-center::before,
  .ant-divider-horizontal.ant-divider-with-text-center::after{
    border-top: 2px solid ${palette("border", 1)};
 }
 .ant-divider-horizontal.ant-divider-with-text-center{
    font-weight: 400;
    color: ${palette("text", 1)};
 }
 .description{
   font-size:14px;
   color:${palette("text", 4)};
 }
`;

export default Style;

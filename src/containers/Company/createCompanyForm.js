import React, { Component } from "react";
import {
  Form,
  Input,
  Select,
  Button
} from 'antd';
import { CountryRegionData } from 'react-country-region-selector';

const { Option } = Select;
export default class CreateCompanyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      address: null,
      postcode: null,
      state: null,
      country: null,
      phone: null,
      email: null,
      regionData: []
    };
  }

  componentDidMount() {
    //
  }

  onCountrySelect(v, o) {
    const region = CountryRegionData[o.key][2].split('|').map((v) => {
      return v.split('~')[0];
    });
    this.setState({
      regionData: region
    }, () => this.props.form.setFieldsValue({ state: undefined }));
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { onSubmit } = this.props;
    const { regionData } = this.state;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    return (
      <Form
        className="formContainer"
        {...formItemLayout}
      >
        <Form.Item label="Name" required={true} >
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter your company name',
                whitespace: true
              },
            ],
          })(
            <Input
              placeholder="Name"
            />,
          )}
        </Form.Item>
        <Form.Item label="Address">
          {getFieldDecorator('address', {
            rules: [
              {
                required: false,
                message: 'Please enter your company address',
                whitespace: true
              },
            ],
          })(
            <Input
              placeholder="Company Address"
            />,
          )}
        </Form.Item>
        <Form.Item label="Postcode">
          {getFieldDecorator('postcode', {
            rules: [
              {
                required: false,
                message: 'Please enter your company postcode',
                whitespace: true
              },
            ],
          })(
            <Input
              placeholder="Company Postcode"
            />,
          )}
        </Form.Item>
        <Form.Item
          label="Country"
        >
          {getFieldDecorator('country', {
            valuePropName: 'country',
            rules: [
              {
                required: false,
                message: 'Please enter your company country'
              },
            ],
          })(
            <Select
              placeholder="Select Country"
              onChange={this.onCountrySelect.bind(this)}
            >
              {CountryRegionData.map((v, i) => (
                <Option value={v[0]} key={i}>{v[0]}</Option>
              ))}
            </Select>,
          )}
        </Form.Item>
        <Form.Item
          label="State"
        >
          {getFieldDecorator('state', {
            rules: [
              {
                required: false,
                message: 'Please enter your company state',
              },
            ],
          })(
            <Select
              placeholder="Select State"
              disabled={regionData.length === 0}
            >
              {regionData.map((v, i) => (
                <Option value={v} key={i}>{v}</Option>
              ))}
            </Select>,
          )}
        </Form.Item>
        <div className="submitButton">
          <Button className="buttonContainer" onClick={onSubmit}>
            Create Company
          </Button>
        </div>
      </Form>
    );
  }
}

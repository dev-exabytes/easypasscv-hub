import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import Style from "./index.style";
import actions from "../../redux/announcement/actions";
import { Carousel, Button, Icon, Card, Avatar, Divider, Modal } from 'antd';
import { Link } from "react-router-dom";
import Linkify from 'react-linkify';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

const { Meta } = Card;
const { getAnnouncementDetail, checkAnnouncementAccess, deleteAnnouncement } = actions;

class AnnouncementDetail extends Component {

  componentDidMount() {
    const { getAnnouncementDetail, checkAnnouncementAccess, currentCompany, idToken } = this.props;
    const { match: { params } } = this.props;

    this.setState(() => checkAnnouncementAccess(
      currentCompany.id,
      idToken
    ));

    const queryString = require('query-string');
    const parsed = queryString.parse(this.props.location.search);
    if (Object.keys(parsed).length !== 0) {
      const { company, token } = parsed;
      if (company && token) {
        this.setState(() => getAnnouncementDetail(
          company,
          params.announcementId,
          token
        ));
      }
    } else if (currentCompany && idToken) {
      return getAnnouncementDetail(
        currentCompany.id,
        params.announcementId,
        idToken
      );
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      isDeleting: false,
      isModalVisible: false,
      zoom: {
        isOpen: false,
        photoIndex: 0,
      }
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.carousel = React.createRef();
    this.handleDelete = this.handleDelete.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.showZoom = this.showZoom.bind(this);
  }

  handleDelete() {
    const { deleteAnnouncement, currentCompany, idToken } = this.props;
    const { match: { params } } = this.props;

    this.hideModal();

    this.setState({ isDeleting: true })

    deleteAnnouncement(
      currentCompany.id,
      params.announcementId,
      idToken
    )

  }

  next() {
    this.carousel.next();
  }
  previous() {
    this.carousel.prev();
  }

  showModal() {
    this.setState({
      isModalVisible: true
    })
  }

  hideModal() {
    this.setState({
      isModalVisible: false
    })
  }

  showZoom(indexNo) {
    this.setState({
      zoom: {
        isOpen: true,
        photoIndex: indexNo,
      }
    })
  }

  render() {

    if (this.props.announcementDetail && this.props.currentCompany) {
      var announcementDetail = this.props.announcementDetail.company_id !== this.props.currentCompany.id ? null : this.props.announcementDetail;
    }
    const access = this.props.access;
    const carouselProps = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    let shouldDisable = this.state.isDeleting && !this.props.deleteFailed

    return (
      <LayoutContentWrapper>
        <Style className="isoLayoutContent">
          {access && announcementDetail ?
            <div className="authorizedBtn">
              <div className="delete-container">
                <Button loading={shouldDisable} onClick={this.showModal}>Delete</Button>
              </div>
              <div className="edit-container">
                <Link to={{ pathname: `/dashboard/announcement/detail/${announcementDetail.id}/edit`, state: { fromLink: true, announcementDetail: announcementDetail } }}><Button disabled={shouldDisable} >Edit</Button></Link>
              </div>
            </div> : <div></div>}
          {announcementDetail ?
            <div>
              {announcementDetail.photos.length ?
                <div className="carousel-container">
                  <div className="carousel">
                    <Carousel ref={node => (this.carousel = node)} {...carouselProps} arrows>
                      {announcementDetail.photos.map((photo, index) => <div className="carousel-img" key={index} onClick={() => this.showZoom(index)}><div className="imageOverlay"><img id="carouselImg" alt="Announcement Photos" height="300px" src={photo.url} /></div></div>)}
                    </Carousel>
                  </div>
                  {announcementDetail.photos.length > 1 ?
                    <div className="arrow-key">
                      <div className="left-arrow">
                        <Icon type="left-circle" onClick={this.previous} />
                      </div>
                      <div className="right-arrow">
                        <Icon type="right-circle" onClick={this.next} />
                      </div>
                    </div> :
                    <div></div>
                  }
                </div>
                :
                <div></div>
              }
              <div className="title">{announcementDetail.title}</div>
              <Card bordered={false}>
                <Meta
                  avatar={announcementDetail.creator_details.avatar ? <Avatar src={announcementDetail.creator_details.avatar} /> : <Avatar icon={"user"} />}
                  title={announcementDetail.creator_details.name}
                  description={announcementDetail.formatted_created_at}
                />
              </Card>
              <Divider />
              <div className="body"><Linkify>{announcementDetail.body}</Linkify></div>
              <AttachmentZoom 
              hide={() => this.setState({ zoom: { isOpen: false } })}
              next={(index) => this.setState({ zoom: { isOpen: true, photoIndex: index } })}
              prev={(index) => this.setState({ zoom: { isOpen: true, photoIndex: index } })}
              images={announcementDetail.photos} 
              isOpen={this.state.zoom.isOpen} 
              photoIndex={this.state.zoom.photoIndex} />
            </div>
            :
            this.props.loadingDetail === true ?
              <div>Getting announcement...</div> :
              <span>Announcement does not exist.</span>
          }

          <Modal
            title="Are you sure?"
            visible={this.state.isModalVisible}
            onCancel={this.hideModal}
            onOk={this.handleDelete}
            footer={[
              <Button key="submit" loading={shouldDisable} onClick={this.handleDelete}>Yes</Button>,
              <Button key="back" type="primary" loading={shouldDisable} onClick={this.hideModal}>Cancel</Button>,
            ]}
          >
            <p>Deleted annoucement cannot be recovered.</p>
          </Modal>


        </Style>
      </LayoutContentWrapper >
    )
  }
}

class AttachmentZoom extends Component {

  render() {
    const { photoIndex, isOpen, images } = this.props;
    return (
      <div>

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex].url}
            nextSrc={images[(photoIndex + 1) % images.length].url}
            prevSrc={images[(photoIndex + images.length - 1) % images.length].url}
            onCloseRequest={() => this.props.hide()}
            onMovePrevRequest={() => this.props.prev((photoIndex + images.length - 1) % images.length)}
            onMoveNextRequest={() => this.props.next((photoIndex + 1) % images.length)}
          />
        )}
      </div>
    );
  }
}


export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      announcementDetail: state.Announcement.get("announcement_detail"),
      loadingDetail: state.Announcement.get("loading_detail"),
      deleteSuccess: state.Announcement.get("delete_success"),
      deleteFailed: state.Announcement.get("delete_failed"),
      access: state.Announcement.get("announcement_access"),
    })
  },
  { getAnnouncementDetail, checkAnnouncementAccess, deleteAnnouncement }
)(AnnouncementDetail);
import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  border-radius:10px;
  background-color:${palette("ew", 1)};
  overflow:hidden;

.arrow-key{
  text-align: center;
}

.arrow-key .left-arrow{
  display: inline-block;
  margin-left: auto;
  margin-right: 25px;
}

.arrow-key .right-arrow{
  display: inline-block;
  margin-right: auto;
  margin-left: 25px;
}

.ant-card-body{
  padding: 24px 0px;
  
}

.title{
  font-size:18px;
  font-weight:600;
  word-wrap:break-word;
  white-space: pre-wrap;
  color:#000;
}

.body{
  font-size:14px;
  word-wrap:break-word;
  white-space: pre-wrap;
  color:${palette("text", 4)};
}

.authorizedBtn{
  display:flex;
  justify-content: flex-end;
}

.authorizedBtn > * {
  margin: 10px;
}

.deleteBtnContainer {
  display:flex;
  justify-content: space-between;
}

.deleteText {
  margin: 0px 10px;
}

.imageOverlay {
  position: relative; 
  height: auto; 
  background: rgba(0,0,0,0);
  cursor: pointer;
  margin: auto;
  transition: .3s ease;
}

.imageOverlay:hover{
  background: rgba(0,0,0,0.7);
}

.carousel-img img{
  display: block;
  margin: auto;
}

`;

export default Style;

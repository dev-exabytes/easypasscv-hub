import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import TableWrapper from "./easyworkTable.style";
import { Input, Row, Col, Button } from "antd";
import { addCompanies, getCompanies } from "./api.js";

const Search = Input.Search;

class Marketing extends Component {
  state = {
    loading: false,
    result: [],
    total: "",
    page: 1,
    value: "",
    columns: []
  };


  componentDidMount() {
    console.log("he");
    this.setState({
      loading: true,
      columns: []
    })
    let page = 1;
    this.getCompanies(page);
  }

  getCompanies = (page) => {
    const { idToken } = this.props;
    getCompanies(idToken, page).then(res => {
      this.setState({
        result: res.data,
        total: res.total,
        loading: false,
        page: page,
      })
    }).catch(err => {
      console.log(err);
    })
  }

  addCompanies = () => {
    const { idToken } = this.props;
    const { value } = this.state
    let loop = 3;

    this.setState({ loading: true }, () =>
      addCompanies(idToken, value, loop).then(res => {
        this.getCompanies(1);
      }).catch(err => {
        console.log(err);
      }))
  };

  render() {

    const columns = [{
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    }, {
      title: 'Domain',
      dataIndex: 'domain',
      key: 'domain',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => a.domain.localeCompare(b.domain),
    }, {
      title: 'Keywords',
      dataIndex: 'keywords',
      key: 'keywords',
    }, {
      title: 'to_send',
      dataIndex: 'to_send',
      key: 'to_send',

    }, {
      title: 'has_sent',
      dataIndex: 'has_sent',
      key: 'has_sent',
    }, {
      title: 'responded',
      dataIndex: 'responded',
      key: 'responded',
    }, {
      title: 'undelivered',
      dataIndex: 'undelivered',
      key: 'undelivered',
    }
    ];

    return (
      < LayoutContentWrapper >
        <TableDemoStyle className="isoLayoutContent">
          <Row type="flex" justify="start">
            <Col md={12} sm={12} xs={24}>
              <Search
                placeholder="Search companies and add in database"
                onSearch={value => this.setState({ value: value }, () => this.addCompanies())}
                style={{
                  marginTop: "10px",
                  marginBottom: "20px",
                  fontSize: "20px"
                }}
              />
            </Col>
            <Col md={12} sm={12} xs={24}>
              <Button type="primary" icon="plus-square" onClick={this.addCompanies}
                style={{
                  marginTop: "10px",
                  marginBottom: "20px",
                  marginLeft: "50px",
                  marginRight: "100px",
                }}>
                Search and Add
        </Button>
            </Col>
          </Row>
          <TableWrapper
            dataSource={this.state.result}
            columns={columns}
            loading={this.state.loading}
            className="isoSearchableTable"
            pagination={{
              showQuickJumper: true,
              pageSize: 10,
              total: this.state.total,
              current: this.state.page,
              showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
              onChange: (page, pageSize) => {
                this.setState({ loading: true }, () => this.getCompanies(page));
              }
            }}
          />
        </TableDemoStyle>
      </LayoutContentWrapper >
    );
  }
}

export default connect(
  state => ({
    idToken: state.Auth.get("idToken"),
  }),
)(Marketing);

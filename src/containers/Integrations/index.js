import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";

class Integration extends Component {
  state = {

  };
  render() {
    return (
      <LayoutContentWrapper>

      </LayoutContentWrapper>
    );
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
    })
  }
)(Integration);

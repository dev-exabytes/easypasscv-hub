import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Button, Icon, Popconfirm } from "antd";
import authAction from "../../redux/auth/actions";
import actions from "../../redux/company/actions";
import integrationActions from "../../redux/integrations/actions";
import { notification } from "antd";
import SlackLogo from '../../image/slack.png';
import { slackConfig } from "../../config.js";
import IntlMessages from "../../components/utility/intlMessages";

const { logout } = authAction;
const { switchCompany } = actions;
const { getTeam, getSlackIntegration, removeSlackIntegration } = integrationActions;
const removeIntegrationText = "Are you sure to remove this slack integration?";
class Slack extends Component {
  state = {
    companyId: this.props.currentCompany ? this.props.currentCompany.id : null,
    currentCompany: this.props.currentCompany,
    slackToken: this.props.slack_token,
    team: this.props.slackTeam,
    status: this.props.slackStatus,
    loading: this.props.slackLoading,
    menu: this.props.menu,
    notAdmin: this.props.notAdmin,
  };

  componentDidMount() {
    if (this.state.currentCompany) {
      const queryString = require('query-string');
      const parsed = queryString.parse(this.props.location.search);
      if (Object.keys(parsed).length !== 0) {
        if (parsed.code !== '' && parsed.state !== '') {
          this.getTeamDetails(parsed);
        } else {
          notification.open({
            message: "Error",
            description: "Oops, integration with Slack failed."
          });
        }
      } else if (this.state.slackToken === null) {
        this.getIntegration(this.state.currentCompany);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.currentCompany !== nextProps.currentCompany) {
      this.setState({ currentCompany: nextProps.currentCompany });
    }
    if (this.props.slack_token !== nextProps.slack_token) {
      this.setState({ slack_token: nextProps.slack_token });
    }
    if (this.props.slackTeam !== nextProps.slackTeam) {
      this.setState({ team: nextProps.slackTeam });
    }
    if (this.props.slackStatus !== nextProps.slackStatus) {
      this.setState({ status: nextProps.slackStatus });
    }
    if (this.props.slackLoading !== nextProps.slackLoading) {
      this.setState({ loading: nextProps.slackLoading });
    }
    if (this.props.notAdmin !== nextProps.notAdmin) {
      this.setState({ notAdmin: nextProps.notAdmin });
    }
  }

  getIntegration = (company) => {
    const { getSlackIntegration } = this.props;
    if (company) {
      getSlackIntegration(company.id);
    }
  }

  getTeamDetails = (parsed) => {
    const { logout, switchCompany, getTeam, companies } = this.props;
    const { currentCompany } = this.state;
    // Compare regardless type
    if (currentCompany.id != parsed.state) {
      // Switch current company
      let company = companies.find((company) => {
        return company.id == parsed.state;
      });
      if (company) {
        switchCompany(company);
      } else {
        logout();
      }
    }
    // Get team details using code
    getTeam(parsed.code, parsed.state);
  }

  removeApp = () => {
    const { removeSlackIntegration } = this.props;
    const { slackToken, currentCompany } = this.state;
    removeSlackIntegration(slackToken, currentCompany.id);
  }

  render() {
    const { slackToken, team, status, loading, notAdmin, currentCompany } = this.state;
    return !currentCompany ? (
      <LayoutContentWrapper >
        <TableDemoStyle className="isoLayoutContent" >
          <div className="isoImageWrapper">
            <img alt="#" src={SlackLogo} /><h1>Slack Integration</h1>
          </div>
          <br></br>
          <br></br>
          <label><IntlMessages id="slack.noCompanyMsg" /></label>
          <br></br>
          <br></br>
        </TableDemoStyle>
      </LayoutContentWrapper >
    ) : (loading ? (
      <LayoutContentWrapper >
        <TableDemoStyle className="isoLayoutContent" >
          <div className="isoImageWrapper">
            <img alt="#" src={SlackLogo} /><h1>Slack Integration</h1>
          </div>
          <br></br>
          <label>Loading ...</label>
          <br></br>
          <br></br>
          <Icon type="loading" />
          <br></br>
        </TableDemoStyle>
      </LayoutContentWrapper >
    ) : slackToken && status ?
        (
          <LayoutContentWrapper >
            <TableDemoStyle className="isoLayoutContent" >
              <div className="isoImageWrapper">
                <img alt="#" src={SlackLogo} /><h1>Slack Integration</h1>
              </div>
              <div className="isoFormWrapper">
                <div className="isoInputWrapper">
                  <h4>Workspace Name:</h4>
                  <span className="isoLabel">{team}</span>
                </div>
                <div className="isoInputWrapper">
                  <h4>Status:</h4>
                  {
                    status ?
                      (<Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" className="isoIcon" />)
                      :
                      (<Icon type="close-circle" theme="twoTone" twoToneColor="#eb2f96" className="isoIcon" />)
                  }
                </div>
                <br></br>
                <div className="isoInputWrapper">
                  <Popconfirm placement="top" title={removeIntegrationText} onConfirm={this.removeApp} okText="Yes" cancelText="No">
                    <Button size="large" className="isoButton">
                      Remove Integration
                    </Button>
                  </Popconfirm>
                </div>
              </div>
              {/* <label>To manage this integration, please visit the your workspace Slack installed apps page</a></label> */}
            </TableDemoStyle>
          </LayoutContentWrapper >
        )
        : notAdmin ? (
          <LayoutContentWrapper >
            <TableDemoStyle className="isoLayoutContent" >
              <div className="isoImageWrapper">
                <img alt="#" src={SlackLogo} /><h1>Slack Integration</h1>
              </div>
              <br></br>
              <br></br>
              <label><IntlMessages id="slack.noCompanyMsg" /></label>
              <br></br>
              <br></br>
            </TableDemoStyle>
          </LayoutContentWrapper >
        ) : (
            <LayoutContentWrapper >
              <TableDemoStyle className="isoLayoutContent" >
                <div className="isoImageWrapper">
                  <img alt="#" src={SlackLogo} /><h1>Slack Integration</h1>
                </div>
                <br></br>

                <label>Install our Slack app to announce activity in Slack and reward points without leaving Slack.</label>
                <br></br>
                <div style={{ marginTop: 30 }}>
                  <a href={`https://slack.com/oauth/authorize?client_id=${slackConfig.clientId}&scope=commands,users:read,users:read.email,team:read,users.profile:read&state=${this.state.companyId}`}>
                    <img alt="Add to Slack" height="40" width="139" src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-9rj1zd/btn-add-to-slack_2x.png">
                    </img>
                  </a>
                </div>
                <br></br>
                <Icon type="question-circle" theme="filled" />
                <label> Can't figure it out? Just email our support via <a href={"mailto:support@easywork.asia?subject=EasyWork Slack Integration"}>support@easywork.asia</a></label>

              </TableDemoStyle>
            </LayoutContentWrapper >
          ))
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      companies: state.Company.get("companies"),
      slack_token: state.Integration.get("slack_token"),
      slackTeam: state.Integration.get("slack_team"),
      slackStatus: state.Integration.get("slack_status"),
      slackLoading: state.Integration.get("slack_loading"),
      menu: state.Menu.get("menu"),
      notAdmin: state.Integration.get("403")
    })
  },
  { logout, switchCompany, getTeam, getSlackIntegration, removeSlackIntegration }
)(Slack);

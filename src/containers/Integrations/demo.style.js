import styled from 'styled-components';
import { palette } from 'styled-theme';

const TableDemoStyle = styled.div`
  border-radius:10px;
  .ant-tabs-content {
    margin-top: 40px;
  }

  .ant-tabs-nav {
    > div {
      color: ${palette('secondary', 2)};

      &.ant-tabs-ink-bar {
        background-color: ${palette('primary', 0)};
      }

      &.ant-tabs-tab-active {
        color: ${palette('primary', 0)};
      }
    }
  }
  .isoImageWrapper{
    display:flex;
    flex-direction:row;
    align-items: center;
    text-align: center;

    h1{
      font-size: 24px;
      padding-left:10px;
    }
    img{
      width:60px;
      height:60px;
    }
  }

  .isoFormWrapper{
    margin:50px;
  }

  .isoInputWrapper{
    display:flex;
    flex-direction:row;
    align-item:center;
    margin-bottom:20px;

    .isoIcon{
      font-size:20px;
      padding-left:10px;
    }

    .isoButton{
      border-radius: 5px;
      border-color: ${palette('ew', 2)};
      background-color: ${palette('ew', 2)};
      color: ${palette('ew', 1)};
    }

    .isoLabel{
      padding-left:10px;
    }
  }
`;

export default TableDemoStyle;

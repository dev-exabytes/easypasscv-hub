import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const RedirectRoute = ({ component: Component, isLoggedIn, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        return isLoggedIn ? (
          <Redirect to={props.location.state || { pathname: "/dashboard" }} />
        ) : (
            <Redirect to={{
              pathname: '/auth',
              state: props.location.state
            }} />
          )
      }
      }
    />
  )
}

export default RedirectRoute

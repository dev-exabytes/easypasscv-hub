import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout, ConfigProvider, Spin } from "antd";
import { IntlProvider } from "react-intl";
import { Debounce } from "react-throttle";
import WindowResizeListener from "react-window-size-listener";
import { ThemeProvider } from "styled-components";
import authAction from "../../redux/auth/actions";
import appActions from "../../redux/app/actions";
import Sidebar from "../Sidebar/Sidebar";
import Topbar from "../Topbar/Topbar";
//import ThemeSwitcher from "../../containers/ThemeSwitcher";
import AppRouter from "./AppRouter";
import { siteConfig } from "../../config.js";
import { AppLocale } from "../../dashApp";
import themes from "../../config/themes";
import AppHolder from "./commonStyle";
import "./global.css";

const { Content, Footer } = Layout;
const { logout } = authAction;
const { toggleAll } = appActions;

export class App extends Component {
  state = {
    isSignUp: this.props.isSignUp,
    current: 0,
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.isSignUp !== nextProps.isSignUp) {
      this.setState({ isSignUp: nextProps.isSignUp });
    }
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { url } = this.props.match;
    const { locale, selectedTheme, loading, companyLoading, location } = this.props;
    const currentAppLocale = AppLocale[locale];
    const { isSignUp } = this.state;

    return (
      <ConfigProvider locale={currentAppLocale.antd}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}
        >
          <ThemeProvider theme={themes[selectedTheme]}>
            <AppHolder>
              <Layout style={{ height: "100vh" }}>
                <Debounce time="1000" handler="onResize">
                  <WindowResizeListener
                    onResize={windowSize =>
                      this.props.toggleAll(
                        windowSize.windowWidth,
                        windowSize.windowHeight
                      )
                    }
                  />
                </Debounce>
                <Topbar url={url} />
                <Layout style={{ flexDirection: "row", overflowX: "hidden" }}>
                  <Sidebar url={url} />
                  <Layout
                    className="isoContentMainLayout"
                    style={{
                      height: "100vh"
                    }}
                  >
                    <Content
                      className="isomorphicContent"
                      style={{
                        padding: "70px 0 0",
                        flexShrink: "0",
                        background: "#f1f3f6"
                      }}
                    >
                      {loading || companyLoading ?
                        (<div className="spinCenter">
                          <Spin tip="Loading..." />
                        </div>)
                        :
                        <AppRouter url={url} isSignUp={isSignUp} location={location} />}

                    </Content>
                    <Footer
                      style={{
                        background: "#ffffff",
                        textAlign: "center",
                        color: '#ff6600',
                        borderTop: "1px solid #ededed"
                      }}
                    >
                      {siteConfig.footerText}
                    </Footer>
                  </Layout>
                </Layout>
                {/* <ThemeSwitcher /> */}
              </Layout>
            </AppHolder>
          </ThemeProvider>
        </IntlProvider>
      </ConfigProvider>
    );
  }
}

export default connect(
  state => ({
    auth: state.Auth,
    locale: state.LanguageSwitcher.toJS().language.locale,
    selectedTheme: state.ThemeSwitcher.toJS().changeThemes.themeName,
    currentCompany: state.Company.get("current_company"),
    loading: state.Menu.get("loading"),
    companyLoading: state.Company.get("loading"),
    isSignUp: state.Auth.get("is_signup"),
  }),
  { logout, toggleAll }
)(App);

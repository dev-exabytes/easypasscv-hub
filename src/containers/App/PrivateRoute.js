import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { getParseCurrentCompany } from "../../helpers/utility";

const PrivateRoute = ({ component: Component, company, ...rest }) => {
  const currentCompany = getParseCurrentCompany();
  return (
    <Route
      {...rest}
      render={props => {
        return currentCompany ? (
          <Component {...props} />
        ) : (
            <Redirect to={{
              pathname: '/dashboard/company/new',
              state: props.location
            }} />
          )
      }
      }
    />
  )
}

export default PrivateRoute

import React from "react";
import { Switch, Route } from "react-router-dom";
import asyncComponent from "../../helpers/AsyncFunc";
import PrivateRoute from "./PrivateRoute";
import Payment from "../Subscriptions/payment";
import { connect } from "react-redux";
import Home from "../Home/index";
class AppRouter extends React.Component {
  render() {
    const { url } = this.props;
    return (
      <Switch>
        <PrivateRoute exact path={`${url}/`} component={Home} />
        <Route exact path={`${url}/my_plan`} component={asyncComponent(() => import("../Subscriptions/plans2"))} />
        <PrivateRoute exact path={`${url}/my_plan/plan_confirmation`} component={Payment} />
        {/* <Route exact path={`${url}/plan/summary`} component={asyncComponent(() => import("../Subscriptions/summary"))} /> */}
        {/* <Route exact path={`${url}/plan/payment`} component={asyncComponent(() => import("../Subscriptions/paymentinfo.js"))} /> */}
        {/* <Route exact path={`${url}/plan/payment/transaction`} component={asyncComponent(() => import("../Subscriptions/braintree.js"))} /> */}
        <Route exact path={`${url}/my_plan/transaction/success`} component={asyncComponent(() => import("../Subscriptions/thankyou.js"))} />
        <Route exact path={`${url}/employee`} component={asyncComponent(() => import("../Employee/index.js"))} />
        <Route exact path={`${url}/integrations`} component={asyncComponent(() => import("../Integrations/index.js"))} />
        <Route exact path={`${url}/integrations/slack`} component={asyncComponent(() => import("../Integrations/slack.js"))} />
        <Route exact path={`${url}/marketing`} component={asyncComponent(() => import("../Marketing/index"))} />
        <Route exact path={`${url}/reporting`} component={asyncComponent(() => import("../Reporting/index"))} />
        <Route exact path={`${url}/reporting/:batch`} component={asyncComponent(() => import("../Reporting/index"))} />
        <Route exact path={`${url}/join`} component={asyncComponent(() => import("../Invites/intro.js"))} />
        <Route exact path={`${url}/join/:workspace`} component={asyncComponent(() => import("../Invites/intro.js"))} />
        <Route exact path={`${url}/company/new`} component={asyncComponent(() => import("../Company/index.js"))} />
        <Route exact path={`${url}/orgchart`} component={asyncComponent(() => import("../OrgChart/index"))} />
        <Route exact path={`${url}/rewards`} component={asyncComponent(() => import("../EasyReward/index"))} />
        <Route exact path={`${url}/rewards/hashtag`} component={asyncComponent(() => import("../EasyReward/hashtag"))} />
        <Route exact path={`${url}/rewards/:rewardId`} component={asyncComponent(() => import("../EasyReward/detail"))} />
        <Route exact path={`${url}/announcement/detail/:announcementId`} component={asyncComponent(() => import("../AnnouncementDetail/index"))} />
        <Route exact path={`${url}/announcement/detail/:announcementId/edit`} component={asyncComponent(() => import("../Announcement/createAnnouncement.js"))} />
        <Route exact path={`${url}/announcement`} component={asyncComponent(() => import("../Announcement/index"))} />
        <Route exact path={`${url}/announcement/create`} component={asyncComponent(() => import("../Announcement/createAnnouncement.js"))} />
        <Route exact path={`${url}/permit`} component={asyncComponent(() => import("../Permit/index.js"))} />
        <Route exact path={`${url}/permit?type=emergency`} component={asyncComponent(() => import("../Permit/index.js"))} />
        <Route exact path={`${url}/permit?type=household`} component={asyncComponent(() => import("../Permit/index.js"))} />
        <Route exact path={`${url}/permit?type=worker`} component={asyncComponent(() => import("../Permit/index.js"))} />
        <Route exact path={`${url}/permit?type=essentialWorker`} component={asyncComponent(() => import("../Permit/index.js"))} />
        <Route exact path={`${url}/utility`} component={asyncComponent(() => import("../Utility/index.js"))} />
      </Switch>
    );
  }
}

export default AppRouter;

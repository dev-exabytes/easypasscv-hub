import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
import appActions from "../../redux/app/actions";
import TopbarWrapper from "./topbar.style";
import { TopbarUser } from "../../components/topbar";
import { siteConfig } from "../../config.js";
import { Link } from "react-router-dom";

const { Header } = Layout;
const { toggleCollapsed } = appActions;
class Topbar extends Component {
  render() {
    const { toggleCollapsed, customizedTheme, locale } = this.props;
    //const { toggleCollapsed, url, customizedTheme, locale } = this.props;
    const collapsed = this.props.collapsed && !this.props.openDrawer;
    const styling = {
      background: customizedTheme.backgroundColor,
      position: "fixed",
      width: "100%",
      height: 70
    };
    return (
      <TopbarWrapper>
        <Header style={styling} className={collapsed ? "isomorphicTopbar collapsed" : "isomorphicTopbar"}>
          <div className="isoLeft">
            <button
              className={collapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"}
              style={{ color: customizedTheme.iconColor }}
              onClick={toggleCollapsed}
            />
          </div>
          <div className="isoCenter">
            <Link to="/dashboard">
              <div className="logo">
                <img src={siteConfig.siteHeader} width="13%" alt="EasyWork Logo" />
                <div>
                  <span>easy<b>pass</b> Hub <sup>Beta</sup></span>
                </div>
              </div>
            </Link>
          </div>
          <div className="isoRight">
            <TopbarUser locale={locale} className="isoUser" />
          </div>

        </Header>
      </TopbarWrapper>
    );
  }
}

export default connect(
  state => ({
    ...state.App.toJS(),
    locale: state.LanguageSwitcher.toJS().language.locale,
    customizedTheme: state.ThemeSwitcher.toJS().topbarTheme
  }),
  { toggleCollapsed }
)(Topbar);

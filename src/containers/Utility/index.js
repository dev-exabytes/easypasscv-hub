import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { notification } from "antd";
import { connect } from "react-redux";
import TableWrapper from "./easyworkTable.style";
import { Input, Row, Col, Divider, Modal, message, Button, Spin, Form, Cascader, Icon, DatePicker, Radio, Checkbox,List } from "antd";
import { getUtilityBill, getUtilityBillByUser, approveUtilityBill ,rejectUtilityBill } from "./api.js";
import "react-intl-tel-input/dist/main.css";

const Search = Input.Search;
const dateFormat = 'YYYY-MM-DD';
const { TextArea } = Input;

class Utility extends Component {
  state = {
    loading: false,
    result: [],
    resultSub : [],
    value: "",
    editVisible: false,
    user_id: "",
    page: 1,
    pageSub : 1,
    filter: [],
    filterType: '&status=0',
    sortText: "",
    sortValue: "",
    selectedRowKeys: [],
    selectedRows: [],
    approveVisible : false,
    rejectVisible : false,
    approveComment : "",
    rejectComment : "",
  };

  componentDidMount() {
    this.setState({ loading: true, columns: [] })
    const { idToken } = this.props;
    const currentCompany = JSON.parse(localStorage.getItem("current_company"));
    this.getCompanyDetails(idToken, currentCompany);
  }

  componentWillReceiveProps(prevProp, prevState) {
    const { currentCompany, idToken, dateOfHire, title } = this.props;
    if (prevProp.currentCompany !== currentCompany) {
      this.setState({
        loading: true,
      })
      this.getCompanyDetails(idToken, currentCompany);
    }
  }

  getCompanyDetails = (idToken, currentCompany) => {
    this.setState({
      companyId: currentCompany.id,
      companyName: currentCompany.name,
    })

    this.getUtilityBillList();
  }

  getUtilityBillList = ()=> {
    const {idToken,currentCompany} = this.props;
    const {value , page, filterType} = this.state;

    getUtilityBill(idToken, currentCompany.id,value, page, filterType).then(res => {
      console.log(res.data);
      this.setState({
        result: res.data.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    })
  } 

  onSearch = (page) => {
    const { idToken } = this.props;
    const { companyId, value, sortValue, filterType } = this.state;

    this.setState({ loading: true }, () =>
    getUtilityBill(idToken, companyId, value, page, filterType).then(res => {
        this.setState({
          result: res.data.data,
          loading: false,
          total: res.total
        })  
      }).catch(err => {
        console.log(err);
      }))
  };

  // Edit modal
  showEditModal = (record) => {
    const { idToken } = this.props;

    this.setState({
      editVisible: true,
      loading: true,
      value : ""
    })

    getUtilityBillByUser(idToken,record.user_id,'').then(res =>{
      this.setState({
        userBills : res.data,
        loading: false
      });
    }).catch(err => {
      console.log(err);
    });
  }

  handleEditCancel = () => {
    const { idToken } = this.props;
    const { companyId, value} = this.state;

    this.setState({
      editVisible: false,
      selectEmployeeVisible: false,
      loading : true
    });

    this.getUtilityBillList();
  }

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys, selectedRows });
  };

  handleApproveUtilityBill = (item)=>{
    const { idToken } = this.props;

    approveUtilityBill(idToken,item.user_id, item.id).then(res => {
      console.log(res.data);
      }).catch(err => {
      console.log(err);
    });

    this.setState({ loading: true }, () =>
    getUtilityBillByUser(idToken,item.user_id,'').then(res =>{
      this.setState({
        userBills : res.data,
        loading: false
      });
    }).catch(err => {
      console.log(err);
    }));
    
  }

  handleRejectUtilityBill = (item) =>{
    const { idToken } = this.props;
  
    rejectUtilityBill(idToken, item.user_id, item.utility_bill_id).then(res => {
      console.log(res.data);
    }).catch(err => {
      console.log(err);
    });
   
    this.setState({ loading: true }, () =>
    getUtilityBillByUser(idToken,item.user_id,'').then(res =>{
      this.setState({
        userBills : res.data,
        loading: false
      });
    }).catch(err => {
      console.log(err);
    }));
  }

  onChangeFilter  = (value) => {
    let filterType ;
    filterType = value[0] == 3 ? '' : `&status=${value[0]}`;
    this.setState({filterType: filterType, loading : true});
    const {idToken,currentCompany} = this.props;
    const {page} = this.state;
    const query = this.state.value;

    getUtilityBill(idToken, currentCompany.id,query, page,filterType).then(res => {
      this.setState({
        result: res.data.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    })
  }


  render() {
    const { selectedRowKeys, loading, supervisor, subordinate, selectEmployeeVisible, formDetails } = this.state;
    const { currentCompany } = this.props;

    const filter = [{ value:0, label: 'Not Verified'},{ value:1, label: 'Verified'},{ value:3, label: 'All'},];

    const columns = [{
      title: 'Name',
      dataIndex: 'submitted_by_name',
      key: 'submitted_by_name',
      sorter: (a, b) => {
        a = a.submitted_by_name || '';
        b = b.submitted_by_name || '';
        return a.localeCompare(b);
      }
    },{
        title: 'NRIC',
        dataIndex: 'identification_number',
        key: 'identification_number',
        sorter: (a, b) => {
          a = a.identification_number || '';
          b = b.identification_number || '';
          return a.localeCompare(b);
        }
    }, {
    },{
      title: 'Address',
      dataIndex: 'formatted_address',
      key: 'formatted_address',
      sorter: (a, b) => {
        a = a.formatted_address || '';
        b = b.formatted_address || '';
        return a.localeCompare(b);
      }
    }, {
      title: 'Address Status',
      dataIndex: 'status',
      key: 'status',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => {
        a = a.status || '';
        b = b.status || '';
        return a.localeCompare(b);
      }    
    },{
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width : 150,
      render: (text, record) => (
        <span>
          <a onClick={() => this.showEditModal(record)}>View and Approve</a>
        </span >
      ),
    }];

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };

    const hasSelected = selectedRowKeys.length > 0;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
      labelAlign: "left",
    };

  
    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

    if (currentCompany.is_admin === 1) {
      return (
        < LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent">
            <Row type="flex" justify="start">
              <Col md={12} sm={12} xs={24}>
                <Search
                  placeholder="Search with name"
                  onSearch={value => this.setState({ value: value }, () => this.onSearch(1))}
                  style={{
                    marginTop: "10px",
                    marginBottom: "20px",
                    fontSize: "20px"
                  }}
                />
              </Col>
              <Col md={12} sm={12} xs={24}>
              <Cascader options={filter} onChange={this.onChangeFilter}>
                  <Button type="primary" icon="filter"
                      style={{
                        marginTop: "10px",
                        marginBottom: "20px",
                        marginLeft: "25px",
                        marginRight: "25px",
                      }}
                    >Filter Permit</Button>
                  </Cascader>
              </Col>
            </Row>
            <TableWrapper
              dataSource={this.state.result}
              columns={columns}
              loading={this.state.loading}
              className="isoSearchableTable"
              scroll={{ x: 1300 }}
              pagination={{
                showQuickJumper: true,
                pageSize: 20,
                total: this.state.total,
                current: this.state.page,
                showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
                onChange: (page, pageSize) => {
                  this.setState({ loading: true, page:page }, () => this.onSearch(page));
                }
              }}
            />

            <Modal
              visible={this.state.editVisible}
              title="View Utility Bills"
              onOk={this.handleEditCancel}
              onCancel={this.handleEditCancel}
              maskClosable={false}
              footer={[
                <Button key="submit" type="primary" loading={this.state.loading} onClick={this.handleEditCancel}>Save</Button>,
              ]}>
              <List
                  size="small"
                  bordered
                  dataSource={this.state.userBills}
                  renderItem={item => <List.Item
                    actions={[<Button disabled={item.is_disable} loading={this.state.loading} onClick={()=> this.handleApproveUtilityBill(item)}>Approve</Button>, <Button loading={this.state.loading} onClick={() =>this.handleRejectUtilityBill(item)}>Reject</Button>]} >
                  <a href={item.file_path}><u>{item.name}</u></a></List.Item>}
              />
            </Modal>
          </TableDemoStyle>
        </LayoutContentWrapper >
      );
    }
    else {
      return (
        < LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent">
            <p>You do not have access to view Employee page. Please ask permission from your company's admin </p>
          </TableDemoStyle>
        </ LayoutContentWrapper>
      );
    }
  }
}

export default connect(
  state => ({
    idToken: state.Auth.get("idToken"),
    currentCompany: state.Company.get("current_company"),
    companies: state.Auth.get("companies"),
  }),
)(Utility);

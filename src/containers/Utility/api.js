import { baseUrl } from "../../config";


export const getUtilityBill= (idToken,companyId,query,page,filter) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/utilitybills?company_id=${companyId}&query=${query}&page=${page}${filter}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v1+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const approveUtilityBill = (idToken, userId, billId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    var body = {
      is_verified : 1
     };
     console.log(`/v1/users/${userId}/utilitybills/${billId}`);
    return fetch(
      `${baseUrl}/v1/users/${userId}/utilitybills/${billId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v1+json"
        },
        body: JSON.stringify(body)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const rejectUtilityBill = (idToken, userId, billId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    var body = {
     is_verified : 2
    }
    return fetch(
      `${baseUrl}/v1/users/${userId}/utilitybills/${billId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v1+json"
        },
        body: JSON.stringify(body)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getUtilityBillByUser = (idToken, userId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/users/${userId}/utilitybills`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

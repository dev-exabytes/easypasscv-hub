import React, { Component } from 'react';
import Style from "./leave_claim.style";
import ProgressCircle from "../../components/custom/progressCircle";

export default class extends Component {
  render() {
    const { privileges, isEmail, leave, claim, border } = this.props;
    return (
      <Style style={{
        border: border ? `1px solid #e9e9e9` : "none",
        margin: border ? "20px auto 0px auto" : "0px auto",
        padding: border ? "20px" : "0px",
        width: border && window.innerWidth > 800 ? "80%" : "100%",
        borderRadius: border ? "10px" : "0"
      }}
        isEmail={isEmail}
      >
        <ProgressCircle
          title='Claim'
          lockText={privileges && !privileges.claim_type.length ? "You need Claim feature turn on to access to this report" : null}
          enlarge={window.innerWidth >= 1480 && isEmail}
          successPercent={claim ? (claim.rejected / claim.total) * 100 : 0}
          percent={claim ? ((claim.approved + claim.rejected) / claim.total) * 100 : 0}
          progressText='Total Submit'
          total={claim ? claim.total : '-'}
          legendLeft={`Approved: ${claim ? claim.approved : '-'}`}
          legendRight={`Rejected: ${claim ? claim.rejected : '-'}`}
          legendGrey={`Pending: ${claim ? claim.pending : '-'}`}
          isEmail={isEmail}
          trailColor={claim && claim.pending > 0 ? 'orange' : '#f3f3f3'}
        />
        <div className="dividerContainer">
          <div className="divider"></div>
        </div>
        <ProgressCircle
          title='Leave'
          lockText={privileges && !privileges.leave_type.length ? "You need Leave feature turn on to access to this report" : null}
          enlarge={window.innerWidth >= 1480 && isEmail}
          successPercent={leave ? (leave.rejected / leave.total) * 100 : 0}
          percent={leave ? ((leave.approved + leave.rejected) / leave.total) * 100 : 0}
          progressText='Total Submit'
          total={leave ? leave.total : '-'}
          legendLeft={`Approved: ${leave ? leave.approved : '-'}`}
          legendRight={`Rejected: ${leave ? leave.rejected : '-'}`}
          legendGrey={`Pending: ${leave ? leave.pending : '-'}`}
          isEmail={isEmail}
          trailColor={leave && leave.pending > 0 ? 'orange' : '#f3f3f3'}
        />
      </Style>
    );
  }
}

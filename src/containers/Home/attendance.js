import React, { Component } from 'react';
import { Progress } from 'antd';
import Style from "./attendance.style";
import timezone from "moment-timezone";

export default class extends Component {
  render() {
    const { privileges, isEmail, attendance, border } = this.props;

    return (
      <Style
        isEmail={isEmail}
        border={border}
        style={{
          border: border ? `1px solid #e9e9e9` : "none",
          margin: border ? "20px auto 0px auto" : "0px auto",
          padding: border ? "20px" : "0px",
          width: border && window.innerWidth > 800 ? "80%" : "100%",
          borderRadius: border ? "10px" : "0"
        }}
      >
        <div className="attendanceSummaryIntro">
          <span className="title">Attendance For Today</span>
          {privileges && !privileges.attendance ? (
            <span className="warningText">You need Attendance feature turn on and "Manage Company Attendance & Leave" permission to access to this report</span>
          ) : (
              <span className="subtitle">Employee Clock In Status<br /><span className="warningText">Last updated at {timezone().tz(timezone.tz.guess()).format("MMMM Do YYYY, h:mm:ss a")}</span></span>
            )}
        </div>
        <div className="summary">
          <Progress
            className="progressContainer"
            width={window.innerWidth >= 1480 && isEmail ? 250 : 150}
            strokeWidth={window.innerWidth >= 1480 && isEmail ? 12 : 8}
            type="circle"
            percent={attendance ? (attendance.on_time / attendance.total_employee) * 100 : 0}
            strokeColor={"ff6600"}
            format={percent => (<span className="pieText"><span className="piePercent"><b>{attendance ? attendance.on_time : '-'}</b></span><br />On Time</span>)}
          />
          <Progress
            className="progressContainer"
            width={window.innerWidth >= 1480 && isEmail ? 250 : 150}
            strokeWidth={window.innerWidth >= 1480 && isEmail ? 12 : 8}
            type="circle"
            percent={attendance ? (attendance.late / attendance.total_employee) * 100 : 0}
            strokeColor={"orange"}
            format={percent => (<span className="pieText"><span className="piePercent"><b>{attendance ? attendance.late : '-'}</b></span><br />Late</span>)}
          />
          <Progress
            className="progressContainer"
            width={window.innerWidth >= 1480 && isEmail ? 250 : 150}
            strokeWidth={window.innerWidth >= 1480 && isEmail ? 12 : 8}
            type="circle"
            percent={attendance ? (attendance.absent / attendance.total_employee) * 100 : 0}
            strokeColor={"red"}
            format={percent => (<span className="pieText"><span className="piePercent"><b>{attendance ? attendance.absent : '-'}</b></span><br />Not Clock In</span>)}
          />
        </div>
      </Style>
    );
  }
}

import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content:space-between;
  background-color:#fff;
  flex-shrink: 0;
  @media only screen and (max-width: 767px) {
    flex-direction: column;
  }

.ant-divider-vertical {
  position: relative;
  top: -0.06em;
  display: inline-block;
  width: 2px;
  height: 200px;
  margin: 0 20px;
  vertical-align: middle;
  @media only screen and (min-width: 1480px) {
    height: ${props => props.isEmail ? "300px" : "200px"};
  }
}
.container{
  display: flex;
  flex-direction: column;
  flex: 1;
  flex-grow: 1;
  @media only screen and (max-width: 767px) {
    width: 100%;
  }
}
.dividerContainer{
  margin: 0 20px;
  @media only screen and (max-width: 767px) {
    margin: 20px 0;
  }
}
.divider{
  border-left: 2px solid ${palette('border', 0)};
  border-top: 0px solid ${palette('border', 0)};
  width:2px;
  height:100%;
  @media only screen and (max-width: 767px) {
    border-left: 0px solid ${palette('border', 0)};
    border-top: 2px solid ${palette('border', 0)};
    height:2px;
    width:100%;
  }
}
`;

export default Style;

import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import Style from "./index.style";
import { connect } from "react-redux";
import { Divider, List, Avatar, Select } from 'antd';
import actions from "../../redux/home/actions";
import announcementActions from "../../redux/announcement/actions";
import "antd/dist/antd.less";
import moment from "moment";
import Attendance from "./attendance";
import LeaveClaim from "./leave_claim";
import ClaimLeave from "./claim_leave";
import { awsS3 } from "../../config";
import { Redirect, Link } from "react-router-dom";

const { getReport } = actions;
const { getAnnouncement } = announcementActions;
const car = `${awsS3}icons/easywork-car.png`;
const { Option } = Select;
class Home extends Component {
  state = {
    start: moment().subtract(7, 'days').format('YYYY-MM-DD'),
    end: moment().format('YYYY-MM-DD'),
    isEmail: false,
    rewardSelection: this.props.reward ? this.props.reward.leaderboard[1].length > 0 ? 1 : 2 : undefined
  };
  componentDidMount() {
    const { getReport, getAnnouncement, currentCompany, idToken } = this.props;
    const queryString = require('query-string');
    const parsed = queryString.parse(this.props.location.search);
    if (Object.keys(parsed).length !== 0) {
      const { company, token, start, end } = parsed;
      if (company && token && start && end) {
        this.setState({
          start,
          end,
          isEmail: true
        }, () => getReport(
          start,
          end + ' 23:59:59',
          company,
          token
        ));
      }
    } else if (currentCompany && idToken) {
      getReport(
        this.state.start,
        this.state.end + ' 23:59:59',
        currentCompany.id,
        idToken
      );
      this.setState(() => getAnnouncement(
        currentCompany.id,
        1,
        idToken
      )
      );
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.reward !== nextProps.reward) {
      this.setState({
        rewardSelection: nextProps.reward ? nextProps.reward.leaderboard[1].length > 0 ? 1 : 2 : undefined
      });
    }
  }

  onChange = value => {
    this.setState({ rewardSelection: value });
  }

  render() {
    const { attendance, leave, claim, mileageClaim, announcement, announcementList, reward, upcomingEvent, user, privileges, usage, currentCompany, upcomingBday } = this.props;
    const { start, end, isEmail, rewardSelection } = this.state;
    return (
      <LayoutContentWrapper>
        <Style className="isoLayoutContent">
          <div className="attendanceSummaryContainer">
            <div className="greeting">
              {
                !isEmail && currentCompany && currentCompany.logo_path ?
                  (
                    <Avatar src={currentCompany.logo_path} className="avatar" />
                  )
                  : null
              }
              <div className="greetingContainer" style={{ paddingLeft: isEmail ? "0px" : "20px" }}>
                <span className="greetingName">Good Day{!isEmail && user ? `, ${user.name}` : ""}</span>
                <span className="dateRangeDesc">Here are your stats for {moment(start).format("MMM D")} - {moment(end).format("MMM D")}</span>
              </div>
            </div>
            <Divider type={"horizontal"} />
            {usage && usage[0].name === "leave" ? (
              <LeaveClaim privileges={privileges} isEmail={isEmail} leave={leave} claim={claim} border={false} />
            ) : usage && usage[0].name === "claim" ? (
              <ClaimLeave privileges={privileges} isEmail={isEmail} leave={leave} claim={claim} border={false} />
            ) : (
                  <Attendance privileges={privileges} isEmail={isEmail} attendance={attendance} border={false} />
                )}
          </div>
        </Style >
        <Style className="isoLayoutContent" isEmail={isEmail} >
          {usage && usage[0].name !== "attendance" ? (
            <Attendance privileges={privileges} isEmail={isEmail} attendance={attendance} border={true} />
          ) : usage && usage[2].name === "leave" ? (
            <ClaimLeave privileges={privileges} isEmail={isEmail} leave={leave} claim={claim} border={true} />
          ) : (
                <LeaveClaim privileges={privileges} isEmail={isEmail} leave={leave} claim={claim} border={true} />
              )}
        </Style>
        <Style className="isoLayoutContent" isEmail={isEmail}>
          <div className="moduleSummaryContent">
            <div className="moduleSummaryContainer">
              <div className="attendanceSummaryIntro">
                <span className="title">Birthday</span>
                <span className="warningText">In these 3 days</span>
              </div>
              <div className="summaryVertical">

                <List
                  style={{ width: "100%", marginTop: "10px" }}
                  itemLayout="horizontal"
                  dataSource={upcomingBday}
                  renderItem={item =>
                    (
                      <List.Item
                        key={item.id}
                      >
                        <List.Item.Meta
                          title={(<div className="rewardList">
                            <div className="dateContainer">
                              <Avatar
                                icon="user"
                                size={30}
                                src={item.avatar}
                              />
                              <span className="subtitle" style={{ paddingLeft: "10px" }}>{item.name}</span>
                            </div>
                            <span className="pieText">{item.dob}</span>
                          </div>)}
                        />
                      </List.Item>
                    )}
                />

              </div>
            </div>
            <div className="dividerContainer">
              <div className="divider"></div>
            </div>
            <div className="moduleSummaryContainer">
              <div className="attendanceSummaryIntro">
                <span className="title">Reward</span>
                {privileges && !privileges.reward ? (
                  <span className="warningText">You need Reward feature turn on and "Manage Company Reward" permission to access to this report</span>
                ) : null}
              </div>
              {reward && reward.usage ? (
                <div style={{ padding: "10px 0px" }}>
                  <span className="subtitle" ><b>{reward.usage || "-"}</b> {!isNaN(reward.usage) && reward.usage > 1 ? "employees" : "employee"} using reward system</span>
                </div>
              ) : null}

              {reward ? (
                <div>
                  <Select
                    style={{ width: "100%", marginTop: "10px" }}
                    placeholder="Select a leaderboard"
                    value={rewardSelection}
                    onChange={this.onChange}
                  >
                    {reward.leaderboard.map((i, index) => (
                      <Option value={index} key={index}>{i.title}</Option>
                    ))}
                  </Select>
                  <List
                    itemLayout="horizontal"
                    dataSource={typeof (rewardSelection) != "undefined" ? reward.leaderboard[rewardSelection].list : []}
                    renderItem={item =>
                      (
                        <List.Item
                          key={item.id}
                        >
                          <List.Item.Meta
                            title={(<div className="rewardList">
                              <div className="dateContainer">
                                <Avatar
                                  icon="user"
                                  size={30}
                                  src={item.avatar}
                                />
                                <span className="subtitle" style={{ paddingLeft: "10px" }}>{item.name}</span>
                              </div>
                              <span className="pieText">{item.point}</span>
                            </div>)}
                          />
                        </List.Item>
                      )}
                  />
                </div>
              ) : null}
            </div>
          </div>
        </Style>
        <Style className="isoLayoutContent" isEmail={isEmail}>
          <div className="moduleSummaryContent">
            <div className="moduleSummaryContainer">
              <div className="attendanceSummaryIntro">
                <span className="title">Upcoming Event</span>
                {privileges && !privileges.calendar ? (
                  <span className="warningText">You need Calendar feature turn on to view events</span>
                ) : null}
              </div>
              <div className="rightLarge" style={{ paddingLeft: "15px" }}>
                <List
                  itemLayout="horizontal"
                  dataSource={upcomingEvent}
                  renderItem={item => (
                    <List.Item
                      key={item.id}
                    >
                      <List.Item.Meta
                        title={(
                          <div className="listCalendarItem">
                            <div className="dateContainer">
                              <div className="dateView">
                                <span className="subtitle">{moment(item.start).format('MMM')}</span>
                                <span className="dateText">{moment(item.start).format('DD')}</span>
                                <span >{moment(item.start).format('ddd')}</span>
                              </div>
                              <div className={item.event_type_id === 3 ? "greenDivider" : "redDivider"}></div>
                            </div>
                            <div className="eventTitle"><span className="pieText">{item.summary}</span></div>
                          </div>)}
                      />
                    </List.Item>
                  )}
                />
              </div>
            </div>
            <div className="dividerContainer">
              <div className="divider"></div>
            </div>
            <div className="moduleSummaryContainer">
              <div className="attendanceSummaryIntro">
                <span className="title">{isEmail ? "Announcement" : <Link to={{ pathname: "/dashboard/announcement" }}>Announcement</Link>}</span>
                {privileges && !privileges.announcement ? (
                  <span className="warningText">You need Announcement feature turn on to access to this report</span>
                ) : null}
              </div>
              <div className="summaryVertical">
                <List
                  itemLayout="horizontal"
                  dataSource={isEmail && announcement ? announcement.list : announcementList ? announcementList.data.length >= 5 ? announcementList.data.slice(0, 5) : announcementList.data : []}
                  renderItem={(item, index) => (
                    <List.Item
                      key={item.index}
                      style={{ border: "none" }}
                    >
                      <List.Item.Meta
                        title={(<div className="listItem">
                          <div className="greenSquare"></div>
                          {isEmail ? <span className="announcementItem">{item.title}</span> :
                            <Link to={{ pathname: `/dashboard/announcement/detail/${item.id}` }}><span className="announcementItem">{item.title}</span></Link>}
                        </div>)}
                      />
                    </List.Item>
                  )}
                  footer={isEmail ? <span className="warningText">{announcement && announcement.more ? `and ${announcement.more} more...` : ''}</span> :
                    <Link to={{ pathname: "/dashboard/announcement" }}><span className="warningText">{announcementList && announcementList.total > 5 ? `and ${announcementList.total - 5} more...` : ''}</span></Link>}
                />
              </div>
            </div>
          </div>
        </Style>
        <Style className="isoLayoutContent" isEmail={isEmail}>
          <div className="singleContent">
            <div className="singleContainer">
              <div className="attendanceSummaryIntro">
                <span className="title">Mileage Claim</span>
                {privileges && !privileges.mileage ? (
                  <span className="warningText">You need Mileage feature turn on to access to this report</span>
                ) : null}
              </div>
              <div className="summaryVertical">
                <img src={car} width="150px" alt="Car" />
                <List
                  itemLayout="horizontal"
                  dataSource={mileageClaim ? mileageClaim.list : []}
                  renderItem={item => (
                    <List.Item
                      key={item.title}
                      style={{ border: "none" }}
                    >
                      <List.Item.Meta
                        title={(<div className="listItem">
                          <div className="greenSquare"></div>
                          <div className="mileageView">
                            <span className="pieText">{moment(item.title).format("DD/MM/YYYY (H:mm A)")}</span>
                            <span className="distanceText">{item.description} km</span>
                          </div>
                        </div>)}
                      />
                    </List.Item>
                  )}
                  footer={<span className="warningText">{mileageClaim && mileageClaim.total ? `and ${mileageClaim.total} more...` : ''}</span>}
                />
              </div>
            </div>
          </div>
        </Style>
      </LayoutContentWrapper >
    )
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      loading: state.Home.get("loading"),
      attendance: state.Home.get("attendance"),
      leave: state.Home.get("leave"),
      claim: state.Home.get("claim"),
      mileageClaim: state.Home.get("mileage_claim"),
      announcement: state.Home.get("announcement"), // Added by Shalon for daily report
      announcementList: state.Announcement.get("announcement_list"),
      reward: state.Home.get("reward"),
      upcomingEvent: state.Home.get("upcoming_event"),
      user: state.Home.get("user"),
      privileges: state.Home.get("privileges"),
      usage: state.Home.get("usage"),
      upcomingBday: state.Home.get("upcoming_bday")
    })
  },
  { getReport, getAnnouncement }
)(Home);

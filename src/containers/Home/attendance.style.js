import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  background-color:#fff;

  .attendanceSummaryIntro{
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
  }
  .title{
    text-align: left;
    font-size: 18px;
    font-weight: 700;
    @media only screen and (max-width: 767px) {
      font-size: 16px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "18px"};
    }
  }
  .warningText{
    color:${palette('ew', 4)};
    @media only screen and (max-width: 767px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "14px"};
    }
  }
  .subtitle{
    text-align: left;
    font-size: 16px;
    @media only screen and (max-width: 767px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "18px"};
    }
  }
  .summary{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    flex-wrap:wrap;
    @media only screen and (max-width: 767px) {
      flex-direction: column;
    }
  }
  .progressContainer{
    padding: 30px 10px;
    @media only screen and (max-width: 767px) {
      padding: 10px;
    }
  }
  .pieText{
    font-size: 16px;
    color:#000;
    @media only screen and (max-width: 767px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "16px"};
    }
  }
  .piePercent{
    font-size: 36px;
    color:#000;
    @media only screen and (max-width: 767px) {
      font-size: 24px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "48px" : "36px"};
    }
  }
`;

export default Style;

import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  background-color: #f1f3f6 !important;
  border: none !important;
  padding:0 !important;
  margin: auto !important;
  width:100%;

  .ant-divider {
    background: ${palette('border', 0)};
  }
  .ant-divider-horizontal {
    height: 2px;
  }
  .attendanceSummaryContainer{
    width: 80%;
    padding: 20px;
    background-color: #ffffff;
    border: 1px solid ${palette('border', 0)};
    height: 100%;
    margin: 0 auto !important;
    border-radius:10px;

    @media only screen and (max-width: 800px) {
      width:100%;
    }
  }
  .moduleSummaryContent{
    display: flex;
    width: 80%;
    padding: 20px;
    flex-wrap: wrap;
    justify-content:space-between;
    border: 1px solid ${palette('border', 0)};
    background-color:#fff;
    margin: 20px auto 0px auto !important;
    border-radius:10px;

    @media only screen and (max-width: 767px) {
      flex-direction: column;
    }

    @media only screen and (max-width: 800px) {
      width:100%;
    }
  }
  .singleContent{
    width: 80%;
    display: flex;
    flex-wrap: wrap;
    justify-content:flex-start;
    margin: 20px auto 0px auto !important;
    @media only screen and (max-width: 767px) {
      flex-direction: column;
    }

    @media only screen and (max-width: 800px) {
      width:100%;
    }
  }
  .moduleSummaryContainer{
    padding: 10px
    display: flex;
    flex-direction: column;
    flex: 1;
    flex-grow: 1;
    @media only screen and (max-width: 800px) {
      width: 100%;
    }
  }
  .singleContainer{
    width:50%;
    padding: 20px;
    display: flex;
    flex-direction: column;
    border: 1px solid ${palette('border', 0)};
    background-color:#fff;
    border-radius:10px;
    @media only screen and (max-width: 800px) {
      width: 100%;
    }
  }
  .attendanceSummaryIntro{
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
  }
  .summary{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    flex-wrap:wrap;
    @media only screen and (max-width: 800px) {
      flex-direction: column;
    }
  }
  .summaryVertical{
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap:wrap;
  }
  .greenSquare{
    width:15px;
    height:15px;
    background-color: #ff6600;
    margin-right:10px;
    border-radius:2px;
  }
  .footer{
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content:center;
    align-items:center;
    margin:50px 0;
  }
  .rightLarge{
    display:flex;
    flex-wrap:wrap;
  }
  .listItem{
    display: flex;
    flex-direction: row;
    align-items:center;
  }
  .dateView{
    display:flex;
    flex-direction:column;
    padding-right:20px;
    align-items:center;
    justify-content:space-around;
  }
  .dividerContainer{
    margin: 0 20px;
  @media only screen and (max-width: 767px) {
    margin: 20px 0;
  }
  }
  .divider{
    border-left: 2px solid ${palette('border', 0)};
    border-top: 0px solid ${palette('border', 0)};
    width:2px;
    height:100%;
    @media only screen and (max-width: 767px) {
      border-left: 0px solid ${palette('border', 0)};
      border-top: 2px solid ${palette('border', 0)};
      height:2px;
      width:100%;
    }
  }
  .greenDivider{
    border-left: 2px solid #ff6600;
    width:2px;
    height:100%;
    @media only screen and (min-width: 1480px) {
      border-left: ${props => props.isEmail ? "4px solid #ff6600" : "2px solid #ff6600"};
      width:${props => props.isEmail ? "4px" : "2px"};
    }
  }
  .redDivider{
    border-left: 2px solid red;
    width:2px;
    height:100%;
    @media only screen and (min-width: 1480px) {
      border-left: ${props => props.isEmail ? "4px solid red" : "2px solid red"};
      width:${props => props.isEmail ? "4px" : "2px"};
    }
  }
  .listCalendarItem{
    display:flex;
    flex-direction: row;
    flex-wrap: wrap;
  }
  .eventTitle{
    display:flex;
    align-items:center;
    flex: 5;
    flex-grow: 1;
    padding-left:10px;
  }
  .dateContainer{
    display:flex;
    flex-direction: row;
    align-items:center;
  }
  .rewardList{
    display:flex;
    align-items:center;
    justify-content:space-between;
    padding: 0px 10px;
  }
  .greetingContainer{
    display: flex;
    flex-direction: column;
    justify-content:flex-end;
    align-items:flex-start;
    @media only screen and (max-width: 800px) {
      padding-left:0px;
    }
  }
  .greeting{
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: flex-start;
  }
  .avatar{
    width: 100px;
    height: 100px;
    border: 3px solid #ff6600;
    border-radius:  50px;
    overflow:hidden;
    @media only screen and (max-width: 800px) {
      display: none;
    }
    @media only screen and (min-width: 1480px) {
      width: ${props => props.isEmail ? "200px" : "100px"};
      height: ${props => props.isEmail ? "200px" : "100px"};
      border: ${props => props.isEmail ? "8px solid #ff6600" : "3px solid #ff6600"};
      border-radius:  ${props => props.isEmail ? "100px" : "50px"};
    }
  }
  .greetingName{
    font-size: 28px;
    @media only screen and (max-width: 800px) {
      font-size: 18px;
      font-weight:900;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "28px"};
    }
  }
  .dateRangeDesc{
    font-size: 20px;
    color: ${palette('text', 3)};
    @media only screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "28px" : "20px"};
    }
  }
  .attendance{
    padding-left: 20px;
    padding-top: 0px;
    @media only screen and (max-width: 800px) {
      padding-left: 10px;
    }
  }
  .warningText{
    color:${palette('ew', 4)};
    @media only screen and (max-width: 800px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "14px"};
    }
  }
  .title{
    text-align: left;
    font-size: 18px;
    font-weight: 700;
    @media only screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "18px"};
    }
  }
  .subtitle{
    text-align: left;
    font-size: 16px;
    @media only screen and (max-width: 800px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "18px" : "16px"};
    }
  }

   a:hover{
    text-decoration: underline;
  }

  .textCenter{
    padding-top: 30px;
    text-align: center;
    font-size: 18px;
    font-weight: 700;
    @media only screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "18px"};
    }
  }
  .pieText{
    font-size: 16px;
    color:#000;
    @media only screen and (max-width: 800px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "16px"};
    }
  }
  .piePercent{
    font-size: 36px;
    color:#000;
    @media only screen and (max-width: 800px) {
      font-size: 24px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "48px" : "36px"};
    }
  }
  .ant-tabs-card > .ant-tabs-content {
    height: 180px;
    margin-top: -16px;
  }
  .ant-tabs-card > .ant-tabs-bar {
    border-color: #fff;
  }

  .ant-tabs-card > .ant-tabs-bar .ant-tabs-tab {
    padding: 0 10px;
  }

  .ant-tabs-card > .ant-tabs-bar .ant-tabs-tab-active {
    color: ${palette('ew', 0)};
  }
  .mileageView{
    display:flex;
    flex-direction:column;
    justify-content:space-around;
  }

  .dateText{
    color: #000;
    padding-top: 2px;
    padding-bottom: 2px;

    text-align: left;
    font-size: 18px;
    font-weight: 700;
    @media only screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "18px"};
      padding-top: ${props => props.isEmail ? "10px" : "2px"};
      padding-bottom: ${props => props.isEmail ? "10px" : "2px"};
    }
  }

  .distanceText{
    color: grey;
    paddingTop: 10px;
    font-size: 16px;
    @media only screen and (max-width: 800px) {
      font-size: ${props => props.isEmail ? "14px" : "16px"};
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "16px"};
    }
  }
  .footerText{
    text-align: center;
    font-size: 14px;
  }
`;

export default Style;

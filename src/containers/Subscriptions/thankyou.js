import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { Provider, connect } from "react-redux";
import { Typography,Button, Spin, Icon, Modal, Checkbox, Row, Input, notification, Col } from "antd";
import { getPlan, getPlanByCompanyDetails, getOptionsInResponse ,postFormResponse} from "./api.js";
import actions from "../../redux/Subscriptions/actions";
import Image from "../../image/done.png";

class Thankyou extends Component {
    state = {
        planName: ' ',
        planId: ' ',
        loading : true,
        hasScheduleChange: null,
        nextBiling: null,
        quantity: 0,
        nextPlanName: ' ',
        nextPlan: ' ',
        modalVisible: false,
        options: [],
        notes : "",
        checked: [],
    };

    componentDidMount() {
        const { idToken, currentCompany} = this.props;

        getOptionsInResponse(idToken).then(res => {
            this.setState({ 
                options : res.data,
                modalVisible : true
            })
        }).catch(err => {
            console.log(err);
        })

        getPlanByCompanyDetails(idToken, currentCompany.id).then(res => {
            this.setState({
                planId : res.data.plan_id,
                planName: res.data.plan_name,
                hasScheduleChange : res.data.has_schedule_changes,
                nextBiling : res.data.next_payment,
                nextPlan: res.data.next_plan_name,
                quantity : res.data.max_employee,
                loading : false
            })
        }).catch(err => {
            console.log(err);
        })

        getOptionsInResponse(idToken).then(res => {
            this.setState({ options : res.data })
        }).catch(err => {
            console.log(err);
        })
    }

    handleOk = () => {
        this.props.history.push(`/dashboard/my_plan`);
    }

    handleCancel =()=>{
        this.setState({ modalVisible :false})
    }

    handleSubmit = () =>{
        var checked = this.state.checked;
        var respond = [];

        if (checked.length === 0){
            notification.error({
                message: "Please select at least one",
            });
        }

        if (checked.length !== 0){
            this.setState({ modalVisible :false})
            var note = "";   
            for (var i = 0; i < checked.length; i++) {
                if (checked[i] === 10 ){
                    note = this.state.notes;
                }
                var data = {
                    respond_type_id : checked[i],
                    notes : note
                }
                respond.push(data);
            }

            postFormResponse(this.props.idToken, respond).then(res => {
                notification.open({
                    message: "Submitted",
                });
            }).catch(err => {
                console.log(err);
            })
        }
    }

    onChangeCheckbox = (checkedValues) =>{
        this.setState({ checked : checkedValues});
    }

    render() {
        const { planId, planName, loading, hasScheduleChange, nextBiling, quantity, nextPlan, modalVisible, options} = this.state;
        const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

        // if (loading){
        //     return (
        //       <LayoutContentWrapper>
        //         <TableDemoStyle className="isoLayoutContent">
        //         <p>Fetching your latest subscription..</p>
        //           <Spin indicator={antIcon} spinning={loading}/>
        //         </TableDemoStyle>
        //       </LayoutContentWrapper>
        //     );
        // }
        // else{
            // if (planId === 1 && hasScheduleChange === false){
                return (
                    <LayoutContentWrapper >
                        <TableDemoStyle className="isoLayoutContent" >
                        { loading?(
                            <div>
                             <p>Fetching your latest subscription..</p>
                             <Spin indicator={antIcon} spinning={loading}/>
                            </div>
                        ):
                        <div style={{ textAlign: "center" }}> 
                        { planId === 1 && hasScheduleChange === false ? (
                          <div style={{ textAlign: "center" }}>
                          <h1>We hope you comeback..</h1>
                            <br></br>
                            <label>Your subscription has been cancelled. We have changed your plan to EasyWork {planName}</label>
                            <br></br>
                        </div>
                        ): null} 
                        { hasScheduleChange === false && planId !== 1 ? (
                           <div style={{ textAlign: "center" }}>
                           <h1>Thank You !</h1>
                               <br></br>
                               <label>We received your payment and updated your plan to EasyWork {planName}</label>
                               <br></br>
                               <br></br>
                               <img shape="square" align="middle" width="400" height="400" src={Image}/>
                           </div>
                        ): null} 
                        { hasScheduleChange === true ? (
                            <div style={{ textAlign: "center" }}>
                            <h1>We received your request</h1>
                                <br></br>
                                <label>Your plan will be downgraded to {nextPlan} at the end of current billing period, {nextBiling}. </label>
                                <br></br>
                                <br></br>
                                <p>From the bottom of our hearts, thank you !</p>
                            </div>
                        ): null} 
                        <br></br>
                        <Button style={{textAlign:"center"}} type="primary" onClick={this.handleOk}>My Plan</Button>
                        </div>}
                        
                            <Modal 
                            title="Tell us how you heard about EasyWork"
                            visible={this.state.modalVisible} 
                            onCancel={this.handleCancel}
                            onOk={this.handleSubmit}>
                            <Checkbox.Group style={{ width: '100%' }} onChange={this.onChangeCheckbox}>
                            {options.map((e, key) => {
                                return<Row>
                                    <Col span={16}>
                                    <Checkbox value={e.id}>{e.respond}</Checkbox> 
                                    {e.is_require_notes === 1?(
                                        <Input style={{ border: 'none', background: 'transparent',  borderBottom: '1px solid '}} onChange={(e) => { this.setState({ notes: e.target.value }) }} />
                                    ):null}
                                    </Col>
                                    </Row>;
                            })}
                            </Checkbox.Group>
                            </Modal>

                        </TableDemoStyle>
                    </LayoutContentWrapper >
                )
       // }
            // else if (hasScheduleChange === false){
            //     return (
            //         <LayoutContentWrapper >
            //             <TableDemoStyle className="isoLayoutContent" >
            //                 <div style={{ textAlign: "center" }}>
            //                 <h1>Thank You !</h1>
            //                     <br></br>
            //                     <label>We received your payment and updated your plan to EasyWork {planName}</label>
            //                     <br></br>
            //                     <br></br>
            //                     <img shape="square" align="middle" width="400" height="400" src={Image}/>
            //                     <br></br>
            //                     <Button style={{textAlign:"center"}} type="primary" onClick={this.handleOk}>My Plan</Button>
            //                 </div>

            //                 <Modal 
            //                 title="Hey there ! Tell us how you heard about us"
            //                 visible={this.state.modalVisible} 
            //                 onCancel={this.handleCancel}
            //                 onOk={this.handleLogin}>
            //                 {options.map((e, key) => {
            //                     return <Checkbox>{e.respond}</Checkbox>;
            //                 })}
            //                 </Modal>

            //             </TableDemoStyle>
            //         </LayoutContentWrapper >
            //     )
            // }
            // else if (hasScheduleChange === true){
            //     return (
            //     <LayoutContentWrapper >
            //             <TableDemoStyle className="isoLayoutContent" >
            //                 <div style={{ textAlign: "center" }}>
            //                 <h1>We received your request</h1>
            //                     <br></br>
            //                     <label>Your plan will be downgraded to {nextPlan} at the end of current billing period, {nextBiling}. </label>
            //                     <br></br>
            //                     <br></br>
            //                     <p>From the bottom of our hearts, thank you !</p>
            //                     <br></br>
            //                     <Button style={{textAlign:"center"}} type="primary" onClick={this.handleOk}>My Plan</Button>
            //                 </div>

            //                 <Modal 
            //                 title="Hey there ! Tell us how you heard about EasyWork"
            //                 visible={this.state.modalVisible} 
            //                 onCancel={this.handleCancel}
            //                 onOk={this.handleLogin}>
            //                 {options.map((e, key) => {
            //                     return <Checkbox>{e.respond}</Checkbox>;
            //                 })}
            //                 </Modal>
            //             </TableDemoStyle>
            //         </LayoutContentWrapper >
                
            
        }
    
}

export default connect(
    state => {
        return ({
            plan: state.Subscriptions.get("planId"),
            idToken: state.Auth.get("idToken"),
            currentCompany: state.Company.get("current_company"),
            companyId: state.Subscriptions.get("companyId"),
            totalUser: state.Subscriptions.get("totalUser"),
        })
    },
)(Thankyou);
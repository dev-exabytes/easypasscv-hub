import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Form, Input, Row, Col, Button, Modal, label, Option, Spin, Select, Card, Icon, notification, Avatar, Checkbox } from "antd";
import { getClientToken, getCreatorDetails } from "./api.js";
import actions from "../../redux/Subscriptions/actions";
import { getSubscriptionDetails } from "../../redux/Subscriptions/api";
import Braintree from "./braintree";
import DropIn from "braintree-web-drop-in-react";
import Image from "../../image/secure.png";
import "./subscription.style";
import IntlTelInput from "react-intl-tel-input";
import "react-intl-tel-input/dist/main.css";
import { getNames, getCode, getName } from "country-list";
import { stat } from "fs";

const { getToken, storebillingDetails, storeNonce, estimate, clearSubs } = actions;

class BillingInfo extends Component {

  state = {
    loading: false,
    companyName: [],
    planName: [],
    quantity: [],
    planName: [],
    billing: [],
    totalPayment: [],
    sst: [],
    grandTotal: [],
    first_name: null,
    last_name: null,
    company_name: null,
    company_id: null,
    postcode: null,
    country: " ",
    address: null,
    state: null,
    invoice_email: null,
    creator: [],
    countries: getNames(),
    countryCode: " ",
    phone: ' ',
    dialCode: ' ',
    country: ' ',
    promo: false,
    promoCode: '',
    billingAddress: null,
    value: ' '
  };

  componentDidMount() {
    const { currentCompany, billingAddress } = this.props;

    if (billingAddress === null) {
      this.getCreator();
    }
    else {
      this.getFilledForm(billingAddress);
    }

    this.setState({
      company_name: currentCompany.name,
      company_id: currentCompany.id
    })
  }

  componentDidUpdate(prevProp, prevState) {
    const { currentCompany, payment } = this.props;
    if (prevProp.currentCompany !== currentCompany) {
      this.props.clearSubs;
      this.getCompanyDetails(currentCompany);
    }
    if (prevProp.payment !== payment) {
      this.setState({ couponLoading: false })
    }
  }

  async buy() {
    const number = this.state.phone;
    const dialCode = this.state.dialCode;
    const phoneNumber = dialCode.concat(number);
    let amount = 0;
    const {isPaidPlan} = this.props;

    if (this.props.discount == 0) {
      amount = this.props.amount;
    }
    else {
      amount = this.props.payment;
    }

    if (isPaidPlan){
      const { nonce, details } = await this.instance.requestPaymentMethod({
      threeDSecure: {
        amount: amount,
        email: this.state.invoice_email,
        billingAddress: {
          givenName: this.state.first_name,
          surname: this.state.first_name,
          postalCode: this.state.postcode,
          countryCodeAlpha2: this.state.countryCode,
          phoneNumber: phoneNumber
        }
      }
      });

      const card = { nonce, details };
      this.props.storeNonce(card);
    }
    // for no paid plan
    else{
      const details ={
        cardType : 0,
        lastFour : 0,
      }
      const card = {
        nonce : 0,
        details
      };
      this.props.storeNonce(card);
    }

    var billingAddress = {
      line1: this.state.address,
      zip: this.state.postcode,
      country: this.state.countryCode,
      state: this.state.state,
      email: this.state.invoice_email,
      firstName: this.state.first_name,
      lastName: this.state.last_name
    }

    this.setState({ billingAddress: billingAddress });
    this.props.storebillingDetails(billingAddress);
    this.props.onHandleSummary();
  }

  getCreator = () => {
    const { currentCompany } = this.props;
    getCreatorDetails(this.props.idToken, currentCompany.created_by).then(res => {
      this.setState({
        first_name: res.data.first_name,
        last_name: res.data.last_name,
        creator: res.data,
        invoice_email: res.data.email,
        postcode: currentCompany.postcode,
        country: currentCompany.country,
        address: currentCompany.address,
        state: currentCompany.state,
        phone: res.data.phone
      })
      if (currentCompany.country) {
        this.setState({
          countryCode: getCode(currentCompany.country),
          country: currentCompany.country
        })
      }

    }).catch(err => {
      notification.open({
        message: err.statusText,
        description: "Cannot get admin details"
      });
    })
  }

  getFilledForm = (billingAddress) => {
    this.setState({
      first_name: billingAddress.firstName,
      last_name: billingAddress.lastName,
      invoice_email: billingAddress.email,
      address: billingAddress.line1,
      postcode: billingAddress.zip,
      state: billingAddress.state,
      country: billingAddress.country,
      promoCode: this.props.coupon
    })
    if (billingAddress.country) {
      this.setState({
        countryCode: billingAddress.country,
        country: getName(billingAddress.country)
      })
    }
  }

  submit = e => {
    e.preventDefault();
    this.props.form.validateFields((err) => {
      if (!err) {
        this.buy();
      }
    });
  }

  goBack = () => {
    this.props.onHandlePrevious();
  }

  getCountry = (value) => {
    this.setState({
      countryCode: getCode(value),
      country: value,
      value: value,
    })
  }

  _onPhoneNumberChangeHandler = (status, phone, number) => {
    this.setState({
      phone: phone,
      dialCode: number.dialCode
    });
  }

  onSubmitPromoCode = () => {
    const { planId, totalUser, billingId, addonsId } = this.props;
    const { company_id, billingAddress, promoCode } = this.state;

    const data = {
      totalUser,
      company_id,
      billingId,
      planId,
      promoCode,
      billingAddress,
      addonsId
    };

    this.props.estimate(data);
  }

  render() {
    const { loading, payment, plan, totalUser, billing, currentCompany, clientToken, couponLoading, discount, amount, coupon, addonsId, addonsName, addonsPrice, planPrice, isPaidPlan } = this.props;
    const { countries, promo, promoCode, value } = this.state;
    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
    const { Meta } = Card;
    const { getFieldDecorator } = this.props.form;
    const { Option } = Select;

    if (isPaidPlan && this.props.clientToken === " "){
      return (
        <Row>
          <Spin indicator={antIcon} spinning={true} />
          <p>Fetching your company details...</p>
        </Row>
      );
    }

    else {
      return (
        <div style={{ position: "relative" }}>
          <Row gutter={50}>
            <Col span={16}>
              <Card title="Billing Information" >
                <Form onSubmit={this.buy.bind(this)} layout="vertical">
                  <Row gutter={30}>
                    <Col span={12}>
                      <Form.Item label="First Name" >{getFieldDecorator('first_name', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your First Name!',
                          },
                        ],
                        initialValue: this.state.first_name,
                      })(<Input onChange={(e) => { this.setState({ first_name: e.target.value }) }} />)}
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item label="Last Name" >{getFieldDecorator('last_name', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Last Name!',
                          },
                        ],
                        initialValue: this.state.last_name,
                      })(<Input onChange={(e) => { this.setState({ last_name: e.target.value }) }} />)}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={30}>
                    <Col span={12}>
                      <Form.Item label="Company Name">
                        <b>{this.state.company_name}</b>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item label="Admin Email">{getFieldDecorator('email', {
                        rules: [
                          {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                          },
                          {
                            required: true,
                            message: 'Please input your E-mail!',
                          },
                        ],
                        initialValue: this.state.invoice_email,
                      })(<Input onChange={(e) => { this.setState({ invoice_email: e.target.value }) }} />)}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={30}>
                    <Col span={24}>
                      <Form.Item label="Address">{getFieldDecorator('address', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Address!',
                          },
                        ],
                        initialValue: this.state.address,
                      })(<Input onChange={(e) => { this.setState({ address: e.target.value }) }} />)}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={30}>
                    <Col span={12}>
                      <Form.Item label="State">{getFieldDecorator('state', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your State!',
                          },
                        ],
                        initialValue: this.state.state,
                      })(<Input onChange={(e) => { this.setState({ state: e.target.value }) }} />)}
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item label="Postcode" >{getFieldDecorator('postcode', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Postcode!',
                          },
                        ],
                        initialValue: this.state.postcode,
                      })(<Input onChange={(e) => { this.setState({ postcode: e.target.value }) }} />)}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={30}>
                    <Col span={12}>
                      <Form.Item label="Country" >{getFieldDecorator('country', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Country!',
                          },
                        ],
                        initialValue: this.state.country,
                      })(<Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select your country"
                        optionFilterProp="children"
                        // onChange={handleChange}
                        filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}

                        onChange={this.getCountry}
                      >
                        {countries.map((e, key) => {
                          return <Option key={key} value={e}>{e}</Option>;
                        })}
                      </Select>)}
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item label="Phone" >
                        <IntlTelInput
                          css={["intl-tel-input", "ant-input-lg"]}
                          preferredCountries={["my", "id", "sg"]}
                          style={{ width: "100%", zIndex: 4 }}
                          placeholder="Mobile Number"
                          telInputProps={{ style: { border: "1px solid #e9e9e9", width: "100%" } }}
                          onPhoneNumberChange={this._onPhoneNumberChangeHandler}
                          onPhoneNumberBlur={this._onPhoneNumberBlurHandler}
                          separateDialCode={true}
                          format={false}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </Card>
              {isPaidPlan ?(
                <Card title="Card Information">
                <DropIn
                  options={{ authorization: clientToken, threeDSecure: true }}
                  onInstance={instance => (this.instance = instance)}
                />
              </Card>
              ):null}
            </Col>


            <Col span={8} style={{ position: "relative" }}>
              <Row gutter={50} style={{ clear: "both" }} >
                <Card title="Your Payment Summary"  >
                  <div>
                    <Col style={{ position: "static" }}>
                      <Row style={{ clear: "both" }}>
                        <Col span={12}>
                          <br></br>
                          <p>Company:</p>
                        </Col>
                        <Col span={12}>
                          <br></br>
                          <b>{currentCompany.name}</b>
                        </Col>
                      </Row>
                      <Row style={{ clear: "both" }}>
                        <Col span={12}>
                          <br></br>
                          <p>Plan:</p>
                        </Col>
                        <Col span={12}>
                          <br></br>
                          <b>{plan}</b>
                        </Col>
                      </Row>
                      <Row style={{ clear: "both" }}>
                        <Col span={12}>
                          <br></br>
                          <p>User(s):</p>
                        </Col>
                        <Col span={12} >
                          <br></br>
                          <b>{totalUser}</b>
                        </Col>
                      </Row>
                      <Row style={{ clear: "both" }}>
                        <Col span={12}>
                          <br></br>
                          <p>Billing Cycle :</p>
                        </Col>
                        <Col span={12} >
                          <br></br>
                          <b>{billing}</b>
                        </Col>
                      </Row>
                      <Row style={{ clear: "both" }}>
                        <Col span={12}>
                          <br></br>
                          <p>Price:</p>
                        </Col>
                        <Col span={12}>
                          <br></br>
                          {discount <= 0 && addonsPrice <= 0 ? (
                            <b style={{ color: "#3bc1ac" }}>USD {amount}</b>
                          ) : <b>USD {planPrice}</b>}
                        </Col>
                      </Row>
                      {addonsId != 0 ? (
                        <div>
                          <Row style={{ clear: "both" }}>
                            <Col span={12}>
                              <br></br>
                              <p>Addons:</p>
                            </Col>
                            <Col span={12}>
                              <br></br>
                              <p>+USD {addonsPrice} {addonsName}</p>
                            </Col>
                          </Row>
                        </div>
                      ) : null}
                      {discount > 0 ? (
                        <div>
                          <Row style={{ clear: "both" }}>
                            <Col span={12}>
                              <br></br>
                              <p>Discount:</p>
                            </Col>
                            <Col span={12}>
                              <br></br>
                              <p>-USD {discount} {coupon}</p>
                            </Col>
                          </Row>
                        </div>
                      ) : null}
                      {discount > 0 || addonsId != 0 ? (
                        <Row style={{ clear: "both" }}>
                          <Col span={12}>
                            <br></br>
                            <p>Total:</p>
                          </Col>
                          <Col span={12}>
                            <br></br>
                            <b style={{ color: "#3bc1ac" }}>USD {payment}</b>
                          </Col>
                        </Row>
                      ) : null}
                      <Row>
                        <Row>
                          <Col span={16}>
                            <br></br>
                            <Input placeholder="Enter Promo Code" onChange={(e) => { this.setState({ promoCode: e.target.value }) }} />
                          </Col>
                          <Col span={8}>
                            <br></br>
                            {promoCode !== '' ? (
                              <Button type="primary" style={{ marginLeft: 8 }} onClick={this.onSubmitPromoCode} loading={couponLoading}>Apply</Button>
                            ) : <Button type="primary" style={{ marginLeft: 8 }} onClick={this.onSubmitPromoCode} loading={couponLoading} disabled>Apply</Button>}
                          </Col>
                        </Row>
                      </Row>
                    </Col>
                  </div>
                </Card>
              </Row>

              <Row gutter={50}>
                {isPaidPlan?(
                  <Card>
                   <div style={{ textAlign: "center" }}>
                     <img shape="square" width="100" height="100" src={Image} />
                   </div>
 
                   <br></br>
                   <h2 style={{ textAlign: "center" }}>Payment Secure</h2>
                   <p style={{ textAlign: "center" }}>Your payment is secured using multiple key of encryption</p>
                 </Card>
                ):null}
                <Card bordered={false}>
                  <Row gutter={50} style={{ position: "absolute", right: 0, bottom: 0, marginRight: 20 }}>
                    <Button style={{ margin: 3 }} onClick={this.goBack} htmlType="submit" >
                      Previous
                </Button>
                    <Button style={{ margin: 3 }} type="primary" onClick={this.submit} htmlType="submit" >
                      Submit
                </Button>
                  </Row>
                </Card>
              </Row>
            </Col>
          </Row>
        </div>

      );
    }

  }
}

const WrappedForm = Form.create()(BillingInfo)
export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      payment: state.Subscriptions.get("totalPayment"),
      plan: state.Subscriptions.get("planName"),
      planId: state.Subscriptions.get("planId"),
      companyId: state.Subscriptions.get("companyId"),
      totalUser: state.Subscriptions.get("totalUser"),
      billing: state.Subscriptions.get("payment"),
      billingId: state.Subscriptions.get("billingId"),
      loading: state.Subscriptions.get("loading"),
      clientToken: state.Subscriptions.get("clientToken"),
      billingAddress: state.Subscriptions.get("billingAddress"),
      couponLoading: state.Subscriptions.get("subsLoading"),
      discount: state.Subscriptions.get("discount"),
      amount: state.Subscriptions.get("amount"),
      coupon: state.Subscriptions.get("coupon"),
      addonsId: state.Subscriptions.get("addonsId"),
      addonsName: state.Subscriptions.get("addonsName"),
      addonsPrice: state.Subscriptions.get("addonsPrice"),
      planPrice: state.Subscriptions.get("planPrice"),
      isPaidPlan : state.Subscriptions.get("isPaidPlan"),
    })
  },
  { getToken, storebillingDetails, storeNonce, estimate, clearSubs }
)(WrappedForm);

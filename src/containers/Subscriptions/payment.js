import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { Provider, connect } from "react-redux";
import { Row, Col, Steps, Button, message, notification, Spin, Icon } from "antd";
import { getClientToken, subscribePlan } from "./api.js";
import actions from "../../redux/Subscriptions/actions";
import Modify from "./modify";
import BillingInfo from "./billingInfo";
import Confirmation from "./confirmation";

const { getToken } = actions;

class Payment extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    current: 0,
    loading: false
  };

  next() {
    if (this.props.haveError === 1) {
      const error = this.props.subsError;
      error.then(function (result) {
        var errorMessage = result.error.message;
        notification.open({
          message: "Invalid",
          description: errorMessage
        });
      });
    }
    else {
      const current = this.state.current + 1;
      this.setState({ current });
    }

  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  submit() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  getBraintreeClientToken = () => {
    const {isPaidPlan} = this.props;
    if (isPaidPlan){
      getClientToken(this.props.idToken).then(res => {
        this.props.getToken(res.data.clientToken);
      }).catch(err => {
        notification.open({
          message: err.statusText,
          description: "Cannot proceed with payment"
        });
      })
    }
  }

  confirm = () => {
    this.setState({
      loading: true
    });
    this.subscribe();
  }

  subscribe = () => {
    const { idToken, companyId, planId, totalUser, card, billing, billingAddress, coupon, addonsId,currentCompany } = this.props;
    subscribePlan(idToken, companyId, planId, totalUser, card, billing, billingAddress, coupon,addonsId).then(res => {
      message.success('Processing complete!');
      this.props.history.push("/dashboard/my_plan/transaction/success");
    }).catch(async error => {
      const err = await error;
      this.setState({ loading: false });
      notification.error({
        message: "Cannot proceed with transaction",
        description: err.error.message
      });
    })
  }

  render() {
    const { Step } = Steps;
    const steps = [
      {
        title: 'Choose Plan',
        content: 'Subscriptions',
      },
      {
        title: 'Payment Details',
        content: 'Summary',
      },
      {
        title: 'Confirm & Pay',
        content: 'Braintree',
      },
    ];
    const { current } = this.state;
    const { currentCompany } = this.props;
    if (currentCompany && !currentCompany.is_superuser) {
      return (
        <LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent" >
            Sorry, you have no permission to view this page
          </TableDemoStyle>
        </LayoutContentWrapper>
      )
    } else {
      return (
        <LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent" >
            <div>
              <Steps current={current}>
                {steps.map(item => (
                  <Step key={item.title} title={item.title} />
                ))}
              </Steps>

              <div className="steps-content">
                <div>
                  <br></br>
                  {steps[current].content === "Subscriptions" ? (
                    <Modify {...this.props}></Modify>
                  ) : steps[current].content === "Summary" ? (
                    <BillingInfo {...this.getBraintreeClientToken()} onHandleSummary={() => this.submit()} onHandlePrevious={() => this.prev()}></BillingInfo>
                  ) : steps[current].content === "Braintree" ? (
                    <Confirmation></Confirmation>
                  ) : null}
                </div>
              </div>

              <div className="steps-action">
                <Row>
                  <Col span={24} style={{ textAlign: "right" }}>
                    {current < steps.length - 1 && current !== 1 && (
                      <Button type="primary" onClick={() => this.next()}>
                        Next
                          </Button>
                    )}
                    {current === steps.length - 1 && (
                      <Button type="primary" onClick={this.confirm} loading={this.state.loading}>
                        Confirm & Pay
                          </Button>

                    )}
                    {current > 0 && current !== 1 && (
                      <Button style={{ marginLeft: 8 }} onClick={() => this.prev()} >
                        Previous
                          </Button>
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          </TableDemoStyle>
        </LayoutContentWrapper >
      )
    }
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      companyId: state.Subscriptions.get("companyId"),
      totalUser: state.Subscriptions.get("totalUser"),
      clientToken: state.Subscriptions.get("clientToken"),
      totalPayment: state.Subscriptions.get("totalPayment"),
      planId: state.Subscriptions.get("planId"),
      billing: state.Subscriptions.get("billingId"),
      billingAddress: state.Subscriptions.get("billingAddress"),
      card: state.Subscriptions.get("nonce"),
      coupon: state.Subscriptions.get("coupon"),
      haveError: state.Subscriptions.get("haveError"),
      subsError: state.Subscriptions.get("subsError"),
      addonsId: state.Subscriptions.get("addonsId"),
      isPaidPlan : state.Subscriptions.get("isPaidPlan"),
    })
  },
  { getToken }
)(Payment);

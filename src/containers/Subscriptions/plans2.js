import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Form, Input, Row, Col, Button, Modal, label, Spin, notification, Select, Card, Divider, Icon, Avatar, Table } from "antd";
import { getPlanByCompany, subscribePlan, getPlanById, getCreatorDetails, getPlanByCompanyDetails, getCompanyInvoices, downgradeToFreePlan } from "./api.js";
import actions from "../../redux/Subscriptions/actions";
import { getSubscriptionDetails } from "../../redux/Subscriptions/api";

const { clearSubs } = actions;

const columns = [
  {
    title: 'Date',
    dataIndex: 'created_at',
    key: 'date',
  },
  {
    title: 'Type',
    dataIndex: 'plan',
    key: 'type',
  },
  {
    // title: 'Total Payment (inc SST)',
    title: 'Total Payment',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: 'Invoice',
    dataIndex: 'pdf',
    key: 'invoice',
    render: (text, record) => <a href={record.pdf}>Download</a>,
  }];

class Plan extends Component {

  state = {
    loading: true,
    companyName: [],
    companyEmail: [],
    companyId: [],
    planId: [],
    totalUser: [],
    planName: [],
    payments: [],
    plan: [],
    paymentMethod: " ",
    maxEmployee: [],
    modalVisible: false,
    buttonloading: false,
    billingloading: false,
    data: [],
    link: " ",
    card: [],
    nextPayment: " ",
    nextPlan: " ",
    nextQuantity: " ",
    schedule: false,
    invoiceLoading: true,
    trialEnd : null,
    trialStatus : null
  };

  async componentDidMount() {
    const { idToken, currentCompany } = this.props;
    if (currentCompany) {
      this.getCompanyDetails(idToken, currentCompany);
      this.getCompanyInvoices();
      setInterval(() => { this.getCompanyDetails(idToken, currentCompany) }, 30000);
    }
  }

  componentDidUpdate(prevProp, prevState) {
    const { currentCompany } = this.props;
    if (prevProp.currentCompany !== currentCompany) {
      this.props.clearSubs;
      this.getCompanyDetails(currentCompany);
    }
  }

  getCompanyDetails = async (idToken, currentCompany) => {
    try {
      getPlanByCompanyDetails(idToken, currentCompany.id).then(res => {
        this.setState({
          planName: res.data.plan_name,
          billingName: res.data.billing_name,
          billingId: res.data.billing_id,
          companyName: currentCompany.name,
          companyEmail: currentCompany.email,
          companyId: currentCompany.id,
          planId: res.data.plan_id,
          totalUser: currentCompany.total_user,
          amountPerUser: res.data.amountPerUser,
          totalPayment: res.data.totalPayment,
          maxEmployee: res.data.max_employee,
          companyLogo: currentCompany.logo_path,
          nextPayment: res.data.next_payment,
          nextPlan: res.data.next_plan_name,
          nextQuantity: res.data.next_plan_quantity,
          schedule: res.data.has_schedule_changes,
          card: res.data.card,
          loading: false,
          first_name: ' ',
          last_name: ' ',
          invoice_email: ' ',
          postcode: ' ',
          country: ' ',
          address: ' ',
          state: ' ',
          trialEnd : res.data.trial_end,
          trialStatus : res.data.trial_status,
        })
      }).catch(err => {
        console.log(err);
      })
    } catch (e) {
      console.log(e);
    }
  };

  getCompanyInvoices = () => {
    const { idToken, currentCompany } = this.props;
    getCompanyInvoices(idToken, currentCompany.id).then(res => {
      this.setState({
        data: res.invoices,
        invoiceLoading: false
      })
    }).catch(err => {
      console.log(err);
    })
  }

  handleOk = () => {
    const planId = this.state.planId;
    const currentCompany = this.props;

    if (this.state.planId === 1 && currentCompany.currentCompany.trial_period_id === 3){
      this.setState({
        billingId: 2
      })
    }

    getPlanById(this.props.idToken, planId).then(res => {
      this.props.history.push(`/dashboard/my_plan/plan_confirmation?version=${res.data.website_id}&cycle=${this.state.billingId}&user=${this.state.totalUser}`);
    }).catch(err => {
      console.log(err);
    })
  }

  handleCancel = () => {
    this.setState({
      modalVisible: false
    });
  }

  handleCancelPayment = () => {
    this.setState({
      modalVisible: true
    });
  }

  CancelSubscription = () => {
    const { idToken } = this.props;
    const { companyId, totalUser } = this.state;

    this.setState({
      buttonloading: true
    });

    downgradeToFreePlan(idToken, companyId, 1, totalUser, 0, 2, null, '').then(res => {
      this.props.history.push("/dashboard/my_plan/transaction/success");
    }).catch(error => {
      error.then(function (result) {
        var errorMessage = result.error.message;
        notification.error({
          message: "Cannot cancel your subscription",
          description: errorMessage
        });
      });

      this.setState({
        buttonloading: false,
        modalVisible: false
      });
    })
  }

  handleReturn = () => {
    this.setState({
      modalVisible: false
    });
  }

  render() {
    const { data, billingLoading, nextPayment, card, buttonloading, modalVisible, totalUser, planName, billingName, companyLogo, companyName, companyEmail, totalPayment, maxEmployee, loading, schedule, nextPlan, nextQuantity, invoiceLoading, trialEnd, trialStatus } = this.state;
    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
    const { Meta } = Card;

    if (loading) {
      return (
        <LayoutContentWrapper>
          <TableDemoStyle className="isoLayoutContent">
            <Spin indicator={antIcon} spinning={this.props.loading} />
          </TableDemoStyle>
        </LayoutContentWrapper>
      );
    }

    else {
      return (
        <LayoutContentWrapper>
          <TableDemoStyle className="isoLayoutContent">
            <Spin indicator={antIcon} spinning={this.state.billingloading} />
            <Row>
              <Col span={18} offset={3}>
                <Card>
                  <div>
                    <Row>
                      <Col span={12}>
                        <h2>{planName} Plan</h2>
                        <p>Max Employee: {maxEmployee} </p>
                        <br style={{ lineHeight: 3 }}></br>
                        <b>Company</b>
                        <p>{companyName}</p>
                        <p>Current Employee: {totalUser}</p>
                        <br style={{ lineHeight: 3 }}></br>
                        <b>Billing Overview</b>
                        <p>Billing Cycle: {billingName}</p>
                        {/* <p>Estimated charge: USD {totalPayment} (SST excl)</p> */}
                        <p>Estimated charge: USD {totalPayment}</p>
                        <p>Next charge on: {nextPayment}</p>
                      </Col>
                      <Col span={12}>
                        {(schedule) ? (
                          <div>
                            <br style={{ lineHeight: 3 }}></br>
                            <b>Next Schedule Changes</b>
                            <p>Changes date: {nextPayment}</p>
                            <p>Incoming Plan: {nextPlan}</p>
                            <p>Incoming Max Employee: {nextQuantity}</p>
                          </div>
                        ) : null}
                        {card != null ? (
                          <div>
                            <br style={{ lineHeight: 3 }}></br>
                            <b>Payment Method</b>
                            <p>Card: {card.card_number}</p>
                            <p>Expiry on: {card.card_expiry_month}/{card.card_expiry_year}</p>
                          </div>
                        ) : null}
                        { trialStatus != null?(
                          <div>
                          <br style={{ lineHeight: 3 }}></br>
                          <b>Trial</b>
                          <p>Status: {trialStatus}</p>
                          <p>Trial end: {trialEnd}</p>
                        </div>
                        ): null}
                      </Col>
                    </Row>
                  </div>
                </Card>

                <br></br>
                <Row gutter={50} style={{ display: "block", margin: "auto" }}>
                  <Button type="primary" onClick={this.handleOk} style={{ margin: 3 }}>Modify Plan</Button>
                  <Button onClick={this.handleCancelPayment} loading={this.state.loading} style={{ margin: 3 }}>Cancel Subscriptions</Button>
                </Row>
                <br></br>
                <Card>
                  <h2>Billing History</h2>
                  <Table columns={columns} dataSource={data} loading={invoiceLoading} />
                </Card>
              </Col>
            </Row>

            <Modal
              title="Are you sure?"
              visible={this.state.modalVisible}
              footer={[
                <Button key="submit" loading={buttonloading} onClick={this.CancelSubscription}>
                  Yes
                </Button>,
                <Button key="back" type="primary" onClick={this.handleCancel}>
                  Return
                  </Button>,
              ]}
            >
              <p>Cancelling the subscriptions will change your plan to Free Version, which has lesser features.
                  Are you sure you want to cancel?</p>
            </Modal>
          </TableDemoStyle>
        </LayoutContentWrapper>
      );
    }

  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
    })
  },
  { clearSubs }
)(Plan);

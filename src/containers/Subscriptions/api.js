import { baseUrl } from "../../config";

export const getPlanById = (token, planId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/plans/${planId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const subscribePlan = (token, companyId, planId, totalUser, card, billing, billingAddress,promoCode, addonsId) => { 
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/${planId}/billings/${billing}
      ?quantity=${totalUser}&card=${card}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,  Accept: "application/x.app.v2+json", "Content-Type": "application/json",
        },
        body:JSON.stringify({
          billingAddress,
          coupon : promoCode,
          add_ons_id : addonsId
        })      
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
  
};


export const getClientToken = (token) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/braintree/customers`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getPlanByCompany = (token, companyId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const getPlanIdByWebsiteId = (token, websiteId,companyId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/plans/website/${websiteId}?companyId=${companyId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getCreatorDetails = (token, userId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/users/${userId}/profile`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,Accept:"application/x.app.v2+json",
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getCompanyInvoices = (token, companyId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/invoices`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,Accept:"application/x.app.v2+json",
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getPlanByCompanyDetails = (token, companyId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/details`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getOptionsInResponse = (token) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/marketing/forms`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const postFormResponse = (token, respond) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/marketing/forms/response`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        },
        body:JSON.stringify({
          respond : respond
        })            
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const downgradeToFreePlan = (token, companyId, planId, totalUser, card, billing, billingAddress,promoCode) => { 
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/${planId}/billings/${billing}/cancel?quantity=${totalUser}&card=${card}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,  Accept: "application/x.app.v2+json", "Content-Type": "application/json",
        },
        body:JSON.stringify({
          billingAddress,
          coupon : promoCode
        })      
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
  
  
};

export const getQuantityByAddons = (token, addonsId) => { 
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/addons/${addonsId}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,  Accept: "application/x.app.v2+json", "Content-Type": "application/json",
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  }); 
  
};

export const getPlan = (token,companyId,websiteId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/selection${websiteId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


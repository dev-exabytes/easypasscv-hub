import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Form, Input, Row, Col, Button, Modal, label, message, notification } from "antd";
import actions from "../../redux/bugs/actions";
import { subscribePlan } from "./api.js";
import DropIn from "braintree-web-drop-in-react";

const { payment } = actions
class Braintree extends Component {

    state = {
        loading : true
    };

    componentDidMount() {
        const hide = message.loading('Please wait a moment..', 0);
        setTimeout(hide, 2000);
    }


    render() {
        const { clientToken } = this.props;
            return (
                <DropIn
                    options={{ authorization: clientToken }}
                    onInstance={instance => (this.instance = instance)}
                />
            );
    }
}

export default connect(
    state => {
        return ({
            clientToken: state.Subscriptions.get("clientToken"),
            idToken: state.Auth.get("idToken"),
            totalPayment: state.Subscriptions.get("totalPayment"),
            planId: state.Subscriptions.get("planId"),
            companyId: state.Subscriptions.get("companyId"),
            totalUser: state.Subscriptions.get("totalUser"),
            payment: state.Subscriptions.get("billingId"),
            billingAddress: state.Subscriptions.get("billingAddress")
        })
    },
    { payment }
)(Braintree);
import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Form, Input, Row, Col, Button, Modal, label, Spin, Select, Card, Divider, Icon, Avatar } from "antd";
import { getPlanByCompany, subscribePlan , getPlanById} from "./api.js";
import actions from "../../redux/Subscriptions/actions";
import { getSubscriptionDetails } from "../../redux/Subscriptions/api";

class Plan extends Component {

  state = {
    loading: true,
    companyName: [],
    companyEmail: [],
    companyId: [],
    planId: [],
    totalUser: [],
    planName: [],
    payments:[],
    plan: [],
    paymentMethod: " ",
    maxEmployee : [],
    modalVisible: false,
    buttonloading: false
  };

  componentDidMount() {
    const { idToken, currentCompany } = this.props;  
    this.getCompanyDetails(currentCompany,idToken);
  }

  componentDidUpdate(prevProp, prevState) {
    const { currentCompany } = this.props;
    if (prevProp.currentCompany !== currentCompany) {
      this.getCompanyDetails(currentCompany);
    }
  }

  getCompanyDetails = (currentCompany,idToken) => {
    getPlanByCompany(idToken, currentCompany.id).then(res => {
       this.setState({
          planName: res.data.plan_name,
          billingName: res.data.billing_name,
          billingId : res.data.billing_id,
          companyName: currentCompany.name,
          companyEmail: currentCompany.email,
          companyId: currentCompany.id,
          planId: res.data.plan_id,
          totalUser: currentCompany.total_user,
          amountPerUser: res.data.amountPerUser,
          totalPayment: res.data.totalPayment,
          maxEmployee : res.data.max_employee,
          companyLogo : currentCompany.logo_path,
          loading: false
        })
    }).catch(err => {
      console.log(err);
    })

    if ( this.state.companyLogo === " "){
      this.setState({
        companyLogo: 'user'
      })
    }
  }

  handleOk = () => {
    const planId = this.state.planId;

    if (planId === 4){
      this.setState({
        planId : 1
      })
    }

    getPlanById(this.props.idToken,this.state.planId).then(res => {
        this.props.history.push(`my_plan/plan_confirmation?version=${res.data.website_id}&cycle=${this.state.billingId}&user=${this.state.totalUser}`);
    }).catch(err => {
      console.log(err);
    })

  }

  handleCancel = () =>{    
    this.setState({
      modalVisible: false
    });
  }

  handleCancelPayment =()=>{
    this.setState({
      modalVisible: true
    });
  }

  CancelSubscription = () =>{
    const {idToken} = this.props;
    const {companyId,totalUser} = this.state;
    
    this.setState({
      buttonloading: true
    });

    subscribePlan(idToken, companyId, 1, totalUser, 0, 1).then(res => {
      this.props.history.push("my_plan/transaction/success");
    }).catch(err => {
      console.log(err);
    })
  }

  handleReturn = () => {
    this.setState({
      modalVisible: false
    });
  }

  render() {
    const {buttonloading, modalVisible, totalUser,planName, billingName, companyLogo, companyName, companyEmail, totalPayment, maxEmployee,loading } = this.state;
    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
    const { Meta } = Card;

    if (loading ){
      return (
        <LayoutContentWrapper>
          <TableDemoStyle className="isoLayoutContent">
            <Spin indicator={antIcon} spinning={this.props.loading}/>
          </TableDemoStyle>
        </LayoutContentWrapper>
      );
    }

    else{
      return (
      <LayoutContentWrapper>
        <TableDemoStyle className="isoLayoutContent">
        <Spin indicator={antIcon} spinning={this.state.loading}/>
        <Row>
          <Col span={12} offset={6}>
        <Row >
        <Card bordered={false}>
          <Row gutter={50}>
            <Col span={6}>
            <Meta
            avatar={
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" size={100}/>
            }
          />
            </Col>
            <Col span={18}>
            <br></br>
            <Meta
            title={companyName}
            description={companyEmail}
          />
            </Col>
          </Row>

          <Row>
            <Col span={24}>
              <Row>
                <Col span={24}>
                <Divider orientation="left">Plan</Divider>
                </Col>
              </Row>
              <Row>            
                <Col span={10}>
                <br></br>
                  <p>Plan</p>
                </Col>
                <Col style={{textAlign:"right"}} span={14}>
                <br></br>
                  <b>{planName}</b>
                </Col>
              </Row>
              <Row>            
                <Col span={10}>              
                  <br></br>
                  <p>Billing Cycle :</p>
                </Col>
                <Col style={{textAlign:"right"}} span={14}>
                  <br></br>
                  <b>{billingName}</b>
                </Col>
              </Row>
              <Row>            
                <Col span={10}>
                  <br></br>
                  <p>Current Employee:</p>
                  </Col>
                <Col style={{textAlign:"right"}} span={14}>
                <br></br>
                <b>{totalUser}</b>
                </Col>
              </Row>
              <Row>            
                <Col span={10}>
                  <br></br>
                  <p>Max Employee:</p>
                  </Col>
                <Col style={{textAlign:"right"}} span={14}>
                <br></br>
                <b>{maxEmployee}</b>
                </Col>
              </Row>
              <Row>
                <Col span={10}>
                <br></br>
                <p>Total Payment :</p>
                </Col>
                <Col style={{textAlign:"right"}} span={14}>
                <br></br>
                <b>USD {totalPayment}</b>
                </Col>
              </Row>

              <Row>
                <Col span={24}>
                <Divider orientation="left">Settings</Divider>
                </Col>
              </Row>

              <Row>
                <Col span={6}>
                    <br></br>
                    <Button type="primary" onClick={this.handleOk}>Modify Plan</Button>
                </Col>                  
                <Col span={6}>  
                  <br></br>                
                  <Button onClick={this.handleCancelPayment} loading={this.state.loading}>Cancel Subscriptions</Button>
                </Col>
              </Row>
            </Col>
            </Row>
        </Card>
        </Row>
        </Col>
        </Row>
        
          <Modal
          title="Are you sure?"
          visible={this.state.modalVisible}
          footer={[
            <Button key="submit" loading={buttonloading} onClick={this.CancelSubscription}>
            Yes
          </Button>,
            <Button key="back" type="primary" onClick={this.handleCancel}>
              Return
            </Button>,
          ]}
          >
          <p>Cancelling the subscriptions will change your plan to Free Version, which has lesser features.
            Are you sure you want to cancel?</p>
        </Modal>

        </TableDemoStyle>
      </LayoutContentWrapper>
    );
    }
    
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
    })
  },
)(Plan);

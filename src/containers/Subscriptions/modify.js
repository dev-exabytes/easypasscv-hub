import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Form, Input, Row, Col, Button, Modal, label, Spin, Select, Card, Icon, notification, InputNumber } from "antd";
import {getQuantityByAddons, getPlan } from "./api.js";
import actions from "../../redux/Subscriptions/actions";

const { subscribe, plans, planName, getToken, clearSubs, modifyPlan, modifyBilling, modifyAddons, storeCurrentCompanyPlan } = actions;

class Modify extends Component {
  state = {
    loading: true,
    companyName: [],
    companyId: [],
    currentplanId: [],
    totalUser: 0,
    planName: [],
    payments: [],
    plan: [],
    addons : [],
    selectedBilling: 1,
    selectedPlan: 1,
    selectedAddons : 0,
    quantity: 0,
    selectedPlanName: " ",
    selectedBillingName: " ",
    disable: false,
    allPlansPayments: [],
    allPlanAddOns: [],
    planMessage: '',
    quantityDisable : false,
  };


  componentDidMount() {
    const { idToken, currentCompany, totalPayment, quantity, billingId, planId, addonsId, currentCompanyPlan} = this.props;
    const companyId = currentCompany.id;
    const params = new URLSearchParams(this.props.location.search);

    if (currentCompany) {
      this.getCompanyDetails(currentCompany);

      if (totalPayment === " " ){
        let websiteId = `?website_id=${params.get('version')}&billing_id=${params.get('cycle')}&quantity=${params.get('user')}`;
        getPlan(idToken,companyId,websiteId).then(res => {
          this.setState({
            selectedPlan: res.data.plan_id,
            selectedBilling: res.data.billing_id,
            selectedAddons : res.data.add_ons_id,
            quantity: parseInt(params.get('user')), 
            allPlansPayments: res.data.billing,
            allPlanAddOns: res.data.add_ons,
            plan: res.data.plans,            
            addons: res.data.add_ons[res.data.plan_id],       
            payments: res.data.billing[res.data.plan_id],
            planMessage : res.data.message,
            quantityDisable : res.data.quantityDisable,      
          });
         
          if (this.state.planMessage !== ''){
            notification.open({
              message: this.state.planMessage,
          });
          }

          this.getSubscriptionDetails();
          this.props.storeCurrentCompanyPlan(res.data);

        }).catch(err => {
          console.log(err);
        })
      }
      else{
          this.setState({
            selectedPlan: parseInt(planId),
            selectedBilling: parseInt(billingId),            
            selectedAddons : parseInt(addonsId),
            quantity: parseInt(quantity),
            plan: currentCompanyPlan.plans,   
            allPlansPayments: currentCompanyPlan.billing,
            allPlanAddOns: currentCompanyPlan.add_ons,           
            addons: currentCompanyPlan.add_ons[parseInt(planId)],
            payments: currentCompanyPlan.billing[parseInt(planId)],  
            planMessage : currentCompanyPlan.message                      
          });
      
          if (this.state.planMessage !== ''){
            notification.open({
              message: this.state.planMessage,
          });
          }
          this.getSubscriptionDetails(); 
      } 
    }
  }

  componentDidUpdate(prevProp, prevState) {
    const { currentCompany } = this.props;
    const { selectedBilling, selectedPlan,selectedAddons, quantity, addons } = this.state;

    if (prevProp.currentCompany !== currentCompany) {
      this.props.clearSubs;
      this.getCompanyDetails(currentCompany);
    }

    if (prevState.selectedBilling !== selectedBilling) {
      this.getSubscriptionDetails();
    }

    if (prevState.selectedPlan !== selectedPlan) {
      this.getSubscriptionDetails();
    }

    if (prevState.selectedAddons !== selectedAddons) {
      this.getSubscriptionDetails();
    }

    if (prevState.quantity !== quantity && quantity != 0) {
      this.getSubscriptionDetails();
    }
  }

  getCompanyDetails = (currentCompany) => {
    this.setState({
      companyName: currentCompany.name,
      companyId: currentCompany.id,
      currentplanId: currentCompany.plan_id,
      totalUser: parseInt(currentCompany.total_user),
    })
  }

  getSubscriptionDetails = () => {
    const { currentCompany } = this.props;
    const { selectedBilling, selectedPlan, selectedAddons, quantity } = this.state;

    if (currentCompany) {
      const { id } = currentCompany;
      const data = {
        quantity,
        id,
        selectedBilling,
        selectedPlan,
        selectedAddons
      };

      this.props.subscribe(data);
    }
  }

  getPaymentSelected = (value) => {
    this.setState({
      selectedBilling: value
    })
  }

  getPlanSelected = (value,key,e) => {
    const {allPlansPayments, allPlanAddOns, totalUser, plan} = this.state;

    this.setState({
      payments : allPlansPayments[value],
      addons : allPlanAddOns[value],
      selectedBilling : allPlansPayments[value][0]['id'],
      selectedPlan: value,
      selectedAddons : 0,
    })

    if(allPlanAddOns[value].length > 0){
      this.setState({selectedAddons : allPlanAddOns[value][0]['id']})
    }
    else{
      this.setState({selectedAddons : 0})
    }

    if(plan[key.key]['is_fixed_quantity']){
      this.setState({quantityDisable : true})
    }
    else{
      this.setState({quantityDisable : false})
    }
  }

  getAddonsSelected = (value) => {
    // const {addons} = this.state;
    
    this.setState({
      selectedAddons: value,
      // quantity : addons[value-1]['quantity']
    });
  }

  getTotalQuantity = (value) => {
    this.setState({
      quantity: value
    })
  }

  render() {
    const { Option } = Select;
    const { amountPerUser, totalPayment, screenLoading, trialEnd, trialStart, trialPeriod, currentCompany, quantity } = this.props;
    const { payments, plan, addons, totalUser, selectedPlan, selectedBilling, selectedAddons, disable, companyName, allPlanAddOns, quantityDisable } = this.state;
    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
  
    console.log(quantity);
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
      labelAlign: "left"
    };

    if (plan.length === 0 && payments.length === 0) {
      return (
        <Spin indicator={antIcon} spinning={this.props.loading} />
      );
    }
    else if (plan.length > 0 && payments.length >0 ) {
      return (
        <Row>
          <Spin indicator={antIcon} spinning={this.props.loading} />
          <Col span={17}>
            <Row>
              <br></br>
              <Form onSubmit={this.handleSubmit} layout="horizontal" >
                <Form.Item label="Company" {...formItemLayout}>
                  <b>{companyName}</b>
                </Form.Item>
                <Form.Item label="Current Employee" {...formItemLayout}>
                  <b>{totalUser}</b>
                </Form.Item>
                <Form.Item label="Plan" {...formItemLayout}>
                  <Select value={selectedPlan} style={{ width: "90%" }} onChange={this.getPlanSelected}>
                    {plan.map((e, key) => {
                      return <Option key={key} value={e.id}>{e.plan_name}</Option>;
                    })}
                  </Select>
                </Form.Item>
                <Form.Item label="Billing Cycle" {...formItemLayout}>
                  <Select value={selectedBilling} style={{ width: "90%" }} onChange={this.getPaymentSelected} disabled={disable}>
                    {payments.map((e, key) => {
                      return <Option key={key} value={e.id}>{e.type}</Option>;
                    })}
                  </Select>
                </Form.Item>
                {addons.length> 1?(
                    <Form.Item label="Add ons" {...formItemLayout} >
                      <Select placeholder="Select add ons" value={selectedAddons} style={{ width: "90%" }} onChange={this.getAddonsSelected}>
                        {addons.map((e, key) => {
                          return <Option key={key} value={e.id}>{e.name}</Option>;
                        })}
                      </Select>                  
                    </Form.Item>
                  ) : null 
                  }
                <Form.Item label="Quantity" {...formItemLayout} >
                     <InputNumber min={totalUser} max={1000} value={quantity} onChange={this.getTotalQuantity} disabled={quantityDisable} />
                </Form.Item>
              </Form >
            </Row>
          </Col>
          <Row gutter={60}>
            <Col span={7}>
              <h3>Amount to be charged:</h3>
              <Card>
                <h1 style={{ textAlign: "center", color: "#ff6600" }}>USD {totalPayment}</h1>
                <p style={{ textAlign: "center" }}>{amountPerUser}</p>
                {trialEnd != 0 ? (
                  <div>
                    <p style={{ textAlign: "center" }}>starting {trialEnd}</p>
                  </div>
                ) : null}
                <p></p>
              </Card>
              <br></br>
              {trialEnd != 0 ? (
                  <Card style={{ backgroundColor: "#ff6600" }}>
                  <h4 style={{ textAlign: "center", color: "#FFFFFF"}}>Free trial {trialPeriod}</h4>
                </Card>                
                ) : null}
              <br></br>
            </Col>
          </Row>
        </Row>
      );
    }
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      plan: state.Subscriptions.get("allPlans"),
      payments: state.Subscriptions.get("allPayments"),
      allPlansPayments: state.Subscriptions.get("allPlansPayments"),
      addons: state.Subscriptions.get("allAddOns"),
      loading: state.Subscriptions.get("loading"),
      amountPerUser: state.Subscriptions.get("amountPerUser"),
      totalPayment: state.Subscriptions.get("totalPayment"),
      selectedPlanName: state.Subscriptions.get("selectedPlanName"),
      billingId: state.Subscriptions.get("billingId"),
      planId: state.Subscriptions.get("planId"),
      quantity: state.Subscriptions.get("totalUser"),
      screenLoading: state.Subscriptions.get("screenLoading"),
      trialEnd: state.Subscriptions.get("trialEnd"),
      trialStart: state.Subscriptions.get("trialStart"),
      trialPeriod: state.Subscriptions.get("trialPeriod"),
      addonsId: state.Subscriptions.get("addonsId"),
      currentCompanyPlan : state.Subscriptions.get("currentCompanyPlan"),
    })
  },
  { subscribe, plans, planName, getToken, clearSubs, modifyPlan, modifyBilling, modifyAddons, storeCurrentCompanyPlan }
)(Modify);

import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
.isoLayoutContent{
  padding: 10px;
}

.ant-input-lg {
  height: 42px;
  padding: 6px 20px;
}

.word-center{
  text-align: center;
}

.image-center {
  display: block;
  margin-left: auto;
  margin-right: auto 
}

.isoInputWrapper {
  display: flex;
  margin-bottom: 15px;
  flex-direction: row;

  &:last-of-type {
    margin-bottom: 0;
  }

  input {
    &::-webkit-input-placeholder {
      color: ${palette("grayscale", 0)};
    }

    &:-moz-placeholder {
      color: ${palette("grayscale", 0)};
    }

    &::-moz-placeholder {
      color: ${palette("grayscale", 0)};
    }
    &:-ms-input-placeholder {
      color: ${palette("grayscale", 0)};
    }
  }
}


.steps-content {
  margin-top: 16px;
  background-color: #fff;
  font-size: 18px;
  text-align: center;
  padding: 50px 20px;
  color: #000;
}

.steps-action {
  margin-top: 24px;
}
`;

export default Style;

import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import { Row, Col, Icon, Spin, notification, Card , Divider} from "antd";
import { getClientToken } from "./api.js";
import actions from "../../redux/Subscriptions/actions";
import { stat } from "fs";
import {getName} from "country-list";

const { getToken, estimate } = actions

class Confirmation extends Component {

    state = {
        loading: false,
        firstName: null,
        lastName: null,
        phone: null,
        email: null,
    };

    componentDidMount(){
        this.onEstimate();
    }

    onEstimate = () =>{
    const { planId, totalUser,billingId, company_id, billingAddress, addonsId} = this.props;
    let promoCode = this.props.coupon;
    
    const data = {
        totalUser,
        company_id,
        billingId,
        planId,
        promoCode,
        billingAddress,
        addonsId
      };
    
    this.props.estimate(data);
    }
    
    handlePay = () => {
        const { idToken } = this.props;
        const { firstName, lastName, email, phone } = this.state;

        getClientToken(idToken, firstName, lastName, email, phone).then(res => {
            this.updateClientToken(res.data.clientToken)
        }).catch(err => {
            notification.open({
                message: err.statusText,
                description: "Cannot proceed with payment"
            });
        })
    }

    updateClientToken = (token) => {
        this.props.getToken(token);
    }

    render() {
        const { planName, totalPayment, totalUser, lastFour,cardType, amountPerUser ,payment, billingAddress, totalPaymentIncTax, loading, discount, coupon, planPrice, addonsName, addonsPrice, amount, isPaidPlan} = this.props;
        const { Meta } = Card;
        const countryName = getName(billingAddress.country);
        const totalPaymentSST  = (parseFloat(totalPayment)*1.06).toFixed(2);
        const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

        return (
            <div style={{ padding: '30px' }}>
                <Row gutter={16}>
                <Col span={8}>
                    <Card>
                    <Meta title="Billing Address"/>
                    <br></br>
                    <p>{billingAddress.firstName} {billingAddress.lastName}</p>
                    <p>{billingAddress.email}</p>
                    <br></br>
                    <p>{billingAddress.line1}</p>
                    <p>{billingAddress.state}</p>
                    <p>{billingAddress.zip}</p>
                    <p>{countryName}</p>
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                    <Meta title="Payment Method"/>
                    <br></br>
                    {isPaidPlan ? (
                        <div>
                            <p>{cardType} Card</p>
                            <p>**** **** **** {lastFour} </p>
                        </div>
                    ):(
                        <p>No payment method needed</p>
                    )}
                    </Card>
                </Col>
                <Col span={8}>
                    < Card>
                    <Spin indicator={antIcon} spinning={loading}/>
                    <Meta title="Order Summary"/>
                    <br></br>
                    {/* Pland details column */}
                    <Row style={{clear:"both"}}>
                        <Col span={12}>
                        <p>{planName}</p>
                        <p>{totalUser} employee(s)</p>
                        <p>{payment}</p>
                        </Col>
                        <Col span={12}>
                        <p style={{textAlign:"right"}}>USD {planPrice}</p>
                        </Col>
                    </Row>
                    {/* Addons details column */}
                    {addonsPrice > 0?(
                    <div>
                    <Row style={{clear:"both"}}>
                        <Col span={12}> 
                            <br></br>
                            <p style={{textAlign:"left"}}>{addonsName}</p>  
                        </Col>
                        <Col span={12}>
                            <br></br>
                            <p style={{textAlign:"right"}}>+ USD {addonsPrice}</p>
                        </Col>
                    </Row> 
                    {discount > 0?(
                        <Row style={{clear:"both"}}>
                            <Divider/>
                            <Col span={12}>
                                <p style={{textAlign:"left"}}>Sub total</p> 
                            </Col>
                            <Col span={12}>
                                <p style={{textAlign:"right"}}>USD {amount}</p>
                            </Col>
                        </Row>
                    ):null}  
                    </div> 
                    ): null}
                    {/* discount details column */}
                    {discount>0?(
                        <Row style={{clear:"both"}}>
                            <Col span={12}>
                                <br></br>
                                <p style={{textAlign:"left"}}>{coupon}</p>
                            </Col>
                            <Col span={12}>
                                <br></br>
                                <p style={{textAlign:"right"}}>- USD {discount}</p>
                            </Col>
                        </Row>
                    ):null} 
                    {/* <p>USD {totalPayment} (excl SST)</p> */}
                    <Row style={{clear:"both"}}>
                        <Divider/>
                        <Col span={12}>
                            {/* <p>Total SST incl.</p> */}
                            <p>Total</p>
                        </Col>
                        <Col span={12}>
                            <h3 style={{textAlign:"right", color:"#3bc1ac"}}> USD {totalPaymentIncTax}</h3>
                        </Col>      
                    </Row>
                    </Card>
                </Col>
                </Row>
            </div>
        )
    }
}

export default connect(
    state => {
        return ({
            planName: state.Subscriptions.get("planName"),
            company_id: state.Subscriptions.get("companyId"),
            clientToken: state.Subscriptions.get("clientToken"),
            idToken: state.Auth.get("idToken"),
            totalPayment: state.Subscriptions.get("totalPayment"),
            planId: state.Subscriptions.get("planId"),
            billingId: state.Subscriptions.get("billingId"),
            totalUser: state.Subscriptions.get("totalUser"),
            payment: state.Subscriptions.get("payment"),
            billingAddress: state.Subscriptions.get("billingAddress"),
            lastFour: state.Subscriptions.get("lastFour"),
            cardType : state.Subscriptions.get("cardType"),
            totalPaymentIncTax: state.Subscriptions.get("totalPaymentIncTax"),
            loading: state.Subscriptions.get("subsLoading"),
            coupon : state.Subscriptions.get("coupon"),
            discount : state.Subscriptions.get("discount"),
            addonsId: state.Subscriptions.get("addonsId"),
            planPrice : state.Subscriptions.get("planPrice"),
            addonsName : state.Subscriptions.get("addonsName"),
            addonsPrice : state.Subscriptions.get("addonsPrice"),
            amount : state.Subscriptions.get("amount"),
            isPaidPlan : state.Subscriptions.get("isPaidPlan")
        })
    },
    { getToken, estimate }
)(Confirmation);
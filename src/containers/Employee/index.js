import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { notification } from "antd";
import { connect } from "react-redux";
import TableWrapper from "./easyworkTable.style";
import { Input, Row, Col, Divider, Modal, message, Button, Spin, Form, Cascader, Icon, DatePicker, Radio } from "antd";
import { getUser, deleteUser, editUser, inviteEmployee, getOrganisation, assignEmployee, getEmployeeDetailsByCompany, editEmployeeDetailsByCompany, setSubordinate, setSupervisor } from "./api.js";
import IntlTelInput from "react-intl-tel-input";
import moment from 'moment';
import "react-intl-tel-input/dist/main.css";

const Search = Input.Search;
const dateFormat = 'YYYY-MM-DD';

class Employee extends Component {
  state = {
    loading: false,
    result: [],
    resultSub : [],
    value: "",
    editVisible: false,
    delVisible: false,
    addVisible: false,
    addEmailVisible: false,
    assignVisible: false,
    user_id: "",
    first_name: "",
    last_name: "",
    phone: "",
    email: "",
    originalEmail: "",
    name: "",
    newPhone: "",
    companyId: "",
    confirmLoading: false,
    companyName: "",
    admin: false,
    invite: "mobile",
    total: "",
    totalSub : "",
    page: 1,
    pageSub : 1,
    country: "",
    filter: [],
    sortText: "",
    sortValue: "",
    selectedRowKeys: [],
    selectedRows: [],
    groupId: 0,
    groupType: "",
    title: "",
    originalTitle: "",
    dob: "",
    dateOfHire: "",
    originalDateOfHire: "",
    supervisor: [],
    subordinate: [],
    selectEmployeeVisible: false,
    addSub: false,
    addSup: false,
    originalSubordinate: [],
    originalSupervisor: []
  };

  countryCode: string = "";

  componentDidMount() {
    this.setState({ loading: true, columns: [] })
    const { idToken } = this.props;
    const currentCompany = JSON.parse(localStorage.getItem("current_company"));
    this.getCompanyDetails(idToken, currentCompany);
  }

  componentWillReceiveProps(prevProp, prevState) {
    const { currentCompany, idToken, dateOfHire, title } = this.props;
    if (prevProp.currentCompany !== currentCompany) {
      this.setState({
        loading: true,
      })
      this.getCompanyDetails(idToken, currentCompany);
    }
  }

  getCompanyDetails = (idToken, currentCompany) => {
    this.setState({
      companyId: currentCompany.id,
      companyName: currentCompany.name,
    })

    let page = 1;
    let value = "";
    getUser(idToken, value, page, this.props.currentCompany.id, this.state.sortValue).then(res => {
      this.setState({
        result: res.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    })

    getOrganisation(idToken, currentCompany.id).then(res => {
      this.setState({
        filter: res
      })
    }).catch(err => {
      console.log(err);
    })
  }

  getUserList = () => {
    let page = 1;
    let value = "";
    getUser(this.props.idToken, value, page, this.props.currentCompany.id, this.state.sortValue).then(res => {
      this.setState({
        result: res.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    })
  }

  onSearch = (page, type) => {
    const { idToken } = this.props;
    const { companyId, value, sortValue } = this.state;

    this.setState({ loading: true }, () =>
      getUser(idToken, value, page, companyId, sortValue).then(res => {
        if (type == 'user'){
          this.setState({
            result: res.data,
            loading: false,
            page: page,
            total: res.total
          })
        }
        else{
          this.setState({
            resultSub: res.data,
            pageSub: page,
            totalSub: res.total,
            loading: false,
            value: "",
          })
        }
        
      }).catch(err => {
        console.log(err);
      }))
  };

  // Edit modal
  showEditModal = (record) => {
    let phone = "";
    const { idToken } = this.props;

    this.setState({
      editVisible: true,
      loading: true,
      value : ""
    })

    if (record.phone !== null) {
      phone = record.phone;
    }

    getEmployeeDetailsByCompany(idToken, record.user_id, record.company_id).then(res => {
      this.setState({
        dateOfHire: res.data.date_of_hire === null ? '' : res.data.date_of_hire,
        originalDateOfHire: res.data.date_of_hire === null ? '' :  res.data.date_of_hire,
        title: res.data.title,
        originalTitle: res.data.title,
        supervisor: res.data.immediate_superiors,
        originalSupervisor: res.data.immediate_superiors,
        subordinate: res.data.immediate_subordinates,
        originalSubordinate: res.data.immediate_subordinates,
        user_id: record.user_id,
        name: record.name,
        first_name: record.first_name,
        last_name: record.last_name,
        phone: phone,
        email: record.email,
        originalEmail: record.email,
        country: record.country_code,
        dob: record.dob === null ? '' : record.dob,
        loading: false
      });
    }).catch(err => {
      console.log(err);
    });
  }

  handleEditCancel = () => {
    this.setState({
      editVisible: false,
      selectEmployeeVisible: false,
      addSub: false,
      addSup: false
    });
  }
  handleEditOk = () => {
    const { idToken } = this.props;
    const { user_id, first_name, last_name, email, phone, companyId, originalEmail, dob, title, dateOfHire, originalTitle, originalDateOfHire, supervisor, originalSupervisor, subordinate, originalSubordinate } = this.state;
    this.setState({ confirmLoading: true });

    var data = {
      first_name: first_name,
      last_name: last_name,
    }
    dob === '' ? null : data["dob"] = dob;
    if (originalEmail !== email) {
      data["email"] = email;
      data["email_verified"] = 0;
    }

    var details = {
      title: title,
    }
    dateOfHire !== '' ? details["date_of_hire"] = dateOfHire : null;

    // if (originalSupervisor !== supervisor){
    //   console.log("here");
    var newSupervisor = [];
    for (let index = 0; index < supervisor.length; index++) {
      newSupervisor[newSupervisor.length] = supervisor[index]['superior_id'] ? supervisor[index]['superior_id'] : supervisor[index]['user_id']
    }

    setSupervisor(idToken, user_id, companyId, newSupervisor).then(res => {
    }).catch(async error => {
      const err = await error;
      notification.error({
        message: "Error",
        description: err.error.message,
      });
    });
    //}

    // if (originalSubordinate !== subordinate){
    var newSubordinate = [];
    for (let index = 0; index < subordinate.length; index++) {
      newSubordinate[newSubordinate.length] = subordinate[index]['subordinate_id'] ? subordinate[index]['subordinate_id'] : subordinate[index]['user_id']
    }

    setSubordinate(idToken, user_id, companyId, newSubordinate).then(res => {
    }).catch(async error => {
      const err = await error;
      notification.error({
        message: "Error",
        description: err.error.message,
      });
    });
    //}

    // Edit user details
    editUser(idToken, user_id, data, companyId).then(res => {
        // Edit company user details
        editEmployeeDetailsByCompany(idToken, user_id, companyId, details).then(res => {
          message.success("Succesfully edited");
          let page = this.state.page;
          this.onSearch(page,'user');
          this.setState({
            confirmLoading: false,
            editVisible: false
          });
        }).catch(async error => {
          const err = await error;
          notification.error({
            message: "Error",
            description: err.error.message,
          });
        });
    }).catch(async error => {
      const err = await error;
      if (err.error.errors != null) {
        if (err.error.errors.email !== null) {
          notification.error({
            message: err.error.message,
            description: err.error.errors.email,
          });
        }
        if (err.error.errors.phone !== null) {
          notification.error({
            message: err.error.message,
            description: err.error.errors.phone,
          });
        }
      }
      else {
        notification.error({
          message: "Error",
          description: err.error.message,
        });
      }
    })

  }

  // Delete modal
  showDelModal = (e) => {
    e.preventDefault();
    this.setState({
      user_id: e.target.id,
      first_name: e.target.name,
      delVisible: true,
    })
  }
  handleDelCancel = () => {
    this.setState({ delVisible: false });
  }
  handleDelOk = () => {
    const { idToken } = this.props;
    const { user_id, companyId } = this.state;
    this.setState({ confirmLoading: true });

    deleteUser(idToken, user_id, companyId).then(res => {
      setTimeout(() => {
        this.setState({
          delVisible: false,
          confirmLoading: false,
          value: "",
        });
      }, 2000);
      message.success(`Employee has been removed`);
    }).catch(async error => {
      const err = await error;
      notification.error({
        message: 'Error',
        description: err.error.message,
      });
      this.setState({
        confirmLoading: false,
      });
    })
    let page = this.state.page;
    this.onSearch(page,'user');
  }

  // Add modal
  showAddModal = (e) => {
    this.setState({
      user_id: e.target.id,
      first_name: e.target.name,
      addVisible: true,
    })
  }
  handleAddCancel = () => {
    this.setState({ addVisible: false });
  }
  handleAddOk = () => {
    const { countryCode } = this;
    const { phone, email } = this.state;
    this.setState({ confirmLoading: true });

    let formData;
    if (this.state.invite === "mobile") {
      formData = new FormData()
      formData.append('user[0][phone]', phone);
      formData.append('user[0][country_code]', countryCode);
    }
    if (this.state.invite === "email") {
      formData = new FormData()
      formData.append('user[0][email]', email);
    }

    this.inviteEmployee(formData);
  }

  inviteEmployee = (formData) => {
    const { idToken } = this.props;
    const { companyId } = this.state;

    inviteEmployee(idToken, formData, companyId).then(res => {
      setTimeout(() => {
        this.setState({
          addVisible: false,
          addEmailVisible: false,
          confirmLoading: false,
        });
      }, 2000);
      message.success(res.message);
    }).catch(async error => {
      const err = await error;
      notification.error({
        message: 'Error',
        description: err.error.message,
      });
      this.setState({
        confirmLoading: false,
      });
    })
  }

  changeInviteInput = () => {
    if (this.state.invite === "mobile") {
      this.setState({
        invite: "email"
      });
    }
    if (this.state.invite === "email") {
      this.setState({
        invite: "mobile"
      });
    }
  }

  onSortChange = (value, selectedOptions) => {
    let sort = `&${value[0]}=${value[1]}`;
    this.setState({
      loading: true,
      sortValue: sort,
      sortText: selectedOptions.map(o => o.label).join(', '),
    });

    getUser(this.props.idToken, "", 1, this.props.currentCompany.id, sort).then(res => {
      this.setState({
        result: res.data,
        total: res.total,
        loading: false,
      })
    }).catch(err => {
      console.log(err);
    })
  };

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys, selectedRows });
  };

  showAssignModal = (e) => {
    this.setState({
      assignVisible: true,
    })
  }
  handleAssignCancel = () => {
    this.setState({ assignVisible: false });
  }

  handleAssignOk = () => {
    this.setState({ assignVisible: false });
    const { selectedRows, groupType, groupId } = this.state;
    let users = new FormData()

    for (let i = 0; i < selectedRows.length; i++) {
      users.append(`users[${i}]`, selectedRows[i]['user_id']);
    }

    assignEmployee(this.props.idToken, groupType, groupId, users).then(res => {
      message.success("Succesfully added");
      this.setState({ selectedRowKeys: [] });
    }).catch(err => {
      console.log(err);
    })
  }

  onAssignChange = (value, selectedOptions) => {
    this.setState({
      groupType: selectedOptions[0]['valueAssign'],
      groupId: selectedOptions[1]['value']
    });
  }

  _onPhoneNumberChangeHandler = (status, phone, number) => {
    this.setState({
      phone
    });

    if (this.countryCode !== number.iso2) {
      this.countryCode = number.iso2 && number.iso2.toUpperCase();
    }
  };

  onDOBChange = (date, dateString) => {
    console.log(dateString);
    this.setState({ dob: dateString });
  }

  onDOHChange = (date, dateString) => {
    this.setState({ dateOfHire: dateString });
  }

  removeSubOrSup = (e, key, type) => {
    const { supervisor, subordinate } = this.state;
    if (type == "sub") {
      subordinate.splice(key, 1);
      this.setState({
        subordinate: subordinate
      });
    }
    else if (type == "sup") {
      supervisor.splice(key, 1);
      this.setState({
        supervisor: supervisor
      });
    }
  }

  addSubOrSup = (record) => {
    const { supervisor, subordinate, addSup, addSub } = this.state

    if (addSup) {
      supervisor.push(record);
      this.setState({
        supervisor: supervisor,
        selectEmployeeVisible: false,
        addSup: false,
      })
    }
    else if (addSub) {
      subordinate.push(record);
      this.setState({
        subordinate: subordinate,
        selectEmployeeVisible: false,
        addSub: false,
      })
    }
  };

  render() {
    const { selectedRowKeys, loading, supervisor, subordinate, selectEmployeeVisible } = this.state;
    const { currentCompany } = this.props;

    const columns = [{
      title: 'First Name',
      dataIndex: 'first_name',
      key: 'first_name',
      sorter: (a, b) => {
        a = a.first_name || '';
        b = b.first_name || '';
        return a.localeCompare(b);
      }
    }, {
      title: 'Last Name',
      dataIndex: 'last_name',
      key: 'last_name',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => {
        a = a.last_name || '';
        b = b.last_name || '';
        return a.localeCompare(b);
      }
    }, {
      title: 'Phone',
      dataIndex: 'phone',
      key: 'phone',
    }, {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => {
        a = a.email || '';
        b = b.email || '';
        return a.localeCompare(b);
      }
    }, {
      title: 'Position',
      dataIndex: 'title',
      key: 'title',
    }, {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width : 150,
      render: (text, record) => (
        <span>
          <a onClick={() => this.showEditModal(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={this.showDelModal} id={record.user_id} name={record.name}>Remove</a>
        </span >
      ),
    }];

    const nameColumns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        a = a.name || '';
        b = b.name || '';
        return a.localeCompare(b);
      }
    }, {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <a onClick={() => this.addSubOrSup(record)}>Add</a>
        </span >
      ),
    }];

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };

    const hasSelected = selectedRowKeys.length > 0;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };

    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };

    const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

    let inviteInput;
    let inviteText;
    let inviteType;

    if (this.state.invite === "mobile") {
      inviteType = "mobile number";
      inviteText = "invite via email";
      inviteInput =
        <Form.Item label="Mobile Number">
          <IntlTelInput
            defaultCountry={"my"}
            preferredCountries={["my", "us", "sg"]}
            css={["intl-tel-input", "ant-input-lg"]}
            style={{ width: "100%", zIndex: 4 }}
            telInputProps={{ style: { border: "1px solid #e9e9e9", width: "100%" } }}
            placeholder="Mobile Number"
            onPhoneNumberChange={this._onPhoneNumberChangeHandler}
            onPhoneNumberBlur={this._onPhoneNumberBlurHandler}
            separateDialCode={true}
            format={false}
          />
        </Form.Item>
    }
    else {
      inviteType = "email";
      inviteText = "invite via mobile";
      inviteInput =
        <Form.Item label="Email">
          <Input onChange={(e) => { this.setState({ email: e.target.value }) }} />
        </Form.Item>
    }

    if (currentCompany.is_admin === 1) {
      return (
        < LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent">
            <Row type="flex" justify="start">
              <Col md={12} sm={12} xs={24}>
                <Search
                  placeholder="Search with name/email"
                  onSearch={value => this.setState({ value: value }, () => this.onSearch(1,'user'))}
                  style={{
                    marginTop: "10px",
                    marginBottom: "20px",
                    fontSize: "20px"
                  }}
                />
              </Col>
              <Col md={12} sm={12} xs={24}>
                <Button type="primary" icon="user-add" onClick={this.showAddModal}
                  style={{
                    marginTop: "10px",
                    marginBottom: "20px",
                    marginLeft: "25px",
                    marginRight: "25px",
                  }}
                >Invite Employee</Button>
                <Cascader options={this.state.filter} onChange={this.onSortChange}>
                  <Button type="primary" icon="filter"
                    style={{
                      marginTop: "10px",
                      marginBottom: "20px",
                      marginLeft: "25px",
                      marginRight: "25px",
                    }}
                  >Filter Employee</Button>
                </Cascader>
                <Button type="primary" icon="user-group-add" onClick={this.showAssignModal} disabled={!hasSelected}
                  loading={loading}
                  style={{
                    marginTop: "10px",
                    marginBottom: "20px",
                    marginLeft: "25px",
                    marginRight: "25px",
                  }}
                >Assign</Button>
              </Col>
            </Row>
            <TableWrapper
              dataSource={this.state.result}
              columns={columns}
              loading={this.state.loading}
              className="isoSearchableTable"
              rowSelection={rowSelection}
              scroll={{ x: 1300 }}
              pagination={{
                showQuickJumper: true,
                pageSize: 10,
                total: this.state.total,
                current: this.state.page,
                showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
                onChange: (page, pageSize) => {
                  this.setState({ loading: true }, () => this.onSearch(page, 'user'));
                }
              }}
            />
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>

            <Modal
              visible={this.state.addVisible}
              title="Invite Employee"
              onOk={this.handleAddOk}
              onCancel={this.handleAddCancel}
              maskClosable={false}
              rowSelection={rowSelection}
              footer={[
                <Button key="back" onClick={this.handleAddCancel}>Cancel</Button>,
                <Button key="submit" type="primary" loading={this.state.confirmLoading} onClick={this.handleAddOk}>Invite</Button>,
              ]}>
              <p>Please enter employee's {inviteType} or <a onClick={this.changeInviteInput}>{inviteText}</a></p>
              <Form onSubmit={this.handleSubmit}>
                {inviteInput}
              </Form>
            </Modal>

            <Modal
              visible={this.state.editVisible}
              title="Edit employee's profile"
              onOk={this.handleEditOk}
              onCancel={this.handleEditCancel}
              maskClosable={false}
              footer={[
                <Button key="back" onClick={this.handleEditCancel}>Cancel</Button>,
                <Button key="submit" type="primary" loading={this.state.confirmLoading} onClick={this.handleEditOk}>Save</Button>,
              ]}>
              {selectEmployeeVisible == false ? (
                <div>
                  <Spin indicator={antIcon} spinning={this.state.loading} />
                  <Form onSubmit={this.handleEditOk} layout="horizontal">
                    <Form.Item label="First Name" {...formItemLayout}>
                      <Input value={this.state.first_name} onChange={(e) => { this.setState({ first_name: e.target.value }) }} />
                    </Form.Item>
                    <Form.Item label="Last Name" {...formItemLayout} >
                      <Input value={this.state.last_name} onChange={(e) => { this.setState({ last_name: e.target.value }) }} />
                    </Form.Item>
                    <Form.Item label="Email" {...formItemLayout} >
                      <Input value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }) }} />
                    </Form.Item>
                    <Form.Item label="D.O.B" {...formItemLayout} >

                      <DatePicker value={this.state.dob == ''? '' : moment(this.state.dob, dateFormat)} format={dateFormat} onChange={this.onDOBChange} />
                    </Form.Item>
                    <Form.Item label="Enrollment Date" {...formItemLayout} >
                      <DatePicker value={this.state.dateOfHire == ''? '' : moment(this.state.dateOfHire, dateFormat)}  format={dateFormat} onChange={this.onDOHChange} />
                    </Form.Item>
                    <Form.Item label="Position" {...formItemLayout} >
                      <Input value={this.state.title} onChange={(e) => { this.setState({ title: e.target.value }) }} />
                    </Form.Item>
                    <Form.Item label="Supervisor" {...formItemLayout} >
                      {supervisor.map((e, key) => {
                        return <Row>
                          <Col md={22} sm={22} xs={22}>
                            <Input disabled={true} value={e.name} />
                          </Col>
                          <Col md={2} sm={2} xs={2}>
                            <Button type="primary" icon="close" onClick={() => { this.removeSubOrSup(e, key, "sup") }}></Button>
                          </Col>
                        </Row>
                      })}
                      <a onClick={() => { this.setState({ selectEmployeeVisible: true, addSup: true,value: "" }); this.onSearch(1, 'sup') }}>Add Supervisor</a>
                    </Form.Item>
                    <Form.Item label="Subordinate" {...formItemLayout} >
                      {subordinate.map((e, key) => {
                        return <Row>
                          <Col md={22} sm={22} xs={22}>
                            <Input disabled={true} value={e.name} />
                          </Col>
                          <Col md={2} sm={2} xs={2}>
                            <Button type="primary" icon="close" onClick={() => { this.removeSubOrSup(e, key, "sub") }}></Button>
                          </Col>
                        </Row>
                      })}
                      <a onClick={() => { this.setState({ selectEmployeeVisible: true, addSub: true, value: "" }); this.onSearch(1, 'sub') }}>Add Subordinate</a>
                    </Form.Item>
                  </Form>
                </div>
              ) : selectEmployeeVisible === true ? (
                <div>
                  <Row style={{
                    marginTop: "10px",
                    marginBottom: "20px",
                    fontSize: "20px"
                  }}>
                    <Col md={4} sm={4} xs={4}>
                      <Button type="primary" onClick={() => { this.setState({ selectEmployeeVisible: false }) }}>Back</Button>
                    </Col>
                    <Col md={20} sm={20} xs={20}>
                      <Search
                        placeholder="Search with name/email"
                        onSearch={value => this.setState({ value: value }, () => this.onSearch(1,'sub'))}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <TableWrapper
                      dataSource={this.state.resultSub}
                      columns={nameColumns}
                      loading={this.state.loading}
                      className="isoSearchableTable"
                      pagination={{
                        showQuickJumper: true,
                        pageSize: 10,
                        total: this.state.totalSub,
                        current: this.state.pageSub,
                        showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
                        onChange: (page, pageSize) => {
                          this.setState({ loading: true }, () => this.onSearch(page,'sub'));
                        }
                      }}
                    />
                  </Row>
                </div>
              ) : null}

            </Modal>

            <Modal
              visible={this.state.delVisible}
              title="Remove employee"
              onOk={this.handleDelOk}
              onCancel={this.handleDelCancel}
              maskClosable={false}
              footer={[
                <Button key="back" onClick={this.handleDelCancel}>Cancel</Button>,
                <Button key="submit" type="primary" loading={this.state.confirmLoading} onClick={this.handleDelOk}>Remove</Button>,
              ]}>
              <p>Are you sure you want to remove employee from {this.state.companyName}?</p>
            </Modal>

            <Modal
              visible={this.state.assignVisible}
              title="Assign Employee"
              onOk={this.handleAssignOk}
              onCancel={this.handleAssignCancel}
              maskClosable={false}
              rowSelection={rowSelection}
              footer={[
                <Button key="back" onClick={this.handleAssignCancel}>Cancel</Button>,
                <Button key="submit" type="primary" loading={this.state.confirmLoading} onClick={this.handleAssignOk}>Assign</Button>,
              ]}>
              <Row>
                <p>Assign to:</p>
                <Cascader options={this.state.filter} onChange={this.onAssignChange}></Cascader>
              </Row>
            </Modal>

          </TableDemoStyle>
        </LayoutContentWrapper >
      );
    }
    else {
      return (
        < LayoutContentWrapper >
          <TableDemoStyle className="isoLayoutContent">
            <p>You do not have access to view Employee page. Please ask permission from your company's admin </p>
          </TableDemoStyle>
        </ LayoutContentWrapper>
      );
    }
  }
}

export default connect(
  state => ({
    idToken: state.Auth.get("idToken"),
    currentCompany: state.Company.get("current_company"),
    companies: state.Auth.get("companies"),
  }),
)(Employee);

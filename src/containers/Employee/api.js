import { baseUrl } from "../../config";

export const getUser = (idToken, value, page, companyId,sort) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/users?limit=10&page=${page}&query=${value}${sort}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json",
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const deleteUser = (idToken, user_id, company_id) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${company_id}/users/${user_id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json",
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

// export const editUser = (idToken, user_id, first_name, last_name, phone, email, country_code, company_id) => {
  export const editUser = (idToken, user_id, data, company_id) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(`${baseUrl}/v1/users?user=${user_id}&company=${company_id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
        body: JSON.stringify(data)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const inviteEmployee = (idToken, formData, companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/invite?company_id=${companyId}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
        body: formData,
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onCheckAdmin = (idToken, companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/access?access_privilege_id=1`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getOrganisation = (idToken, companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/hub/companies/${companyId}/organisation`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const assignEmployee = (idToken, groupType, groupId, users) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/${groupType}/${groupId}/users`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
        body : users 
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getEmployeeDetailsByCompany = (idToken, userId , companyId) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/users/${userId}/companies/${companyId}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const editEmployeeDetailsByCompany = (idToken, userId , companyId, data) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/users/${userId}/companies/${companyId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
        body: JSON.stringify(data)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const setSupervisor = (idToken, userId , companyId, data) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    var body = {
      users : data,
      sub : userId
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/hierarchy/sups`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
        body: JSON.stringify(body)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const setSubordinate = (idToken, userId , companyId, data) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    var body = {
      subs : data,
      sup : userId
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/hierarchy/subs`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v3+json"
        },
        body: JSON.stringify(body)
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};
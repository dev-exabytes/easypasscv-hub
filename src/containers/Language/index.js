import React, { Component } from "react";
//import Tabs, { TabPane } from "../../../components/uielements/tabs";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import TableDemoStyle from "./demo.style";
import { connect } from "react-redux";
import TableWrapper from "./easyworkTable.style";
import IsoSelectBox, { SelectOption } from "../../components/uielements/select";
import actions from "../../redux/language/actions";
import { Input, Popconfirm, Divider, Switch, Form } from "antd";

const {
  groupSearch,
  langSearch,
  langsSearch,
  langsStatusChanged,
  langEdit,
  langDelete
} = actions;
const FormItem = Form.Item;

const EditableCell = ({ editable, value, onChange, onSubmit }) => (
  <div>
    {editable ? (
      <Input
        style={{ margin: "-5px 0" }}
        value={value}
        onChange={e => onChange(e.target.value)}
        onPressEnter={e => onSubmit(e.target.value)}
      />
    ) : (
      value
    )}
  </div>
);

class Language extends Component {
  state = {
    groups: [],
    selectedGroup: 0,
    dataList: [],
    columns: [],
    loading: false,
    queries: [],
    selectedQuery: 1,
    langs: [],
    switchLoading: []
  };
  cacheData = this.state.dataList.map(item => ({ ...item }));

  componentWillReceiveProps(nextProps) {
    if (nextProps.Language.groups) {
      this.setState({
        groups: nextProps.Language.groups,
        selectedGroup: nextProps.Language.search_group
      });
    }
    if (nextProps.Language.langs) {
      this.setState({
        langs: nextProps.Language.langs
      });
    }
    if (nextProps.Language.lang_id) {
      const switchLoading = this.state.switchLoading;
      switchLoading[nextProps.Language.lang_id] = false;
      this.setState({
        switchLoading
      });
    }
    if (
      nextProps.Language.columns.length > 0 &&
      nextProps.Language.result !== this.props.Language.result
    ) {
      let columns = nextProps.Language.columns.map(column => {
        if (column.key !== "key") {
          column.render = (text, record) =>
            this.renderColumns(text, record, column.dataIndex);
        }
        return column;
      });
      this.setState(
        {
          dataList: nextProps.Language.result,
          columns: columns.concat([
            {
              title: "Action",
              dataIndex: "operation",
              key: "operation",
              fixed: "right",
              width: 100,
              render: (text, record) => {
                const { editable } = record;
                return (
                  <div className="editable-row-operations">
                    {editable ? (
                      <span>
                        {/* <a onClick={() => this.save(record.key)}>Save</a> */}
                        <Popconfirm
                          title="Sure to cancel?"
                          onConfirm={() => this.cancel(record.key)}
                        >
                          <a>Cancel</a>
                        </Popconfirm>
                      </span>
                    ) : (
                      <a onClick={() => this.edit(record.key)}>Edit</a>
                    )}
                    <Divider type="vertical" />
                    <Popconfirm
                      title="Sure to delete?"
                      onConfirm={() => this.delete(record.key)}
                    >
                      <a>Delete</a>
                    </Popconfirm>
                  </div>
                );
              }
            }
          ]),
          loading: false
        },
        () => (this.cacheData = this.state.dataList.map(item => ({ ...item })))
      );
    }
  }

  componentDidMount() {
    this.onLangsSearch();
    this.onGroupSearch();
  }

  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
        onSubmit={value => this.handleSubmit(value, record.key, column)}
      />
    );
  }

  handleChange(value, key, column) {
    const newData = [...this.state.dataList];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ dataList: newData });
    }
  }

  handleSubmit(value, key, column) {
    if (value.length > 0) {
      const newData = [...this.state.dataList];

      const target = newData.filter(item => key === item.key)[0];
      if (target) {
        delete target.editable;
        target[column] = value;
        this.onTransChanged(value, key, column, this.state.selectedGroup);
        this.cacheData = newData.map(item => ({ ...item }));
      }
    } else {
      this.cancel(key);
    }
  }

  edit(key) {
    const newData = [...this.state.dataList];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target.editable = true;
      this.setState({ dataList: newData });
    }
  }
  save(value, key, column) {
    if (value.length > 0) {
      const newData = [...this.state.dataList];

      const target = newData.filter(item => key === item.key)[0];
      if (target) {
        delete target.editable;
        this.setState({ dataList: newData });
        this.cacheData = newData.map(item => ({ ...item }));
      }
    } else {
      this.cancel(key);
    }
  }

  cancel(key) {
    const newData = [...this.state.dataList];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
      delete target.editable;
      this.setState({ dataList: newData });
    }
  }
  delete = key => {
    const dataList = [...this.state.dataList];
    this.setState({ dataList: dataList.filter(item => item.key !== key) }, () =>
      this.props.langDelete(key, this.state.selectedGroup)
    );
  };

  onTransChanged = (value, key, column, selectedGroup) => {
    this.props.langEdit(value, key, column, selectedGroup);
  };

  onGroupSearch = () => {
    this.props.groupSearch();
  };

  onLangsSearch = () => {
    this.props.langsSearch();
  };

  onSearch = value => {
    this.setState({ loading: true }, () => this.props.langSearch(value));
  };

  groupChange(value) {
    this.setState(
      {
        selectedGroup: value,
        loading: true
      },
      () => this.onSearch(this.state.selectedGroup)
    );
  }

  langsStatusChange(lang, id, checked) {
    const switchLoading = this.state.switchLoading;
    switchLoading[id] = true;

    this.setState(
      {
        switchLoading
      },
      () => this.props.langsStatusChanged(lang, id, checked)
    );
  }

  render() {
    const { groups, langs } = this.props.Language;
    return (
      <LayoutContentWrapper>
        <TableDemoStyle className="isoLayoutContent">
          <div style={{ marginBottom: "40px" }}>
            <Form layout="inline">
              {langs.map((item, i) => (
                <FormItem label={item.description} key={item.id}>
                  <Switch
                    checked={Boolean(item.status)}
                    onChange={this.langsStatusChange.bind(
                      this,
                      item.lang,
                      item.id
                    )}
                    loading={
                      this.state.switchLoading &&
                      this.state.switchLoading[item.id]
                        ? this.state.switchLoading[item.id]
                        : false
                    }
                    disabled={item.id === 1 ? true : false}
                  />
                </FormItem>
              ))}
            </Form>
          </div>
          <div style={{ marginBottom: "40px" }}>
            <IsoSelectBox
              style={{ width: "100%" }}
              mode="single"
              placeholder="Select a group to edit"
              onChange={this.groupChange.bind(this)}
            >
              <SelectOption value="Select a group to edit" disabled>
                Select a group to edit
              </SelectOption>
              {groups.map((item, i) => (
                <SelectOption key={item} value={item}>
                  {item.charAt(0).toUpperCase() + item.slice(1)}
                </SelectOption>
              ))}
            </IsoSelectBox>
          </div>
          <TableWrapper
            loading={this.state.loading}
            rowKey={record => record.key}
            dataSource={this.state.dataList}
            columns={this.state.columns}
            className={["isoSimpleTable"]}
            expandedRowRender={record => (
              <p style={{ margin: 0, color: "#43C0AC" }}>
                {record.description}
              </p>
            )}
            scroll={{ x: 500 }}
          />
        </TableDemoStyle>
      </LayoutContentWrapper>
    );
  }
}

function mapStateToProps(state) {
  return { Language: state.Language.toJS() };
}

export default connect(mapStateToProps, {
  groupSearch,
  langSearch,
  langsSearch,
  langsStatusChanged,
  langEdit,
  langDelete
})(Language);

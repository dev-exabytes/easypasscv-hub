import styled from "styled-components";
import { palette } from "styled-theme";

const TableDemoStyle = styled.div`
  .ant-tabs-content {
    margin-top: 40px;
  }

  .ant-tabs-nav {
    > div {
      color: ${palette("secondary", 2)};

      &.ant-tabs-ink-bar {
        background-color: ${palette("primary", 0)};
      }

      &.ant-tabs-tab-active {
        color: ${palette("primary", 0)};
      }
    }
  }
  .editable-row-operations a {
    margin-right: 8px;
  }
  .refresh-btn {
    float: right;
    margin-bottom: 8px;
  }
  .custom-filter-dropdown {
    padding: 8px;
    border-radius: 6px;
    background: #fff;
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.2);
  }

  .custom-filter-dropdown input {
    width: 130px;
    margin-right: 8px;
  }

  .highlight {
    color: #f50;
  }
`;

export default TableDemoStyle;

const chartData = {
    "message": "Organization chart is found.",
    "data": [{
            "level": 1,
            "name": "Super Admin",
            "avatar": null,
            "job_title": "",
            "removed": false,
            "first_level_sub_count": 0,
            "expand": false,
            "id": 2,
            "hasChild": true,
            "children": [{
                "level": 2,
                "name": "Derrick Ooi Ruen Shin",
                "avatar": "https://lh6.googleusercontent.com/-ZQz2tYVP1W8/AAAAAAAAAAI/AAAAAAAAABQ/RdjY3H8NF3g/s120/photo.jpg",
                "job_title": "OOPS",
                "removed": false,
                "first_level_sub_count": 0,
                "expand": false,
                "id": 3,
                "hasChild": true,
                "children": [{
                        "level": 3,
                        "name": "Chan",
                        "avatar": null,
                        "job_title": "",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 4,
                        "hasChild": false
                    },
                    {
                        "level": 3,
                        "name": "Shiiiiinn Tan",
                        "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/519d85fd174dcde880d1ac6ca20bf7d3.jpg",
                        "job_title": "Ddb",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 5,
                        "hasChild": false
                    }
                ]
            }]
        },
        {
            "level": 1,
            "name": "886 Shu Yih",
            "avatar": null,
            "job_title": "",
            "removed": false,
            "first_level_sub_count": 0,
            "expand": false,
            "id": 6,
            "hasChild": true,
            "children": [{
                "level": 2,
                "name": "Derrick Ooi Ruen Shin",
                "avatar": "https://lh6.googleusercontent.com/-ZQz2tYVP1W8/AAAAAAAAAAI/AAAAAAAAABQ/RdjY3H8NF3g/s120/photo.jpg",
                "job_title": "OOPS",
                "removed": false,
                "first_level_sub_count": 0,
                "expand": false,
                "id": 7,
                "hasChild": true,
                "children": [{
                        "level": 3,
                        "name": "Chan",
                        "avatar": null,
                        "job_title": "",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 8,
                        "hasChild": false
                    },
                    {
                        "level": 3,
                        "name": "Shiiiiinn Tan",
                        "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/519d85fd174dcde880d1ac6ca20bf7d3.jpg",
                        "job_title": "Ddb",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 9,
                        "hasChild": false
                    }
                ]
            }]
        },
        {
            "level": 1,
            "name": "Izzati Lim",
            "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/8c76d900f99bbb49581f0019a66fa932.jpg",
            "job_title": "",
            "removed": false,
            "first_level_sub_count": 0,
            "expand": false,
            "id": 10,
            "hasChild": true,
            "children": [{
                "level": 2,
                "name": "Derrick Ooi Ruen Shin",
                "avatar": "https://lh6.googleusercontent.com/-ZQz2tYVP1W8/AAAAAAAAAAI/AAAAAAAAABQ/RdjY3H8NF3g/s120/photo.jpg",
                "job_title": "OOPS",
                "removed": false,
                "first_level_sub_count": 0,
                "expand": false,
                "hasChild": true,
                "children": [{
                        "level": 3,
                        "name": "Chan",
                        "avatar": null,
                        "job_title": "",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 11,
                        "hasChild": false
                    },
                    {
                        "level": 3,
                        "name": "Shiiiiinn Tan",
                        "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/519d85fd174dcde880d1ac6ca20bf7d3.jpg",
                        "job_title": "Ddb",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 12,
                        "hasChild": false
                    }
                ]
            }]
        },
        {
            "level": 1,
            "name": "Grace",
            "avatar": null,
            "job_title": "T",
            "removed": false,
            "first_level_sub_count": 0,
            "expand": false,
            "id": 13,
            "hasChild": true,
            "children": [{
                "level": 2,
                "name": "Derrick Ooi Ruen Shin",
                "avatar": "https://lh6.googleusercontent.com/-ZQz2tYVP1W8/AAAAAAAAAAI/AAAAAAAAABQ/RdjY3H8NF3g/s120/photo.jpg",
                "job_title": "OOPS",
                "removed": false,
                "first_level_sub_count": 0,
                "expand": false,
                "id": 14,
                "hasChild": true,
                "children": [{
                        "level": 3,
                        "name": "Chan",
                        "avatar": null,
                        "job_title": "",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 15,
                        "hasChild": false
                    },
                    {
                        "level": 3,
                        "name": "Shiiiiinn Tan",
                        "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/519d85fd174dcde880d1ac6ca20bf7d3.jpg",
                        "job_title": "Ddb",
                        "removed": false,
                        "first_level_sub_count": 0,
                        "expand": false,
                        "id": 16,
                        "hasChild": false
                    }
                ]
            }]
        },
        {
            "level": 1,
            "name": "Shu Yih",
            "avatar": null,
            "job_title": "",
            "removed": false,
            "first_level_sub_count": 0,
            "expand": false,
            "id": 17,
            "hasChild": true,
            "children": [{
                "level": 2,
                "name": "Shiiiiinn Tan",
                "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/519d85fd174dcde880d1ac6ca20bf7d3.jpg",
                "job_title": "Ddb",
                "removed": false,
                "first_level_sub_count": 0,
                "expand": false,
                "id": 18,
                "hasChild": false
            }]
        },
        {
            "level": 1,
            "name": "Anthonyc",
            "avatar": "https://easywork-dev.s3.ap-southeast-1.amazonaws.com/public/avatars/f99c3337edc87314131edf9652345ea3.jpg",
            "job_title": "",
            "removed": false,
            "first_level_sub_count": 0,
            "expand": false,
            "id": 19,
            "hasChild": true,
            "children": [{
                "level": 2,
                "name": "Chan",
                "avatar": null,
                "job_title": "",
                "removed": false,
                "first_level_sub_count": 0,
                "expand": false,
                "id": 20,
                "hasChild": false
            }]
        }
    ]
}

export default chartData

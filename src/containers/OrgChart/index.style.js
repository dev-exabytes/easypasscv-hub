import styled from 'styled-components'
import {
  palette
} from 'styled-theme'

const Style = styled.div `
  .zoomBtn{
    flex: none;
    height:30px;
    width: 30px;
    border-radius:20px;
    background-color: ${palette('ew', 6)};
    color:${palette('ew', 1)};
    border:none;
    margin-top: 5px;
    margin-right: 5px;
    padding-top: 8px;
  }

  #zoomBtnDiv {
    display: flex;
    justify-content: flex-end;
  }

  .org-chart-search {
    width: 40%;
  }

  .org-chart-filter {
    width: 40%;
  }
`

export default Style

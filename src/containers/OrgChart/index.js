import React, {
  Component
} from 'react';
import './ChartComponent.css'
import Style from './index.style'
import { Icon, Input, Spin } from 'antd'
import { connect } from 'react-redux'
import OrgChartComponent from './OrgChart'
import orgChartActions from '../../redux/orgchart/actions'
import { baseUrl } from "../../config"

const { getChart } = orgChartActions

class ChartComponent extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isFetching: true,
      filter: []
    }

  }

  componentDidMount() {

    const currentCompanyId = JSON.parse(localStorage.getItem('current_company'))['id']
    const bearerToken = `Bearer ${localStorage.getItem('id_token')}`
    const fetchUrl = `${baseUrl}/v1/companies/${currentCompanyId}/hierarchy/chart`

    fetch(fetchUrl, {
      headers: {
        'Authorization': bearerToken,
        Accept: "application/x.app.v2+json",
        "Access-Control-Allow-Origin": "http://localhost:3005", mode: 'no-cors'
      }
    })
    .then(response => response.json())
    .then(response => {
      this.setState({isFetching: false})
      OrgChartComponent.OrgChart.create(this.props, response)
    })

  }

  render() {
    const {isFetching} = this.state
    return (
      <Style>
        <div id="orgchartDiv">
          {
            isFetching ? (
              <div>
                <div id="zoomBtnDiv">
                  <Icon type="pushpin" className="zoomBtn" id="myselfBtn"/>
                  <Icon type="zoom-in" className="zoomBtn" id="zoomInBtn"/>
                  <Icon type="zoom-out" className="zoomBtn" id="zoomOutBtn"/>
                </div>
                <div className="noResultsDiv">
                  <Spin tip="Loading organization chart..."/>
                </div>
              </div>

            ) : (
              <div>
              <div className="department-information">
                <div className="dept-name">
                  dept name
                </div>
                <div className="dept-emp-count">
                  dept emp test, this is department emp
                </div>
                <div className="dept-description">
                  dept description test, this is department description
                </div>
              </div>
              <div id="zoomBtnDiv">
                <Icon type="pushpin" className="zoomBtn" id="myselfBtn"/>
                <Icon type="zoom-in" className="zoomBtn" id="zoomInBtn"/>
                <Icon type="zoom-out" className="zoomBtn" id="zoomOutBtn"/>
              </div>
              <div id="orgChart">
              </div>
              </div>
            )
          }
        </div>
      </Style>
    )
  }

}

function mapStateToProps(state) {
  const { chartData, chartConfig } = state.OrgChart.toJS()
  return {
    chartData,
    chartConfig
  }
}

export default connect(mapStateToProps,
  {
    getChart
  })(ChartComponent)

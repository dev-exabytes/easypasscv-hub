import * as d3 from 'd3'
import { saveAs } from 'file-saver'

let params = {
  selector: "#orgChart",
  dataLoadUrl: "https://raw.githubusercontent.com/bumbeishvili/Assets/master/Projects/D3/Organization%20Chart/redesignedChartLongData.json",
  chartWidth: window.innerWidth - 40,
  chartHeight: window.innerHeight - 40,
  funcs: {
    showMySelf: null,
    search: null,
    closeSearchBox: null,
    clearResult: null,
    findInTree: null,
    reflectResults: null,
    departmentClick: null,
    back: null,
    toggleFullScreen: null,
    locate: null
  },
  data: null
}

const OrgChart = {
  attrs: null,
  dynamic: null,
  tree: null,
  zoomBehaviours: null,
  svg: null,
  tooltip: null,
  lineFunc: null,
  state: null,
  u_opts: null,
  viewerWidth: null,
  viewerHeight: null,
  timeoutID: 0,
  counter: 0,
  check: 0,
  depthArray: []
};

OrgChart.create = (props, data) => {

  OrgChart.state = props.chartConfig
  OrgChartHelper.initChart(data)
  OrgChart.viewerWidth = window.screen.width
  OrgChart.viewerHeight = window.screen.height

}

class OrgChartHelper {

  /**
   * Function to retrieve tree node using its id
   */
  static getNodeWithId(node, idToCheck) {

    if (node.children) {

      if (node.id === idToCheck) {
        OrgChartHelper.centerNode(node)
        const nodeToAnimate = document.getElementById(`rect-${node.id}`)
        nodeToAnimate.classList.add("border-glow")
        const animationTime = 8000 // animation time in ms
        setTimeout(() => {
          nodeToAnimate.classList.remove("border-glow")
        }, animationTime)
        //nodeToAnimate.classList.remove("border-glow")
        // d3.select(`g#${node.id}`)
        //   .style("color", "red")
        return
      }


      for (let i = 0; i < node.children.length; i++) {
        this.getNodeWithId(node.children[i], idToCheck)
      }

    } else {

      if (node.id === idToCheck) {
        OrgChartHelper.centerNode(node)
      }
      return

    }

  }

  static loadChilds(actualElement, successFunction) {

    const curNodeData = OrgChartHelper.getNodeWithId(actualElement.id)
    successFunction(curNodeData)

  }

  static recursiveChild(childNode, id) {

    const defaultUserAvatar = 'https://raw.githubusercontent.com/bumbeishvili/Assets/master/Projects/D3/Organization%20Chart/general.jpg'

    if (childNode.subordinates || childNode.children) {

      if (!childNode.children) {
        childNode.children = childNode.subordinates && !childNode.children ? JSON.parse(JSON.stringify(childNode.subordinates)) : undefined
        delete childNode['subordinates']
        if (!childNode.children) delete childNode['children']
      }

      if (!childNode.photo) {
        childNode.photo = childNode.avatar ? childNode.avatar : defaultUserAvatar
        delete childNode['avatar']
      }

      childNode.id = ++id.counter
      childNode.hasChild = Boolean(childNode.children)

      if (childNode.hasChild) {
        for (let i = 0; i < childNode.children.length; i++) {
          this.recursiveChild(childNode.children[i], id)
        }
      }

    } else {

      if (!childNode.photo) {
        childNode.photo = childNode.avatar ? childNode.avatar : defaultUserAvatar
        delete childNode['avatar']
      }

      childNode.id = ++id.counter
      childNode.hasChild = false
      childNode.isLastChild = true
      return

    }

  }

  /**
   * Function to modify chart data for org chart
   * @param {Object} chartData Chart data to be modified for org chart generation
   * @param {Number} counter Counter used to record id of tree nodes
   */
  static modifyChartData(chartData, counter) {

    let newChartData = JSON.parse(JSON.stringify(chartData))
    let modifiedChartData = newChartData.children
    let childID = {
      counter,
      largestDepth: 0
    }

    for (let i = 0; i < modifiedChartData.length; i++) {

      this.recursiveChild(modifiedChartData[i], childID)

    }

    return newChartData

  }

  static handleArrowKeys(e) {
    switch (e.keyCode) {
      // if key downed is the left arrow key
      case 37:
        OrgChartHelper.scrollLeft()
        break

        // if key downed is the right arrow key
      case 39:
        OrgChartHelper.scrollRight()
        break

      default:
        break
    }
  }

  static async initChart(fetchedData) {
    let initialChartData = fetchedData

    const company = JSON.parse(localStorage.getItem('current_company'))
    const companyName = company['name']
    const companyLogo = company['logo_path']

    let data = {
      "level": 0,
      "name": `${companyName}`,
      "job_title": "",
      "removed": false,
      "first_level_sub_count": 0,
      "expand": false,
      "id": 1,
      "uniqueIdentifier": 1,
      "hasChild": true,
      "photo": `${companyLogo}`,
      "children": !(Object.is(initialChartData['data'], null) || Object.is(initialChartData['data'], undefined)) ? [...initialChartData['data']] : []
    }
    data = OrgChartHelper.modifyChartData(data, 1)

    params.data = data
    params.pristinaData = JSON.parse(JSON.stringify(data))
    OrgChart.depthArray.length = 0

    if (Object.is(initialChartData['data'], null) || Object.is(initialChartData['data'], undefined)) {

      d3.select("#orgChart")
        .style("overflow-x", "hidden")

      const noResultsDiv = document.getElementsByClassName("noResultsDiv")

      if (Object.is(noResultsDiv.length, 0)) {
        d3.select('#orgChart')
          .append("div")
          .attr("class", "noResultsDiv")
          .append("p")
          .text("Sorry, no results found")
      }

    } else {
      OrgChartHelper.drawOrganizationChart(params, {
        id: '#orgChart',
        modus: 'line',
        loadFunc: OrgChartHelper.loadChilds,
        data
      })

      // Declare interval variable for speed of
      // zoom in and zoom out
      const intervalTime = 100

      // Add click and click-and-hold event listeners
      // to zoom in and zoom out buttons
      d3.select('#zoomInBtn')
        .on('click', () => {
          sessionStorage.setItem('zoomAction', 'zoom-in')
          OrgChartHelper.zoomClick()
          sessionStorage.removeItem('zoomAction')
        })
        .on('mousedown', () => {
          OrgChart.timeoutID = setInterval(() => {
            sessionStorage.setItem('zoomAction', 'zoom-in');
            OrgChartHelper.zoomClick()
          }, intervalTime)
        })
        .on('mouseup', () => {
          sessionStorage.removeItem('zoomAction')
          clearInterval(OrgChart.timeoutID)
        })
        .on('mouseleave', () => {
          sessionStorage.removeItem('zoomAction')
          clearInterval(OrgChart.timeoutID)
        })

      d3.select('#zoomOutBtn')
        .on('click', () => {
          sessionStorage.setItem('zoomAction', 'zoom-out')
          OrgChartHelper.zoomClick()
          sessionStorage.removeItem('zoomAction')
        })
        .on('mousedown', () => {
          sessionStorage.setItem('zoomAction', 'zoom-out')
          OrgChart.timeoutID = setInterval(OrgChartHelper.zoomClick, intervalTime)
        })
        .on('mouseup', () => {
          sessionStorage.removeItem('zoomAction')
          clearInterval(OrgChart.timeoutID)
        })
        .on('mouseleave', () => {
          sessionStorage.removeItem('zoomAction')
          clearInterval(OrgChart.timeoutID)
        })

      // Add scroll left and right to document event listeners
      document.addEventListener('keydown', this.handleArrowKeys)

      d3.select('#myselfBtn')
        .on('click', () => {
          //OrgChartHelper.findMySelf()
          OrgChartHelper.showMySelf()
        })

      // d3.select('#exportBtn')
      //   .on('click', this.exportToImage2)

      d3.select('#exportBtn')
        .on('click', OrgChartHelper.exportToImage)
    }

  }

  static centerNode(source) {
    let scale = OrgChart.zoomBehaviours.scale()

    let x = -source.x0,
      y = -source.y0

    x = x * scale + OrgChart.viewerWidth / 2
    y = y * scale + OrgChart.viewerHeight / 2

    const transitionDuration = 750

    d3.select('g').transition()
      .duration(transitionDuration)
      .attr('transform', `translate(${x}, ${y})scale(${scale})`)

    OrgChart.zoomBehaviours.scale(scale)
    OrgChart.zoomBehaviours.translate([x, y])

    const animationTime = 5000
    const nodeToAnimate = document.getElementById(`rect-${source.id}`)
    nodeToAnimate.classList.add('border-glow-centerNode')
    setTimeout(() => {
      nodeToAnimate.classList.remove('border-glow-centerNode')
    }, animationTime)
  }

  // new functions

  static drawOrganizationChart(params, options) {

    OrgChart.u_opts = Object.assign({
      id: '',
      data: {},
      modus: 'line',
      loadFunc: () => {}
    }, options)

    OrgChart.state['_loadFunction'] = OrgChart.u_opts.loadFunc
    OrgChart.state['_mode'] = OrgChart.u_opts.modus
    OrgChart.state['_root'] = OrgChart.u_opts.data
    OrgChart.state['_fixedDepth'] = Object.is(OrgChart.state['_mode'], 'line') ? 80 : 110

    OrgChart.attrs = {
      EXPAND_SYMBOL: '+',
      COLLAPSE_SYMBOL: '-',
      selector: params.selector,
      root: params.data,
      width: window.screen.width,
      height: window.screen.height,
      index: 0,
      nodePadding: 9,
      collapseCircleRadius: 10,
      nodeHeight: 60,
      //nodeWidth: 170,
      nodeWidth: 170,
      duration: 750,
      rootNodeTopMargin: 20,
      minMaxZoomProportions: [0.05, 3],
      linkLineSize: 100,
      collapsibleFontSize: '22px',
      userIcon: '\uf007',
      nodeStroke: "#ccc",
      nodeStrokeWidth: '1px'
    }

    OrgChart.dynamic = {}

    OrgChart.dynamic.nodeImageWidth = OrgChart.attrs.nodeHeight * 100 / 140;
    OrgChart.dynamic.nodeImageHeight = OrgChart.attrs.nodeHeight - 2 * OrgChart.attrs.nodePadding;
    OrgChart.dynamic.nodeTextLeftMargin = OrgChart.attrs.nodePadding * 2 + OrgChart.dynamic.nodeImageWidth
    OrgChart.dynamic.rootNodeLeftMargin = OrgChart.attrs.width / 2;
    OrgChart.dynamic.nodePositionNameTopMargin = OrgChart.attrs.nodePadding + 8 + OrgChart.dynamic.nodeImageHeight / 4 * 1
    OrgChart.dynamic.nodeChildCountTopMargin = OrgChart.attrs.nodePadding + 14 + OrgChart.dynamic.nodeImageHeight / 4 * 3

    // const horizontalSeparationBetweenNodes = 5;
    // const verticalSeperationBetweenNodes = 5;
    const horizontalSeparationBetweenNodes = 5;
    const verticalSeperationBetweenNodes = 5;


    OrgChart.tree = d3.layout.tree()
      .nodeSize([OrgChart.attrs.nodeWidth + horizontalSeparationBetweenNodes, OrgChart.attrs.nodeHeight + verticalSeperationBetweenNodes])
      .separation((a, b) => {
        // if(a.isLastChild) {
        //   return 1.7
        // }
        // if(b.isLastChild) {
        //   return 1.9
        // }
        return Object.is(a.parent, b.parent) ? 1 : 1.5
      })
    OrgChart.state['_tree'] = OrgChart.tree

    OrgChart.lineFunc = d3.svg.line()
      .x(d => d.x)
      .y(d => d.y)
      .interpolate('linear')

    OrgChart.zoomBehaviours = d3.behavior
      .zoom()
      .scaleExtent(OrgChart.attrs.minMaxZoomProportions)
      .on('zoom', this.redraw)

    OrgChart.svg = d3.select(OrgChart.attrs.selector)
      .insert('svg')
      .attr("class", "chart-svg")
      .attr('width', OrgChart.attrs.width)
      .attr('height', OrgChart.attrs.height)
      .attr('id', 'chart-svg')
      .call(OrgChart.zoomBehaviours)
      .on('dblclick.zoom', null)
      .append('g')
      .attr('transform', `translate(${OrgChart.attrs.width / 2}, 20)`)

    // necessary so that zoom knows where to zoom and unzoom from
    OrgChart.zoomBehaviours.translate([OrgChart.dynamic.rootNodeLeftMargin, OrgChart.attrs.rootNodeTopMargin])

    OrgChart.attrs.root.x0 = 0
    OrgChart.attrs.root.y0 = OrgChart.dynamic.rootNodeLeftMargin

    if (params.mode !== 'department') {
      // adding uniquevalues to each node recursively
      let uniq = 1
      this.addPropertyRecursive('uniqueIdentifier', v => {
        return uniq++;
      }, OrgChart.attrs.root)
    }

    this.expand(OrgChart.attrs.root)

    if (OrgChart.attrs.root.children) {
      OrgChart.attrs.root.children.forEach(this.expand);
    }

    this.update(OrgChart.attrs.root, {});

    d3.select(OrgChart.attrs.selector).style('height', OrgChart.attrs.height)

    OrgChart.tooltip = d3.select('body')
      .append('div')
      .attr('class', 'customTooltip-wrapper')

    const rootRect = document.getElementById('rect-1')
    const companyNameLength = JSON.parse(localStorage.getItem('current_company'))['name'].length
    rootRect.style.width = (OrgChart.attrs.collapseCircleRadius + companyNameLength) * 9.3
  }

  static checkIfOtherChildrenHaveSubChild(child_array) {

    let hasSubChild = true;
    let childCount = 0
    const child_array_copy = child_array.children
    for (let child_index = 0; child_index < child_array_copy.length; child_index++) {
      const current_child = child_array_copy[child_index]
      if ((
          Object.is(current_child.children, null) ||
          Object.is(current_child.children, undefined)
        )) {
        hasSubChild = false
        childCount++
      } else {
        hasSubChild = true
      }
    }

    return !hasSubChild && Object.is(childCount, child_array_copy.length)
  }

  static update(source, param) {

    // Declare constants
    const substringLimit = 16,
      substringStart = 0
    const companyNameLimit = 8
    const rootNodeId = 1

    // Compute new tree layout
    const nodes = OrgChart.tree.nodes(OrgChart.attrs.root)
      .reverse()
    const links = OrgChart.tree.links(nodes)
    OrgChart.state['_nodes'] = nodes

    let biggestDepth = 0;

    for (let nodeIndex = OrgChart.state['_nodes'].length - 1; nodeIndex >= 0; nodeIndex--) {
      if (nodeIndex === OrgChart.state['_nodes'].length - 1) continue
      else if (nodeIndex === 0) {
        const depth = OrgChart.state['_nodes'][nodeIndex].level
        OrgChart.depthArray.push(depth)
      } else {
        const depth = OrgChart.state['_nodes'][nodeIndex].level
        if (biggestDepth < depth) biggestDepth = depth
        if (depth === 1) {
          OrgChart.depthArray.push(biggestDepth)
          biggestDepth = 0
        }
      }
    }
    // normalize for fixed depth
    // nodes.forEach(d => {
    //   d.y = d.depth * OrgChart.attrs.linkLineSize
    //   d.x -= 120
    // })

    // update the nodes
    const node = OrgChart.svg.selectAll('g.node')
      .data(nodes, d => {
        return d.id || (d.id = ++OrgChart.attrs.index)
      })

    // Enter any new nodes at parent's previous position
    const nodeEnter = node.enter()
      .append('g')
      .attr('class', 'node')
      .attr('transform', d => `translate(${source.x0}, ${source.y0})`)
      .attr('id', d => d.id)

    const nodeGroup = nodeEnter.append('g')
      .attr('class', 'node-group')

    nodeGroup.append('rect')
      //.attr('width', OrgChart.attrs.nodeWidth)
      .attr('width', d => {

        if(Object.is(d.id, 1)) {
          return ( OrgChart.attrs.collapseCircleRadius + d.name.length * 4 )
        } else {
        const testNameLength = d.name.substring(substringStart, substringLimit).length < d.name.length
        const veryLongName = d.name.substring(substringStart, substringLimit) + '...'
        const shortName = d.name.substring(substringStart, substringLimit)
        //return (d.children || d._children) ? OrgChart.attrs.collapseCircleRadius : 0
        return OrgChart.attrs.collapseCircleRadius + d.name.length * 2
        }

      })
      .attr('height', OrgChart.attrs.nodeHeight)
      .attr('data-node-group-id', d => d.id)
      .attr('id', d => `rect-${d.id}`)
      .attr('class', d => d._children || d.children ? "nodeHasChildren" : "nodeDoesNotHaveChildren")

    const collapsibleWrapper = nodeEnter.append('g')
      .attr('data-id', v => v.uniqueIdentifier)

    collapsibleWrapper.append('rect')
      .attr('class', 'node-collapse-right-rect')
      .attr('height', OrgChart.attrs.collapseCircleRadius)
      .attr('fill', 'black')
      .attr('x', OrgChart.attrs.nodeWidth - OrgChart.attrs.collapseCircleRadius - 3)
      .attr('y', 0)
      .attr('width', d => {
        return (d.children || d._children) ? OrgChart.attrs.collapseCircleRadius : 0
      })

    const collapsibles = collapsibleWrapper.append('circle')
      .attr('class', 'node-collapse')
      .attr('cx', OrgChart.attrs.nodeWidth - OrgChart.attrs.collapseCircleRadius + 5)
      .attr('cy', 5)
      .attr('', this.setCollapsibleSymbolProperty)

    // hide collapse rect when node does not have children
    collapsibles.attr('r', d => {
        return (d.children || d._children) ? OrgChart.attrs.collapseCircleRadius : 0
      })
      .attr('height', OrgChart.attrs.collapseCircleRadius)

    collapsibleWrapper.append('text')
      .attr('class', 'text-collapse')
      .attr('x', OrgChart.attrs.nodeWidth - OrgChart.attrs.collapseCircleRadius + 5)
      .attr('y', 11)
      .attr('width', OrgChart.attrs.collapseCircleRadius)
      .attr('height', OrgChart.attrs.collapseCircleRadius)
      .style('font-size', OrgChart.attrs.collapsibleFontSize)
      .attr('text-anchor', 'middle')
      .text(d => d.collapseText)

    collapsibleWrapper.on('click', this.click)

    nodeGroup.append('text')
      .attr('x', OrgChart.dynamic.nodeTextLeftMargin)
      .attr('y', OrgChart.attrs.nodePadding + 10)
      .attr('class', d => Object.is(d.id, rootNodeId) ? '' : 'emp-name')
      .attr('text-anchor', 'left')
      .text(d => {
        if (Object.is(d.id, rootNodeId)) return ''
        this.setSelfNode(d)
        const testNameLength = d.name.substring(substringStart, substringLimit).length < d.name.length
        const veryLongName = d.name.substring(substringStart, substringLimit) + '...'
        const shortName = d.name.substring(substringStart, substringLimit)
        return testNameLength ? veryLongName : shortName
      })

    nodeGroup.append('text')
      .attr('x', OrgChart.dynamic.nodeTextLeftMargin)
      .attr('y', OrgChart.dynamic.nodePositionNameTopMargin)
      .attr('class', d => Object.is(d.id, rootNodeId) ? 'emp-name' : 'emp-position-name')
      .attr('dy', '.35em')
      .attr('text-anchor', 'left')
      .text(d => {
        if (Object.is(d.id, rootNodeId)) {
          const companyName = d.name
          return companyName
          //return companyName.substring(substringStart, companyNameLimit).length < companyNameLimit.length ? companyName.substring(substringStart, companyNameLimit) + '...' : companyName.substring(substringStart, companyNameLimit)
        }
        return d.job_title.substring(substringStart, substringLimit).length < d.job_title.length ? d.job_title.substring(substringStart, substringLimit) + '...' : d.job_title.substring(substringStart, substringLimit)
      })
      .style('font-size', d => Object.is(d.id, rootNodeId) ? '20px' : '12px')

    nodeGroup.append("text")
      .attr('x', OrgChart.dynamic.nodeTextLeftMargin)
      .attr('y', OrgChart.dynamic.nodeChildCountTopMargin)
      .attr('class', 'emp-count-icon')
      .attr('text-anchor', 'left')
      .text(d => {
        if (Object.is(d.id, 1)) return
        if (d.children) return `Subordinates: ${d.children.length}`
        if (d._children) return `Subordinates: ${d._children.length}`
        return
      })

    nodeGroup.append('defs').append('svg:clipPath')
      .attr('id', 'clip')
      .append('svg:rect')
      .attr('id', 'clip-rect')
      .attr('rx', 3)
      .attr('x', OrgChart.attrs.nodePadding)
      .attr('y', 2 + OrgChart.attrs.nodePadding)
      .attr('width', OrgChart.dynamic.nodeImageWidth)
      .attr('fill', 'none')
      .attr('height', OrgChart.dynamic.nodeImageHeight - 4)

    nodeGroup.append('svg:image')
      .attr('y', 2 + OrgChart.attrs.nodePadding)
      .attr('x', OrgChart.attrs.nodePadding)
      .attr('preserveAspectRatio', 'none')
      .attr('width', OrgChart.dynamic.nodeImageWidth)
      .attr('height', OrgChart.dynamic.nodeImageHeight - 4)
      .attr('clip-path', 'url(#clip)')
      .attr('xlink:href', v => v.photo)

    const largestDepthArray = JSON.parse(sessionStorage.getItem('largestDepth'))
    largestDepthArray.sort()

    //OrgChart.depthArray = [...new Set(OrgChart.depthArray)].filter(depth => ![1, 2, 4, 5, 6].includes(depth)).sort()
    OrgChart.depthArray = [...new Set(OrgChart.depthArray)].sort()
    console.log(`orgchart deptharray`)
    console.log(OrgChart.depthArray)

    nodes.forEach(d => {
      //const largestDepth = Number(sessionStorage.getItem('largestDepth'))

      d.y = d.depth * OrgChart.attrs.linkLineSize
      d.x -= 120

      // // Executes when d is the last child in the tree
      if ((Object.is(d.hasChild, false)) && (
          //Object.is(d.depth, largestDepthArray[largestDepthArray.length-1])
          OrgChart.depthArray.includes(d.depth)
        )) {
        const parentNode = d.parent

        if (
          OrgChartHelper.checkIfOtherChildrenHaveSubChild(parentNode) &&
          parentNode.children.length >= 4
        ) {

          let yToAdd = 0

          for (let i = 0; i < parentNode.children.length; i++) {
            parentNode.children[i].x = parentNode.x
            parentNode.children[i].y = parentNode.children[i].depth * OrgChart.attrs.linkLineSize + yToAdd
            yToAdd += 100
          }

        }

      }

    })

    // Transition nodes to their new position
    const nodeUpdate = node.transition()
      .duration(OrgChart.attrs.duration)
      .attr('transform', d => `translate(${d.x}, ${d.y})`)

    // todo replace with attrs object
    nodeUpdate.select('rect')
      .attr('width', OrgChart.attrs.nodeWidth)
      .attr('height', OrgChart.attrs.nodeHeight)
      .attr('rx', 3)
      .attr('stroke', d => {
        if (param && Object.is(d.uniqueIdentifier, param.locate)) {
          return `#a1ceed`
        }
        return OrgChart.attrs.nodeStroke
      })
      .attr('stroke-width', d => {
        if (param && Object.is(d.uniqueIdentifier, param.locate)) {
          return 6
        }
        return OrgChart.attrs.nodeStrokeWidth
      })

    // Transition exiting nodes to the parent's new position
    const nodeExit = node.exit().transition()
      .duration(OrgChart.attrs.duration)
      .attr('transform', d => `translate(${source.x}, ${source.y})`)
      .remove()

    nodeExit.select('rect')
      .attr('width', OrgChart.attrs.nodeWidth)
      .attr('height', OrgChart.attrs.nodeHeight)

    // Update the links
    const link = OrgChart.svg.selectAll('path.link')
      .data(links, d => d.target.id)

    // Enter new links at the parent's previous position
    link.enter().append('path', 'g')
      .attr('class', 'link')
      .attr(d => {
        const u_line = (d => {
          // const u_linedata = [{
          //     "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
          //   },
          //   {
          //     "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
          //   },
          //   {
          //     "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
          //   },
          //   {
          //     "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
          //   }
          // ]
          const u_linedata = [{
              "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
            },
            {
              "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
            },
            {
              "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
            },
            {
              "x": d.source.x0 + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              "y": d.source.y0 + OrgChart.attrs.nodeHeight + 1
            }
          ]
          // console.log(`link.enter()`)
          // console.log(u_linedata)
          return u_linedata
        })(d)

        return OrgChart.lineFunc(u_line)
      })

    // Transition links to their new position
    link.transition()
      .duration(OrgChart.state['_duration'])
      .attr('d', d => {
        const u_line = (d => {
          // const u_linedata = [{
          //     'x': d.source.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     'y': d.source.y + OrgChart.attrs.nodeHeight
          //   },
          //   {
          //     'x': d.source.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     'y': d.target.y - OrgChart.state['_margin'].top / 2
          //   },
          //   {
          //     'x': d.target.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     'y': d.target.y - OrgChart.state['_margin'].top / 2
          //   },
          //   {
          //     'x': d.target.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
          //     'y': d.target.y
          //   }
          // ]
          const u_linedata = [{
              'x': d.source.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': d.source.y + OrgChart.attrs.nodeHeight
            },
            {
              'x': d.source.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': d.target.y - OrgChart.state['_margin'].top / 2
            },
            {
              'x': d.target.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': d.target.y - OrgChart.state['_margin'].top / 2
            },
            {
              'x': d.target.x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': d.target.y
            }
          ]
          // console.log(`link to new positions`)
          // console.log(u_linedata)
          return u_linedata
        })(d)

        return OrgChart.lineFunc(u_line)
      })

    // Transition exiting nodes to the parent's new position
    link.exit().transition()
      .duration(OrgChart.state['_duration'])
      .attr('d', d => {
        // this is needed to draw the lines back to the caller
        const u_line = (d => {
          const u_linedata = [{
              'x': OrgChart.state['_callerNode'].x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': OrgChart.state['_callerNode'].y + OrgChart.attrs.nodeHeight + 2
            },
            {
              'x': OrgChart.state['_callerNode'].x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': OrgChart.state['_callerNode'].y + OrgChart.attrs.nodeHeight + 2
            },
            {
              'x': OrgChart.state['_callerNode'].x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': OrgChart.state['_callerNode'].y + OrgChart.attrs.nodeHeight + 2
            },
            {
              'x': OrgChart.state['_callerNode'].x + parseInt(OrgChart.attrs.nodeWidth / 2, 10),
              'y': OrgChart.state['_callerNode'].y + OrgChart.attrs.nodeHeight + 2
            }
          ]
          // console.log(`link exit`)
          // console.log(u_linedata)
          return u_linedata
        })(d)
        return OrgChart.lineFunc(u_line)
      })
      .each('end', () => {
        OrgChart.state['_callerNode'] = null // after transition clear the caller
      })

    // Stash the old positions for transition
    nodes.forEach(d => {
      d.x0 = d.x
      d.y0 = d.y
    })

    nodeGroup.on('mouseover', this.tooltipHoverHandler)
    nodeGroup.on('mouseout', this.tooltipOutHandler)
    nodeGroup.on('click', OrgChartHelper.centerNode)

  }

  static tooltipContent(item) {

    return `
      <div class="customTooltip">
        <div class="profile-image-wrapper" style="background-image: url(${item.photo})">
        </div>
        <div class="tooltip-hr"></div>
        <div class="tooltip-desc">
          <a class="name" href="${item.photo}" target="_blank">
            ${item.name}
          </a>
          <p class="position">${item.job_title}</p>
          <h4 class="tags-wrapper">
          </h4>
        </div>
        <div class="bottom-tooltip-hr"></div>
      </div>
    `

  }

  static tooltipHoverHandler(d) {

    const content = OrgChartHelper.tooltipContent(d)
    OrgChart.tooltip.html(content)

    OrgChart.tooltip.transition()
      .duration(200)
      .style('opacity', '1')
      .style('display', 'block')

    d3.select(this).attr('cursor', 'pointer')
      .attr('stroke-width', 50)

    let x = d3.event.pageX
    let y = d3.event.pageY

    // restrict tooltip to fit in borders
    if (y < 220) {
      y += 220 - y
      x -= 10
    }

    if (y > OrgChart.attrs.height - 300) {
      y -= 300 - (OrgChart.attrs.height - y)
    }

    OrgChart.tooltip.style('top', `${y - 175}px`)
      .style('left', `${x - 200}px`)
  }

  static tooltipOutHandler(d) {

    OrgChart.tooltip.transition()
      .duration(200)
      .style('opacity', 0)
      .style('display', 'none')

    d3.select(this).attr('stroke-width', 5)

  }

  static equalToEventTarget() {
    return this === d3.event.target
  }

  // Toggle children on click
  static click(d) {
    d3.select(this).select('text')
      .text(dv => {
        if (Object.is(dv.collapseText, OrgChart.attrs.EXPAND_SYMBOL)) {
          dv.collapseText = OrgChart.attrs.COLLAPSE_SYMBOL
        } else {
          if (dv.children) {
            dv.collapseText = OrgChart.attrs.EXPAND_SYMBOL
          }
        }
        return dv.collapseText
      })

    if (!d.children && !d._children && d.hasChild) {
      // if there are no child nodes, try to load child nodes
      OrgChart.state['_loadFunction'](d, childs => {
        const response = {
          id: d.id,
          name: d.name,
          job_title: childs.job_title,
          photo: childs.photo,
          children: childs.children
        }

        response.children.forEach(child => {
          if (!OrgChart.tree.nodes(d)[0]._children) {

            OrgChart.tree.nodes(d)[0]._children = []
          }

          child.x = this.x
          child.y = this.y
          child.x0 = this.x0
          child.y0 = this.y0
          OrgChart.tree.nodes(d)[0]._children.push(child)

        })

        if (d.children) {
          OrgChart.state['_callerNode'] = d
          OrgChart.state['_callerMode'] = 0 // Collapse
          d._children = d.children
          d.children = null
        } else {
          OrgChart.state['_callerNode'] = null
          OrgChart.state['_callerMode'] = 1 // Expand
          d.children = d._children
          d._children = null
        }
      })
    } else {
      if (d.children) {
        OrgChart.state['_callerNode'] = d
        OrgChart.state['_callerMode'] = 0 // Collapse
        d._children = d.children
        d.children = null
      } else {
        OrgChart.state['_callerNode'] = d
        OrgChart.state['_callerMode'] = 1 // Expand
        d.children = d._children
        d._children = null
      }
    }

    OrgChartHelper.update(d)

  }

  // Redraw for zoom
  static redraw() {
    OrgChart.svg.attr('transform', `translate(${d3.event.translate})scale(${d3.event.scale})`)
  }

  static addPropertyRecursive(propertyName, propertyValueFunction, element) {
    if (element[propertyName]) {
      element[propertyName] = element[propertyName] + ' ' + propertyValueFunction(element)
    } else {
      element[propertyName] = propertyValueFunction(element)
    }

    if (element.children) {
      element.children.forEach(v => {
        this.addPropertyRecursive(propertyName, propertyValueFunction, v)
      })
    }

    if (element._children) {
      element._children.forEach(v => {
        this.addPropertyRecursive(propertyName, propertyValueFunction, v)
      })
    }

  }

  static countChilds(node, obj) {
    const childs = node.children ? node.children : node._children
    if (childs) {
      childs.forEach(v => {
        obj.counter++
        this.countChilds(v, obj)
      })
    }

  }

  static getEmployeesCount(node) {
    const obj = {
      counter: 1
    }

    this.countChilds(node, obj)

    return obj.counter

  }

  // Expand child nodes
  static expand(d) {
    if (d.children) {
      d.children.forEach(OrgChartHelper.expand)
    }

    if (d._children) {
      d.children = d._children
      d.children.forEach(OrgChartHelper.expand)
      d._children = null
    }

    if (d.children) {
      // if node has children and it's expanded, then display -
      OrgChartHelper.setToggleSymbol(d, OrgChart.attrs.COLLAPSE_SYMBOL)
    }

  }

  // Collapse child nodes
  static collapse(d) {
    if (d._children) {
      d._children.forEach(OrgChartHelper.collapse)
    }

    if (d.children) {
      d._children = d.children
      d._children.forEach(OrgChartHelper.collapse)
      d.children = null
    }

    if (d._children) {
      // if node has children and it's collapsed, then display +
      OrgChartHelper.setToggleSymbol(d, OrgChart.attrs.EXPAND_SYMBOL)
    }

  }

  static setCollapsibleSymbolProperty(d) {
    if (d._children) {
      d.collapseText = OrgChart.attrs.EXPAND_SYMBOL
    } else if (d.children) {
      d.collapseText = OrgChart.attrs.COLLAPSE_SYMBOL
    }
  }

  static setToggleSymbol(d, symbol) {
    d.collapseText = symbol
    d3.select(`*[data-id='${d.uniqueIdentifier}']`)
      .select('text')
      .text(symbol)
  }

  static deepClone(item) {
    return JSON.parse(JSON.stringify(item))
  }

  static show(selectors) {
    this.display(selectors, 'initial')
  }

  static hide(selectors) {
    this.display(selectors, 'none')
  }

  static display(selectors, displayProp) {
    selectors.forEach(selector => {
      const elements = this.getAll(selector)
      elements.forEach(element => {
        element.style.display = displayProp
      })
    })
  }

  static set(selector, value) {
    const elements = this.getAll(selector)
    elements.forEach(element => {
      element.innerHTML = value
      element.value = value
    })
  }

  static clear(selector) {
    this.set(selector, '')
  }

  static get(selector) {
    return document.querySelector(selector)
  }

  static getAll(selector) {
    return document.querySelectorAll(selector)
  }

  static scrollRight() {
    document.getElementById('orgchartDiv').scrollLeft += 30
  }

  static scrollLeft() {
    document.getElementById('orgchartDiv').scrollRight += 30
  }

  static zoomed() {
    OrgChart.svg.attr("transform",
      `translate(${OrgChart.zoomBehaviours.translate()})scale(${OrgChart.zoomBehaviours.scale()})`)
  }

  static interpolateZoom(translate, scale) {
    const transitionDuration = 150
    return d3.transition()
      .duration(transitionDuration)
      .tween('zoom', () => {
        const iTranslate = d3.interpolate(OrgChart.zoomBehaviours.translate(), translate)
        const iScale = d3.interpolate(OrgChart.zoomBehaviours.scale(), scale)
        return t => {
          OrgChart.zoomBehaviours
            .scale(iScale(t))
            .translate(iTranslate(t))
          OrgChartHelper.zoomed()
        }
      })
  }

  static zoomClick() {
    let direction = 1,
      factor = 0.2,
      target_zoom = 1,
      center = [window.screen.width / 2, window.screen.height / 2],
      extent = OrgChart.zoomBehaviours.scaleExtent(),
      translate = OrgChart.zoomBehaviours.translate(),
      translate0 = [],
      l = [],
      view = {
        x: translate[0],
        y: translate[1],
        k: OrgChart.zoomBehaviours.scale()
      }

    let zoomAction = sessionStorage.getItem('zoomAction')

    direction = (Object.is(zoomAction, 'zoom-in')) ? 1 : -1
    target_zoom = OrgChart.zoomBehaviours.scale() * (1 + factor * direction)

    if (target_zoom < extent[0] || target_zoom > extent[1]) return false
    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k]
    view.k = target_zoom
    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y]

    view.x += center[0] - l[0]
    view.y += center[1] - l[1]

    OrgChartHelper.interpolateZoom([view.x, view.y], view.k)

  }

  static showMySelf() {

    const nodeId = Number(sessionStorage.getItem('selfNodeID'))
    const rootNode = OrgChart.state['_root']
    OrgChartHelper.getNodeWithId(rootNode, nodeId)

  }

  static setSelfNode(d) {
    const self = JSON.parse(localStorage.getItem('user'))
    const nameToSearch = self.name
    if (Object.is(d.name, nameToSearch)) {
      const selfNodeID = sessionStorage.getItem("selfNodeID")
      if (Object.is(selfNodeID, undefined) || Object.is(selfNodeID, null) || Object.is(selfNodeID, ""))
        sessionStorage.setItem('selfNodeID', d.id)
    }

    const largestDepth = JSON.parse(sessionStorage.getItem('largestDepth'))
    if (Object.is(largestDepth, undefined) || Object.is(largestDepth, null)) sessionStorage.setItem('largestDepth', JSON.stringify([d.depth]))
    else if (Object.is(d.isLastChild, true) && !largestDepth.includes(d.depth) && ![1, 2, 3].includes(d.depth)) {
      const depthArray = JSON.parse(sessionStorage.getItem('largestDepth'))
      depthArray.push(d.depth)
      depthArray.sort()
      sessionStorage.setItem('largestDepth', JSON.stringify(depthArray))
    }
  }

  static contains(str, arr) {
    return !(arr.indexOf(str) === -1)
  }

}

export default {
  OrgChart,
  OrgChartHelper
}

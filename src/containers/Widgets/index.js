import React, { Component } from "react";
import { Row, Col, Collapse } from "antd";
import basicStyle from "../../config/basicStyle";
import IsoWidgetsWrapper from "./widgets-wrapper";
import { ListStyleWrapper } from "./style";

const Panel = Collapse.Panel;

const customPanelStyle = {
	background: "#f7f7f7",
	borderRadius: 4,
	marginTop: 20,
	border: 0,
	overflow: "hidden"
};

const endPanelStyle = {
	background: "#f7f7f7",
	borderRadius: 4,
	marginTop: 20,
	border: 0,
	overflow: "hidden",
	marginBottom: 20
};

export default class IsoWidgets extends Component {
	render() {
		const { colStyle } = basicStyle;
		const wisgetPageStyle = {
			display: "flex",
			flexFlow: "row wrap",
			alignItems: "flex-start",
			padding: "15px",
			overflow: "hidden"
		};

		return (
			<div style={wisgetPageStyle}>
				<Row type="flex" justify="center">
					<Col md={18} sm={18} xs={24} style={colStyle}>
						<IsoWidgetsWrapper>
							<ListStyleWrapper>
								<div
									style={{
										backgroundColor: "#fff",
										padding: "20px",
										paddingRight: "30px"
									}}
								>
									<h1>Bug Bounty Program T&C</h1>
									<hr />
									<ul style={{ paddingTop: "20px" }}>
										<li>
											Steps
											<ol type="1">
												<li>
													Download latest EasyWork apps. Do whatever you want to
													find bugs in any of the features.
												</li>
												<li>
													When you see a bug, check if it's already filed in
													Bugs list.
												</li>
												<li>
													If it’s new, file it using App Feedback in the apps.
												</li>
												<li>
													Admin will decide and set it to one of the following
													statuses
													<Collapse bordered={false} defaultActiveKey={["1"]}>
														<Panel
															header={<b>Pending</b>}
															key="1"
															style={customPanelStyle}
														>
															<p>Pending for admin review</p>
														</Panel>
														<Panel
															header={<b>Acknowledged</b>}
															key="2"
															style={customPanelStyle}
														>
															<ul>
																<li>
																	<b>Old Build</b>
																	<br />It's valid. But we've fixed this and and
																	it's already available in public before you
																	file the case. Please update to latest build.
																	We will not support old version due to
																	resource constraint.
																</li>
																<li>
																	<b>Duplicate</b>
																	<br />Someone filed this before you did. It’s
																	in the Bug List already.
																</li>
																<li>
																	<b>Not reproducible</b>
																	<br />We follow your description in your bug
																	case, try our best, but still couldn't see the
																	problem you reported. If you still able to see
																	the same problem, you may file it again with
																	more info.
																	<ul>
																		<li>
																			Which is why you should always try to
																			repeat the same steps few times and try to
																			figure out how to reproduce it before
																			filing it.
																		</li>
																		<li>
																			You can consider attaching a video from
																			your phone to show it. Would be easier for
																			you too. No need to type long winded steps
																			to describe it.
																		</li>
																		<li>
																			If it's a crash and you manage to capture
																			and submit the crash message but still
																			can't reproduce? No worry. The bug did
																			happen. It's our problem. It will be
																			accepted.
																		</li>
																	</ul>
																</li>
																<li>
																	<b>Not In Scope</b>
																	<ul>
																		<li>
																			Bugs outside of our control and we
																			couldn't or decide not to do anything
																			about it due to resource constraint. E.g.
																			Well, your phone problem. Out of memory?
																			Or any other area we will add here from
																			time to time.
																			<ol type="1">
																				<li>Android 4.4, iOS 10 and above</li>
																			</ol>
																		</li>
																		<li>
																			Area outside of the Apps itself and not
																			public facing. So the bug bounty website
																			itself or any other internal service that
																			are not affecting public customer is not
																			included.
																		</li>
																	</ul>
																</li>
															</ul>
														</Panel>
														<Panel
															header={<b>Accepted</b>}
															key="3"
															style={endPanelStyle}
														>
															<ul>
																<li>
																	<b>Critical</b> <span>[50 points]</span>
																	<br />Crash, malfunction that blocks a major
																	user flow e.g. login.
																</li>
																<li>
																	<b>Medium</b> <span>[20 points]</span>
																	<br />Default priority
																</li>
																<li>
																	<b>Low</b> <span>[10 points]</span>
																	<br />Typo, layout, UI.
																</li>
															</ul>
														</Panel>
													</Collapse>
												</li>
											</ol>
										</li>
										<li>
											Accepted cases will be tallied at month end and pay out
											via EasyReward within first 7 days of next month.
										</li>
										<li>
											<b>DO NOT</b> attempt to to perform any action that might
											negatively impact user experience of other users. E.g.
											Brute-force attacks, Denial of Service attack, social
											engineering, spam etc. Do not hunt for other user data.
										</li>
										<li>
											Admin will have the final decision on the point awards and
											reserve the right to change any of the terms and
											conditions of this campaign. All decisions are final.
										</li>
									</ul>
								</div>
							</ListStyleWrapper>
						</IsoWidgetsWrapper>
					</Col>
				</Row>
			</div>
		);
	}
}

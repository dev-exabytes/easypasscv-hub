import React, { Component } from "react";
import { Row, Col } from "antd";
import basicStyle from "../../config/basicStyle";
import IsoWidgetsWrapper from "./widgets-wrapper";
import IsoWidgetBox from "./widget-box";
import SaleWidget from "./sale/sale-widget";
import { GoogleChart } from "../Charts/googleChart/";
import { connect } from "react-redux";
import actions from "../../redux/dashboard/actions";
import { DatePicker } from "antd";
import Moment from "moment";

const { RangePicker } = DatePicker;

const { dashboardSearch, chartSearch } = actions;

class IsoWidgets extends Component {
	state = {
		loading: false,
		total: [],
		openRangePicker: false,
		ModalText: "Filter by Date",
		startDate: [Moment(), Moment(), Moment(), Moment()],
		endDate: [Moment(), Moment(), Moment(), Moment()],
		chart: []
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.DashboardSearch.total) {
			this.setState({
				total: nextProps.DashboardSearch.total,
				loading: false
			});
		}
		if (
			nextProps.DashboardSearch.chart &&
			nextProps.DashboardSearch.chart.length > 0 &&
			nextProps.DashboardSearch.chart !== this.props.DashboardSearch.chart
		) {
			this.setState({
				chart: nextProps.DashboardSearch.chart
			});
		}
	}

	onSearch = () => {
		this.setState({ loading: true }, () => this.props.dashboardSearch());
	};

	componentDidMount() {
		this.onSearch();
	}

	onCalendarChange = (i, model, dates, dateStrings) => {
		let startDates = this.state.startDate;
		let endDates = this.state.endDate;
		startDates[i] = dates[0];
		endDates[i] = dates[1];
		this.setState(
			{
				startDate: startDates,
				endDate: endDates
			},
			() => {
				if (this.state.startDate[i] && this.state.endDate[i]) {
					this.setState(
						{
							chart: []
						},
						() =>
							this.props.chartSearch(
								model,
								dates[0].format("YYYY-MM-DD"),
								dates[1].format("YYYY-MM-DD")
							)
					);
				}
			}
		);
	};

	render() {
		const { rowStyle, colStyle } = basicStyle;
		const { total, chart } = this.state;
		const wisgetPageStyle = {
			display: "flex",
			flexFlow: "row wrap",
			alignItems: "flex-start",
			padding: "15px",
			overflow: "hidden"
		};

		return (
			<div style={wisgetPageStyle}>
				<Row style={rowStyle} gutter={0} justify="start">
					{total.map((item, i) => (
						<Col md={6} sm={12} xs={24} style={colStyle} key={i}>
							<IsoWidgetsWrapper>
								<SaleWidget
									label={item.title}
									price={item.count}
									fontColor="#ff6600"
								/>
							</IsoWidgetsWrapper>
						</Col>
					))}
				</Row>

				<Row style={rowStyle} gutter={0} justify="start">
					{chart.map((item, i) => (
						<Col md={12} sm={24} xs={24} style={colStyle} key={i}>
							<IsoWidgetsWrapper>
								<IsoWidgetBox height={455}>
									<div
										style={{
											display: "flex",
											justifyContent: "flex-end",
											flex: 1
										}}
									>
										<RangePicker
											key={i}
											allowClear={false}
											value={[this.state.startDate[i], this.state.endDate[i]]}
											style={{ borderWidth: "0px" }}
											dateRender={current => {
												const style = {};
												if (current.date() === 1) {
													style.border = "1px solid #1890ff";
													style.borderRadius = "50%";
												}
												return (
													<div className="ant-calendar-date" style={style}>
														{current.date()}
													</div>
												);
											}}
											onCalendarChange={this.onCalendarChange.bind(
												this,
												i,
												item.model
											)}
										/>
									</div>
									<GoogleChart
										title="Line Chart"
										chartType="LineChart"
										key="LineChart"
										width="100%"
										height={"350px"}
										columns={[
											{
												label: item.x_axis_label,
												type: item.x_axis_type
											},
											{
												label: `Number of new ${item.title}`,
												type: "number"
											}
										]}
										rows={item.rows}
										options={{
											legend: "none",
											hAxis: {
												textStyle: {
													color: "#788195"
												},
												title: item.x_axis_label,
												titleTextStyle: {
													color: "#788195"
												}
											},
											vAxis: {
												textStyle: {
													color: "#788195"
												},
												title: `Number of new ${item.title}`,
												titleTextStyle: {
													color: "#788195"
												}
											},
											colors: ["#ff6600"],
											dataOpacity: "1.0",
											animation: {
												duration: 1000,
												easing: "in",
												startup: true
											},
											tooltip: {
												textStyle: {
													color: "#788195"
												}
											}
										}}
									/>
								</IsoWidgetBox>
							</IsoWidgetsWrapper>
						</Col>
					))}
				</Row>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { DashboardSearch: state.DashboardSearch.toJS() };
}

export default connect(mapStateToProps, {
	dashboardSearch,
	chartSearch
})(IsoWidgets);

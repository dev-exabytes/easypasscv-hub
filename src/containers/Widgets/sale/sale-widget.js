import React, { Component } from "react";
import { SaleWidgetWrapper } from "./style";

export default class SaleWidget extends Component {
	render() {
		//const { fontColor, label, price, details } = this.props;
		const { fontColor, label, price } = this.props;

		const textColor = {
			color: fontColor
		};

		return (
			<SaleWidgetWrapper className="isoSaleWidget">
				<h3 className="isoSaleLabel">
					Total<br />
					{label}
				</h3>
				<span className="isoSalePrice" style={textColor}>
					{price}
				</span>
				{/* <p className="isoSaleDetails">
          {details}
        </p> */}
			</SaleWidgetWrapper>
		);
	}
}

import React, { Component } from "react";
import { connect } from "react-redux";
import clone from "clone";
import { Link } from "react-router-dom";
import { Layout, Select } from "antd";
import { Scrollbars } from "react-custom-scrollbars";
import Menu from "../../components/uielements/menu";
import IntlMessages from "../../components/utility/intlMessages";
import menuActions from "../../redux/menu/actions";
import SidebarWrapper from "./sidebar.style";
import actions from "../../redux/company/actions"
import appActions from "../../redux/app/actions";
import subscriptionActions from "../../redux/Subscriptions/actions";
import Logo from "../../components/utility/logo";
import { rtl } from "../../config/withDirection";

const { getMenu } = menuActions;
const { Sider } = Layout;
const { SubMenu } = Menu;
const Option = Select.Option;

const { toggleOpenDrawer, changeOpenKeys, changeCurrent, toggleCollapsed } = appActions;
const stripTrailingSlash = str => {
  if (str.substr(-1) === "/") {
    return str.substr(0, str.length - 1);
  }
  return str;
};

const { storeCompany, getCompanies,getListCompanies } = actions;
const { clearSubs } = subscriptionActions;

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.onOpenChange = this.onOpenChange.bind(this);

  }

  componentDidMount() {
    if (this.props.currentCompany) {
      this.props.getMenu(this.props.currentCompany.id);
    } else {
      this.props.getMenu(null);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentCompany && this.props.currentCompany != nextProps.currentCompany) {
      this.props.getMenu(nextProps.currentCompany.id);
    }
  }

  handleClick(e) {
    this.props.changeCurrent([e.key]);
    if (this.props.app.view === "MobileView") {
      setTimeout(() => {
        this.props.toggleCollapsed();
        this.props.toggleOpenDrawer();
      }, 100);
    }
  }
  handleChange = (id) => {
    const { idToken } = this.props;
    this.props.clearSubs();
    this.props.storeCompany(id, idToken);
  }

  refreshCompany = ()=> {
    const {currentCompany} = this.props;
    this.props.getListCompanies(currentCompany);
  }

  onOpenChange(newOpenKeys) {
    const { app, changeOpenKeys } = this.props;
    const latestOpenKey = newOpenKeys.find(key => !(app.openKeys.indexOf(key) > -1));
    const latestCloseKey = app.openKeys.find(key => !(newOpenKeys.indexOf(key) > -1));
    let nextOpenKeys = [];
    if (latestOpenKey) {
      nextOpenKeys = this.getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }
    if (latestCloseKey) {
      nextOpenKeys = this.getAncestorKeys(latestCloseKey);
    }
    changeOpenKeys(nextOpenKeys);
  }
  getAncestorKeys = key => {
    const map = {
      sub3: ["sub2"]
    };
    return map[key] || [];
  };

  renderView({ style, ...props }) {
    const viewStyle = {
      marginRight: rtl === "rtl" ? "0" : "-17px",
      paddingRight: rtl === "rtl" ? "0" : "9px",
      marginLeft: rtl === "rtl" ? "-17px" : "0",
      paddingLeft: rtl === "rtl" ? "9px" : "0"
    };
    return <div className="box" style={{ ...style, ...viewStyle }} {...props} />;
  }

  render() {
    const { app, toggleOpenDrawer, customizedTheme, user, currentCompany, companiesData, menu } = this.props;
    const url = stripTrailingSlash(this.props.url);
    const collapsed = clone(app.collapsed) && !clone(app.openDrawer);
    const { openDrawer } = app;
    const mode = collapsed === true ? "vertical" : "inline";
    const scroll = currentCompany ? currentCompany.name : undefined;
    const onMouseEnter = event => {
      if (openDrawer === false) {
        toggleOpenDrawer();
      }
      return;
    };
    const onMouseLeave = () => {
      if (openDrawer === true) {
        toggleOpenDrawer();
      }
      return;
    };
    const scrollheight = app.height;
    const styling = {
      backgroundColor: customizedTheme.backgroundColor
    };
    const submenuColor = {
      color: customizedTheme.textColor
    };

    return (
      <SidebarWrapper>
        <Sider
          trigger={null}
          collapsible={true}
          collapsed={collapsed}
          width="240"
          className="isomorphicSidebar"
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          style={styling}
        >
          <div className="header">
            <Logo collapsed={collapsed} user={user} />
            <div className="isoSelectWrapper">
              <Select title="company"
                value={scroll}
                style={{
                  width: "100%",
                  borderRadius: "20px"
                }}
                onChange={this.handleChange}
                collapsed={collapsed}
                placeholder="No Company"
                onDropdownVisibleChange ={this.refreshCompany}
              >
                {companiesData ? companiesData.map((e, key) => {
                  return <Option key={key} value={e.id}>{e.name}</Option>;
                }) : null}
              </Select>
            </div>
          </div>
          <Scrollbars renderView={this.renderView} style={{ height: scrollheight - 70 }}>
            <Menu
              onClick={this.handleClick}
              theme="light"
              mode={mode}
              openKeys={collapsed ? [] : app.openKeys}
              selectedKeys={app.current}
              onOpenChange={this.onOpenChange}
              className="isoDashboardMenu"
            >
              {menu ? menu.map(function (item, index) {
                if (item.type === "menu") {
                  return <Menu.Item key={item.key}>
                    <Link to={`${url}/${item.url}`}>
                      <span className="isoMenuHolder" style={submenuColor}>
                        <img src={require(`../../image/${item.icon}`)} alt="binary code" height="30" width="30" style={{ marginRight: "10px" }} />
                        <span className="nav-text">
                          <IntlMessages id={`sidebar.${item.text}`} />
                        </span>
                      </span>
                    </Link>
                  </Menu.Item>;
                }
                else {
                  return <SubMenu
                    key={item.key}
                    title={
                      <span className="isoMenuHolder" style={submenuColor}>
                        <img src={require(`../../image/${item.icon}`)} alt="integration" height="30" width="30" style={{ marginRight: "10px" }} />
                        <span className="nav-text">
                          <IntlMessages id={`sidebar.${item.text}`} />
                        </span>
                      </span>
                    }
                  >
                    {item.sub.map(function (subitem, index) {
                      return <Menu.Item key={subitem.key}>
                        <Link to={`${url}/${subitem.url}`}>
                          <span className="isoMenuHolder" style={submenuColor}>
                            <img src={require(`../../image/${subitem.icon}`)} alt="binary code" height="30" width="30" style={{ marginRight: "10px" }} />
                            <span className="nav-text">
                              <IntlMessages id={`sidebar.${subitem.text}`} />
                            </span>
                          </span>
                        </Link>
                      </Menu.Item>;
                    })}
                  </SubMenu>;
                }
              }) : null}
            </Menu>
          </Scrollbars>
        </Sider>
      </SidebarWrapper>
    );
  }
}

export default connect(
  state => ({
    app: state.App.toJS(),
    customizedTheme: state.ThemeSwitcher.toJS().sidebarTheme,
    idToken: state.Auth.get("idToken"),
    companiesData: state.Company.get("companies"),
    currentCompany: state.Company.get("current_company"),
    menu: state.Menu.get("menu"),
    user: state.Auth.get("user")
  }),
  { toggleOpenDrawer, changeOpenKeys, changeCurrent, toggleCollapsed, storeCompany, getMenu, clearSubs, getCompanies, getListCompanies },
)(Sidebar);

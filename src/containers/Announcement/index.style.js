import styled from 'styled-components';
import { palette } from 'styled-theme';
import { transition } from '../../config/style-util';

const Style = styled.div`
  border-radius:10px;
  background-color:${palette("ew", 1)};
  overflow:hidden;
  .title{
    font-size:18px;
    font-weight:600;
    color:#000;
  }

  .buttonContainer{
    height:40px;
    border-radius:20px;
    background-color: ${palette('ew', 6)};
    color:${palette("ew", 1)};
    border:none;
  }

  .uploadButton{
    height:40px;
    border-radius:20px;
    background-color: ${palette('ew', 6)};
    color:${palette("ew", 1)};
    border:none;
    margin: 10px 0px;
    width: 200px;
    transition: ${transition()};
    display: flex;
  }

  .uploadButton:hover{
    cursor: pointer;
    background-color: ${palette('ew', 1)};
    color:${palette("ew", 6)};
  }

  .uploadText{
    width: 100%;
    height: 100%;
    line-height:40px;
    text-align: center;
    margin: auto;
  }

  .uploadText:hover{
    cursor: pointer;
  }

  input[type="file"] {
    display: none;
  }
  
 .description{
    font-size:14px;
    color:${palette("text", 4)};
 }
 .header-container{
    display: flex;
    justify-content: space-between;
 }

 .submit {
    display: flex;
    justify-content: center;
    flex-direction: column;
 }

 .submitItem {
   min-height: 50px;
   display: flex;
   justify-content: center;
 }
 .header {
   font-size: 30px;
   text-decoration: bold;
 }

 .section {
   margin: 5px auto;
   min-height: 80px;
 }
 
 a:hover{
   text-decoration: underline;
 }

.image-list {
  display: flex;
  flex-wrap: wrap;
}

.image-item {
  position: relative;
  padding: 10px;
}

.btnRemoveAtt{
  position:absolute;
  top: -8px;
  right: -8px;
}

`;

export default Style;

import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import Style from "./index.style";
import { baseUrl, apiHeader } from "../../config.js";
import actions from "../../redux/announcement/actions";
import { notification, Spin, Button, Input, Card, Avatar } from 'antd';
const { createAnnouncement, updateAnnouncement, checkAnnouncementAccess } = actions;

const displayText = {
    header: "Create Announcement",
    altHeader: "Update Announcement",
    title: "Title",
    message: "Message",
    images: "Images",
    errorTitle: "Title is required",
    errorMessage: "Message is required",
    required: " *",
    existingImg: "Existing Images :",
    notificationTitle: "File size limit exceeded!",
    notificationDesc: "Please keep the total attachment file size within 50MB.",
    placeholderTitle: "Enter title here",
    placeholderMsg: "Enter announcement here",
    btnSubmit: "Submit",
    btnUpdate: "Update Announcement",
    btnImages: "Add Image(s)",
    submittingTip: "Uploading attachment(s)...",
    accessGettingTip: "Loading...",
    unauthorized: "Sorry, you have no permission to view this page",
    differentCompany: "Announcement does not exist."
};

class CreateAnnouncement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            body: '',
            attachment: [],
            existingAttachment: [],
            id: null,
            errors: {
                title: '',
                body: ''
            },
            isSubmitting: false,
            isEdit: false,
            editDetail: null,
            removeUploadAttachments: [],
            removeAttachments: "",
            companyId: 0,
        };
        this.fileInput = React.createRef();
        this.formatBytes = this.formatBytes.bind(this);
        this.deleteExistingAttachment = this.deleteExistingAttachment.bind(this);
        this.deleteUploadedAttachment = this.deleteUploadedAttachment.bind(this);
    }

    async componentDidMount() {
        const { checkAnnouncementAccess, currentCompany, idToken } = this.props;
        const { match: { params } } = this.props;
        const { fromLink, announcementDetail } = this.props.location.state ? this.props.location.state : false

        this.setState(() => checkAnnouncementAccess(
            currentCompany.id,
            idToken
        ))
        if (fromLink || params.announcementId) {
            this.setState({
                isEdit: true,
            })
            if (fromLink) {
                this.setState({
                    title: announcementDetail.title,
                    body: announcementDetail.body,
                    id: announcementDetail.id,
                    existingAttachment: announcementDetail.photos,
                    companyId: announcementDetail.company_id,
                });
            } else {

                const response = await new Promise((resolve, reject) => {
                    return fetch(`${baseUrl}/v1/companies/${currentCompany.id}/announcements/${params.announcementId}`, {
                        headers: {
                            ...apiHeader, ...{
                                "Content-Type": "application/json",
                                "Authorization": `bearer ${idToken}`
                            }
                        },
                        method: "GET"
                    })
                        .then(response => (response.status !== 200 ? reject(response) : response))
                        .then(response => response.json())
                        .then(response => resolve(response))
                        .catch(error => reject(error));
                })
                const announcementDetail = response.data
                this.setState({
                    title: announcementDetail.title,
                    body: announcementDetail.body,
                    id: announcementDetail.id,
                    existingAttachment: announcementDetail.photos,
                    companyId: announcementDetail.company_id,
                });
            }
        }
    }

    handleSubmit = async (event) => {

        event.preventDefault();
        const { currentCompany, idToken } = this.props;
        let valid = true;
        var errorsCopy = this.state.errors;

        if (this.state.title.length === 0) {
            valid = false;
            errorsCopy.title = displayText.errorTitle
            this.setState({ errors: errorsCopy })
        }
        if (this.state.body.length === 0) {
            valid = false;
            errorsCopy.body = displayText.errorMessage
            this.setState({ errors: errorsCopy })
        }
        if (valid === true) {
            let formData = new FormData();

            formData.append('title', this.state.title);
            formData.append('body', this.state.body);
            for (var i = 0; i < this.fileInput.current.files.length; i++) {
                console.log("att> " + this.state.attachment)
                console.log("arr > " + this.fileInput.current.files[i].name)
                if (this.state.attachment.find(el => el.name === this.fileInput.current.files[i].name)) {
                    formData.append('attachment[]', this.fileInput.current.files[i], `${i}`);
                }
            }
            if (!this.state.isEdit) {
                this.props.createAnnouncement(
                    currentCompany.id,
                    formData,
                    idToken
                )
            } else {
                if (this.state.removeAttachments) {
                    formData.append("remove_attachments", this.state.removeAttachments)
                }
                formData.append("_method", "PUT");
                this.props.updateAnnouncement(
                    currentCompany.id,
                    formData,
                    this.state.id,
                    idToken
                )
            }
            this.setState({ isSubmitting: true });
        }

    }

    validateForm = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'title':
                errors.title =
                    value.trim() === ""
                        ? displayText.errorTitle
                        : '';
                break;
            case 'body':
                errors.body =
                    value.trim() === ""
                        ? displayText.errorMessage
                        : '';
                break;
            default:
                break;
        }
    }

    handleInputChange(event) {
        this.validateForm(event);
        const target = event.target;
        const value = target.value
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleUpload(event) {
        event.preventDefault();
        let files = event.target.files;
        let newURLs = [];
        let totalFileSize = 0;
        let maxSize = 50 * 1024 * 1024; //50Mb
        for (var i = 0; i < files.length; i++) {
            totalFileSize += files[i].size;
            if (files[i].size > maxSize || totalFileSize > maxSize) {
                notification.open({
                    message: displayText.notificationTitle,
                    description: displayText.notificationDesc
                });
                event.target.value = null;
                this.setState({ 'attachment': [] })
                return false;
            } else {
                newURLs.push({ name: files[i].name, url: URL.createObjectURL(files[i]), size: files[i].size });
            }
        }
        this.setState({ 'attachment': newURLs });
    }


    deleteExistingAttachment(id) {
        let newRemoveAtt = this.state.removeAttachments;
        if (!newRemoveAtt) {
            newRemoveAtt = newRemoveAtt.concat('', id)
        } else {
            newRemoveAtt = newRemoveAtt.concat(', ', id);
        }
        this.setState(prevState => ({
            existingAttachment: prevState.existingAttachment.filter(att => att.id !== id),
            removeAttachments: newRemoveAtt,
        }));
    }

    deleteUploadedAttachment(name) {
        let newAtt = this.state.attachment;
        newAtt = newAtt.filter(e => e.name !== name);
        this.setState({
            attachment: newAtt,
        });
    }

    formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    renderImg(image, key) {
        let extension = image.name.substring(image.name.lastIndexOf('.') + 1, image.name.length).toUpperCase()
        return (
            <div key={key} className="image-item">
                <Card style={{ width: 250 }}>
                    <Card.Meta
                        avatar={<Avatar shape="square" size={64} src={image.url} />}
                        title={image.name}
                        key={key}
                        description={extension + "\n" + this.formatBytes(image.size)}
                    />
                </Card>
                <div className="btnRemoveAtt"><Button icon={"close"} shape="circle" value={image.name} onClick={e => this.deleteUploadedAttachment(e.target.value)} /></div>
            </div>
        );
    }


    render() {

        const { access, isAccessing } = this.props;
        const failedSubmit = this.props.failed;
        const { title, body, attachment, existingAttachment, errors, isEdit, isSubmitting } = this.state;
        let shouldDisable = isSubmitting && !failedSubmit ? true : false;
        let isSameCompany = this.state.companyId === this.props.currentCompany.id ? true : false;
        const requiredStyle = {
            color: `red`
        };
        if (access && isSameCompany) {

            return (
                <LayoutContentWrapper>
                    <form name="form" onSubmit={this.handleSubmit.bind(this)}>
                        <Style className="isoLayoutContent">
                            <div className="header">{this.state.isEdit ? displayText.altHeader : displayText.header}</div>
                            <div className="section">
                                <div className="section-header">{displayText.title}<span style={requiredStyle}>{displayText.required}</span></div>
                                <Input maxLength={191} name="title" placeholder={displayText.placeholderTitle} value={title} onChange={evt => this.handleInputChange(evt)} />
                                <span style={requiredStyle}>{errors.title}</span>
                            </div>
                            <div className="section">
                                <div className="section-header">{displayText.images}</div>
                                <div>
                                    <div htmlFor="attachment" className="uploadButton">
                                        <label htmlFor="attachment" className="uploadText">{displayText.btnImages}</label>
                                        <input id="attachment" accept=".png, .jpg, .jpeg, .bmp" type="file" multiple ref={this.fileInput} name="attachment" onChange={evt => this.handleUpload(evt)} />
                                    </div>
                                    {attachment.length ? <div className="image-list">{attachment.map((img, key) => this.renderImg(img, key))}</div> : <div></div>}
                                    {existingAttachment.length ? <div><div className="section-header">{isEdit ? displayText.existingImg : ""}</div><EditableAttachment deleteExistingAttachment={this.deleteExistingAttachment} data={existingAttachment}></EditableAttachment></div> : <div></div>}
                                </div>
                            </div>
                            <div className="section">
                                <div className="section-header">{displayText.message}<span style={requiredStyle}>{displayText.required}</span></div>
                                <Input.TextArea maxLength={65535} name="body" rows={8} placeholder={displayText.placeholderMsg} value={body} onChange={evt => this.handleInputChange(evt)} />
                                <span style={requiredStyle}>{errors.body}</span>
                            </div>
                            <div className="submit">
                                <div className="submitItem">
                                    {shouldDisable ? <Spin tip={attachment.length ? displayText.submittingTip : ""}></Spin> : <div></div>}
                                </div>
                                <div className="submitItem">
                                    <Button disabled={shouldDisable} className="buttonContainer" htmlType="submit">{isEdit ? displayText.btnUpdate : displayText.btnSubmit}</Button>
                                </div>
                            </div>
                        </Style>
                    </form>
                </LayoutContentWrapper>
            )
        } else {
            if (isSameCompany) {
                return (
                    <LayoutContentWrapper>
                        <Style className="isoLayoutContent">
                            {!isAccessing ? displayText.unauthorized : <Spin tip={displayText.accessGettingTip} />}
                        </Style>
                    </LayoutContentWrapper>
                )
            } else {
                return (
                    <LayoutContentWrapper>
                        <Style className="isoLayoutContent">
                            {displayText.differentCompany}
                        </Style>
                    </LayoutContentWrapper>
                )
            }
        }
    }

}

class EditableAttachment extends Component {

    renderImg(image, key) {
        return (
            <div key={key} className="image-item">
                <Card
                    style={{ width: 180 }}
                    key={key}>
                    <Card.Meta
                        avatar={<Avatar alt="attachments" shape="square" size={64} src={image.url} />}
                    />
                </Card>
                <div className="btnRemoveAtt"><Button icon={"close"} shape="circle" onClick={this.delete.bind(this, image.id)} /></div>
            </div>
        );
    }

    delete(id) {
        this.props.deleteExistingAttachment(id);
    }

    render() {

        const attachment = this.props.data;

        return (
            <div className="image-list">
                {attachment.map((img, key) => this.renderImg(img, key))}
            </div>
        )
    }
}


export default connect(
    state => {
        return ({
            idToken: state.Auth.get("idToken"),
            currentCompany: state.Company.get("current_company"),
            creating: state.Announcement.get("loading_create"),
            failed: state.Announcement.get("failed_create"),
            access: state.Announcement.get("announcement_access"),
            isAccessing: state.Announcement.get("announcement_access_getting"),
        })
    },
    { createAnnouncement, updateAnnouncement, checkAnnouncementAccess }
)(CreateAnnouncement);
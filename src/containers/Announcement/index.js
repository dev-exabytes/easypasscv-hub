import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper";
import { connect } from "react-redux";
import Style from "./index.style";
import actions from "../../redux/announcement/actions";
import { List, Avatar, Spin, Button, Pagination } from 'antd';
import { Link } from "react-router-dom";

const { getAnnouncement, checkAnnouncementAccess } = actions;

class Announcement extends Component {

  componentDidMount() {
    const { getAnnouncement, checkAnnouncementAccess, currentCompany, idToken } = this.props;
    var pageNum = 1;
    this.setState(() => getAnnouncement(
      currentCompany.id,
      pageNum,
      idToken
    )
    );
    this.setState(() => checkAnnouncementAccess(
      currentCompany.id,
      idToken
    ))
  }

  onPageChange = (page) => {
    const { getAnnouncement, currentCompany, idToken } = this.props;
    this.setState(() => getAnnouncement(
      currentCompany.id,
      page,
      idToken
    )
    );
  }

  render() {
    const announcement = (this.props.announcementList ? this.props.announcementList.data : []);
    const announcementPagingInfo = this.props.announcementList;
    const access = this.props.access;

    return (
      <LayoutContentWrapper>
        <Style className="isoLayoutContent">
          <div className="header-container">
            <div className="header">
              <span style={{ fontSize: "36px", color: "#000" }}>Announcement</span>
            </div>
            <div className="create-announcement">
            {access ? <Link to={{pathname: "/dashboard/announcement/create", state: {isEdit: false}}}><Button className="buttonContainer">Create Announcement</Button></Link> : <div></div>}
            </div>
          </div>

          {
            announcementPagingInfo ?
              <Paginator value={announcementPagingInfo} onChange={this.onPageChange.bind(this)} /> :
              <div></div>
          }

          {Array.isArray(announcement) && announcement.length ?
            <AnnouncementList value={announcement} />
            : this.props.loading === true ?
              <div><br /><Spin tip="Getting announcement..." /></div> :
              <span><br />No announcement.<br />Easy like Sunday morning.</span>
          }
        </Style>
      </LayoutContentWrapper >
    )
  }
}

class Paginator extends Component {

  onPageChange = (e) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e);
    }
  }

  render() {
    const announcementPagingInfo = this.props.value;

    return (
      <div className="paging-container">
        <Pagination
          total={announcementPagingInfo.total}
          pageSize={announcementPagingInfo.per_page}
          onChange={this.onPageChange}
          current={announcementPagingInfo.current_page}
          hideOnSinglePage={true}
        />
      </div>
    )
  }
}

class AnnouncementList extends Component {
  render() {
    const announcement = this.props.value;

    return (
      <div className="announcementList">
        <List
          itemLayout="vertical"
          dataSource={announcement}
          renderItem={(item, index) => (
            <List.Item
              key={item.index}
            >
              <List.Item.Meta
                avatar={item.creator_details.avatar ? <Avatar src={item.creator_details.avatar} /> : <Avatar icon={"user"} />}
                title={(<div className="listItem">
                  <Link to={{ pathname: `/dashboard/announcement/detail/${item.id}` }}>{item.title.length > 60 ? item.title.substring(0, 59) + "..." : item.title}</Link>
                </div>)}
                description={item.elapsed}
              />
              {item.body.length > 100 ? item.body.substring(0, 99) + "..." : item.body}
            </List.Item>

          )}
        />
      </div>
    )
  }
}

export default connect(
  state => {
    return ({
      idToken: state.Auth.get("idToken"),
      currentCompany: state.Company.get("current_company"),
      announcementList: state.Announcement.get("announcement_list"),
      loading: state.Announcement.get("loading_announcement"),
      access: state.Announcement.get("announcement_access")
    })
  },
  { getAnnouncement, checkAnnouncementAccess }
)(Announcement);
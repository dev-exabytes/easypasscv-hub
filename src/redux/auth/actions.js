const actions = {
  LOGIN_REQUEST: "LOGIN_REQUEST",
  LOGOUT: "LOGOUT",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_ERROR: "LOGIN_ERROR",
  LOGIN_ADMIN_ERROR: "LOGIN_ADMIN_ERROR",
  LOGIN_LOADING: "LOGIN_LOADING",
  AUTH_REQUEST: "AUTH_REQUEST",
  UPDATE_PROFILE_REQUEST: "UPDATE_PROFILE_REQUEST",
  UPDATE_PROFILE_SUCCESS: "UPDATE_PROFILE_SUCCESS",
  UPDATE_PROFILE_ERROR: "UPDATE_PROFILE_ERROR",
  UPDATE_PROFILE_LOADING: "UPDATE_PROFILE_LOADING",
  LOGIN_BY_PHONE: "LOGIN_BY_PHONE",
  LOGIN_BY_EMAIL: "LOGIN_BY_EMAIL",
  FORGET_PASSWORD_BY_PHONE: "FORGET_PASSWORD_BY_PHONE",
  FORGET_PASSWORD_BY_EMAIL: "FORGET_PASSWORD_BY_EMAIL",
  FORGET_PASSWORD_LOADING: "FORGET_PASSWORD_LOADING",
  FORGET_PASSWORD_ERROR: "FORGET_PASSWORD_ERROR",
  FORGET_PASSWORD_SUCCESS: "FORGET_PASSWORD_SUCCESS",
  FORGET_PASSWORD_COUNTDOWN_RESET: "FORGET_PASSWORD_COUNTDOWN_RESET",
  CHECK_OTP_BY_PHONE: "CHECK_OTP_BY_PHONE",
  CHECK_OTP_BY_EMAIL: "CHECK_OTP_BY_EMAIL",
  CHECK_OTP_SUCCESS: "CHECK_OTP_SUCCESS",
  CHECK_OTP_ERROR: "CHECK_OTP_ERROR",
  RESET_PASSWORD_BY_PHONE: "RESET_PASSWORD_BY_PHONE",
  RESET_PASSWORD_BY_EMAIL: "RESET_PASSWORD_BY_EMAIL",
  RESET_PASSWORD_SUCCESS: "RESET_PASSWORD_SUCCESS",
  RESET_PASSWORD_ERROR: "RESET_PASSWORD_ERROR",
  RESET_PASSWORD_LOADING: "RESET_PASSWORD_LOADING",
  SOCIAL_MEDIA_LOGIN: "SOCIAL_MEDIA_LOGIN",
  SOCIAL_LOGIN_LOADING: "SOCIAL_LOGIN_LOADING",
  EMAIL_RESUBSCRIBE: "EMAIL_RESUBSCRIBE",
  EMAIL_RESUBSCRIBE_LOADING: "EMAIL_RESUBSCRIBE_LOADING",
  EMAIL_RESUBSCRIBE_SUCCESS: "EMAIL_RESUBSCRIBE_SUCCESS",
  EMAIL_RESUBSCRIBE_ERROR: "EMAIL_RESUBSCRIBE_ERROR",
  SIGNUP_OTP_BY_PHONE: "SIGNUP_OTP_BY_PHONE",
  SIGNUP_OTP_BY_EMAIL: "SIGNUP_OTP_BY_EMAIL",
  SIGNUP_COUNTDOWN_RESET: "SIGNUP_COUNTDOWN_RESET",
  SIGNUP_OTP_SUCCESS: "SIGNUP_OTP_SUCCESS",
  SIGNUP_OTP_ERROR: "SIGNUP_OTP_ERROR",
  SIGNUP_OTP_LOADING: "SIGNUP_OTP_LOADING",
  SIGNUP_BY_PHONE: "SIGNUP_BY_PHONE",
  SIGNUP_BY_EMAIL: "SIGNUP_BY_EMAIL",
  SIGNUP_LOADING: "SIGNUP_LOADING",
  SIGNUP_ERROR: "SIGNUP_ERROR",
  SIGNUP_SUCCESS: "SIGNUP_SUCCESS",
  PRELOGIN_BY_PHONE: "PRELOGIN_BY_PHONE",
  PRELOGIN_SUCCESS: "PRELOGIN_SUCCESS",
  PRELOGIN_ERROR: "PRELOGIN_ERROR",
  ACC_EXIST: "ACC_EXIST",
  PRELOGIN_BY_EMAIL: "PRELOGIN_BY_EMAIL",
  loginByPhone: (phone, countryCode, password, history) => ({
    type: actions.LOGIN_BY_PHONE,
    phone,
    countryCode,
    password,
    history
  }),
  loginByEmail: (email, password, history) => ({
    type: actions.LOGIN_BY_EMAIL,
    email,
    password,
    history
  }),
  login: (data, history) => ({
    type: actions.LOGIN_REQUEST,
    data,
    history
  }),
  logout: () => ({
    type: actions.LOGOUT
  }),
  accountKitAuth: (data, history) => ({
    type: actions.AUTH_REQUEST,
    data,
    history
  }),
  updateProfile: (data) => ({
    type: actions.UPDATE_PROFILE_REQUEST,
    data
  }),
  forgetPasswordByPhone: (phone, countryCode, history) => ({
    type: actions.FORGET_PASSWORD_BY_PHONE,
    phone,
    countryCode,
    history
  }),
  forgetPasswordByEmail: (email, history) => ({
    type: actions.FORGET_PASSWORD_BY_EMAIL,
    email,
    history
  }),
  forgetPasswordCountDownReset: () => ({
    type: actions.FORGET_PASSWORD_COUNTDOWN_RESET
  }),
  checkOtpByPhone: (phone, countryCode, code, action, history) => ({
    type: actions.CHECK_OTP_BY_PHONE,
    phone,
    countryCode,
    code,
    action,
    history
  }),
  checkOtpByEmail: (email, code, action, history) => ({
    type: actions.CHECK_OTP_BY_EMAIL,
    email,
    code,
    action,
    history
  }),
  resetPasswordByPhone: (phone, countryCode, otp, password, passwordConfirm) => ({
    type: actions.RESET_PASSWORD_BY_PHONE,
    phone, countryCode, otp, password, passwordConfirm
  }),
  resetPasswordByEmail: (email, otp, password, passwordConfirm) => ({
    type: actions.RESET_PASSWORD_BY_EMAIL,
    email, otp, password, passwordConfirm
  }),
  socialMediaLogin: (email, provider, id, history, isSignUp = false) => ({
    type: actions.SOCIAL_MEDIA_LOGIN,
    email,
    provider,
    id,
    isSignUp,
    history
  }),
  resubscribe: (email) => ({
    type: actions.EMAIL_RESUBSCRIBE,
    email
  }),
  sendOtpByPhone: (phone, countryCode, password, history, resend) => ({
    type: actions.SIGNUP_OTP_BY_PHONE,
    phone,
    countryCode,
    password,
    history,
    resend
  }),
  sendOtpByEmail: (email, password, history, resend) => ({
    type: actions.SIGNUP_OTP_BY_EMAIL,
    email,
    password,
    history,
    resend
  }),
  signupCountDownReset: () => ({
    type: actions.SIGNUP_COUNTDOWN_RESET
  }),
  signupWithPhone: (phone, country_code, password, otp, history) => ({
    type: actions.SIGNUP_BY_PHONE,
    phone, country_code, password, otp, history
  }),
  signupWithEmail: (email, password, otp, history) => ({
    type: actions.SIGNUP_BY_EMAIL,
    email, password, otp, history
  }),
  preloginWithPhone: (phone, country_code) => ({
    type: actions.PRELOGIN_BY_PHONE,
    phone, country_code
  }),
  preloginWithEmail: (email) => ({
    type: actions.PRELOGIN_BY_EMAIL,
    email
  }),
};
export default actions;

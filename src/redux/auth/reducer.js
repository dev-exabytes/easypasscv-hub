import { Map } from "immutable";
import { getToken, getUser, getIsLogin } from "../../helpers/utility";
import actions from "./actions";

const initState = new Map({
  idToken: null,
  user: null,
  companies: null,
  current_company: null,
  loginLoading: false,
  is_signup: false,
  update_profile_loading: false,
  update_profile_succeed: false,
  forgot_password_loading: false,
  forgot_password_otp_sent: false,
  phone: null,
  country_code: null,
  email: null,
  forget_password_count_down: null,
  disable_otp: false,
  otp: null,
  reset_password_loading: false,
  social_login_loading: false,
  social_provider: null,
  resubscribe_loading: false,
  resubscribe: false,
  signup_count_down: null,
  signup_otp_sent: false,
  signup_otp_loading: false,
  password: null,
  signup_loading: false,
  prelogin_result: null
});

export default function authReducer(state = initState.merge(getToken(), getUser(), getIsLogin()), action) {
  switch (action.type) {
    case actions.LOGIN_LOADING:
      return state.set("loginLoading", true);
    case actions.PRELOGIN_SUCCESS:
      return state.set("loginLoading", false)
        .set("prelogin_result", action.preloginResult)
        .set("phone", action.phone)
        .set("country_code", action.country_code)
        .set("email", action.email);
    case actions.PRELOGIN_ERROR:
      return state.set("loginLoading", false)
        .set("prelogin_result", null);
    case actions.ACC_EXIST:
      return state.set("acc_exist", true);
    case actions.SIMILAR_ACC_EXIST:
      return state.set("similar_acc_exist", true);
    case actions.LOGIN_SUCCESS:
      return state.set("idToken", action.token)
        .set("user", action.user)
        .set("loginLoading", false)
        .set("is_signup", action.is_signup)
        .set("social_login_loading", false);
    case actions.LOGIN_ADMIN_ERROR:
    case actions.LOGIN_ERROR:
      return state.set("idToken", null)
        .set("user", null)
        .set("loginLoading", false)
        .set("is_signup", false)
        .set("social_login_loading", false);
    case actions.LOGOUT:
      return initState;
    case actions.SIGNUP_SUCCESS:
      return state.set("idToken", action.token)
        .set("user", action.user)
        .set("is_signup", true)
        .set("signup_loading", false)
        .set("social_login_loading", false);
    case actions.UPDATE_PROFILE_LOADING:
      return state.set("update_profile_loading", true)
        .set("update_profile_succeed", false);
    case actions.UPDATE_PROFILE_SUCCESS:
      return state.set("user", action.user)
        .set("update_profile_loading", false)
        .set("update_profile_succeed", true);
    case actions.UPDATE_PROFILE_ERROR:
      return state.set("update_profile_loading", false)
        .set("update_profile_succeed", false);
    case actions.FORGET_PASSWORD_LOADING:
      return state.set("forgot_password_loading", true)
        .set("otp", null);
    case actions.FORGET_PASSWORD_ERROR:
      return state.set("forgot_password_loading", false)
        .set("forgot_password_otp_sent", false)
        .set("forget_password_count_down", action.count_down);
    case actions.FORGET_PASSWORD_SUCCESS:
      return state.set("forgot_password_loading", false)
        .set("forgot_password_otp_sent", true)
        .set("phone", action.phone)
        .set("country_code", action.country_code)
        .set("email", action.email)
        .set("forget_password_count_down", action.count_down)
        .set("disable_otp", false);
    case actions.FORGET_PASSWORD_COUNTDOWN_RESET:
      return state.set("forget_password_count_down", null);
    case actions.CHECK_OTP_BY_PHONE:
    case actions.CHECK_OTP_BY_EMAIL:
      return state.set("disable_otp", true);
    case actions.CHECK_OTP_SUCCESS:
      return state.set("disable_otp", false)
        .set("otp", action.otp);
    case actions.CHECK_OTP_ERROR:
      return state.set("disable_otp", false)
        .set("otp", null);
    case actions.RESET_PASSWORD_LOADING:
      return state.set("reset_password_loading", true);
    case actions.RESET_PASSWORD_SUCCESS:
      return state.set("reset_password_loading", false);
    case actions.RESET_PASSWORD_ERROR:
      return state.set("reset_password_loading", false);
    case actions.SOCIAL_LOGIN_LOADING:
      return state.set("social_login_loading", true)
        .set("social_provider", action.provider);
    case actions.EMAIL_RESUBSCRIBE_LOADING:
      return state.set("resubscribe_loading", true);
    case actions.EMAIL_RESUBSCRIBE_SUCCESS:
      return state.set("resubscribe_loading", false)
        .set("resubscribe", true);
    case actions.EMAIL_RESUBSCRIBE_ERROR:
      return state.set("resubscribe_loading", false);
    case actions.SIGNUP_OTP_ERROR:
      return state.set("signup_otp_loading", false)
        .set("signup_otp_sent", false)
        .set("signup_count_down", action.count_down);
    case actions.SIGNUP_OTP_SUCCESS:
      return state.set("signup_otp_loading", false)
        .set("signup_otp_sent", true)
        .set("phone", action.phone)
        .set("country_code", action.country_code)
        .set("email", action.email)
        .set("password", action.password)
        .set("signup_count_down", action.count_down)
        .set("disable_otp", false);
    case actions.SIGNUP_COUNTDOWN_RESET:
      return state.set("signup_count_down", null);
    case actions.SIGNUP_OTP_LOADING:
      return state.set("signup_otp_loading", true)
        .set("otp", null);
    case actions.SIGNUP_LOADING:
      return state.set("signup_loading", true);
    case actions.SIGNUP_ERROR:
      return state.set("signup_loading", false)
        .set("social_login_loading", false);
    default:
      return state;
  }
}

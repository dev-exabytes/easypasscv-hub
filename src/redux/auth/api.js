import { baseUrl, apiHeader } from "../../config";

export const onLoginRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/login`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 && response.status !== 422 && response.status !== 401 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onCheckSuperAdmin = (idToken) => {
  return new Promise((resolve, reject) => {
    if (!idToken) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/login/admin`,
      {
        method: "GET",
        headers: {
          ...apiHeader, ...{
            "Content-Type": "application/json",
            Authorization: `Bearer ${idToken}`,
            Accept: "application/x.app.v2+json"
          }
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onAuthRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/accountkit/hub`, {
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/x.app.v3+json"
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onUpdateProfileRequest = (data: any, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/users`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`,
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "PUT",
      body: JSON.stringify(data)
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onSendResetPasswordOTPRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/recovery`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onCheckOTP = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/check`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onResetPasswordRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/password/reset`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const onSocialMediaLoginRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/oauth`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onMandrillResubscribe = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`https://mandrillapp.com/api/1.0/rejects/delete.json`, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
}

export const onSendSignupOTPRequest = (data: any, isEmail: any) => {
  return new Promise((resolve, reject) => {
    let url = `${baseUrl}/v1/auth/code`;
    if (isEmail) {
      url = `${baseUrl}/v1/auth/code/email`;
    }
    return fetch(url, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status === 200 || response.status === 422 ? response : reject(response)))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onSignUpRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/signup`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onPreLoginRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/login/check`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onPreSignUpRequest = (data: any) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/auth/signup/check`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Accept": "application/x.app.v3+json"
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import { push } from "react-router-redux";
import { clearToken, clearUser, clearCompany, clearCurrentCompany, clearIsLogin } from "../../helpers/utility";
import actions from "./actions";
import companyActions from "../company/actions";
import { notification, Modal } from "antd";
import {
  onLoginRequest,
  onSignUpRequest,
  onCheckSuperAdmin,
  onAuthRequest,
  onUpdateProfileRequest,
  onSendResetPasswordOTPRequest,
  onCheckOTP,
  onResetPasswordRequest,
  onSocialMediaLoginRequest,
  onMandrillResubscribe,
  onSendSignupOTPRequest,
  onPreLoginRequest,
  onPreSignUpRequest
} from "./api";
import { mandrillKey } from "../../config";

const timezone = require("moment-timezone");
const { confirm } = Modal;
export function* loginByPhoneRequest() {
  yield takeEvery(actions.LOGIN_BY_PHONE, function* (request) {
    try {
      yield put({
        type: actions.LOGIN_LOADING
      });
      const data = JSON.stringify({
        phone: request.phone,
        country_code: request.countryCode.toUpperCase(),
        password: request.password,
        os_token: 'hub',
      });
      const loginResult = yield call(onLoginRequest, data);

      if ("error" in loginResult) {
        yield put({
          type: actions.LOGIN_ERROR,
          message: 'Invalid credential'
        });
      } else {
        const { token, user } = loginResult.data;
        if (token) {
          yield put({
            type: actions.LOGIN_SUCCESS,
            token: token,
            user: user,
            profile: "Profile",
            url: request.history
          });
        } else {
          yield put({ type: actions.LOGIN_ERROR });
        }
      }
    } catch (error) {
      yield put({ type: actions.LOGIN_ERROR });
    }
  });
}

export function* loginByEmailRequest() {
  yield takeEvery(actions.LOGIN_BY_EMAIL, function* (request) {
    try {
      yield put({
        type: actions.LOGIN_LOADING
      });
      const data = JSON.stringify({
        email: request.email,
        password: request.password,
        os_token: 'hub',
      });
      const loginResult = yield call(onLoginRequest, data);

      if ("error" in loginResult) {
        yield put({
          type: actions.LOGIN_ERROR,
          message: 'Invalid credential'
        });
      } else {
        const { token, user } = loginResult.data;
        if (token) {
          yield put({
            type: actions.LOGIN_SUCCESS,
            token: token,
            user: user,
            profile: "Profile",
            url: request.history
          });
        } else {
          yield put({ type: actions.LOGIN_ERROR });
        }
      }
    } catch (error) {
      yield put({ type: actions.LOGIN_ERROR });
    }
  });
}
export function* loginRequest() {
  yield takeEvery(actions.LOGIN_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.LOGIN_LOADING
      });
      const loginResult = yield call(onLoginRequest, request.data);
      const { token, user } = loginResult.data;
      if (token) {
        const adminResult = yield call(onCheckSuperAdmin, token);
        const { company, message } = adminResult;
        if (company) {
          yield put({
            type: actions.LOGIN_SUCCESS,
            token: token,
            user: user,
            profile: "Profile",
            url: request.history
          });
        } else {
          yield put({
            type: actions.LOGIN_ERROR,
            message
          });
        }
      } else {
        yield put({
          type: actions.LOGIN_ERROR,
          message: loginResult.message
        });
      }
    } catch (error) {
      yield put({ type: actions.LOGIN_ERROR });
    }
  });
}

export function* loginSuccess() {
  yield takeEvery(actions.LOGIN_SUCCESS, function* (payload) {
    yield localStorage.setItem("id_token", payload.token);
    yield localStorage.setItem("user", JSON.stringify(payload.user));
    yield localStorage.setItem("is_login", true);
    yield put({
      type: companyActions.COMPANIES,
    });
    if (payload.url) {
      yield put(push({
        pathname: payload.url.pathname,
        search: payload.url.search,
        state: payload.url
      }));
    } else {
      yield put(push("/dashboard"));
    }
  });
}

export function* loginError() {
  yield takeEvery(actions.LOGIN_ERROR, function* (payload) {
    clearToken();
    clearUser();
    clearCompany();
    clearCurrentCompany();
    clearIsLogin();
    if (payload.message) {
      notification.open({
        message: "Error",
        description: payload.message
      });
    } else {
      notification.open({
        message: "Error",
        description: "Unable to login"
      });
    }
    yield put(push("/auth"));
  });
}

export function* notAdmin() {
  yield takeEvery(actions.LOGIN_ADMIN_ERROR, function* () {
    console.log('not admin');
    clearToken();
    clearUser();
    clearCompany();
    clearCurrentCompany();
    clearIsLogin();
    notification.open({
      message: "Oops",
      description: "Only Superadmin of company can login to this page"
    });
    yield put(push("/auth"));
  });
}

export function* logout() {
  yield takeEvery(actions.LOGOUT, function* () {
    clearToken();
    clearUser();
    clearCompany();
    clearCurrentCompany();
    clearIsLogin();
    yield put({
      type: companyActions.CLEAR_COMPANY
    });
    yield put(push("/"));
  });
}

export function* signupByPhone() {
  yield takeEvery(actions.SIGNUP_BY_PHONE, function* (request) {
    try {
      yield put({
        type: actions.SIGNUP_LOADING
      });
      const data = JSON.stringify({
        phone: request.phone,
        country_code: request.country_code.toUpperCase(),
        os_token: 'hub',
        verification_code: request.otp,
        password: request.password,
        timezone: timezone.tz.guess()
      });
      const signUpResult = yield call(onSignUpRequest, data);
      const { token, user } = signUpResult.data;
      if (token) {
        yield put({
          type: actions.SIGNUP_SUCCESS,
          token: token,
          user: user,
          url: request.history
        });
      } else {
        yield put({
          type: actions.SIGNUP_ERROR
        });
      }
    } catch (error) {
      yield put({
        type: actions.SIGNUP_ERROR
      });
    }
  });
}

export function* signupByEmail() {
  yield takeEvery(actions.SIGNUP_BY_EMAIL, function* (request) {
    try {
      yield put({
        type: actions.SIGNUP_LOADING
      });
      const data = JSON.stringify({
        email: request.email,
        os_token: 'hub',
        verification_code: request.otp,
        password: request.password,
        timezone: timezone.tz.guess()
      });
      const signUpResult = yield call(onSignUpRequest, data);
      const { token, user } = signUpResult.data;
      if (token) {
        yield put({
          type: actions.SIGNUP_SUCCESS,
          token: token,
          user: user,
          url: request.history
        });
      } else {
        yield put({
          type: actions.SIGNUP_ERROR
        });
      }
    } catch (error) {
      yield put({
        type: actions.SIGNUP_ERROR
      });
    }
  });
}

export function* signupSuccess() {
  yield takeEvery(actions.SIGNUP_SUCCESS, function* (payload) {
    yield localStorage.setItem("id_token", payload.token);
    yield localStorage.setItem("is_signup", true);
    yield put({
      type: companyActions.COMPANIES,
    });
    if (payload.url) {
      yield put(push({
        pathname: payload.url.pathname,
        search: payload.url.search
      }));
    } else {
      yield put(push("/dashboard"));
    }
  });
}

export function* signupError() {
  yield takeEvery(actions.SIGNUP_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: payload.message || "Sign Up unsuccessful."
    });
  });
}

export function* authRequest() {
  yield takeEvery(actions.AUTH_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.LOGIN_LOADING
      });
      const result = yield call(onAuthRequest, request.data);
      const { token, user, is_signup } = result.data;
      yield put({
        type: actions.LOGIN_SUCCESS,
        token,
        user,
        profile: "Profile",
        url: request.history,
        is_signup
      });
    }
    catch (error) {
      yield put({ type: actions.LOGIN_ERROR });
    }
  });
}

export function* updateProfileRequest() {
  yield takeEvery(actions.UPDATE_PROFILE_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.UPDATE_PROFILE_LOADING
      });
      const result = yield call(onUpdateProfileRequest, request.data, localStorage.getItem("id_token"));
      const { user } = result.data;
      if (user) {
        yield localStorage.setItem("user", JSON.stringify(user));
        yield put({
          type: actions.UPDATE_PROFILE_SUCCESS,
          user
        });
      } else {
        yield put({ type: actions.UPDATE_PROFILE_ERROR });
      }
    } catch (error) {
      yield put({ type: actions.UPDATE_PROFILE_ERROR });
    }
  });
}

export function* updateProfileSuccess() {
  yield takeEvery(actions.UPDATE_PROFILE_SUCCESS, function* (payload) {
    yield notification.open({
      message: "Success",
      description: "Update profile successful."
    });
  });
}

export function* updateProfileError() {
  yield takeEvery(actions.UPDATE_PROFILE_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: "Update profile failed."
    });
  });
}

export function* forgetPasswordByPhoneRequest() {
  yield takeEvery(actions.FORGET_PASSWORD_BY_PHONE, function* (request) {
    try {
      yield put({
        type: actions.FORGET_PASSWORD_LOADING
      });
      const data = JSON.stringify({
        phone: request.phone,
        country_code: request.countryCode.toUpperCase(),
        os_token: 'hub',
      });
      const result = yield call(onSendResetPasswordOTPRequest, data);
      yield put({
        type: actions.FORGET_PASSWORD_SUCCESS,
        message: result.message,
        phone: request.phone,
        country_code: request.countryCode,
        email: null,
        count_down: Date.now() + 30000,
        history: request.history
      });
    } catch (error) {
      if (error.headers.get("Retry-After")) {
        notification.open({
          message: "Error",
          description: "Too many attempt"
        });
        yield put({
          type: actions.FORGET_PASSWORD_ERROR,
          count_down: Date.now() + (error.headers.get("Retry-After") * 1000),
        });
      } else {
        notification.open({
          message: "Error",
          description: "Invalid mobile number"
        });
        yield put({
          type: actions.FORGET_PASSWORD_ERROR,
          count_down: null
        });
      }
    }
  });
}

export function* forgetPasswordByEmailRequest() {
  yield takeEvery(actions.FORGET_PASSWORD_BY_EMAIL, function* (request) {
    try {
      yield put({
        type: actions.FORGET_PASSWORD_LOADING
      });
      const data = JSON.stringify({
        email: request.email,
        os_token: 'hub',
      });
      const result = yield call(onSendResetPasswordOTPRequest, data);
      yield put({
        type: actions.FORGET_PASSWORD_SUCCESS,
        message: result.message,
        phone: null,
        country_code: null,
        email: request.email,
        count_down: Date.now() + 30000,
        history: request.history
      });
    } catch (error) {
      if (error.headers.get("Retry-After")) {
        notification.open({
          message: "Error",
          description: "Please wait"
        });
        yield put({
          type: actions.FORGET_PASSWORD_ERROR,
          count_down: Date.now() + (error.headers.get("Retry-After") * 1000),
        });
      } else {
        notification.open({
          message: "Error",
          description: "Invalid mobile number"
        });
        yield put({
          type: actions.FORGET_PASSWORD_ERROR,
          count_down: null
        });
      }
    }
  });
}

export function* forgetPasswordSuccess() {
  yield takeEvery(actions.FORGET_PASSWORD_SUCCESS, function* (payload) {
    notification.open({
      message: "Success",
      description: payload.message
    });
    yield put(push({
      pathname: "/auth/resetpassword/otp",
      state: payload.history
    }));
  });
}

export function* checkOtpByPhoneRequest() {
  yield takeEvery(actions.CHECK_OTP_BY_PHONE, function* (request) {
    try {
      const data = JSON.stringify({
        phone: request.phone,
        country_code: request.countryCode.toUpperCase(),
        action: request.action,
        code: request.code
      });
      yield call(onCheckOTP, data);
      yield put({
        type: actions.CHECK_OTP_SUCCESS,
        otp: request.code,
        action: request.action,
        url: request.history
      });
    } catch (error) {
      yield put({ type: actions.CHECK_OTP_ERROR });
    }
  });
}

export function* checkOtpByEmailRequest() {
  yield takeEvery(actions.CHECK_OTP_BY_EMAIL, function* (request) {
    try {
      const data = JSON.stringify({
        email: request.email,
        action: request.action,
        code: request.code
      });
      yield call(onCheckOTP, data);
      yield put({
        type: actions.CHECK_OTP_SUCCESS,
        otp: request.code,
        action: request.action,
        url: request.history
      });
    } catch (error) {
      yield put({ type: actions.CHECK_OTP_ERROR });
    }
  });
}

export function* checkOtpSuccess() {
  yield takeEvery(actions.CHECK_OTP_SUCCESS, function* (payload) {
    if (payload.action === 3) {
      yield put(push({
        pathname: "/auth/resetpassword",
        state: payload.url
      }));
    } else if (payload.url) {
      yield put(push({
        pathname: '/auth/signup',
        search: "?signup=1",
        state: payload.url
      }));
    } else {
      yield put(push("/auth/signup?signup=1"));
    }
  });
}

export function* checkOtpError() {
  yield takeEvery(actions.CHECK_OTP_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: "Invalid OTP"
    });
  });
}

export function* resetPasswordByPhoneRequest() {
  yield takeEvery(actions.RESET_PASSWORD_BY_PHONE, function* (request) {
    try {
      yield put({
        type: actions.RESET_PASSWORD_LOADING
      });
      let data = JSON.stringify({
        phone: request.phone,
        country_code: request.countryCode.toUpperCase(),
        password: request.password,
        password_confirmation: request.passwordConfirm,
        security_code: request.otp
      });
      yield call(onResetPasswordRequest, data);
      yield put({
        type: actions.RESET_PASSWORD_SUCCESS
      });
      yield put({
        type: actions.LOGIN_BY_PHONE,
        phone: request.phone,
        countryCode: request.countryCode,
        password: request.password,
        history: null
      });
    } catch (error) {
      yield put({ type: actions.RESET_PASSWORD_ERROR });
    }
  });
}

export function* resetPasswordByEmailRequest() {
  yield takeEvery(actions.RESET_PASSWORD_BY_EMAIL, function* (request) {
    try {
      yield put({
        type: actions.RESET_PASSWORD_LOADING
      });
      let data = JSON.stringify({
        email: request.email,
        password: request.password,
        password_confirmation: request.passwordConfirm,
        security_code: request.otp
      });
      yield call(onResetPasswordRequest, data);
      yield put({
        type: actions.RESET_PASSWORD_SUCCESS
      });
      yield put({
        type: actions.LOGIN_BY_EMAIL,
        email: request.email,
        password: request.password,
        history: null
      });
    } catch (error) {
      yield put({ type: actions.RESET_PASSWORD_ERROR });
    }
  });
}

export function* resetPasswordSuccess() {
  yield takeEvery(actions.RESET_PASSWORD_SUCCESS, function* (payload) {
    notification.open({
      message: "Success",
      description: "Your password has been reset."
    });
  });
}

export function* resetPasswordError() {
  yield takeEvery(actions.RESET_PASSWORD_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: "Could not reset password. Please try again."
    });
  });
}

export function* socialMediaLoginRequest() {
  yield takeEvery(actions.SOCIAL_MEDIA_LOGIN, function* (request) {
    try {
      yield put({
        type: actions.SOCIAL_LOGIN_LOADING,
        provider: request.provider
      });
      let proceedSignUp = true;
      if (request.isSignUp) {
        const body = JSON.stringify({
          email: request.email
        });
        const result = yield call(onPreSignUpRequest, body);
        const { error } = result;
        if (error) {
          proceedSignUp = false;
          yield put({
            type: actions.SIGNUP_ERROR,
            message: error.errors.hasOwnProperty('email') ? error.errors.email[0] : error.message
          });
        }
      }
      if (proceedSignUp) {
        const data = JSON.stringify({
          email: request.email,
          provider: request.provider,
          id: request.id,
          os_token: "hub"
        });
        const result = yield call(onSocialMediaLoginRequest, data);

        if ("error" in result) {
          yield put({
            type: actions.LOGIN_ERROR,
            message: result.error.message
          });
        } else {
          const { token, user } = result.data;
          if (token) {
            yield put({
              type: actions.LOGIN_SUCCESS,
              token: token,
              user: user,
              profile: "Profile",
              url: request.history
            });
          } else {
            yield put({ type: actions.LOGIN_ERROR });
          }
        }
      }
    } catch (error) {
      yield put({ type: actions.LOGIN_ERROR });
    }
  });
}

export function* emailResubscribeRequest() {
  yield takeEvery(actions.EMAIL_RESUBSCRIBE, function* (request) {
    try {
      yield put({
        type: actions.EMAIL_RESUBSCRIBE_LOADING
      });
      const data = JSON.stringify({
        email: request.email,
        key: mandrillKey
      });
      const result = yield call(onMandrillResubscribe, data);
      const { deleted, message } = result;
      if (deleted) {
        yield put({
          type: actions.EMAIL_RESUBSCRIBE_SUCCESS
        });
      } else {
        yield put({
          type: actions.EMAIL_RESUBSCRIBE_ERROR,
          message
        });
      }
    } catch (error) {
      yield put({ type: actions.EMAIL_RESUBSCRIBE_ERROR });
    }
  });
}

export function* resubscribeError() {
  yield takeEvery(actions.EMAIL_RESUBSCRIBE_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: payload.message || "Could not resubscribe your email. Please try again."
    });
  });
}

export function* signupOtpByPhoneRequest() {
  yield takeEvery(actions.SIGNUP_OTP_BY_PHONE, function* (request) {
    try {
      yield put({
        type: actions.SIGNUP_OTP_LOADING
      });
      const data = JSON.stringify({
        phone: request.phone,
        country_code: request.countryCode.toUpperCase()
      });
      const result = yield call(onSendSignupOTPRequest, data);
      const { error } = result;
      if (error) {
        notification.open({
          message: "Error",
          description: error.message
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: null
        });
      } else {
        yield put({
          type: actions.SIGNUP_OTP_SUCCESS,
          message: result.message,
          phone: request.phone,
          country_code: request.countryCode,
          email: null,
          password: request.password,
          count_down: Date.now() + 30000,
          resend: request.resend,
          url: request.history
        });
      }

    } catch (error) {
      if (error.headers.get("Retry-After")) {
        notification.open({
          message: "Error",
          description: "Too many attempt"
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: Date.now() + (error.headers.get("Retry-After") * 1000),
        });
      } else {
        notification.open({
          message: "Error",
          description: "Invalid mobile number"
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: null
        });
      }
    }
  });
}

export function* signupOtpByEmailRequest() {
  yield takeEvery(actions.SIGNUP_OTP_BY_EMAIL, function* (request) {
    try {
      yield put({
        type: actions.SIGNUP_OTP_LOADING
      });
      const data = JSON.stringify({
        email: request.email
      });
      const result = yield call(onSendSignupOTPRequest, data, 1);
      const { error } = result;
      if (error && error.hasOwnProperty('errors')) {
        notification.open({
          message: "Error",
          description: error.errors.email[0]
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: null
        });
      } else if (error) {
        notification.open({
          message: "Error",
          description: error.message
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: null
        });
      } else {
        yield put({
          type: actions.SIGNUP_OTP_SUCCESS,
          message: result.message,
          phone: null,
          country_code: null,
          email: request.email,
          password: request.password,
          count_down: Date.now() + 30000,
          resend: request.resend,
          url: request.history
        });
      }
    } catch (error) {
      if (error.headers.get("Retry-After")) {
        notification.open({
          message: "Error",
          description: "Too many attempt"
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: Date.now() + (error.headers.get("Retry-After") * 1000),
        });
      } else {
        notification.open({
          message: "Error",
          description: "Invalid email"
        });
        yield put({
          type: actions.SIGNUP_OTP_ERROR,
          count_down: null
        });
      }
    }
  });
}

export function* signupOtpSuccess() {
  yield takeEvery(actions.SIGNUP_OTP_SUCCESS, function* (payload) {
    notification.open({
      message: "Success",
      description: payload.message
    });
    if (!payload.resend) {
      if (payload.url) {
        yield put(push({
          pathname: '/auth/signup/otp',
          state: payload.url
        }));
      } else {
        yield put(push("/auth/signup/otp"));
      }

    }
  });
}

export function* preloginByPhoneRequest() {
  yield takeEvery(actions.PRELOGIN_BY_PHONE, function* (request) {
    try {
      yield put({
        type: actions.LOGIN_LOADING
      });
      const body = JSON.stringify({
        phone: request.phone,
        country_code: request.country_code.toUpperCase()
      });
      const result = yield call(onPreLoginRequest, body);
      const { error, data } = result;
      if (error) {
        notification.open({
          message: "Error",
          description: error.message
        });
        yield put({
          type: actions.PRELOGIN_ERROR
        });
      } else {
        yield put({
          type: actions.PRELOGIN_SUCCESS,
          preloginResult: data,
          phone: request.phone,
          country_code: request.country_code,
          email: null
        });
      }
    } catch (error) {
      notification.open({
        message: "Error",
        description: "Oops, something went wrong."
      });
      yield put({
        type: actions.PRELOGIN_ERROR
      });
    }
  });
}

export function* preloginByEmailRequest() {
  yield takeEvery(actions.PRELOGIN_BY_EMAIL, function* (request) {
    try {
      yield put({
        type: actions.LOGIN_LOADING
      });
      const body = JSON.stringify({
        email: request.email
      });
      const result = yield call(onPreLoginRequest, body);
      const { error, data } = result;
      if (error) {
        notification.open({
          message: "Error",
          description: error.message
        });
        yield put({
          type: actions.PRELOGIN_ERROR
        });
      } else {
        yield put({
          type: actions.PRELOGIN_SUCCESS,
          preloginResult: data,
          phone: null,
          country_code: null,
          email: request.email
        });
      }
    } catch (error) {
      notification.open({
        message: "Error",
        description: "Oops, something went wrong."
      });
      yield put({
        type: actions.PRELOGIN_ERROR
      });
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(loginRequest),
    fork(loginSuccess),
    fork(loginError),
    fork(notAdmin),
    fork(logout),
    fork(signupByPhone),
    fork(signupByEmail),
    fork(signupSuccess),
    fork(authRequest),
    fork(updateProfileRequest),
    fork(updateProfileSuccess),
    fork(updateProfileError),
    fork(loginByPhoneRequest),
    fork(loginByEmailRequest),
    fork(forgetPasswordByPhoneRequest),
    fork(forgetPasswordSuccess),
    fork(forgetPasswordByEmailRequest),
    fork(checkOtpByPhoneRequest),
    fork(checkOtpByEmailRequest),
    fork(checkOtpSuccess),
    fork(checkOtpError),
    fork(resetPasswordByPhoneRequest),
    fork(resetPasswordByEmailRequest),
    fork(resetPasswordSuccess),
    fork(resetPasswordError),
    fork(socialMediaLoginRequest),
    fork(emailResubscribeRequest),
    fork(resubscribeError),
    fork(signupOtpByPhoneRequest),
    fork(signupOtpByEmailRequest),
    fork(signupOtpSuccess),
    fork(signupError),
    fork(preloginByPhoneRequest),
    fork(preloginByEmailRequest)
  ]);
}

import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "./actions";
import { notification, Modal } from "antd";
import {
  onGetJoinListRequest,
  onJoinCompanyRequest,
  onRequestToJoinCompany
} from "./api";

export function* getJoinList() {
  yield takeEvery(actions.JOIN_LIST, function* (request) {
    try {
      const result = yield call(onGetJoinListRequest, localStorage.getItem("id_token"));
      const { data } = result;
      if (data.length > 0) {
        yield put({
          type: actions.JOIN_LIST_SUCCESS,
          data
        });
      }
    } catch (error) {
      //
    }
  });
}

export function* joinCompanyRequest() {
  yield takeEvery(actions.JOIN_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.JOIN
      });
      const result = yield call(onJoinCompanyRequest, request.workspace, localStorage.getItem("id_token"));
      const { message, error } = result;
      if (message) {
        yield put({
          type: actions.JOIN_SUCCESS,
          message
        });
      }
      if (error) {
        yield put({
          type: actions.JOIN_422,
          message: error.message
        });
      }

    } catch (error) {
      yield put({
        type: actions.JOIN_ERROR
      });
    }
  });
}

export function* joinCompanySuccess() {
  yield takeEvery(actions.JOIN_SUCCESS, function* (payload) {
    yield Modal.success({
      title: 'Submitted!',
      content: payload.message,
    });
  });
}

export function* joinCompanyError() {
  yield takeEvery(actions.JOIN_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: "Join company request not submitted."
    });
  });
}

export function* joinCompany422() {
  yield takeEvery(actions.JOIN_422, function* (payload) {
    yield notification.open({
      message: "Error",
      description: payload.message
    });
  });
}

export function* requestToJoin() {
  yield takeEvery(actions.RTJ, function* (request) {
    try {
      yield put({
        type: actions.LOAD_RTJ
      });
      const result = yield call(onRequestToJoinCompany, request.employee_id, request.email, request.company_name, localStorage.getItem("id_token"));
      const { message, data } = result;
      if (message && data) {
        yield put({
          type: actions.RTJ_SUCCESS
        });
        yield Modal.success({
          title: 'Submitted!',
          content: message,
        });
      } else {
        yield put({
          type: actions.RTJ_ERROR
        });
        yield Modal.info({
          title: 'Oops!',
          content: message,
        });
      }
    } catch (error) {
      yield put({
        type: actions.RTJ_ERROR
      });
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getJoinList),
    fork(joinCompanyRequest),
    fork(joinCompanySuccess),
    fork(joinCompanyError),
    fork(joinCompany422),
    fork(requestToJoin)
  ]);
}



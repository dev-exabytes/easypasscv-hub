import { baseUrl, apiHeader } from "../../config.js";

export const onGetJoinListRequest = (token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/join/requests?type=workspace`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onJoinCompanyRequest = (workspace: string, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/invites`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "POST",
      body: JSON.stringify({
        workspace_name: workspace
      })
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => {
        return reject(error);
      });
  });
};

export const onRequestToJoinCompany = (employee_id: int, email: string, company_name: string, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/invites/requests`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "POST",
      body: JSON.stringify({
        employee_id, email, company_name
      })
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => {
        return reject(error);
      });
  });
};

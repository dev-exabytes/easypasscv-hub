import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  data: [],
  loading: false,
  join422: false,
  join_company_succeed: false,
  rtjLoading: false
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.JOIN:
      return state
        .set("loading", true)
        .set("join422", false)
        .set("join_company_succeed", false);
    case actions.JOIN_LIST_SUCCESS:
      return state
        .set("data", action.data);
    case actions.JOIN_SUCCESS:
      return state
        .set("loading", false)
        .set("join_company_succeed", true);
    case actions.JOIN_ERROR:
      return state
        .set("loading", false)
        .set("join422", false)
        .set("join_company_succeed", false);
    case actions.JOIN_422:
      return state
        .set("loading", false)
        .set("join422", true);
    case actions.CLEAR_ERROR:
      return state
        .set("loading", false)
        .set("join422", false);
    case actions.LOAD_RTJ:
      return state
        .set("rtjLoading", true)
        .set("join_company_succeed", false);
    case actions.RTJ_SUCCESS:
      return state
        .set("rtjLoading", false)
        .set("join_company_succeed", true);
    case actions.RTJ_ERROR:
      return state
        .set("rtjLoading", false)
        .set("join_company_succeed", false);
    default:
      return state;
  }
}

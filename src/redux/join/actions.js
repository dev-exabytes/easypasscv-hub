const joinActions = {
  JOIN: "JOIN",
  JOIN_SUCCESS: "JOIN_SUCCESS",
  JOIN_ERROR: "JOIN_ERROR",
  JOIN_LIST: "JOIN_LIST",
  JOIN_LIST_SUCCESS: "JOIN_LIST_SUCCESS",
  JOIN_REQUEST: "JOIN_REQUEST",
  JOIN_422: "JOIN_422",
  CLEAR_ERROR: "CLEAR_ERROR",
  LOAD_RTJ: "LOAD_RTJ",
  RTJ: "RTJ",
  RTJ_SUCCESS: "RTJ_SUCCESS",
  RTJ_ERROR: "RTJ_ERROR",
  getSubmitted: () => ({
    type: joinActions.JOIN_LIST
  }),
  joinCompany: (workspace) => ({
    type: joinActions.JOIN_REQUEST,
    workspace
  }),
  clearError: () => ({
    type: joinActions.CLEAR_ERROR
  }),
  sendRequestToAdmin: (employee_id, email, company_name) => ({
    type: joinActions.RTJ,
    employee_id, email, company_name
  })
};
export default joinActions;


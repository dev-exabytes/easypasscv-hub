const actions = {
  ANNOUNCEMENT_LOADING: "ANNOUNCEMENT_LOADING",
  GET_ANNOUNCEMENT: "GET_ANNOUNCEMENT",
  ANNOUNCEMENT_ERROR: "ANNOUNCEMENT_ERROR",
  ANNOUNCEMENT_SUCCESS: "ANNOUNCEMENT_SUCCESS",
  LOADING_DETAIL: "LOADING_DETAIL",
  GET_DETAIL: "GET_DETAIL",
  DETAIL_ERROR: "DETAIL_ERROR",
  DETAIL_SUCCESS: "DETAIL_SUCCESS",
  LOADING_CREATE: "LOADING_CREATE",
  CREATE_ANNOUNCEMENT: "CREATE_ANNOUNCEMENT",
  CREATE_ERROR: "CREATE_ERROR",
  CREATE_SUCCESS: "CREATE_SUCCESS",
  UPDATE_ANNOUNCEMENT: "UPDATE_ANNOUNCEMENT",
  UPDATE_SUCCESS: "UPDATE_SUCCESS",
  UPDATE_FAILED: "UPDATE_FAILED",
  DELETE_ANNOUNCEMENT: "DELETE_ANNOUNCEMENT",
  DELETE_ERROR: "DELETE_ERROR",
  DELETE_SUCCESS: "DELETE_SUCCESS",
  CHECK_ACCESS: "CHECK_ACCESS",
  SUCCESS_ACCESS: "SUCCESS_ACCESS",
  FAILED_ACCESS: "FAILED_ACCESS",
  getAnnouncement: (companyId, page, token) => ({
    type: actions.GET_ANNOUNCEMENT,
    companyId,
    page,
    token
  }),
  getAnnouncementDetail: (companyId, announcementId, token) => ({
    type: actions.GET_DETAIL,
    companyId,
    announcementId,
    token
  }),
  createAnnouncement: (companyId, data, token) => ({
    type: actions.CREATE_ANNOUNCEMENT,
    companyId,
    data,
    token
  }),
  updateAnnouncement: (companyId, data, announcementId, token) => ({
    type: actions.UPDATE_ANNOUNCEMENT,
    companyId,
    data,
    announcementId,
    token
  }),
  deleteAnnouncement: (companyId, announcementId, token) => ({
    type: actions.DELETE_ANNOUNCEMENT,
    companyId,
    announcementId,
    token
  }),
  checkAnnouncementAccess: (companyId, token) => ({
    type: actions.CHECK_ACCESS,
    companyId,
    token
  }),
};
export default actions;

import { baseUrl, apiHeader } from "../../config.js";

export const onGetAnnouncementRequest = (companyId: integer, page: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/announcements?page=${page}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetAnnouncementDetailRequest = (companyId: integer, announcementId: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/announcements/${announcementId}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onCreateOrUpdateAnnouncementRequest = (companyId: integer, data: any, announcementId: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/announcements${announcementId === 0 ? '' : '/' + announcementId}`, {
      headers: {
        ...apiHeader, ...{
          //"Content-Type": "application/json", //Not sure which type is suitable, so let it automatically detect
          "Authorization": `bearer ${token}`
        }
      },
      method: "POST",
      body: data
    })
      .then(response => response.json().then(data => ({ status: response.status, body: data })))
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onDeleteAnnouncementRequest = (companyId: integer, announcementId: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/announcements/${announcementId}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "DELETE"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onCheckAnnouncementAccessRequest = (companyId, idToken) => {
  return new Promise((resolve, reject) => {
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/access?access_privilege_id=10`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};
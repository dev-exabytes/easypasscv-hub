import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "./actions";
import { notification } from "antd";
import { history } from '../store';
import {
  onGetAnnouncementRequest,
  onGetAnnouncementDetailRequest,
  onCreateOrUpdateAnnouncementRequest,
  onDeleteAnnouncementRequest,
  onCheckAnnouncementAccessRequest,
} from "./api";

export function* getAnnouncementRequest() {
  yield takeEvery(actions.GET_ANNOUNCEMENT, function* (request) {
    try {
      yield put({
        type: actions.ANNOUNCEMENT_LOADING,
        loading: true
      });
      const result = yield call(onGetAnnouncementRequest, request.companyId, request.page, request.token);
      yield put({
        type: actions.ANNOUNCEMENT_SUCCESS,
        result
      });
    } catch (error) {
      yield put({
        type: actions.ANNOUNCEMENT_ERROR
      });
    }
  });
}

export function* getAnnouncementError() {
  yield takeEvery(actions.ANNOUNCEMENT_ERROR, function* () {
    yield notification.open({
      message: "Error",
      description: "Oops, unable to retrieve announcement. Please reload the page to try again."
    });
  });
}

export function* getAnnouncementDetailRequest() {
  yield takeEvery(actions.GET_DETAIL, function* (request) {
    try {
      yield put({
        type: actions.LOADING_DETAIL,
        loadingDetail: true
      });
      const result = yield call(onGetAnnouncementDetailRequest, request.companyId, request.announcementId, request.token);
      yield put({
        type: actions.DETAIL_SUCCESS,
        result
      });
    } catch (error) {
      yield put({
        type: actions.DETAIL_ERROR
      });
    }
  });
}

export function* getAnnouncementDetailError() {
  yield takeEvery(actions.DETAIL_ERROR, function* () {
    yield notification.open({
      message: "Error",
      description: "Oops, unable to retrieve announcement detail. Please reload the page to try again."
    });
  });
}

export function* createAnnouncementRequest() {
  yield takeEvery(actions.CREATE_ANNOUNCEMENT, function* (request) {
    try {
      yield put({
        type: actions.LOADING_CREATE,
      });
      yield call(onCreateOrUpdateAnnouncementRequest, request.companyId, request.data, 0, request.token);
      yield put({
        type: actions.CREATE_SUCCESS,
      });
      yield call(forwardTo, '/dashboard/announcement');
    } catch (error) {
      let tempMsg1 = error.body.error.errors ? Object.values(error.body.error.errors) : error.body.error.message;
      let tempMsg2 = Array.isArray(tempMsg1) ? tempMsg1.join(" ") : tempMsg1;
      const errMsg = tempMsg2;
      yield put({
        type: actions.CREATE_ERROR,
        message: errMsg
      });
    }
  });
}

export function* createAnnouncementSuccess() {
  yield takeEvery(actions.CREATE_SUCCESS, function* () {
    yield notification.open({
      message: "Successful!",
      description: "Notification has been sent."
    });
  });
}

export function* createAnnouncementError() {
  yield takeEvery(actions.CREATE_ERROR, function* (error) {
    yield notification.open({
      message: "Error",
      description: error.message
    });
  });
}

export function* updateAnnouncementRequest() {
  yield takeEvery(actions.UPDATE_ANNOUNCEMENT, function* (request) {
    try {
      yield put({
        type: actions.LOADING_CREATE,
      });
      yield call(onCreateOrUpdateAnnouncementRequest, request.companyId, request.data, request.announcementId, request.token);
      yield put({
        type: actions.UPDATE_SUCCESS,
      });
      yield call(forwardTo, '/dashboard/announcement');
} catch (error) {
      let tempMsg1 = error.body.error.errors ? Object.values(error.body.error.errors) : error.body.error.message;
      let tempMsg2 = Array.isArray(tempMsg1) ? tempMsg1.join(" ") : tempMsg1;
      const errMsg = tempMsg2;
      yield put({
        type: actions.UPDATE_FAILED,
        message: errMsg
      });
    }
  });
}

export function* updateAnnouncementSuccess() {
  yield takeEvery(actions.UPDATE_SUCCESS, function* () {
    yield notification.open({
      message: "Successful!",
      description: "The announcement is updated."
    });
  });
}

export function* updateAnnouncementError() {
  yield takeEvery(actions.UPDATE_FAILED, function* (error) {
    yield notification.open({
      message: "Error",
      description: error.message
    });
  });
}

export function* deleteAnnouncementRequest() {
  yield takeEvery(actions.DELETE_ANNOUNCEMENT, function* (request) {
    try {
      yield call(onDeleteAnnouncementRequest, request.companyId, request.announcementId, request.token);
      yield put({
        type: actions.DELETE_SUCCESS,
      });
      yield call(forwardTo, '/dashboard/announcement');
    } catch (error) {
      yield put({
        type: actions.DELETE_ERROR,
      });
    }
  });
}

export function* deleteAnnouncementSuccess() {
  yield takeEvery(actions.DELETE_SUCCESS, function* () {
    yield notification.open({
      message: "Announcement Removed",
      description: "The announcement is deleted!"
    });
  });
}

export function* deleteAnnouncementError() {
  yield takeEvery(actions.DELETE_ERROR, function* () {
    yield notification.open({
      message: "Error",
      description: "Oops, unable to delete announcement. Please reload the page to try again."
    });
  });
}

export function* checkAnnouncementAccessRequest() {
  yield takeEvery(actions.CHECK_ACCESS, function* (request) {
    try {
      const result = yield call(onCheckAnnouncementAccessRequest, request.companyId, request.token);
      yield put({
        type: actions.SUCCESS_ACCESS,
        result
      });
    } catch (error) {
      yield put({
        type: actions.FAILED_ACCESS,
      });
    }
  });
}


export default function* rootSaga() {
  yield all([
    fork(getAnnouncementRequest),
    fork(getAnnouncementError),
    fork(getAnnouncementDetailRequest),
    fork(getAnnouncementDetailError),
    fork(createAnnouncementRequest),
    fork(createAnnouncementSuccess),
    fork(createAnnouncementError),
    fork(updateAnnouncementRequest),
    fork(updateAnnouncementSuccess),
    fork(updateAnnouncementError),
    fork(deleteAnnouncementRequest),
    fork(deleteAnnouncementSuccess),
    fork(deleteAnnouncementError),
    fork(checkAnnouncementAccessRequest)
  ]);
}

function forwardTo(location) {
  history.push(location);
}



import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  loading_announcement: false,
  loading_detail: false,
  announcement_list: null,
  announcement_detail: null,
  loading_create: false,
  failed_create: false,
  delete_success: false,
  delete_failed: false,
  announcement_access: false,
  announcement_access_getting: false,
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.ANNOUNCEMENT_LOADING:
      return state
        .set("loading_announcement", action.loading)
        .set("announcement_list", null)
    case actions.ANNOUNCEMENT_SUCCESS:
      return state
        .set("loading_announcement", false)
        .set("announcement_list", action.result)
    case actions.ANNOUNCEMENT_ERROR:
      return state
        .set("loading_announcement", false)
    case actions.LOADING_DETAIL:
      return state
        .set("loading_detail", action.loadingDetail)
        .set("announcement_detail", null)
    case actions.DETAIL_SUCCESS:
      return state
        .set("loading_detail", false)
        .set("announcement_detail", action.result.data)
    case actions.DETAIL_ERROR:
      return state
        .set("loading_detail", false)
    case actions.CREATE_ANNOUNCEMENT:
      return state
        .set("loading_create", true)
        .set("failed_create", false)
    case actions.CREATE_SUCCESS:
      return state
        .set("loading_create", false)
    case actions.CREATE_ERROR:
      return state
        .set("loading_create", false)
        .set("failed_create", true)
    case actions.DELETE_ANNOUNCEMENT:
      return state
        .set("delete_success", false)
        .set("delete_failed", false)
    case actions.DELETE_ERROR:
      return state
        .set("delete_failed", true)
    case actions.DELETE_SUCCESS:
      return state
        .set("delete_success", true)
    case actions.CHECK_ACCESS:
      return state
        .set("announcement_access", false)
        .set("announcement_access_getting", true)
    case actions.SUCCESS_ACCESS:
      return state
        .set("announcement_access", action.result.data.status)
        .set("announcement_access_getting", false)
    case actions.FAILED_ACCESS:
      return state
        .set("announcement_access", false)
        .set("announcement_access_getting", false)
    default:
      return state;
  }
}

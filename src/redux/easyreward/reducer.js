import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  gettingHastag: false,
  tags: [],
  gettingTrans: false,
  trans: [],
  gettingTagReport: false,
  tagReport: [],
  transPaginate: {
    total: 0,
    per_page: 5,
    current_page: 1
  },
  tagReportPaginate: {
    total: 0,
    per_page: 5,
    current_page: 1
  },
  hasPrivilege: true,
  gettingRewardsList: false,
  rewardsList: null,
  gettingRewardsListPagination: false,
  rewardsListPagination: null,
  gettingReward: false,
  reward: null,
  gettingRewardAccess: false,
  rewardAccess: false,
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.GETTING_HASHTAG:
      return state
        .set("gettingHastag", true)
        .set("trans", [])
        .set("hasPrivilege", true)
        .set("tagReport", []);
    case actions.GET_HASHTAG_SUCCESS:
      return state
        .set("gettingHastag", false)
        .set("tags", action.data);
    case actions.GET_HASHTAG_FAILED:
      return state
        .set("gettingHastag", false)
        .set("tags", []);
    case actions.GET_HASHTAG_NO_PRIVILEGE:
      return state.set("hasPrivilege", false);
    case actions.GETTING_TRANS:
      return state
        .set("gettingTrans", true);
    case actions.GET_TRANS_SUCCESS:
      return state
        .set("gettingTrans", false)
        .set("trans", action.data)
        .set("transPaginate", action.paginate);
    case actions.GET_TRANS_FAILED:
      return state
        .set("gettingTrans", false)
        .set("trans", []);
    case actions.GETTING_TAG_REPORT:
      return state
        .set("gettingTagReport", true);
    case actions.GET_TAG_REPORT_SUCCESS:
      return state
        .set("gettingTagReport", false)
        .set("tagReport", action.data)
        .set("tagReportPaginate", action.paginate);
    case actions.GET_TAG_REPORT_FAILED:
      return state
        .set("gettingTagReport", false)
        .set("tagReport", []);
    case actions.GETTING_REWARDS_LIST:
      return state
        .set("gettingRewardsList", true)
        .set("rewardsList", null)
        .set("hasPrivilege", true);
    case actions.GET_REWARDS_LIST_SUCCESS:
      return state
        .set("gettingRewardsList", false)
        .set("rewardsList", action.result);
    case actions.GET_REWARDS_LIST_FAILED:
      return state
        .set("gettingRewardsList", false);
    case actions.GET_REWARDS_LIST_NO_PRIVILEGE:
      return state
        .set("hasPrivilege", false)
        .set("gettingRewardsList", false);
    case actions.GETTING_REWARDS_LIST_PAGINATION:
      return state
        .set("gettingRewardsListPagination", true)
        .set("rewardsListPagination", null)
    case actions.GET_REWARDS_LIST_PAGINATION_SUCCESS:
      return state
        .set("gettingRewardsListPagination", false)
        .set("rewardsListPagination", action.result);
    case actions.GET_REWARDS_LIST_PAGINATION_FAILED:
      return state
        .set("gettingRewardsListPagination", false);
    case actions.GETTING_REWARD:
      return state
        .set("gettingReward", true)
        .set("reward", null)
    case actions.GET_REWARD_SUCCESS:
      return state
        .set("gettingReward", false)
        .set("reward", action.result)
    case actions.GET_REWARD_FAILED:
      return state
        .set("gettingReward", false)
    case actions.CHECK_REWARD_ACCESS:
      return state
        .set("rewardAccess", false)
        .set("gettingRewardAccess", true)
    case actions.CHECK_REWARD_ACCESS_SUCCESS:
      return state
        .set("rewardAccess", action.result.data.status)
        .set("gettingRewardAccess", false)
    case actions.CHECK_REWARD_ACCESS_FAILED:
      return state
        .set("rewardAccess", false)
        .set("gettingRewardAccess", false)
    default:
      return state;
  }
}

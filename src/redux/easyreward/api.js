import { baseUrl, apiHeader } from "../../config.js";

export const onGetHashtag = (companyId: int, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/rewards/hub/hashtags?company=${companyId}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`,
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 && response.status !== 403 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetTrans = (companyId: int, tag: string, start: string, end: string, min_point: int, page: int, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/rewards/hub/hashtags/history?company=${companyId}&tag=${tag}&start=${start}&end=${end}&min_point=${min_point}&page=${page}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`,
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetHashtagReport = (companyId: int, tag: string, start: string, end: string, min_point: int, dept_multiplier: int, page: int, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/rewards/hub/hashtags/report?company=${companyId}&tag=${tag}&start=${start}&end=${end}&min_point=${min_point}&dept_multiplier=${dept_multiplier}&page=${page}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`,
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetRewardsList = (companyId: integer, page: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/products?page=${page}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetReward = (id: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/rewardProducts/${id}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onCheckRewardAccess = (companyId, idToken) => {
  return new Promise((resolve, reject) => {
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/access?access_privilege_id=8`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${idToken}`, Accept: "application/x.app.v2+json"
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};
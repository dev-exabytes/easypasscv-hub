import { all, takeEvery, takeLatest, put, fork, call } from "redux-saga/effects";
import actions from "./actions";
import { notification } from "antd";
import {
  onGetHashtag,
  onGetTrans,
  onGetHashtagReport,
  onGetRewardsList,
  onGetReward,
  onCheckRewardAccess,
} from "./api";

export function* getHashtag() {
  yield takeEvery(actions.GET_HASHTAG, function* (request) {
    try {
      yield put({
        type: actions.GETTING_HASHTAG
      });
      const result = yield call(onGetHashtag, request.companyId, localStorage.getItem("id_token"));
      const { data, error } = result;
      if (error) {
        yield put({
          type: actions.GET_HASHTAG_NO_PRIVILEGE
        });
      } else {
        yield put({
          type: actions.GET_HASHTAG_SUCCESS,
          data: data.tags
        });
      }
    } catch (error) {
      yield put({
        type: actions.GET_HASHTAG_FAILED
      });
    }
  });
}

export function* getTrans() {
  yield takeEvery(actions.GET_TRANS, function* (request) {
    try {
      yield put({
        type: actions.GETTING_TRANS
      });
      const result = yield call(onGetTrans, request.companyId, request.tag, request.start, request.end, request.min_point, request.page, localStorage.getItem("id_token"));
      const { data, total, per_page, current_page } = result;
      yield put({
        type: actions.GET_TRANS_SUCCESS,
        data,
        paginate: {
          total, per_page, current_page
        }
      });
    } catch (error) {
      yield put({
        type: actions.GET_TRANS_FAILED
      });
    }
  });
}

export function* getTagReport() {
  yield takeEvery(actions.GET_TAG_REPORT, function* (request) {
    try {
      yield put({
        type: actions.GETTING_TAG_REPORT
      });
      const result = yield call(onGetHashtagReport, request.companyId, request.tag, request.start, request.end, request.min_point, request.dept_multiplier, request.page, localStorage.getItem("id_token"));
      const { data, total, per_page, current_page } = result;
      yield put({
        type: actions.GET_TAG_REPORT_SUCCESS,
        data,
        paginate: {
          total, per_page, current_page
        }
      });
    } catch (error) {
      yield put({
        type: actions.GET_TAG_REPORT_FAILED
      });
    }
  });
}

export function* getRewardsList() {
  yield takeLatest(actions.GET_REWARDS_LIST, function* (request) {
    try {
      yield put({
        type: actions.GETTING_REWARDS_LIST
      });
      const result = yield call(onGetRewardsList, request.companyId, request.page, request.token);
      yield put({
        type: actions.GET_REWARDS_LIST_SUCCESS,
        result
      });
    } catch (error) {
      yield put({
        type: actions.GET_REWARDS_LIST_FAILED,
      });
    }
  });
}

export function* getRewardsListFailed() {
  yield takeEvery(actions.GET_REWARDS_LIST_FAILED, function* () {
    yield notification.open({
      message: "Error",
      description: "Oops, unable to retrieve rewards. Please reload the page to try again."
    });
  });
}

export function* getRewardsListPagination() {
  yield takeLatest(actions.GET_REWARDS_LIST_PAGINATION, function* (request) {
    try {
      yield put({
        type: actions.GETTING_REWARDS_LIST_PAGINATION
      });
      const result = yield call(onGetRewardsList, request.companyId, request.page, request.token);
      yield put({
        type: actions.GET_REWARDS_LIST_PAGINATION_SUCCESS,
        result
      });
    } catch (error) {
      yield put({
        type: actions.GET_REWARDS_LIST_PAGINATION_FAILED,
      });
    }
  });
}

export function* getReward() {
  yield takeEvery(actions.GET_REWARD, function* (request) {
    try {
      yield put({
        type: actions.GETTING_REWARD,
      });
      const result = yield call(onGetReward, request.id, request.token);
      yield put({
        type: actions.GET_REWARD_SUCCESS,
        result: result.data
      });
    } catch (error) {
      yield put({
        type: actions.GET_REWARD_FAILED,
        message: error
      });
    }
  });
}

export function* getRewardFailed() {
  yield takeEvery(actions.GET_REWARDS_LIST_FAILED, function* (error) {
    yield notification.open({
      message: "Error",
      description: error.message
    });
  });
}

export function* checkRewardAccess() {
  yield takeEvery(actions.CHECK_REWARD_ACCESS, function* (request) {
    try {
      const result = yield call(onCheckRewardAccess, request.companyId, request.token);
      yield put({
        type: actions.CHECK_REWARD_ACCESS_SUCCESS,
        result
      });
    } catch (error) {
      yield put({
        type: actions.CHECK_REWARD_ACCESS_FAILED,
      });
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getHashtag),
    fork(getTrans),
    fork(getTagReport),
    fork(getRewardsList),
    fork(getRewardsListFailed),
    fork(getRewardsListPagination),
    fork(getReward),
    fork(getRewardFailed),
    fork(checkRewardAccess),
  ]);
}



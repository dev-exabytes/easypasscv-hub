import { all } from "redux-saga/effects";
import authSagas from "./auth/saga";
import contactSagas from "./contacts/saga";
import mailSagas from "./mail/saga";
import notesSagas from "./notes/saga";
import todosSagas from "./todos/saga";
import devSagas from "../customApp/redux/sagas";
import dashboardSagas from "./dashboard/sagas";
import languageSagas from "./language/sagas";
import bugSagas from "./bugs/sagas";
import subscriptionSagas from "./Subscriptions/sagas";
import companySagas from "./company/saga";
import integrationSagas from "./integrations/saga";
import menuSagas from "./menu/saga";
import joinSagas from "./join/saga";
import CompanyReportSagas from "./reporting/saga";
import HomeSagas from "./home/saga";
import OrgChartSagas from './orgchart/sagas';
import EasyRewardSagas from "./easyreward/saga";
import AnnouncementSagas from "./announcement/saga";

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    contactSagas(),
    mailSagas(),
    notesSagas(),
    todosSagas(),
    devSagas(),
    dashboardSagas(),
    languageSagas(),
    bugSagas(),
    subscriptionSagas(),
    companySagas(),
    integrationSagas(),
    menuSagas(),
    joinSagas(),
    CompanyReportSagas(),
    HomeSagas(),
    OrgChartSagas(),
    EasyRewardSagas(),
    AnnouncementSagas(),
  ]);
}

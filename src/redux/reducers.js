import Auth from "./auth/reducer";
import App from "./app/reducer";
import Mails from "./mail/reducer";
import Calendar from "./calendar/reducer";
import Box from "./box/reducer";
import Notes from "./notes/reducer";
import Todos from "./todos/reducer";
import Contacts from "./contacts/reducer";
import DynamicChartComponent from "./dynamicEchart/reducer";
import ThemeSwitcher from "./themeSwitcher/reducer";
import LanguageSwitcher from "./languageSwitcher/reducer";
import DashboardSearch from "./dashboard/reducers";
import DevReducers from "../customApp/redux/reducers";
import Language from "./language/reducers";
import Subscriptions from "./Subscriptions/reducers"
import Company from "./company/reducer";
import Integration from "./integrations/reducer";
import Menu from "./menu/reducer";
import Join from "./join/reducer";
import CompanyReport from "./reporting/reducer";
import Home from "./home/reducer";
import OrgChart from './orgchart/reducer';
import EasyReward from "./easyreward/reducer";
import Announcement from "./announcement/reducer";

export default {
  Auth,
  App,
  ThemeSwitcher,
  LanguageSwitcher,
  Mails,
  Calendar,
  Box,
  Notes,
  Todos,
  Contacts,
  DynamicChartComponent,
  DashboardSearch,
  Language,
  ...DevReducers,
  Subscriptions,
  Company,
  Integration,
  Menu,
  Join,
  CompanyReport,
  Home,
  OrgChart,
  EasyReward,
  Announcement
};

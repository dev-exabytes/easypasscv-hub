const actions = {
	BUG_SEARCH: "BUG_SEARCH",
	BUG_SUCCESS_RESULT: "BUG_SUCCESS_RESULT",
	BUG_ERROR_RESULT: "BUG_ERROR_RESULT",
	bugSearch: searcText => ({
		type: actions.BUG_SEARCH,
		payload: { searcText }
	}),
	onPageChange: (searcText, pageToken) => ({
		type: actions.BUG_SEARCH,
		payload: { searcText, pageToken }
	}),
	searchSuccess: (
		columns,
		result,
		total_count,
		per_page,
		current_page
	) => ({
		type: actions.BUG_SUCCESS_RESULT,
		columns,
		result,
		total_count,
		per_page,
		current_page
	}),
	searchError: () => ({
		type: actions.BUG_ERROR_RESULT
	})
};
export default actions;
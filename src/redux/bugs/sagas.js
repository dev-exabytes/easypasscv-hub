import { all, takeEvery, put, call } from "redux-saga/effects";
import actions from "./actions";
import { baseUrl } from "../../config";

export const per_page = 10;

const onSearchReqeust = async (searcText, pageToken, token) =>
  await fetch(`${baseUrl}/v1/jira/issues/search?${searcText}${pageToken}${token}`)
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);

function* searchRequest({ payload }) {
  const { searcText, pageToken } = payload;
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(
      onSearchReqeust,
      searcText ? `query=${searcText}` : "query=",
      pageToken ? `&page=${pageToken}` : "&page=1",
      token ? `&token=${token}` : "&token="
    );

    if (searchResult.data) {
      yield put(
        actions.searchSuccess(
          searchResult.columns,
          searchResult.data,
          searchResult.total,
          searchResult.per_page,
          searchResult.current_page
        )
      );
    } else {
      yield put(actions.searchError());
    }
  } catch (error) {
    yield put(actions.searchError());
  }
}

export default function* rootSaga() {
  yield all([takeEvery(actions.BUG_SEARCH, searchRequest)]);
}

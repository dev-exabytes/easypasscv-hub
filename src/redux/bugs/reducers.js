import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
	searcText: null,
	total_count: 0,
	result: [],
	loading: false,
	error: false,
	columns: [],
	per_page: 0,
	current_page: 0
});

export default function reducer(state = initState, action) {
	switch (action.type) {
		case actions.BUG_SEARCH:
			return state
				.set("loading", true)
				.set("searcText", action.payload.searcText);
		case actions.BUG_SUCCESS_RESULT:
			return state
				.set("columns", action.columns)
				.set("result", action.result)
				.set("total_count", action.total_count)
				.set("per_page", action.per_page)
				.set("current_page", action.current_page)
				.set("loading", false)
				.set("error", false);
		case actions.BUG_ERROR_RESULT:
			return state
				.set("columns", [])
				.set("result", [])
				.set("loading", false)
				.set("error", false);
		default:
			return state;
	}
}

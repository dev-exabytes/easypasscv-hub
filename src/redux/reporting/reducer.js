import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  data: [],
  generating: false,
  getting: false,
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.GENERATING:
      return state
        .set("generating", true);
    case actions.GENERATE_REPORT_ERROR:
    case actions.GENERATE_REPORT_SUCCESS:
      return state
        .set("generating", false);
    case actions.GETTING:
      return state
        .set("getting", true)
        .set("data", []);
    case actions.GET_HISTORY_SUCCESS:
      return state
        .set("getting", false)
        .set("data", action.data);
    case actions.GET_HISTORY_ERROR:
      return state
        .set("getting", false);
    default:
      return state;
  }
}

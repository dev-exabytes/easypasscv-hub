import { baseUrl, apiHeader, v2 } from "../../config.js";

export const onGenerateCompanyReport = (companyId: int, start: string, end: string, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/reports?start_dt=${start}&end_dt=${end}&from_hub=${1}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`,
        }, ...v2
      },
      method: "GET"
    })
      .then(response => (response.status === 500 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetHistory = (companyId: int, batch: any, token: string) => {
  return new Promise((resolve, reject) => {
    const batch = batch ? `&batch=${batch}` : '';
    return fetch(`${baseUrl}/v1/companies/${companyId}/reports/history?${batch}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`,
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

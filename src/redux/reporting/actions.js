const actions = {
  GENERATING: "GENERATING",
  GENERATE_REPORT: "GENERATE_REPORT",
  GENERATE_REPORT_SUCCESS: "GENERATE_REPORT_SUCCESS",
  GENERATE_REPORT_ERROR: "GENERATE_REPORT_ERROR",
  GET_HISTORY: "GET_HISTORY",
  GET_HISTORY_SUCCESS: "GET_HISTORY_SUCCESS",
  GET_HISTORY_ERROR: "GET_HISTORY_ERROR",
  GETTING: "GETTING",
  generateReport: (companyId, start, end) => ({
    type: actions.GENERATE_REPORT,
    companyId,
    start,
    end
  }),
  getHistory: (companyId, batch) => ({
    type: actions.GET_HISTORY,
    companyId,
    batch
  }),
};
export default actions;

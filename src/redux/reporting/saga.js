import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "./actions";
import { notification } from "antd";
import {
  onGenerateCompanyReport,
  onGetHistory
} from "./api";

export function* generateCompanyReport() {
  yield takeEvery(actions.GENERATE_REPORT, function* (request) {
    try {
      yield put({
        type: actions.GENERATING
      });
      const result = yield call(onGenerateCompanyReport, request.companyId, request.start, request.end, localStorage.getItem("id_token"));
      const { message } = result;
      if (message) {
        yield put({
          type: actions.GENERATE_REPORT_SUCCESS,
          companyId: request.companyId
        });
      } else {
        yield put({
          type: actions.GENERATE_REPORT_ERROR,
          message: result.error.message
        });
      }
    } catch (error) {
      yield put({
        type: actions.GENERATE_REPORT_ERROR
      });
    }
  });
}

export function* generateCompanyReportSuccess() {
  yield takeEvery(actions.GENERATE_REPORT_SUCCESS, function* (payload) {
    notification.open({
      message: "Success",
      description: "Report generated"
    });
    yield put({
      type: actions.GET_HISTORY,
      companyId: payload.companyId
    });
  });
}

export function* generateCompanyReportError() {
  yield takeEvery(actions.GENERATE_REPORT_ERROR, function* (payload) {
    yield notification.open({
      message: "Error",
      description: payload.message || "Report not generated."
    });
  });
}

export function* getHistory() {
  yield takeEvery(actions.GET_HISTORY, function* (request) {
    try {
      yield put({
        type: actions.GETTING
      });
      const result = yield call(onGetHistory, request.companyId, request.batch, localStorage.getItem("id_token"));
      const { data } = result;
      if (data.length > 0) {
        yield put({
          type: actions.GET_HISTORY_SUCCESS,
          data
        });
      } else {
        yield put({
          type: actions.GET_HISTORY_ERROR
        });
      }
    } catch (error) {
      yield put({
        type: actions.GET_HISTORY_ERROR
      });
    }
  });
}
export default function* rootSaga() {
  yield all([
    fork(generateCompanyReport),
    fork(generateCompanyReportSuccess),
    fork(generateCompanyReportError),
    fork(getHistory)
  ]);
}



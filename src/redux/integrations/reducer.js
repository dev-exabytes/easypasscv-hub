import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  code: null,
  team: null,
  slack_token: null,
  slack_manage_app_url: null,
  slack_team: null,
  slack_status: null,
  slack_loading: false,
  403: false
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.SLACK_TEAM:
      return state
        .set("code", action.code)
    case actions.SLACK_TEAM_SUCCESS:
      return state
        .set("team", action.team)
    case actions.SLACK_INTEGRATION_ERROR:
    case actions.SLACK_TEAM_ERROR:
      return state
        .set("team", null)
        .set("slack_token", false)
        .set("slack_status", false)
        .set("403", false)
    case actions.SLACK_TEAM_STORE:
      return state
        .set("slack_token", action.slack_token)
        .set("slack_manage_app_url", action.configuration_url)
        .set("slack_loading", false)
    case actions.SLACK_TEST_SUCCESS:
      return state
        .set("slack_status", action.ok)
        .set("slack_team", action.team)
    case actions.SLACK_LOADING:
      return state
        .set("slack_loading", action.slack_loading)
    case actions.SLACK_TEAM_REMOVE:
      return state
        .set("code", null)
        .set("team", null)
        .set("slack_token", null)
        .set("slack_status", null)
        .set("slack_manage_app_url", null)
        .set("slack_loading", false)
        .set("slack_team", null)
        .set("403", false)
    case actions.SLACK_403:
      return state
        .set("team", null)
        .set("slack_token", false)
        .set("slack_status", false)
        .set("slack_loading", false)
        .set("403", true)
    default:
      return state;
  }
}

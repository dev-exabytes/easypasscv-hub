const integrationActions = {
  SLACK_TEAM: "SLACK_TEAM",
  SLACK_TEAM_SUCCESS: "SLACK_TEAM_SUCCESS",
  SLACK_TEAM_ERROR: "SLACK_TEAM_ERROR",
  SLACK_INTEGRATION: "SLACK_INTEGRATION",
  SLACK_TEST: "SLACK_TEST",
  SLACK_TEST_SUCCESS: "SLACK_TEST_SUCCESS",
  SLACK_TEST_ERROR: "SLACK_TEST_ERROR",
  SLACK_TEAM_STORE: "SLACK_TEAM_STORE",
  SLACK_INTEGRATION_ERROR: "SLACK_INTEGRATION_ERROR",
  SLACK_LOADING: "SLACK_LOADING",
  SLACK_REMOVE_INTEGRATION: "SLACK_REMOVE_INTEGRATION",
  SLACK_REMOVE_INTEGRATION_ERROR: "SLACK_REMOVE_INTEGRATION_ERROR",
  SLACK_TEAM_REMOVE: "SLACK_TEAM_REMOVE",
  SLACK_403: "SLACK_403",
  getTeam: (code, companyId) => ({
    type: integrationActions.SLACK_TEAM,
    code,
    companyId
  }),
  getSlackIntegration: companyId => ({
    type: integrationActions.SLACK_INTEGRATION,
    companyId
  }),
  testSlack: token => ({
    type: integrationActions.SLACK_TEST,
    token
  }),
  removeSlackIntegration: (slackToken, companyId) => ({
    type: integrationActions.SLACK_REMOVE_INTEGRATION,
    slackToken,
    companyId
  })
};
export default integrationActions;


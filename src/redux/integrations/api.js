import { slackConfig, baseUrl, slackUrl, slackRedirectUri, apiHeader } from "../../config.js";

export const onGetTeamRequest = (code: string) => {
  return new Promise((resolve, reject) => {
    var data = new URLSearchParams();
    data.append("client_id", slackConfig.clientId);
    data.append("client_secret", slackConfig.clientSecret);
    data.append("code", code);
    data.append("redirect_uri", slackRedirectUri);

    return fetch(`${slackUrl}oauth.access`, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "*/*",
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onGetSlackIntegrationRequest = (companyId: integer, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/integration/slack`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onTestSlackTokenRequest = (token: string) => {
  return new Promise((resolve, reject) => {
    var data = new URLSearchParams();
    data.append("token", token);
    return fetch(`${slackUrl}auth.test`, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "*/*",
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onPostSlackIntegrationRequest = (data, token, companyId) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/integration/slack`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "POST",
      body: JSON.stringify({
        oauth_token: data.access_token,
        team_id: data.team_id,
        scopes: data.scope,
        incoming_webhook: data.incoming_webhook
      })
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onRemoveAppRequest = (token) => {
  return new Promise((resolve, reject) => {
    var data = new URLSearchParams();
    data.append("token", token);

    return fetch(`${slackUrl}auth.revoke`, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "*/*",
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const onDeleteSlackIntegrationRequest = (token, companyId) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies/${companyId}/integration/slack`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "DELETE"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import { push } from "react-router-redux";
import actions from "./actions";
import { notification } from "antd";
import {
  onGetTeamRequest,
  onGetSlackIntegrationRequest,
  onTestSlackTokenRequest,
  onPostSlackIntegrationRequest,
  onRemoveAppRequest,
  onDeleteSlackIntegrationRequest
} from "./api";

export function* getSlackIntegrationRequest() {
  yield takeEvery(actions.SLACK_INTEGRATION, function* (request) {
    try {
      yield put({
        type: actions.SLACK_LOADING,
        slack_loading: true
      });
      const result = yield call(onGetSlackIntegrationRequest, request.companyId, localStorage.getItem("id_token"));
      const { oauth_token, incoming_webhook, not_admin } = result.data;
      if (not_admin) {
        yield put({
          type: actions.SLACK_403
        });
      } else if (oauth_token) {
        yield put({
          type: actions.SLACK_TEAM_STORE,
          slack_token: oauth_token,
          configuration_url: incoming_webhook ? incoming_webhook.configuration_url : '',
        });
        yield put({
          type: actions.SLACK_TEST,
          token: oauth_token
        });
        yield put(push("/dashboard/integrations/slack"));
      }
      else {
        yield put({
          type: actions.SLACK_TEAM_ERROR,
          slack_status: false
        });
      }
    } catch (error) {
      yield put({
        type: actions.SLACK_TEAM_ERROR
      });
    }
  });
}

export function* testSlackRequest() {
  yield takeEvery(actions.SLACK_TEST, function* (request) {
    try {
      const result = yield call(onTestSlackTokenRequest, request.token);
      const { ok, team } = result;
      yield put({
        type: actions.SLACK_TEST_SUCCESS,
        ok: ok,
        team: team
      });
    } catch (error) {
    }
  });
}

export function* getTeamRequest() {
  yield takeEvery(actions.SLACK_TEAM, function* (request) {
    try {
      yield put({
        type: actions.SLACK_LOADING,
        slack_loading: true
      });

      const result = yield call(onGetTeamRequest, request.code);

      const { ok } = result;
      if (ok === true) {
        const stored = yield call(onPostSlackIntegrationRequest, result, localStorage.getItem("id_token"), request.companyId);
        const { oauth_token, incoming_webhook } = stored.data;
        if (oauth_token) {
          yield put({
            type: actions.SLACK_TEAM_STORE,
            slack_token: oauth_token,
            configuration_url: incoming_webhook ? incoming_webhook.configuration_url : '',
          });
          yield put({
            type: actions.SLACK_TEST,
            token: oauth_token
          });
          yield put(push("/dashboard/integrations/slack"));
        }
      }
      else {
        yield put({
          type: actions.SLACK_INTEGRATION_ERROR,
        });
      }

    } catch (error) {
      yield put({ type: actions.SLACK_INTEGRATION_ERROR });
    }
  });
}

export function* getTeamError() {
  yield takeEvery(actions.SLACK_TEAM_ERROR, function* () {
    yield put({
      type: actions.SLACK_LOADING,
      slack_loading: false
    });
    yield put(push("/dashboard/integrations/slack"));
  });
}

export function* getSlackIntegrationError() {
  yield takeEvery(actions.SLACK_INTEGRATION_ERROR, function* () {
    notification.open({
      message: "Error",
      description: "Oops, integration with Slack failed."
    });
    yield put({
      type: actions.SLACK_LOADING,
      slack_loading: false
    });
    yield put(push("/dashboard/integrations/slack"));
  });
}

export function* removeSlackIntegrationError() {
  yield takeEvery(actions.SLACK_REMOVE_INTEGRATION_ERROR, function* () {
    notification.open({
      message: "Error",
      description: "Oops, remove Slack integration failed. Please try again."
    });
    yield put({
      type: actions.SLACK_LOADING,
      slack_loading: false
    });
    yield put(push("/dashboard/integrations/slack"));
  });
}

export function* removeSlackIntegrationRequest() {
  yield takeEvery(actions.SLACK_REMOVE_INTEGRATION, function* (request) {
    try {
      yield put({
        type: actions.SLACK_LOADING,
        slack_loading: true
      });
      yield call(onRemoveAppRequest, request.slackToken);
      yield call(onDeleteSlackIntegrationRequest, localStorage.getItem("id_token"), request.companyId);
      yield put({
        type: actions.SLACK_TEAM_REMOVE,
      });
      yield put(push("/dashboard/integrations/slack"));
    } catch (error) {
      yield put({ type: actions.SLACK_INTEGRATION_ERROR });
    }
  });
}



export default function* rootSaga() {
  yield all([
    fork(getTeamRequest),
    fork(getTeamError),
    fork(getSlackIntegrationRequest),
    fork(testSlackRequest),
    fork(getSlackIntegrationError),
    fork(removeSlackIntegrationRequest),
    fork(removeSlackIntegrationError)
  ]);
}



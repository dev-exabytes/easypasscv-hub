import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  loading: false,
  attendance: null,
  leave: null,
  claim: null,
  mileage_claim: null,
  announcement: null,
  reward: null,
  upcoming_event: [],
  user: null,
  privileges: null,
  usage: null,
  upcoming_bday: []
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.LOADING:
      return state
        .set("loading", action.loading)
        .set("attendance", null)
        .set("leave", null)
        .set("claim", null)
        .set("mileage_claim", null)
        .set("announcement", null)
        .set("reward", null)
        .set("upcoming_event", [])
        .set("user", null)
        .set("privileges", null)
        .set("usage", null)
        .set("upcoming_bday", []);
    case actions.SUCCESS:
      return state
        .set("loading", false)
        .set("attendance", action.attendance)
        .set("leave", action.leave)
        .set("claim", action.claim)
        .set("mileage_claim", action.mileage_claim)
        .set("announcement", action.announcement)
        .set("reward", action.reward)
        .set("upcoming_event", action.upcoming_event)
        .set("user", action.user)
        .set("privileges", action.privileges)
        .set("usage", action.usage)
        .set("upcoming_bday", action.upcoming_bday);
    case actions.ERROR:
      return state
        .set("loading", false)
    default:
      return state;
  }
}

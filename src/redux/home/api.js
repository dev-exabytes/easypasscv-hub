import { baseUrl, apiHeader } from "../../config.js";

export const onGetReportRequest = (companyId: integer, start: string, end: string, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/hub/reports/daily?company=${companyId}&start=${start}&end=${end}`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

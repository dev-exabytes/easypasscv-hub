const actions = {
  LOADING: "LOADING",
  GET_REPORT: "GET_REPORT",
  ERROR: "ERROR",
  SUCCESS: "SUCCESS",
  getReport: (start, end, companyId, token) => ({
    type: actions.GET_REPORT,
    start,
    end,
    companyId,
    token
  }),
};
export default actions;

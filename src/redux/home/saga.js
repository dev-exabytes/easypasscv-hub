import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "./actions";
import { notification } from "antd";
import {
  onGetReportRequest
} from "./api";

export function* getReportRequest() {
  yield takeEvery(actions.GET_REPORT, function* (request) {
    try {
      yield put({
        type: actions.LOADING,
        loading: true
      });
      const result = yield call(onGetReportRequest, request.companyId, request.start, request.end, request.token);
      yield put({
        type: actions.SUCCESS,
        ...result.data
      });
    } catch (error) {
      yield put({
        type: actions.ERROR
      });
    }
  });
}

export function* getReportError() {
  yield takeEvery(actions.ERROR, function* () {
    yield notification.open({
      message: "Error",
      description: "Oops, unable to retrieve report. Please reload the page to try again."
    });
  });
}

export default function* rootSaga() {
  yield all([
    fork(getReportRequest),
    fork(getReportError),
  ]);
}



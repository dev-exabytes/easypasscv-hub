import { baseUrl } from "../../config";

export const getAttendanceData = (token, data) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    if (!(data.start || data.end)) {
      reject("Invalid Data");
      return;
    }

    return fetch(
      `${baseUrl}/v1/charts/attendance?sort=asc&page=1&start=${encodeURIComponent(data.start)}&end=${encodeURIComponent(
        data.end
      )}`,
      {
        method: "GET",
        headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

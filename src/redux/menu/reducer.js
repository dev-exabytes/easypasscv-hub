import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  menu: [],
  loading: false
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.MENU_LOADING:
      return state
        .set("loading", true)
    case actions.MENU_SUCCESS:
      return state
        .set("loading", false)
        .set("menu", action.menu)
    case actions.MENU_ERROR:
      return state
        .set("loading", false)
        .set("menu", [])
    default:
      return state;
  }
}

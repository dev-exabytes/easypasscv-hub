import { baseUrl, apiHeader } from "../../config.js";

export const onGetMenuRequest = (companyId: any, token: string) => {
  return new Promise((resolve, reject) => {
    let params = "";
    if (companyId) {
      params = `?company=${companyId}`;
    }
    return fetch(`${baseUrl}/v1/menu/hub${params}`, {
      headers: {
        ...apiHeader,
        ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "GET"
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

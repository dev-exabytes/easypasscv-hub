import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import { push } from "react-router-redux";
import { history } from '../store';
import actions from "./actions";
import { notification } from "antd";
import {
  onGetMenuRequest,
} from "./api";

export function* getMenuRequest() {
  yield takeEvery(actions.MENU, function* (request) {
    try {
      yield put({
        type: actions.MENU_LOADING
      });
      const result = yield call(onGetMenuRequest, request.companyId, localStorage.getItem("id_token"));
      const { data } = result;

      yield put({
        type: actions.MENU_SUCCESS,
        menu: data,
        url: request.history
      });
    } catch (error) {
      yield put({
        type: actions.MENU_ERROR
      });
    }
  });
}

export function* getMenuSuccess() {
  yield takeEvery(actions.MENU_SUCCESS, function* (request) {
    try {
      if (history.location) {
        if (history.location.state) {
          yield put(push(history.location.state));
        } else {
          if (history.location.pathname === "/dashboard/company/new") {
            yield put(push("/dashboard"));
          } else {
            yield put(push(history.location));
          }
        }
      } else {
        yield put(push("/dashboard"));
      }
    } catch (error) {
      yield put(push("/dashboard"));
    }
  })
}
export function* getMenuError() {
  yield takeEvery(actions.MENU_ERROR, function* () {
    yield notification.open({
      message: "Error",
      description: "Oops, unable to get menu."
    });
  });
}

export default function* rootSaga() {
  yield all([
    fork(getMenuRequest),
    fork(getMenuError),
    fork(getMenuSuccess)
  ]);
}



const menuActions = {
  MENU: "MENU",
  MENU_SUCCESS: "MENU_SUCCESS",
  MENU_ERROR: "MENU_ERROR",
  MENU_LOADING: "MENU_LOADING",
  getMenu: (companyId: any, history: any = undefined) => ({
    type: menuActions.MENU,
    companyId,
    history
  })
};
export default menuActions;


const actions = {
  GROUP_SEARCH: "GROUP_SEARCH",
  GROUP_SUCCESS_RESULT: "GROUP_SUCCESS_RESULT",
  GROUP_ERROR_RESULT: "GROUP_ERROR_RESULT",
  LANG_SEARCH: "LANG_SEARCH",
  LANG_SUCCESS_RESULT: "LANG_SUCCESS_RESULT",
  LANG_ERROR_RESULT: "LANG_ERROR_RESULT",
  LANG_EDIT: "LANG_EDIT",
  LANGS_EDIT_ERROR: "LANGS_EDIT_ERROR",
  LANGS_SEARCH: "LANGS_SEARCH",
  LANGS_SUCCESS_RESULT: "LANGS_SUCCESS_RESULT",
  LANGS_ERROR_RESULT: "LANGS_ERROR_RESULT",
  LANGS_STATUS_CHANGED: "LANGS_STATUS_CHANGED",
  LANG_DELETE: "LANG_DELETE",
  LANGS_DELETE_ERROR: "LANGS_DELETE_ERROR",
  groupSearch: () => ({
    type: actions.GROUP_SEARCH
  }),
  groupSearchSuccess: groups => ({
    type: actions.GROUP_SUCCESS_RESULT,
    groups
  }),
  groupSearchError: () => ({
    type: actions.GROUP_ERROR_RESULT
  }),
  langSearch: search_group => ({
    type: actions.LANG_SEARCH,
    search_group
  }),
  langSearchSuccess: (columns, result) => ({
    type: actions.LANG_SUCCESS_RESULT,
    columns,
    result
  }),
  langSearchError: () => ({
    type: actions.LANG_ERROR_RESULT
  }),
  langEdit: (value, key, column, selectedGroup) => ({
    type: actions.LANG_EDIT,
    value,
    key,
    column,
    selectedGroup
  }),
  langEditError: () => ({
    type: actions.LANGS_EDIT_ERROR
  }),
  langsSearch: () => ({
    type: actions.LANGS_SEARCH
  }),
  langsSearchSuccess: langs => ({
    type: actions.LANGS_SUCCESS_RESULT,
    langs
  }),
  langsSearchError: () => ({
    type: actions.LANGS_ERROR_RESULT
  }),
  langsStatusChanged: (lang, id, status) => ({
    type: actions.LANGS_STATUS_CHANGED,
    lang,
    id,
    status
  }),
  langDelete: (key, selectedGroup) => ({
    type: actions.LANG_DELETE,
    key,
    selectedGroup
  }),
  langDeleteError: () => ({
    type: actions.LANGS_DELETE_ERROR
  })
};
export default actions;

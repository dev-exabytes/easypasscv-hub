import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  search_group: null,
  groups: [],
  total_count: 0,
  result: [],
  loading: false,
  error: false,
  columns: [],
  per_page: 0,
  current_page: 0,
  queries: [],
  trans: null,
  langs: [],
  lang_id: null
});

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.GROUP_SEARCH:
      return state.set("loading", true);
    case actions.GROUP_SUCCESS_RESULT:
      return state
        .set("groups", action.groups)
        .set("loading", false)
        .set("error", false);
    case actions.GROUP_ERROR_RESULT:
      return state
        .set("groups", [])
        .set("loading", false)
        .set("error", false);
    case actions.LANG_SEARCH:
      return state
        .set("loading", true)
        .set("search_group", action.search_group);
    case actions.LANG_SUCCESS_RESULT:
      return state
        .set("columns", action.columns)
        .set("result", action.result)
        .set("loading", false)
        .set("error", false);
    case actions.LANG_ERROR_RESULT:
      return state
        .set("columns", [])
        .set("result", [])
        .set("loading", false)
        .set("error", false);
    case actions.LANG_EDIT:
      return state.set("loading", true);
    case actions.LANGS_EDIT_ERROR:
      return state.set("loading", false).set("error", false);
    case actions.LANGS_SEARCH:
      return state.set("loading", true);
    case actions.LANGS_SUCCESS_RESULT:
      return state
        .set("langs", action.langs)
        .set("loading", false)
        .set("error", false);
    case actions.LANGS_ERROR_RESULT:
      return state
        .set("langs", [])
        .set("loading", false)
        .set("error", false);
    case actions.LANGS_STATUS_CHANGED:
      return state
        .set("lang_id", action.id)
        .set("loading", false)
        .set("error", false);
    case actions.LANG_DELETE:
      return state.set("loading", true);
    case actions.LANGS_DELETE_ERROR:
      return state.set("loading", false).set("error", false);
    default:
      return state;
  }
}

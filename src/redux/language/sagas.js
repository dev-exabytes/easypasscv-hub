import { all, takeEvery, put, call, fork } from "redux-saga/effects";
import actions from "./actions";
import { baseUrl } from "../../config";
import { notification } from "antd";

export const per_page = 10;

const onSearchReqeust = async token =>
  await fetch(`${baseUrl}/v1/langs/groups?${token}`)
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);

const onLangSearchRequest = async (group, token) =>
  await fetch(`${baseUrl}/v1/langs/groups/${group}?${token}`)
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);
const onGetAllLangRequest = async token =>
  await fetch(`${baseUrl}/v1/langs?${token}`)
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);
const onChangeLangStatusRequest = async (lang, status, token) =>
  await fetch(`${baseUrl}/v1/langs/${lang}?${token}`, {
    headers: { "Content-Type": "application/json" },
    method: "PUT",
    body: JSON.stringify({
      status: status
    })
  })
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);
const onLangEditRequest = async (value, key, column, selectedGroup, token) =>
  await fetch(`${baseUrl}/v1/langs?${token}`, {
    headers: {
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({
      group: selectedGroup,
      lang: column,
      key: key,
      value: value
    })
  })
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);

const onLangDeleteRequest = async (key, selectedGroup, token) =>
  await fetch(`${baseUrl}/v1/langs/groups/${selectedGroup}?${key}${token}`, {
    headers: {
      "Content-Type": "application/json"
    },
    method: "DELETE"
  })
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);

function* searchRequest() {
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(onSearchReqeust, token ? `token=${token}` : "token=");

    if (searchResult.data) {
      yield put(actions.groupSearchSuccess(searchResult.data));
    } else {
      yield put(actions.groupSearchError());
    }
  } catch (error) {
    yield put(actions.groupSearchError());
  }
}

function* langSearchRequest({ search_group }) {
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(onLangSearchRequest, search_group, token ? `token=${token}` : "token=");
    if (searchResult.data) {
      yield put(actions.langSearchSuccess(searchResult.columns, searchResult.data));
    } else {
      yield put(actions.langSearchError());
    }
  } catch (error) {
    yield put(actions.langSearchError());
  }
}

function* getAllLangRequest() {
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(onGetAllLangRequest, token ? `token=${token}` : "token=");
    if (searchResult.data) {
      yield put(actions.langsSearchSuccess(searchResult.data));
    } else {
      yield put(actions.langsSearchError());
    }
  } catch (error) {
    yield put(actions.langsSearchError());
  }
}

function* changeLangStatusRequest({ lang, status }) {
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(onChangeLangStatusRequest, lang, status, token ? `token=${token}` : "token=");
    if (searchResult.data) {
      yield put(actions.langsSearchSuccess(searchResult.data));
    } else {
      yield put(actions.langsSearchError());
    }
  } catch (error) {
    yield put(actions.langsSearchError());
  }
}

function* langEditRequest({ value, key, column, selectedGroup }) {
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(
      onLangEditRequest,
      value,
      key,
      column,
      selectedGroup,
      token ? `token=${token}` : "token="
    );
    if (!searchResult.data) {
      yield put({ type: actions.LANGS_EDIT_ERROR });
    }
  } catch (error) {
    yield put({ type: actions.LANGS_EDIT_ERROR });
  }
}

function* langDeleteRequest({ key, selectedGroup }) {
  let token = localStorage.getItem("id_token");

  try {
    const searchResult = yield call(
      onLangDeleteRequest,
      key ? `key=${key}` : "key=",
      selectedGroup,
      token ? `&token=${token}` : "&token="
    );
    if (!searchResult.data) {
      yield put({ type: actions.LANGS_DELETE_ERROR });
    }
  } catch (error) {
    yield put({ type: actions.LANGS_DELETE_ERROR });
  }
}

function* landEditError() {
  yield takeEvery(actions.LANGS_EDIT_ERROR, function*() {
    notification.open({
      message: "Error",
      description: "Translation update failed."
    });
    yield put(actions.langEditError);
  });
}

function* landDeleteError() {
  yield takeEvery(actions.LANGS_DELETE_ERROR, function*() {
    notification.open({
      message: "Error",
      description: "Translation delete failed."
    });
    yield put(actions.langDeleteError);
  });
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.GROUP_SEARCH, searchRequest),
    takeEvery(actions.LANG_SEARCH, langSearchRequest),
    takeEvery(actions.LANGS_SEARCH, getAllLangRequest),
    takeEvery(actions.LANGS_STATUS_CHANGED, changeLangStatusRequest),
    takeEvery(actions.LANG_EDIT, langEditRequest),
    takeEvery(actions.LANG_DELETE, langDeleteRequest),
    fork(landEditError),
    fork(landDeleteError)
  ]);
}

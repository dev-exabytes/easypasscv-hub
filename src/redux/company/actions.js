const actions = {
  GETTING_COMPANY: "GETTING_COMPANY",
  COMPANY_REQUEST: "COMPANY_REQUEST",
  COMPANY_ERROR: "COMPANY_ERROR",
  COMPANY_SUCCESS: "COMPANY_SUCCESS",
  SWITCH_COMPANY: "SWITCH_COMPANY",
  CREATE_COMPANY: "CREATE_COMPANY",
  CREATING_COMPANY: "CREATING_COMPANY",
  CREATE_COMPANY_SUCCESS: "CREATE_COMPANY_SUCCESS",
  CREATE_COMPANY_ERROR: "CREATE_COMPANY_ERROR",
  COMPANIES: "COMPANIES",
  GET_COMPANIES_SUCCESS: "GET_COMPANIES_SUCCESS",
  GET_COMPANIES_ERROR: "GET_COMPANIES_ERROR",
  LIST_COMPANIES: "LIST_COMPANIES",
  LIST_COMPANIES_SUCCESS: "LIST_COMPANIES_SUCCESS",
  LIST_COMPANIES_ERROR: "LIST_COMPANIES_ERROR",
  CLEAR_COMPANY: "CLEAR_COMPANY",
  CHECKING_COMPANY: "CHECKING_COMPANY",
  CHECK_COMPANY_EXIST: "CHECK_COMPANY_EXIST",
  COMPANY_EXIST_SUCCESS: "COMPANY_EXIST_SUCCESS",
  COMPANY_EXIST_FAILED: "COMPANY_EXIST_FAILED",
  storeCompany: (id, idToken) => ({
    type: actions.COMPANY_REQUEST,
    id, idToken
  }),
  switchCompany: (company) => ({
    type: actions.SWITCH_COMPANY,
    company
  }),
  createCompany: (name, history) => ({
    type: actions.CREATE_COMPANY,
    name, history
  }),
  getCompanies: (currentCompany) => ({
    type: actions.COMPANIES,
    currentCompany
  }),
  getListCompanies : (currentCompany) => ({
    type : actions.LIST_COMPANIES,
    currentCompany
  }),
  checkCompany: (name) => ({
    type: actions.CHECK_COMPANY_EXIST,
    name
  })
};
export default actions;

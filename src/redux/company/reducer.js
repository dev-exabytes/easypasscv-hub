import { Map } from "immutable";
import { getCurrentCompany, getCompany } from "../../helpers/utility";
import actions from "./actions";

const initState = new Map({
  current_company: null,
  companies: [],
  creating: false,
  loading: false,
  company_exist: null,
  checking: false
});

export default function reducer(state = initState.merge(getCurrentCompany(), getCompany()), action) {
  switch (action.type) {
    case actions.GETTING_COMPANY:
      return state.set("loading", true);
    case actions.COMPANY_SUCCESS:
      return state.set("current_company", action.currentCompany);
    case actions.COMPANY_ERROR:
      return state.set("current_company", null);
    case actions.SWITCH_COMPANY:
      return state.set("current_company", action.company);
    case actions.CREATING_COMPANY:
      return state.set("creating", true)
        .set("company_exist", null);
    case actions.CREATE_COMPANY_SUCCESS:
    case actions.CREATE_COMPANY_ERROR:
      return state.set("creating", false);
    case actions.GET_COMPANIES_SUCCESS:
      return state.set('companies', action.companies)
        .set('current_company', action.currentCompany)
        .set("loading", false);
    case actions.GET_COMPANIES_ERROR:
      return state.set('companies', [])
        .set('current_company', null)
        .set("loading", false);
    case actions.CHECKING_COMPANY:
      return state.set('checking', true)
        .set('company_exist', null);
    case actions.COMPANY_EXIST_SUCCESS:
      return state.set('checking', false)
        .set('company_exist', action.company_exist);
    case actions.COMPANY_EXIST_FAILED:
      return state.set('checking', false);
    case actions.CLEAR_COMPANY:
      return initState;
    case actions.LIST_COMPANIES_SUCCESS:
        return state.set('companies', action.companies)
    default:
      return state;
  }
}

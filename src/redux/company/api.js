import { baseUrl, apiHeader } from "../../config.js";

export const onCreateCompanyRequest = (data: any, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(`${baseUrl}/v1/companies`, {
      headers: {
        ...apiHeader, ...{
          "Content-Type": "application/json",
          "Authorization": `bearer ${token}`
        }
      },
      method: "POST",
      body: data
    })
      .then(response => (response.status !== 200 && response.status !== 422 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => {
        return reject(error);
      });
  });
};

export const onGetAllCompanies = (token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(
      `${baseUrl}/v1/hub/companies`,
      {
        method: "GET",
        headers: {
          ...apiHeader, ...{
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          }
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getCompanyById = (companyId: any, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(
      `${baseUrl}/v1/companies/${companyId}`,
      {
        method: "GET",
        headers: {
          ...apiHeader, ...{
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          }
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


export const onCheckCompanyExist = (name: string, token: string) => {
  return new Promise((resolve, reject) => {
    return fetch(
      `${baseUrl}/v1/companies/name/check?name=${name}`,
      {
        method: "GET",
        headers: {
          ...apiHeader, ...{
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          }
        },
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};


import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import { push } from "react-router-redux";
import actions from "./actions";
import { clearCurrentCompany } from "../../helpers/utility";
import { notification } from "antd";
import { onCreateCompanyRequest, onGetAllCompanies, onCheckCompanyExist } from "./api";
import slackActions from "../integrations/actions";
import menuActions from "../menu/actions";
import { getParseCompanies } from "../../helpers/utility";
export function* companyRequest() {
  yield takeEvery(actions.COMPANY_REQUEST, function* (request) {
    try {
      // Filter companies list and store new current company
      const companies = getParseCompanies();
      const currentCompany = companies.find((v, i) => {
        return v.id === request.id
      });
      yield put({
        type: actions.COMPANY_SUCCESS,
        currentCompany
      });
    } catch (error) {
      yield put(actions.companyError());
    }
  });
}

export function* companySuccess() {
  yield takeEvery(actions.COMPANY_SUCCESS, function* (payload) {
    yield localStorage.setItem("current_company", JSON.stringify(payload.currentCompany));
    yield put({
      type: slackActions.SLACK_TEAM_REMOVE
    });
    yield put({
      type: menuActions.MENU,
      companyId: payload.currentCompany.id
    });
  });

}

export function* switchCompany() {
  yield takeEvery(actions.SWITCH_COMPANY, function* (payload) {
    yield localStorage.setItem("current_company", JSON.stringify(payload.company));
  });

}

export function* companyError() {
  yield takeEvery(actions.COMPANY_ERROR, function* () {
    yield clearCurrentCompany();
  });
}

export function* createCompany() {
  yield takeEvery(actions.CREATE_COMPANY, function* (request) {
    try {
      yield put({
        type: actions.CREATING_COMPANY
      });
      const body = JSON.stringify({
        name: request.name
      });
      const result = yield call(onCreateCompanyRequest, body, localStorage.getItem("id_token"));
      const { data } = result;
      yield put({
        type: actions.CREATE_COMPANY_SUCCESS
      });
      // Refresh company list
      yield put({
        type: actions.COMPANIES,
        currentCompany: data,
      });
      // Refresh menu
      yield put({
        type: menuActions.MENU,
        companyId: data.id,
        history: request.history
      });
    } catch (error) {
      notification.open({
        message: "Error",
        description: "Unable create company"
      });
      yield put({
        type: actions.CREATE_COMPANY_ERROR
      });
    }
  });
}

export function* getCompanies() {
  yield takeEvery(actions.COMPANIES, function* (request) {
    try {
      yield put({
        type: actions.GETTING_COMPANY
      });
      const result = yield call(onGetAllCompanies, localStorage.getItem("id_token"));
      const { data } = result;
      yield put({
        type: actions.GET_COMPANIES_SUCCESS,
        companies: data,
        currentCompany: request.currentCompany ? data.find(x => x.id === request.currentCompany.id) : (data.length > 0 ? data[0] : null)
      });
       } catch (error) {
      yield put({
        type: actions.GET_COMPANIES_ERROR
      });
    }
  });
}

export function* getCompaniesSuccess() {
  yield takeEvery(actions.GET_COMPANIES_SUCCESS, function* (payload) {
    try {
      yield localStorage.setItem("current_company", JSON.stringify(payload.currentCompany));
      yield localStorage.setItem("companies", JSON.stringify(payload.companies));
    } catch (error) {
      //
    }
  })
}

export function* checkCompanyExist() {
  yield takeEvery(actions.CHECK_COMPANY_EXIST, function* (request) {
    try {
      yield put({
        type: actions.CHECKING_COMPANY
      });
      const result = yield call(onCheckCompanyExist, request.name, localStorage.getItem("id_token"));
      const { data } = result;
      yield put({
        type: actions.COMPANY_EXIST_SUCCESS,
        company_exist: data.is_exist
      });
    } catch (error) {
      notification.open({
        message: "Oops",
        description: "Something went wrong"
      });
      yield put({
        type: actions.COMPANY_EXIST_FAILED
      });
    }
  });
}

export function* getListCompanies() {
  yield takeEvery(actions.LIST_COMPANIES, function* (request) {
    try {
      const result = yield call(onGetAllCompanies, localStorage.getItem("id_token"));
      const { data } = result;
      yield put({
        type: actions.LIST_COMPANIES_SUCCESS,
        companies: data,
      });
       } catch (error) {
      yield put({
        type: actions.LIST_COMPANIES_ERROR
      });
    }
  });
}

export function* getListCompaniesSuccess() {
  yield takeEvery(actions.LIST_COMPANIES_SUCCESS, function* (payload) {
  });
}

export function* getListCompaniesError() {
  yield takeEvery(actions.LIST_COMPANIES_ERROR, function* (payload) {
  });
}

export default function* rootSaga() {
  yield all([
    fork(companyRequest),
    fork(companySuccess),
    fork(companyError),
    fork(switchCompany),
    fork(createCompany),
    fork(getCompanies),
    fork(getCompaniesSuccess),
    fork(checkCompanyExist),
    fork(getListCompanies),
    fork(getListCompaniesSuccess),
    fork(getListCompaniesError),
  ]);
}

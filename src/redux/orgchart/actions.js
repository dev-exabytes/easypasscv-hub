const orgchartActions = {

  GET_CHART: 'GET_CHART',
  
  getChart: () => {
    return (dispatch, getState) => {
      const chartData = getState().OrgChart.get('chartData')
      const chartConfig = getState().OrgChat.get('chartConfig')
      dispatch({
        type: orgchartActions.GET_CHART,
        chartData,
        chartConfig
      })
    }
  }

}

export default orgchartActions

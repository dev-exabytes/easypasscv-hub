import { Map } from 'immutable'
import chartData from '../../containers/OrgChart/fakeData'
import actions from './actions'

const initialChartData = {
  "level": 1,
  "name": "Company",
  "avatar": null,
  "job_title": "",
  "removed": false,
  "first_level_sub_count": 0,
  "expand": false,
  "id": 1,
  "hasChild": true,
  "children": [...chartData['data']]
}

const chartConfig = {
  _margin: {
    top: 20,
    right: 20,
    bottom: 20,
    left: 20
  },
  _root: {},
  _nodes: [],
  _counter: 0,
  _svgroot: null,
  _svg: null,
  _tree: null,
  _diagonal: null,
  _lineFunction: null,
  _loadFunction: null,
  /* Configuration */
  _duration: 750, // Duration of animation
  _rectW: 150, // Width of rectangle
  _rectH: 50, // Height of rectangle
  _rectSpacing: 20, // Spacing between the rectangles
  _fixedDepth: 80, // Height of the line for child nodes
  _mode: 'diagonal', // Choose the values 'line' or 'diagonal'
  _callerNode: null,
  _callerMode: 0
}

const initState = new Map({
  chartData: initialChartData,
  chartConfig
})

export default function orgchartReducer(state = initState, action) {
  switch(action.type) {
    case actions.GET_CHART:
      return {
        chartData: state.chartData,
        chartConfig: state.chartConfig
      }

    default:
      return state
  }
}

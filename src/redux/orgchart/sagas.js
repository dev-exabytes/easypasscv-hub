import { all, takeEvery, fork } from 'redux-saga/effects';
import actions from './actions'

export function* getChartData() {
  yield takeEvery(actions.GET_CHART, function*() {})
}

export default function* rootSaga() {
  yield all([
    fork(getChartData)
  ])
}

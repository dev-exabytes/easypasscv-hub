import { all, takeEvery, put, call } from "redux-saga/effects";
import actions from "./actions";
import { baseUrl } from "../../config";

export const per_page = 10;

const onSearchReqeust = async token =>
  await fetch(`${baseUrl}/v1/dashboard/web?${token}`)
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);

function* searchRequest() {
  let token = localStorage.getItem("id_token");
  try {
    const searchResult = yield call(onSearchReqeust, token ? `token=${token}` : "&token=");

    if (searchResult.data) {
      yield put(actions.gitSearchSuccess(searchResult.data.total, searchResult.data.chart));
    } else {
      yield put(actions.gitSearchError());
    }
  } catch (error) {
    yield put(actions.gitSearchError());
  }
}

const onChartReqeust = async (model, start, end, token) =>
  await fetch(`${baseUrl}/v1/dashboardChart/web?${model}${start}${end}${token}`)
    .then(res => res.json())
    .then(res => res)
    .catch(error => error);

function* chartRequest(request) {
  let token = localStorage.getItem("id_token");
  try {
    const chartResult = yield call(
      onChartReqeust,
      request.model ? `model=${request.model}` : `model=User`,
      request.start ? `&start=${request.start}` : "&start=2018-04-01",
      request.end ? `&end=${request.end}` : "&end=2018-05-01",
      token ? `&token=${token}` : "&token="
    );

    if (chartResult.data) {
      let charts = JSON.parse(localStorage.getItem("charts"));
      let index = charts.findIndex(c => c.model === chartResult.data.model);

      if (index !== -1) {
        charts[index] = chartResult.data;
        yield put(actions.chartSearchSuccess(charts));
      }
    } else {
      yield put(actions.chartSearchError());
    }
  } catch (error) {
    yield put(actions.chartSearchError());
  }
}

export default function* rootSaga() {
  yield all([takeEvery(actions.DASHBOARD_SEARCH, searchRequest), takeEvery(actions.CHART_SEARCH, chartRequest)]);
}

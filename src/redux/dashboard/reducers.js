import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
	loading: false,
	error: false,
	total: [],
	chart: []
});

export default function reducer(state = initState, action) {
	switch (action.type) {
		case actions.DASHBOARD_SEARCH:
			return state.set("loading", true);
		case actions.DASHBOARD_SUCCESS_RESULT:
			localStorage.setItem("charts", JSON.stringify(action.chart));
			return state
				.set("total", action.total)
				.set("chart", action.chart)
				.set("loading", false)
				.set("error", false);
		case actions.DASHBOARD_ERROR_RESULT:
			return state
				.set("total", [])
				.set("chart", [])
				.set("loading", false)
				.set("error", false);
		case actions.CHART_SEARCH:
			return state.set("loading", true);
		case actions.CHART_SUCCESS_RESULT:
			localStorage.setItem("charts", JSON.stringify(action.chart));
			return state
				.set("chart", action.chart)
				.set("loading", false)
				.set("error", false);
		case actions.CHART_ERROR_RESULT:
			return state.set("loading", false).set("error", false);
		default:
			return state;
	}
}

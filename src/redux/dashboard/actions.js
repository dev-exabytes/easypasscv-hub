const actions = {
	DASHBOARD_SEARCH: "DASHBOARD_SEARCH",
	DASHBOARD_SUCCESS_RESULT: "DASHBOARD_SUCCESS_RESULT",
	DASHBOARD_ERROR_RESULT: "DASHBOARD_ERROR_RESULT",
	CHART_SEARCH: "CHART_SEARCH",
	CHART_SUCCESS_RESULT: "CHART_SUCCESS_RESULT",
	CHART_ERROR_RESULT: "CHART_ERROR_RESULT",
	dashboardSearch: () => ({
		type: actions.DASHBOARD_SEARCH
	}),
	gitSearchSuccess: (total, chart) => ({
		type: actions.DASHBOARD_SUCCESS_RESULT,
		total,
		chart
	}),
	gitSearchError: () => ({
		type: actions.DASHBOARD_ERROR_RESULT
	}),
	chartSearch: (model, start, end) => ({
		type: actions.CHART_SEARCH,
		model,
		start,
		end
	}),
	chartSearchSuccess: chart => ({
		type: actions.CHART_SUCCESS_RESULT,
		chart
	}),
	chartSearchError: () => ({
		type: actions.CHART_ERROR_RESULT
	})
};
export default actions;

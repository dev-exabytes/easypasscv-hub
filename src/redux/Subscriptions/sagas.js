import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import { push } from "react-router-redux";
import actions from "./actions";
import {getPlan, getPaymentType, getSubscriptionDetails, getSubscriptionEstimate, getPlanName, getPlanIdByWebsiteId, modifyPlan} from "./api";
import { notification } from "antd";

export function* subscriptionsRequest() {
  yield takeEvery(actions.SUBSCRIPTIONS_REQUEST, function* (request) {
    try {
      let token = localStorage.getItem("id_token");
      const subscription = yield call(getSubscriptionDetails,token,request.data.id,request.data.selectedPlan,request.data.selectedBilling,request.data.quantity,request.data.selectedAddons);
      yield put({
        type: actions.SUBSCRIPTIONS_SUCCESS,
        planId: subscription.data.planId,
        planName: subscription.data.planName,
        chargebeePlanName: subscription.data.chargebeePlanName,
        billingId : subscription.data.billingId,
        payment: subscription.data.payment,
        totalUser: subscription.data.totalUser,
        amountPerUser: subscription.data.amountPerUser,
        totalPayment: subscription.data.totalPayment,
        companyId: subscription.data.company,
        trialEnd : subscription.data.trialEnd,
        trialStart : subscription.data.trialStart,
        trialPeriod : subscription.data.trialPeriod,
        addonsId : request.data.selectedAddons,
        addonsPrice : subscription.data.addonsPrice,
        addonsName : subscription.data.addonsName,
        planPrice : subscription.data.planPrice,
        isPaidPlan : subscription.data.isPaidPlan
      });

    } catch (error) {
      yield put({
        type: actions.SUBSCRIPTIONS_ERROR,
        error : error
      });
    }
  });
}

export function* subscriptionsSuccess() {
  yield takeEvery(actions.SUBSCRIPTIONS_SUCCESS, function* () {
  });
}

export function* subscriptionError() {
  yield takeEvery(actions.SUBSCRIPTIONS_ERROR, function* () {
    // const error = request.error
    // error.then(function(result) {
    //   var errorMessage = result.error.message;
    //   // notification.open({
    //   //   message: "Invalid",
    //   //   description: errorMessage
    //   // });
    // });

  });
}

export function* estimateRequest() {
  yield takeEvery(actions.ESTIMATE_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.SUBS_LOADING
      });

      let token = localStorage.getItem("id_token");
      let billingAddress = request.data.billingAddress;
      const estimate = yield call(getSubscriptionEstimate,token,request.data.company_id,request.data.planId,request.data.billingId,request.data.totalUser,request.data.promoCode,billingAddress,request.data.addonsId);

      yield put({
        type: actions.ESTIMATE_SUCCESS,
        planId: estimate.data.planId,
        planName: estimate.data.planName,
        chargebeePlanName: estimate.data.chargebeePlanName,
        billingId : estimate.data.billingId,
        payment: estimate.data.payment,
        totalUser: estimate.data.totalUser,
        amountPerUser: estimate.data.amountPerUser,
        totalPayment: estimate.data.totalPayment,
        companyId: estimate.data.company,
        billingAddress: request.data.billingAddress,
        totalPaymentIncTax : estimate.data.totalPaymentIncTax,
        coupon : estimate.data.coupon,
        discount: estimate.data.discount,
        amount: estimate.data.amount,
        addonsId : request.data.addonsId,
        addonsName : estimate.data.addons,
        addonsPrice : estimate.data.addonsPrice,
        planPrice : estimate.data.planPrice,
      });
    } catch(error){
          yield put({
          type: actions.ESTIMATE_ERROR,
          error : error
        });
      }

  });
}

export function* estimateSuccess() {
  yield takeEvery(actions.ESTIMATE_SUCCESS, function* () {
  });
}

export function* estimateError() {
  yield takeEvery(actions.ESTIMATE_ERROR, function* (request) {
    const error = request.error
    error.then(function(result) {
      var errorMessage = result.error.message;
      var err = errorMessage.replace('coupon_ids[0] : ','');
      notification.open({
        message: "Invalid",
        description: err
      });
    });

  });
}

export function* tokenRequest() {
  yield takeEvery(actions.TOKEN_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.TOKEN_SUCCESS,
        token: request.token,
      });

    } catch (error) {
      yield put({ type: actions.TOKEN_ERROR });
    }
  });
}

export function* tokenSuccess() {
  yield takeEvery(actions.TOKEN_SUCCESS, function* () {
  });
}

export function* tokenError() {
  yield takeEvery(actions.TOKEN_ERROR, function* () {
    notification.open({
      message: "Error",
    });
  });
}

export function* paymentRequest() {
  yield takeEvery(actions.PAYMENT_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.PAYMENT_SUCCESS,
        subscibedPlan: request.plan,
      });

    } catch (error) {
      yield put({ type: actions.PAYMENT_ERROR });
    }
  });
}

export function* paymentSuccess() {
  yield takeEvery(actions.PAYMENT_SUCCESS, function* () {
    yield put(push("payment/transaction/success"));
  });
}

export function* paymentError() {
  yield takeEvery(actions.PAYMENT_ERROR, function* () {
    yield put(push("payment/transaction"));
  });
}

export function* planRequest() {
  yield takeEvery(actions.PLAN_REQUEST, function* (request) {
    try {
      const data = request.data;
      const allPlan = yield call(getPlan,data.idToken,data.companyId);
      const allPaymentType = yield call (getPaymentType , data.idToken);

      yield put({
        type: actions.PLAN_SUCCESS,
        allPlan: allPlan.data,
        allPaymentType: allPaymentType.data,
      });

    } catch (error) {
      yield put({ type: actions.PLAN_ERROR });
    }
  });
}

export function* planSuccess() {
  yield takeEvery(actions.PLAN_SUCCESS, function* () {
  });
}

export function* planError() {
  yield takeEvery(actions.PLAN_ERROR, function* () {
  });
}

export function* planNameRequest() {
  yield takeEvery(actions.PLAN_NAME_REQUEST, function* (request) {
    try {
      let token = localStorage.getItem("id_token");
      const plan = yield call(getPlanName,token,request.id);

      yield put({
        type: actions.PLAN_NAME_SUCCESS,
        planName: plan.data.plan_name,
      });

    } catch (error) {
      yield put({ type: actions.PLAN_NAME_ERROR });
    }
  });
}

export function* planNameSuccess() {
  yield takeEvery(actions.PLAN_NAME_SUCCESS, function* () {
  });
}

export function* planNameError() {
  yield takeEvery(actions.PLAN_NAME_ERROR, function* () {
  });
}

export function* billingDetailsRequest() {
  yield takeEvery(actions.BILLING_DETAILS_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.BILLING_DETAILS_SUCCESS,
        billingDetails: request.billing,
      });

    } catch (error) {
      yield put({ type: actions.BILLING_DETAILS_ERROR });
    }
  });
}

export function* billingDetailsSuccess() {
  yield takeEvery(actions.BILLING_DETAILS_SUCCESS, function* () {
  });
}

export function* billingDetailsError() {
  yield takeEvery(actions.BILLING_DETAILS_ERROR, function* () {
  });
}

export function* nonceRequest() {
  yield takeEvery(actions.NONCE_REQUEST, function* (request) {
    try {
      yield put({
        type: actions.NONCE_SUCCESS,
        nonce: request.nonce.nonce,
        cardType: request.nonce.details.cardType,
        lastFour: request.nonce.details.lastFour
      });

    } catch (error) {
      yield put({ type: actions.NONCE_ERROR });
    }
  });
}

export function* nonceSuccess() {
  yield takeEvery(actions.NONCE_SUCCESS, function* () {
  });
}

export function* nonceError() {
  yield takeEvery(actions.NONCE_ERROR, function* () {
  });
}

export function* paramsRequest() {
  yield takeEvery(actions.PARAMS_REQUEST, function* (request) {
    try {
      let token = localStorage.getItem("id_token");
      const planId = yield call(getPlanIdByWebsiteId, token, request.params.get('version'));

      yield put({
        type: actions.PARAMS_SUCCESS,
        requestedPlan: planId.data.id,
        requestedBilling: request.params.get('cycle'),
        requestedUser: request.params.get('user'),
      });

    } catch (error) {
      yield put({ type: actions.PARAMS_ERROR });
    }
  });
}

export function* paramsSuccess() {
  yield takeEvery(actions.PARAMS_SUCCESS, function* () {
  });
}

export function* paramsError() {
  yield takeEvery(actions.PARAMS_ERROR, function* () {
  });
}

export function* clearSubs() {
  yield takeEvery(actions.CLEAR_SUBS, function* () {
  });
}
export function* storeCurrentCompanyPlanRequest() {
  yield takeEvery(actions.STORE_CURRENT_COMPANY_PLAN_REQUEST, function* (request) {
    try{
      yield put({
        type: actions.STORE_CURRENT_COMPANY_PLAN_SUCCESS,
        currentCompanyPlan : request.data
      });
    }
    catch (error) {
     yield put({ type: actions.STORE_CURRENT_COMPANY_PLAN_ERROR });
    }
  });
}

export function* storeCurrentCompanyPlanSuccess() {
  yield takeEvery(actions.STORE_CURRENT_COMPANY_PLAN_SUCCESS, function* () {
  });
}

export function* storeCurrentCompanyPlanError() {
  yield takeEvery(actions.STORE_CURRENT_COMPANY_PLAN_ERROR, function* () {
  });
}

export function* modifyPlanRequest() {
  yield takeEvery(actions.MODIFY_PLAN_REQUEST, function* (request) {
    try {
      let token = localStorage.getItem("id_token");
      const modify  = yield call(modifyPlan,token,request.data.company_id,request.data.param);
      console.log(modify.data.billing);
      yield put({
        type: actions.MODIFY_PLAN_SUCCESS,
        billing : modify.data.billing,
        addOns : modify.data.add_ons
      });

    } catch (error) {
      yield put({ type: actions.MODIFY_PLAN_ERROR });
    }
  });
}

export function* modifyPlanSuccess() {
  yield takeEvery(actions.MODIFY_PLAN_SUCCESS, function* () {
  });
}

export function* modifyPlanError() {
  yield takeEvery(actions.MODIFY_PLAN_ERROR, function* () {
  });
}

export function* modifyBillingRequest() {
  yield takeEvery(actions.MODIFY_BILLING_REQUEST, function* (request) {
    try {
      let token = localStorage.getItem("id_token");
      const modify  = yield call(modifyPlan,token,request.data.company_id,request.data.param);

      console.log(modify);
      yield put({
        type: actions.MODIFY_BILLING_SUCCESS,
        plan: modify.data.plans,
        addOns : modify.data.add_ons
      });

    } catch (error) {
      yield put({ type: actions.MODIFY_BILLING_ERROR });
    }
  });
}

export function* modifyBillingSuccess() {
  yield takeEvery(actions.MODIFY_BILLING_SUCCESS, function* () {
  });
}

export function* modifyBillingError() {
  yield takeEvery(actions.MODIFY_BILLING_ERROR, function* () {
  });
}

export function* modifyAddonsRequest() {
  yield takeEvery(actions.MODIFY_ADDONS_REQUEST, function* (request) {
    try {
      let token = localStorage.getItem("id_token");
      const modify  = yield call(modifyPlan,token,request.data.company_id,request.data.param);

      console.log(modify);
      yield put({
        type: actions.MODIFY_ADDONS_SUCCESS,
        plan: modify.data.plans,
        billing : modify.data.billing,
      });

    } catch (error) {
      yield put({ type: actions.MODIFY_ADDONS_ERROR });
    }
  });
}

export function* modifyAddonsSuccess() {
  yield takeEvery(actions.MODIFY_ADDONS_SUCCESS, function* () {
  });
}

export function* modifyAddonsError() {
  yield takeEvery(actions.MODIFY_ADDONS_ERROR, function* () {
  });
}

export default function* rootSaga() {
  yield all([
    fork(subscriptionsRequest),
    fork(subscriptionsSuccess),
    fork(subscriptionError),
    fork(estimateRequest),
    fork(estimateSuccess),
    fork(estimateError),
    fork(tokenRequest),
    fork(tokenSuccess),
    fork(tokenError),
    fork(paymentRequest),
    fork(paymentSuccess),
    fork(paymentError),
    fork(planRequest),
    fork(planSuccess),
    fork(planError),
    fork(planNameRequest),
    fork(planNameSuccess),
    fork(planNameError),
    fork(billingDetailsRequest),
    fork(billingDetailsSuccess),
    fork(billingDetailsError),
    fork(nonceRequest),
    fork(nonceSuccess),
    fork(nonceError),
    fork(paramsRequest),
    fork(paramsSuccess),
    fork(paramsError),
    fork(clearSubs),
    fork(modifyPlanRequest),
    fork(modifyPlanSuccess),
    fork(modifyPlanError),
    fork(modifyBillingRequest),
    fork(modifyBillingSuccess),
    fork(modifyBillingError),
    fork(modifyAddonsRequest),
    fork(modifyAddonsSuccess),
    fork(modifyAddonsError),
    fork(storeCurrentCompanyPlanRequest),
    fork(storeCurrentCompanyPlanSuccess),
    fork(storeCurrentCompanyPlanError),
  ]);
}



import { baseUrl } from "../../config";

export const getCompany = (token) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/users/companies`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`, Accept: "application/x.app.v2+json",
          "Access-Control-Allow-Origin": "http://localhost:3005", mode: 'no-cors'
        }
      }
    )
      // .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => { response.json() })
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getPlan = (token,companyId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/plans/companies/${companyId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getPaymentType = (token) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/paymentTypes`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getSubscriptionDetails = (token,companyId,plan,payment,totalUser,addOns) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/${plan}/billings/${payment}/quantity/${totalUser}/addOns/${addOns}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`, Accept: "application/x.app.v2+json"
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getSubscriptionEstimate = (token,companyId,plan,payment,totalUser,promoCode,billingAddress,addonsId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/${plan}/billings/${payment}/quantity/${totalUser}/estimate`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`, Accept: "application/x.app.v2+json"
        },
        body:JSON.stringify({
          billingAddress,
          coupon : promoCode,
          addonsId : addonsId,
        })
      }
    )
      .then(response => (response.status !== 200 ? reject(response.json()) : response))
      .then(response => response.json())
      .then(response => resolve( response))
      .catch( error => reject(error));
  });
};

export const getPlanName = (token, planId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }

    return fetch(
      `${baseUrl}/v1/plans/${planId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const getPlanIdByWebsiteId = (token, websiteId) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/plans/website/${websiteId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const modifyPlan= (token, companyId, param) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject("Invalid Token.");
      return;
    }
    return fetch(
      `${baseUrl}/v1/companies/${companyId}/plans/selection?${param}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json", Authorization: `Bearer ${token}`,
        }
      }
    )
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

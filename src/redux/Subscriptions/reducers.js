import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
	clientToken: " ",
	planName: " ",
	amountPerUser: " ",
	totalPayment: " ",
	allPlans:[],
	allPayments:[],
	allAddOns: [],
	loading : true,
	chargebeePlanName: " ",
	payment: " ",
	planId: " ",
	companyId: " ",
	totalUser: " ",
	selectedPlanName: " ",
	billingAddress: null,
	nonce: " ",
	braintreeLoading: false,
	requestedPlan: " ",
	requestedBilling: " ",
	requestedUser: " ",
	screenLoading : true,
	cardType: null,
	lastFour: null,
	discount : 0,
	amount: 0,
	coupon : '',
	trialEnd : 0,
	trialStart : 0,
	trialPeriod : 0,
	subsError: '',
	haveError : 0,
	addonsId: 0,
	addonsName : "",
	addonsPrice : 0,
	planPrice : 0,
	currentCompanyPlan : [],
	isPaidPlan: 1,
});

export default function reducer(state = initState, action) {
	switch (action.type) {
		case actions.UPDATE_CLIENT_TOKEN:
			return state
				.set("clientToken", action.clientToken)
		case actions.GET_SUBSCRIPTION_DETAILS:
			return state
				.set("planName", action.planName)
				.set("amountPerUser", action.amountPerUser)
				.set("totalPayment", action.totalPayment)
		case actions.SUBSCRIPTIONS_SUCCESS:
			return state
				.set("planName", action.planName)
				.set("amountPerUser", action.amountPerUser)
				.set("totalPayment", action.totalPayment)
				.set("amount", action.totalPayment)
				.set("planId", action.planId)
				.set("companyId", action.companyId)
				.set("totalUser", action.totalUser)
				.set("chargebeePlanName", action.chargebeePlanName)
				.set("billingId", action.billingId)
				.set("addonsId", action.addonsId)
				.set("addonsPrice",action.addonsPrice)
				.set("addonsName",action.addonsName)
				.set("planPrice", action.planPrice)
				.set("payment",action.payment)
				.set("loading",false)
				.set("coupon",'')
				.set("discount", 0)
				.set("trialEnd", action.trialEnd)
				.set("trialStart", action.trialStart)
				.set("trialPeriod", action.trialPeriod)
				.set("isPaidPlan", action.isPaidPlan)
				.set("haveError",0)
		case actions.ESTIMATE_SUCCESS:
			return state
				.set("planName", action.planName)
				.set("totalPayment", action.totalPayment)
				.set("totalPaymentIncTax", action.totalPaymentIncTax)
				.set("planId", action.planId)
				.set("companyId", action.companyId)
				.set("totalUser", action.totalUser)
				.set("chargebeePlanName", action.chargebeePlanName)
				.set("billingId", action.billingId)
				.set("addonsId", action.addonsId)
				.set("payment",action.payment)
				.set("billingAddress", action.billingAddress)
				.set("coupon", action.coupon)
				.set("amount",action.amount)
				.set("discount", action.discount)
				.set("addonsName", action.addonsName)
				.set("addonsPrice",action.addonsPrice)
				.set("planPrice", action.planPrice)
				.set("subsLoading",false)
				.set("haveError",0)
		case actions.TOKEN_SUCCESS:
			return state
				.set("clientToken", action.token)
		case actions.PAYMENT_SUCCESS:
			return state
				.set("subcribedPlan", action.subcribedPlan)
				.set("coupon", '')
		case actions.PLAN_SUCCESS:
			return state
				.set("allPlans", action.allPlan)
				.set("allPayments", action.allPaymentType)
				.set("screenLoading",false)
		case actions.PLAN_NAME_SUCCESS:
			return state
				.set("selectedPlanName", action.planName)
		case actions.BILLING_DETAILS_SUCCESS:
			return state
				.set("billingAddress", action.billingDetails)
		case actions.NONCE_SUCCESS:
			return state
				.set("nonce", action.nonce)
				.set("cardType", action.cardType)
				.set("lastFour", action.lastFour)
				.set("braintreeLoading", true)
		case actions.PARAMS_SUCCESS:
			return state
				.set("requestedPlan", action.requestedPlan)
				.set("requestedBilling", action.requestedBilling)
				.set("requestedUser", action.requestedUser)
		case actions.SUBSCRIPTIONS_ERROR:
			return state
				.set("subsError",action.error)
				.set("haveError",1)
		case actions.ESTIMATE_ERROR:
			return state
				.set("subsLoading",false)
				.set("coupon",'')
				.set("discount", 0)
		case actions.SUBS_LOADING:
			return state
				.set("subsLoading",true)
		case actions.CLEAR_SUBS:
			return state
				.set("planName", " ")
				.set("amountPerUser", " ")
				.set("totalPayment", " ")
				.set("amount", " ")
				.set("planId", " ")
				.set("companyId"," ")
				.set("totalUser", " ")
				.set("chargebeePlanName", " ")
				.set("billingId", " ")
				.set("addonsId", 0)
				.set("payment"," ")
				.set("loading",false)
				.set("coupon",'')
				.set("discount", 0)
				.set("trialEnd", 0)
				.set("trialStart", 0)
				.set("trialPeriod", 0)
				.set("addonsName", "")
				.set("addonsPrice",0)
				.set("planPrice", 0)
				.set("isPaidPlan",1)
				.set("screenLoading",true)
		case actions.STORE_CURRENT_COMPANY_PLAN_SUCCESS:
			return state
				.set("currentCompanyPlan", action.currentCompanyPlan)
		case actions.MODIFY_PLAN_SUCCESS:
			return state
				.set("allPayments", action.billing)
				.set("allAddOns", action.addOns)
		case actions.MODIFY_BILLING_SUCCESS:
			return state
				.set("allPlans", action.plan)
				.set("allAddOns", action.addOns)
		case actions.MODIFY_ADDONS_SUCCESS:
			return state
				.set("allPlans", action.plan)
				.set("allPayments", action.billing)
		default:
			return state;
	}
}

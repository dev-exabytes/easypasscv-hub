import React from "react";
import { Link } from "react-router-dom";
import { siteConfig } from "../../config.js";
import { Avatar } from "antd";

// export default function({ collapsed, styling }) {
export default function ({ collapsed, user }) {
  return (
    <div className="isoLogoWrapper" style={{ height: collapsed ? "60px" : "120px" }}>
      {collapsed ? (
        <Link to="/dashboard">
          <img src={siteConfig.siteIcon} width="60%" alt="EasyWork Logo" />
        </Link>
      ) : (
          <Link to="/dashboard">
            {user && user.avatar ? (
              <Avatar className="avatar" size={100} src={user.avatar} />
            ) : (
                <Avatar className="avatar" icon={"user"} style={{ color: "#fff" }} size={100} />
              )}
          </Link>
        )}
    </div>
  );
}

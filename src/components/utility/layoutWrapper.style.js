import styled from 'styled-components';

const LayoutContentWrapper = styled.div`
  padding:50px;
  display: flex;
  flex-flow: column wrap;
  overflow: hidden;
  background-color: #f1f3f6;
  flex-direction:column;

  @media only screen and (max-width: 767px) {
    padding: 50px 20px;
  }

  @media (max-width: 580px) {
    padding: 15px;
  }
`;

export { LayoutContentWrapper };

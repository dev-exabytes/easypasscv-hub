import React, { Component } from "react";
import { connect } from "react-redux";
import Popover from "../uielements/popover";
import IntlMessages from "../utility/intlMessages";
import authAction from "../../redux/auth/actions";
import TopbarDropdownWrapper from "./topbarDropdown.style";
import { Avatar } from "antd";

const { logout } = authAction;

class TopbarUser extends Component {
  constructor(props) {
    super(props);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
    this.hide = this.hide.bind(this);
    this.state = {
      visible: false
    };
  }

  hide() {
    this.setState({ visible: false });
  }

  handleVisibleChange() {
    this.setState({ visible: !this.state.visible });
  }

  render() {
    const { user } = this.props;
    const content = (
      <TopbarDropdownWrapper className="isoUserDropdown">
        <p style={{ paddingLeft: "15px", fontWeight: "700" }}>Hi, {(user && user.name) || "Guest"}</p>
        <a className="isoDropdownLink" onClick={this.props.logout}>
          <IntlMessages id="topbar.logout" />
        </a>
      </TopbarDropdownWrapper>
    );

    return (
      <Popover
        content={content}
        trigger="click"
        visible={this.state.visible}
        onVisibleChange={this.handleVisibleChange}
        arrowPointAtCenter={true}
        placement="bottomLeft"
      >
        <div className="isoImgWrapper">
          {user && user.avatar ? (
            <Avatar size={35} src={user.avatar} />
          ) : (
              <Avatar icon={"user"} size={35} style={{ color: "#fff" }} />
            )}
          <span className="userActivity online" />
        </div>
      </Popover>
    );
  }
}
export default connect(
  state => ({
    user: state.Auth.get("user")
  }),
  { logout }
)(TopbarUser);

import styled from 'styled-components';
import { palette } from 'styled-theme';

const Style = styled.div`
  .introTextContainer{
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
  }
  .title{
    text-align: left;
    font-size: 18px;
    font-weight: 700;
    @media only screen and (max-width: 767px) {
      font-size: 16px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "18px"};
    }
  }
  .warningText{
    color:${palette('ew', 4)};
    @media only screen and (max-width: 767px) {
      font-size: 14px;
    }
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "24px" : "14px"};
    }
  }
  .body{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    @media only screen and (max-width: 480px) {
      flex-direction: column;
    }
  }
  .progressContainer{
    padding: 10px;
    height:100%;
    flex:1;
    @media only screen and (max-width: 767px) {
      padding: 5px;
    }
  }
  .progressText{
    font-size: 14px;
    color:#000;
    @media only screen and (max-width: 767px) {
      font-size: 12px;
    }
  }
  .progressTotal{
    font-size: 24px;
    color:#000;
    @media only screen and (min-width: 1480px) {
      font-size: ${props => props.isEmail ? "36px" : "24px"};
    }
  }
  .footer{
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content:space-between;
    align-items:center;
    flex: 1 0 auto;
  }
  .legend{
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content:center;
    align-items:center;
  }
  .legends{
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    align-items:flex-start;
    flex:1;
  }
  .greenSquare{
    width:15px;
    height:15px;
    background-color: #ff6600;
    margin-right:10px;
    border-radius:2px;
  }
  .orangeSquare{
    width:15px;
    height:15px;
    background-color: orange;
    margin-right:10px;
    border-radius:2px;
  }
  .redSquare{
    width:15px;
    height:15px;
    background-color: #ff0000;
    margin-right:10px;
    border-radius:2px;
  }
`;

export default Style;

import React, { Component } from 'react';
import { Divider } from 'antd';
import Style from "./progressCircle.style";
import { palette } from 'styled-theme';
import Progress from "../../components/progress";
export default class extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, lockText, enlarge, successPercent, percent, progressText, total, legendLeft, legendRight, isEmail, legendGrey, trailColor } = this.props;

    return (
      <Style className="container" isEmail={isEmail}>
        <div className="introTextContainer">
          <span className="title">{title}</span>
          {lockText ? (
            <span className="warningText">{lockText}</span>
          ) : null}
        </div>
        <div className="body">
          <Progress
            className="progressContainer"
            width={enlarge ? 280 : 120}
            strokeWidth={enlarge ? 12 : 8}
            successPercent={successPercent}
            percent={percent}
            type="circle"
            strokeColor="#ff6600"
            format={(percent) => (<span className="progressText">{progressText}<br /><span className="progressTotal"><b>{total}</b></span></span>)}
            strokeLinecap="square"
            trailColor={trailColor}
          />
          <div className="legends">
            <div className="legend">
              <div className="greenSquare"></div>
              <span className="progressText">{legendLeft}</span>
            </div>
            <div className="legend">
              <div className="redSquare"></div>
              <span className="progressText">{legendRight}</span>
            </div>
            <div className="legend">
              <div className="orangeSquare"></div>
              <span className="progressText">{legendGrey}</span>
            </div>
          </div>
        </div>
      </Style >
    );
  }
}

import styled from 'styled-components';

const Style = styled.div`
.isoLayoutContent{
  padding: 10px;
}
.filterContainer{
  display:flex;
  flex-direction: row;
  justify-content: flex-start;

  @media only screen and (max-width: 767px) {
    flex-direction: column;
  }

  div{
    padding-right: 5px;
    min-width: 50px;

    @media only screen and (max-width: 767px) {
      flex-direction: column;
      min-width: 0px;
      min-height: 30px;
      padding-right: 0px;
    }
  }
}
`;
export default Style;

import React, { Component } from 'react';
import { Radio, DatePicker, Form } from 'antd';
import moment from 'moment';
import Style from "./index.style";

const { RangePicker } = DatePicker;

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timezone: null,
      dates: null,
      days: null
    };
  }

  componentDidMount() {
    const { form, dates, days } = this.props;

    form.setFieldsValue({
      dates,
      days
    });
  }

  onChange(dates, dateStrings) {
    const { form } = this.props;
    form.setFieldsValue({
      dates,
      days: null
    });
  }

  onRadioChange(e) {
    const { form, dates, timezone } = this.props;
    const dayChange = e.target.value;
    let dateRange = dates;

    switch (dayChange) {
      case '1':
        dateRange = [moment().tz(timezone), moment().tz(timezone)];
        break;
      case '7':
        dateRange = [moment().tz(timezone).subtract(7, 'days'), moment().tz(timezone)];
        break;
      case '30':
        dateRange = [moment().tz(timezone).subtract(30, 'days'), moment().tz(timezone)];
        break;
      default:
        break;
    }

    form.setFieldsValue({
      dates: dateRange,
      days: dayChange
    });
  }

  render() {
    const { timezone, form, orFlag } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Style >
        <Form>
          <div className="filterContainer">
            <div>
              <Form.Item >
                {getFieldDecorator('days', {})(
                  <Radio.Group
                    buttonStyle="solid"
                    onChange={this.onRadioChange.bind(this)}
                  >
                    <Radio.Button value="1">Today</Radio.Button>
                    <Radio.Button value="7">Last 7 Days</Radio.Button>
                    <Radio.Button value="30">Last 30 Days</Radio.Button>
                  </Radio.Group>,
                )}
              </Form.Item>

            </div>
            {orFlag ? (
              <div>
                <h3>or</h3>
              </div>
            ) : null}
            <div>
              <Form.Item >
                {getFieldDecorator('dates', {
                  rules: [
                    {
                      type: "array",
                      required: true,
                      message: 'Please select a date range.',
                    }
                  ],
                })(
                  <RangePicker
                    ranges={{
                      Today: [moment().tz(timezone), moment().tz(timezone)],
                      'This Week': [moment().tz(timezone).startOf('week'), moment().tz(timezone).endOf('week')],
                      'This Month': [moment().tz(timezone).startOf('month'), moment().tz(timezone).endOf('month')],
                    }}
                    onChange={this.onChange.bind(this)}
                  />
                )}
              </Form.Item>
            </div>
          </div>
        </Form>
      </Style>
    );
  }
}

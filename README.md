# Isomorphic - React Redux Admin Dashboard `Version 2.4.0`

## EasyWork Webportal

Repository for hub.easywork.asia

### Please check `src/config.js` & edit as your app.

Clone the project

`yarn install` (Recommended)

setup `.env`, `.env.production`, `.env.staging` (**Never commit these files**)

## Main files to update/create when add new dashboard page

1. Define route for YOUR_COMPONENT `containers > App > AppRouter.js`
2. Create new YOUR_COMPONENT class in `containers`
3. Define side menu text `languageProviders > locales > en_US.json`
4. Add API integration and redux for YOUR_COMPONENT in `redux`
5. Register YOUR_COMPONENT reducer in `redux > reducers.js`
6. Register YOUR_COMPONENT sagas in `redux > sagas.js`
